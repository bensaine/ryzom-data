scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1555]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 119,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1557]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    TextManager = 0,  
    Position = 0,  
    TextManagerEntry = 0,  
    ChatSequence = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1558]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1560]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1559]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1561]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_1562]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1564]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1563]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1568]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1569]],  
                x = 31305.34375,  
                y = -11095.67188,  
                z = -7.046875
              },  
              Angle = 0.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1566]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1570]],  
                    Name = [[test]],  
                    Components = {
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 13,  
              HairType = 5622062,  
              HairColor = 5,  
              Tattoo = 29,  
              EyesColor = 1,  
              MorphTarget1 = 5,  
              MorphTarget2 = 3,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              MorphTarget7 = 7,  
              MorphTarget8 = 5,  
              Sex = 0,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 5,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[fyros-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_1565]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1573]],  
        Count = 2,  
        Text = [[blablabla]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1574]],  
        Count = 1,  
        Text = [[blablabla]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1577]],  
        Count = 2,  
        Text = [[bliblibli]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1578]],  
        Count = 1,  
        Text = [[bliblibli]]
      }
    },  
    InstanceId = [[Client1_1556]]
  }
}