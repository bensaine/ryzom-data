scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    ConditionStep = 0,  
    Scenario = 1,  
    Act = 3,  
    ChatSequence = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManager = 0,  
    TextManagerEntry = 0,  
    ChatAction = 0,  
    ActivityStep = 1,  
    DefaultFeature = 0,  
    ActivitySequence = 0,  
    LogicEntityReaction = 0,  
    ConditionType = 0,  
    Region = 0,  
    Road = 0,  
    Npc = 0,  
    ActionStep = 0,  
    EventType = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ChatStep = 0,  
    Position = 0,  
    Behavior = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    WayPoint = 0
  },  
  Acts = {
    {
      Cost = 7,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_14]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -1347.984375,  
                x = 21585.89063,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = 112.015625
              },  
              Angle = 1.765625,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_16]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[alezao 1]],  
              Position = {
                y = -1344.34375,  
                x = 21592.26563,  
                InstanceId = [[Client1_19]],  
                Class = [[Position]],  
                z = 111.125
              },  
              Angle = 0.25,  
              Base = [[palette.entities.botobjects.ju_s3_banana_tree]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_22]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_20]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[arino 1]],  
              Position = {
                y = -1344.4375,  
                x = 21579.1875,  
                InstanceId = [[Client1_23]],  
                Class = [[Position]],  
                z = 111.078125
              },  
              Angle = 1.59375,  
              Base = [[palette.entities.botobjects.ju_s3_fougere]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_30]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_28]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[faneng 1]],  
              Position = {
                y = -1355.59375,  
                x = 21582.9375,  
                InstanceId = [[Client1_31]],  
                Class = [[Position]],  
                z = 112.671875
              },  
              Angle = 1.59375,  
              Base = [[palette.entities.botobjects.ju_s3_fantree]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_34]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_32]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[salvo 1]],  
              Position = {
                y = -1363.109375,  
                x = 21576.625,  
                InstanceId = [[Client1_35]],  
                Class = [[Position]],  
                z = 111.390625
              },  
              Angle = 1.59375,  
              Base = [[palette.entities.botobjects.ju_s3_tree]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_67]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_65]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 5 1]],  
              Position = {
                y = -1340.4375,  
                x = 21581.9375,  
                InstanceId = [[Client1_68]],  
                Class = [[Position]],  
                z = 110.53125
              },  
              Angle = 2.078125,  
              Base = [[palette.entities.botobjects.pack_5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_71]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_69]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 3 1]],  
              Position = {
                y = -1344.765625,  
                x = 21579.03125,  
                InstanceId = [[Client1_72]],  
                Class = [[Position]],  
                z = 111.125
              },  
              Angle = 2.078125,  
              Base = [[palette.entities.botobjects.pack_3]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_196]],  
              Class = [[Road]],  
              Position = {
                y = -4.671875,  
                x = 3.1875,  
                InstanceId = [[Client1_195]],  
                Class = [[Position]],  
                z = 3.640625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_198]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1316.765625,  
                    x = 21529.1875,  
                    InstanceId = [[Client1_199]],  
                    Class = [[Position]],  
                    z = 85.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_204]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1309.65625,  
                    x = 21573.51563,  
                    InstanceId = [[Client1_205]],  
                    Class = [[Position]],  
                    z = 91.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_207]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1292.296875,  
                    x = 21542.28125,  
                    InstanceId = [[Client1_208]],  
                    Class = [[Position]],  
                    z = 72.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_213]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1272.203125,  
                    x = 21513.84375,  
                    InstanceId = [[Client1_214]],  
                    Class = [[Position]],  
                    z = 71.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_216]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1271.765625,  
                    x = 21503.89063,  
                    InstanceId = [[Client1_217]],  
                    Class = [[Position]],  
                    z = 73.484375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_219]],  
              Class = [[Region]],  
              Position = {
                y = -7.25,  
                x = -9.6875,  
                InstanceId = [[Client1_218]],  
                Class = [[Position]],  
                z = 0.078125
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_221]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1335.734375,  
                    x = 21555.5,  
                    InstanceId = [[Client1_222]],  
                    Class = [[Position]],  
                    z = 98.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_224]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1330.78125,  
                    x = 21551.09375,  
                    InstanceId = [[Client1_225]],  
                    Class = [[Position]],  
                    z = 96.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_227]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1324.5625,  
                    x = 21551.875,  
                    InstanceId = [[Client1_228]],  
                    Class = [[Position]],  
                    z = 95.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_230]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1319.09375,  
                    x = 21557.73438,  
                    InstanceId = [[Client1_231]],  
                    Class = [[Position]],  
                    z = 94.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_233]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1319.984375,  
                    x = 21570.15625,  
                    InstanceId = [[Client1_234]],  
                    Class = [[Position]],  
                    z = 99.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_236]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1327.890625,  
                    x = 21569.07813,  
                    InstanceId = [[Client1_237]],  
                    Class = [[Position]],  
                    z = 101.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_239]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1335.25,  
                    x = 21563.45313,  
                    InstanceId = [[Client1_240]],  
                    Class = [[Position]],  
                    z = 100.9375
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Name = [[Permanent]],  
      Title = [[Permanent]],  
      Events = {
      }
    },  
    {
      Cost = 12,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_10]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_79]],  
              ActivitiesId = {
              },  
              HairType = 5622830,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 7,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 8,  
              Notes = [[Fyros itin�rant.
]],  
              GabaritHeight = 8,  
              HairColor = 2,  
              Aggro = 120,  
              EyesColor = 7,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 7,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_77]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_86]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_87]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_88]],  
                            Who = [[Client1_79]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_89]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_91]],  
                    Event = {
                      Type = [[]],  
                      InstanceId = [[Client1_90]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_83]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_82]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_84]],  
                          Value = r2.RefId([[Client1_86]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_85]],  
                        Entity = r2.RefId([[Client1_79]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                      {
                        InstanceId = [[Client1_93]],  
                        Entity = r2.RefId([[Client1_79]]),  
                        Class = [[ConditionStep]],  
                        Condition = {
                          Type = [[is in chat step]],  
                          InstanceId = [[Client1_92]],  
                          Value = r2.RefId([[Client1_87]]),  
                          Class = [[ConditionType]]
                        }
                      }
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_83]],  
                    Name = [[]],  
                    InstanceId = [[Client1_81]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_85]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              InheritPos = 1,  
              FeetModel = 5605422,  
              Speed = [[run]],  
              Angle = 1.875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b4.creature]],  
              SheetClient = [[basic_matis_male.creature]],  
              PlayerAttackable = 1,  
              BotAttackable = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Ren�]],  
              Position = {
                y = -1338.140625,  
                x = 21575.10938,  
                InstanceId = [[Client1_80]],  
                Class = [[Position]],  
                z = 108.890625
              },  
              Sex = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 3,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_96]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 4,  
              HandsModel = 5613870,  
              FeetColor = 5,  
              GabaritBreastSize = 1,  
              GabaritHeight = 2,  
              HairColor = 1,  
              EyesColor = 4,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 2,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_94]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 1,  
              FeetModel = 5653550,  
              Angle = 2.40625,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              JacketModel = 5614638,  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[tryker-dressed civilian 1]],  
              Position = {
                y = -1338.15625,  
                x = 21573.375,  
                InstanceId = [[Client1_97]],  
                Class = [[Position]],  
                z = 108.40625
              },  
              Sex = 1,  
              MorphTarget7 = 1,  
              MorphTarget3 = 3,  
              Tattoo = 26
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_104]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 5605678,  
              FeetColor = 3,  
              GabaritBreastSize = 8,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 0,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 7,  
              HandsColor = 2,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_102]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 5605422,  
              Angle = 2.421875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[fyros-dressed civilian 1]],  
              Position = {
                y = -1335.59375,  
                x = 21578.65625,  
                InstanceId = [[Client1_105]],  
                Class = [[Position]],  
                z = 108.640625
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_164]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 5606958,  
              FeetColor = 5,  
              GabaritBreastSize = 7,  
              GabaritHeight = 10,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 0,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_162]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 5606702,  
              Angle = 1.375,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              Sheet = [[ring_melee_damage_dealer_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 5607470,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595182,  
              ArmColor = 4,  
              Name = [[desert armsman 1]],  
              Position = {
                y = -1334.84375,  
                x = 21574.48438,  
                InstanceId = [[Client1_165]],  
                Class = [[Position]],  
                z = 107.515625
              },  
              Sex = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 1,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_168]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 8,  
              HandsModel = 5604142,  
              FeetColor = 2,  
              GabaritBreastSize = 14,  
              GabaritHeight = 13,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 3,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_166]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_241]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_242]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_219]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_243]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_196]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_244]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_196]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5606702,  
              Speed = [[run]],  
              Angle = 2.6875,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_20]],  
              Sheet = [[ring_melee_tank_slash_b2.creature]],  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              ArmModel = 5607470,  
              WeaponLeftHand = 5637166,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 2,  
              JacketModel = 5607726,  
              WeaponRightHand = 5594670,  
              ArmColor = 0,  
              Name = [[desert warrior 1]],  
              Position = {
                y = -1330.34375,  
                x = 21578.48438,  
                InstanceId = [[Client1_169]],  
                Class = [[Position]],  
                z = 106.234375
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 2,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_172]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 5,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 9,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_170]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 5603886,  
              Angle = 1.546875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 5604910,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595950,  
              ArmColor = 0,  
              Name = [[desert guard 1]],  
              Position = {
                y = -1337.078125,  
                x = 21576.8125,  
                InstanceId = [[Client1_173]],  
                Class = [[Position]],  
                z = 108.90625
              },  
              Sex = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 1,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_193]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 11,  
              HandsModel = 5605678,  
              FeetColor = 4,  
              GabaritBreastSize = 2,  
              GabaritHeight = 8,  
              HairColor = 5,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 9,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_191]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 5605422,  
              Angle = 2.28125,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 1,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[fyros-dressed civilian 2]],  
              Position = {
                y = -1336.3125,  
                x = 21573.09375,  
                InstanceId = [[Client1_194]],  
                Class = [[Position]],  
                z = 107.6875
              },  
              Sex = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 1,  
              Tattoo = 10
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_247]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_248]],  
                x = 21580.53125,  
                y = -1332.703125,  
                z = 107.640625
              },  
              Angle = 2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_245]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 14,  
              HairType = 2862,  
              HairColor = 3,  
              Tattoo = 21,  
              EyesColor = 6,  
              MorphTarget1 = 7,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 7,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              MorphTarget7 = 7,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5618222,  
              FeetModel = 0,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 5,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[zorai-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          InstanceId = [[Client1_56]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0.109375,  
            x = -0.296875,  
            InstanceId = [[Client1_55]],  
            Class = [[Position]],  
            z = -0.015625
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_54]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_50]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Roaming Capryni]],  
              Position = {
                y = -1335.640625,  
                x = 21587.90625,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = 109.0625
              },  
              Angle = 1.921875,  
              Base = [[palette.entities.creatures.chcdb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_38]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_36]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_52]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_53]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Sprightly Capryni]],  
              Position = {
                y = -1337.765625,  
                x = 21590.42188,  
                InstanceId = [[Client1_39]],  
                Class = [[Position]],  
                z = 109.578125
              },  
              Angle = 2.25,  
              Base = [[palette.entities.creatures.chcdb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Roaming Yubo]],  
              Position = {
                y = -1337.640625,  
                x = 21585.15625,  
                InstanceId = [[Client1_43]],  
                Class = [[Position]],  
                z = 109.734375
              },  
              Angle = 0.34375,  
              Base = [[palette.entities.creatures.chddb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Prime Izam]],  
              Position = {
                y = -1338.21875,  
                x = 21587.82813,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = 109.875
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.creatures.cbbdb4]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Name = [[Act 1]],  
      Title = [[Act 1]],  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 1,  
        InstanceId = [[Client1_89]],  
        Class = [[TextManagerEntry]],  
        Text = [[Ah ... Il est mort.]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}