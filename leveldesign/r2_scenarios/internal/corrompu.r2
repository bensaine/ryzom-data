scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Jungle27]],  
      Time = 0,  
      Name = [[(Jungle 27)]],  
      Season = [[winter]],  
      ManualSeason = 1,  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Name = [[New scenario]],  
  Behavior = {
    InstanceId = [[Client1_3]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_4]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    Scenario = 2,  
    Act = 3,  
    ChatSequence = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    Behavior = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    NpcGrpFeature = 0,  
    EventType = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    ActionStep = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 2,  
    LocationId = 137,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_6]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_8]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_7]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 4,  
      Behavior = {
        InstanceId = [[Client1_10]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_31]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_34]],  
                Entity = r2.RefId([[Client1_29]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_33]],  
                  Type = [[Kill]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_32]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_98]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_99]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_101]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_104]],  
                Entity = r2.RefId([[Client1_93]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_103]],  
                  Type = [[starts dialog]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_102]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_130]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_131]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_12]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_11]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 87,  
      LocationId = [[Client1_14]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_107]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_108]],  
                x = 31196.73438,  
                y = -12691.40625,  
                z = 1.140625
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_105]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 1,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 16,  
              EyesColor = 0,  
              MorphTarget1 = 6,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 0,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Boekos]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_111]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_112]],  
                x = 31195.96875,  
                y = -12688.92188,  
                z = 1.296875
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_109]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 3,  
              HairType = 5622318,  
              HairColor = 1,  
              Tattoo = 25,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 1,  
              MorphTarget7 = 5,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5610030,  
              FeetModel = 0,  
              HandsModel = 5609774,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Liccio]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_93]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_94]],  
              Time = 5,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_95]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_107]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_114]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_115]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_116]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_111]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_117]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Active = 1,  
          Behavior = {
            InstanceId = [[Client1_91]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_92]],  
            x = 31196.78125,  
            y = -12685.5,  
            z = 1
          },  
          Base = [[palette.entities.botobjects.tomb_4]],  
          Repeating = 0
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_128]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_124]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_125]],  
                x = 31203.09375,  
                y = -12693.4375,  
                z = 0.625
              },  
              Angle = 0.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_122]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 13,  
              HairType = 5166,  
              HairColor = 5,  
              Tattoo = 29,  
              EyesColor = 7,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              MorphTarget7 = 0,  
              MorphTarget8 = 7,  
              Sex = 0,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 5609774,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Sirgio]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_120]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_121]],  
                x = 31204.84375,  
                y = -12693.70313,  
                z = 0.671875
              },  
              Angle = 0.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_118]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 13,  
              HairType = 3118,  
              HairColor = 3,  
              Tattoo = 6,  
              EyesColor = 3,  
              MorphTarget1 = 1,  
              MorphTarget2 = 5,  
              MorphTarget3 = 2,  
              MorphTarget4 = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 1,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Gaxius]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_127]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_126]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_96]],  
        Count = 2,  
        Text = [[blablabla]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_113]],  
        Count = 1,  
        Text = [[blabla premier]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_114]],  
        Count = 1,  
        Text = [[blabla premier]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_117]],  
        Count = 2,  
        Text = [[blabla second]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}