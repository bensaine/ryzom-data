scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client1_1263]],  
      Season = [[summer]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts22]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_1254]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    MapDescription = 0,  
    Position = 0,  
    Location = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1252]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 59,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[A place to hang out. There are a few kitins trying to invade, but don't worry, they're being killed by the guards.]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1255]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1257]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1256]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1258]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1259]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[Nothing to see here.]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_1261]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1260]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 608,  
      LocationId = [[Client1_1263]],  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1262]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:You are here]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1253]]
  }
}