scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_42512]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts09]],  
      Time = 0,  
      Name = [[Two Ponds (Desert 09)]],  
      Season = [[Autumn]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_42503]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_42501]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_42502]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_42499]]
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    UserTrigger = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManager = 0,  
    TextManagerEntry = 0,  
    EventType = 0,  
    ZoneTrigger = 1,  
    DefaultFeature = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    ChatSequence = 0,  
    Region = 0,  
    Road = 0,  
    NpcCreature = 0,  
    ActivityStep = 1,  
    NpcCustom = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 1,  
    ActionStep = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 1,  
    WayPoint = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_42506]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_42504]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Name = [[Permanent]],  
      Events = {
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      Title = [[]],  
      Version = 5,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_42519]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42517]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 2]],  
              Position = {
                y = -1205.28125,  
                x = 27638.03125,  
                InstanceId = [[Client1_42520]],  
                Class = [[Position]],  
                z = 75.375
              },  
              Angle = 3.125,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42523]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42521]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1232.796875,  
                x = 27624.64063,  
                InstanceId = [[Client1_42524]],  
                Class = [[Position]],  
                z = 75.546875
              },  
              Angle = -4.296875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42527]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42525]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1230.890625,  
                x = 27605.8125,  
                InstanceId = [[Client1_42528]],  
                Class = [[Position]],  
                z = 75.78125
              },  
              Angle = 0.984375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42531]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42529]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              Position = {
                y = -1204.203125,  
                x = 27588.09375,  
                InstanceId = [[Client1_42532]],  
                Class = [[Position]],  
                z = 75.828125
              },  
              Angle = 0.078125,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42535]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42533]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              Position = {
                y = -1196.609375,  
                x = 27585.78125,  
                InstanceId = [[Client1_42536]],  
                Class = [[Position]],  
                z = 76.390625
              },  
              Angle = -1.203125,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42539]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42537]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 1]],  
              Position = {
                y = -1182.265625,  
                x = 27617.73438,  
                InstanceId = [[Client1_42540]],  
                Class = [[Position]],  
                z = 74.828125
              },  
              Angle = -1.09375,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42543]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42541]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 2]],  
              Position = {
                y = -1185.40625,  
                x = 27626.48438,  
                InstanceId = [[Client1_42544]],  
                Class = [[Position]],  
                z = 75.28125
              },  
              Angle = -2.4375,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42551]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42549]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1205,  
                x = 27615.65625,  
                InstanceId = [[Client1_42552]],  
                Class = [[Position]],  
                z = 73.53125
              },  
              Angle = 2.78125,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_42697]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_42696]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42699]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1209.015625,  
                    x = 27629.28125,  
                    InstanceId = [[Client1_42700]],  
                    Class = [[Position]],  
                    z = 74.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42702]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1212.078125,  
                    x = 27628.25,  
                    InstanceId = [[Client1_42703]],  
                    Class = [[Position]],  
                    z = 74.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42705]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1216.046875,  
                    x = 27625.6875,  
                    InstanceId = [[Client1_42706]],  
                    Class = [[Position]],  
                    z = 74.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42708]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1219.171875,  
                    x = 27619.89063,  
                    InstanceId = [[Client1_42709]],  
                    Class = [[Position]],  
                    z = 74.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42711]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1217.0625,  
                    x = 27614.34375,  
                    InstanceId = [[Client1_42712]],  
                    Class = [[Position]],  
                    z = 74.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42714]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1217.671875,  
                    x = 27611.65625,  
                    InstanceId = [[Client1_42715]],  
                    Class = [[Position]],  
                    z = 74.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42717]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1213.109375,  
                    x = 27610.60938,  
                    InstanceId = [[Client1_42718]],  
                    Class = [[Position]],  
                    z = 74.234375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_42723]],  
              Class = [[Region]],  
              Position = {
                y = -1.859375,  
                x = -1.53125,  
                InstanceId = [[Client1_42722]],  
                Class = [[Position]],  
                z = 0.21875
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42725]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1198.46875,  
                    x = 27634.04688,  
                    InstanceId = [[Client1_42726]],  
                    Class = [[Position]],  
                    z = 75.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42728]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1196.328125,  
                    x = 27632.9375,  
                    InstanceId = [[Client1_42729]],  
                    Class = [[Position]],  
                    z = 75.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42731]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1188.21875,  
                    x = 27628.92188,  
                    InstanceId = [[Client1_42732]],  
                    Class = [[Position]],  
                    z = 73.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42734]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1187.78125,  
                    x = 27620.17188,  
                    InstanceId = [[Client1_42735]],  
                    Class = [[Position]],  
                    z = 73.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42737]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1182.015625,  
                    x = 27611.48438,  
                    InstanceId = [[Client1_42738]],  
                    Class = [[Position]],  
                    z = 74.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42740]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1187.9375,  
                    x = 27594.20313,  
                    InstanceId = [[Client1_42741]],  
                    Class = [[Position]],  
                    z = 76.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42743]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1232.84375,  
                    x = 27586.53125,  
                    InstanceId = [[Client1_42744]],  
                    Class = [[Position]],  
                    z = 76.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42746]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1235.203125,  
                    x = 27597.8125,  
                    InstanceId = [[Client1_42747]],  
                    Class = [[Position]],  
                    z = 76.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42749]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1229.578125,  
                    x = 27606.78125,  
                    InstanceId = [[Client1_42750]],  
                    Class = [[Position]],  
                    z = 75.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42752]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1228.734375,  
                    x = 27621.26563,  
                    InstanceId = [[Client1_42753]],  
                    Class = [[Position]],  
                    z = 75.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42755]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1219.3125,  
                    x = 27633.14063,  
                    InstanceId = [[Client1_42756]],  
                    Class = [[Position]],  
                    z = 75.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42758]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1201.71875,  
                    x = 27635.96875,  
                    InstanceId = [[Client1_42759]],  
                    Class = [[Position]],  
                    z = 75.421875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client1_42767]],  
              Class = [[Road]],  
              Position = {
                y = 2.5,  
                x = -5.921875,  
                InstanceId = [[Client1_42766]],  
                Class = [[Position]],  
                z = 0.109375
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42769]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1219.515625,  
                    x = 27609.51563,  
                    InstanceId = [[Client1_42770]],  
                    Class = [[Position]],  
                    z = 74.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42772]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1207.578125,  
                    x = 27606.70313,  
                    InstanceId = [[Client1_42773]],  
                    Class = [[Position]],  
                    z = 74.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42775]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1198.09375,  
                    x = 27606.32813,  
                    InstanceId = [[Client1_42776]],  
                    Class = [[Position]],  
                    z = 73.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42778]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1195.640625,  
                    x = 27607.4375,  
                    InstanceId = [[Client1_42779]],  
                    Class = [[Position]],  
                    z = 73.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42781]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1194.296875,  
                    x = 27611.0625,  
                    InstanceId = [[Client1_42782]],  
                    Class = [[Position]],  
                    z = 73.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42784]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1200.328125,  
                    x = 27623.90625,  
                    InstanceId = [[Client1_42785]],  
                    Class = [[Position]],  
                    z = 73.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42787]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1201.21875,  
                    x = 27631,  
                    InstanceId = [[Client1_42788]],  
                    Class = [[Position]],  
                    z = 74.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42790]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1227.046875,  
                    x = 27659.98438,  
                    InstanceId = [[Client1_42791]],  
                    Class = [[Position]],  
                    z = 75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42793]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1224.21875,  
                    x = 27632.20313,  
                    InstanceId = [[Client1_42794]],  
                    Class = [[Position]],  
                    z = 75.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42796]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1235.140625,  
                    x = 27619.375,  
                    InstanceId = [[Client1_42797]],  
                    Class = [[Position]],  
                    z = 75.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42799]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1230.8125,  
                    x = 27605.3125,  
                    InstanceId = [[Client1_42800]],  
                    Class = [[Position]],  
                    z = 75.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42802]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1222.375,  
                    x = 27608.09375,  
                    InstanceId = [[Client1_42803]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_42870]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_42869]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42872]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1132.4375,  
                    x = 27780.82813,  
                    InstanceId = [[Client1_42873]],  
                    Class = [[Position]],  
                    z = 72.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42875]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1123.828125,  
                    x = 27800.59375,  
                    InstanceId = [[Client1_42876]],  
                    Class = [[Position]],  
                    z = 75.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42878]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1105.625,  
                    x = 27807.82813,  
                    InstanceId = [[Client1_42879]],  
                    Class = [[Position]],  
                    z = 74.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42881]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1097.109375,  
                    x = 27787.82813,  
                    InstanceId = [[Client1_42882]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42884]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1095.71875,  
                    x = 27775.5625,  
                    InstanceId = [[Client1_42885]],  
                    Class = [[Position]],  
                    z = 75.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42887]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1118.265625,  
                    x = 27762.09375,  
                    InstanceId = [[Client1_42888]],  
                    Class = [[Position]],  
                    z = 75
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_42981]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42979]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 1]],  
              Position = {
                y = -1110.09375,  
                x = 27854.40625,  
                InstanceId = [[Client1_42982]],  
                Class = [[Position]],  
                z = 73.640625
              },  
              Angle = -2.0625,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42985]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42983]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 2]],  
              Position = {
                y = -1109.53125,  
                x = 27857.15625,  
                InstanceId = [[Client1_42986]],  
                Class = [[Position]],  
                z = 73.5
              },  
              Angle = -3.71875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42989]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42987]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 3]],  
              Position = {
                y = -1104.015625,  
                x = 27858.625,  
                InstanceId = [[Client1_42990]],  
                Class = [[Position]],  
                z = 73.765625
              },  
              Angle = -2.3125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42993]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42991]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 4]],  
              Position = {
                y = -1106.296875,  
                x = 27859.51563,  
                InstanceId = [[Client1_42994]],  
                Class = [[Position]],  
                z = 73.625
              },  
              Angle = -3.53125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42997]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42995]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 5]],  
              Position = {
                y = -1105,  
                x = 27856.25,  
                InstanceId = [[Client1_42998]],  
                Class = [[Position]],  
                z = 73.671875
              },  
              Angle = -0.546875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_43018]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43016]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 1]],  
              Position = {
                y = -1094,  
                x = 27851.70313,  
                InstanceId = [[Client1_43019]],  
                Class = [[Position]],  
                z = 74.421875
              },  
              Angle = -2.25,  
              Base = [[palette.entities.botobjects.pack_2]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 3]],  
              InstanceId = [[Client1_43077]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43079]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1112.25,  
                    x = 27850.84375,  
                    InstanceId = [[Client1_43080]],  
                    Class = [[Position]],  
                    z = 73.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43082]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1114.078125,  
                    x = 27853.9375,  
                    InstanceId = [[Client1_43083]],  
                    Class = [[Position]],  
                    z = 73.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43085]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1110.859375,  
                    x = 27858.90625,  
                    InstanceId = [[Client1_43086]],  
                    Class = [[Position]],  
                    z = 73.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43088]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1105.890625,  
                    x = 27861.39063,  
                    InstanceId = [[Client1_43089]],  
                    Class = [[Position]],  
                    z = 72.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43091]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1101.484375,  
                    x = 27862.01563,  
                    InstanceId = [[Client1_43092]],  
                    Class = [[Position]],  
                    z = 74.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43094]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1100.625,  
                    x = 27857.1875,  
                    InstanceId = [[Client1_43095]],  
                    Class = [[Position]],  
                    z = 74.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43097]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1098.203125,  
                    x = 27854.07813,  
                    InstanceId = [[Client1_43098]],  
                    Class = [[Position]],  
                    z = 73.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43100]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1108.828125,  
                    x = 27849.96875,  
                    InstanceId = [[Client1_43101]],  
                    Class = [[Position]],  
                    z = 73.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43103]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1112.25,  
                    x = 27850.84375,  
                    InstanceId = [[Client1_43104]],  
                    Class = [[Position]],  
                    z = 73.953125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_43076]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_43117]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43115]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              Position = {
                y = -1095.953125,  
                x = 27842.32813,  
                InstanceId = [[Client1_43118]],  
                Class = [[Position]],  
                z = 73.484375
              },  
              Angle = -1.75,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 4]],  
              InstanceId = [[Client1_43170]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_43169]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43172]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1093.859375,  
                    x = 27838.54688,  
                    InstanceId = [[Client1_43173]],  
                    Class = [[Position]],  
                    z = 73
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43175]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1098.265625,  
                    x = 27820.07813,  
                    InstanceId = [[Client1_43176]],  
                    Class = [[Position]],  
                    z = 74.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43178]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1096.5,  
                    x = 27791.26563,  
                    InstanceId = [[Client1_43179]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43181]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1093.078125,  
                    x = 27770.82813,  
                    InstanceId = [[Client1_43182]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43184]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1076.328125,  
                    x = 27741.82813,  
                    InstanceId = [[Client1_43185]],  
                    Class = [[Position]],  
                    z = 74.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43187]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1044.390625,  
                    x = 27675.76563,  
                    InstanceId = [[Client1_43188]],  
                    Class = [[Position]],  
                    z = 74.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43190]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1026.1875,  
                    x = 27612.76563,  
                    InstanceId = [[Client1_43191]],  
                    Class = [[Position]],  
                    z = 75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43193]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1016.4375,  
                    x = 27550.28125,  
                    InstanceId = [[Client1_43194]],  
                    Class = [[Position]],  
                    z = 74.5
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 5]],  
              InstanceId = [[Client1_43215]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43217]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1107.5,  
                    x = 27855.17188,  
                    InstanceId = [[Client1_43218]],  
                    Class = [[Position]],  
                    z = 73.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43220]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1108.6875,  
                    x = 27852.01563,  
                    InstanceId = [[Client1_43221]],  
                    Class = [[Position]],  
                    z = 73.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43223]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1113.1875,  
                    x = 27849.57813,  
                    InstanceId = [[Client1_43224]],  
                    Class = [[Position]],  
                    z = 73
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43226]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1115.140625,  
                    x = 27848.60938,  
                    InstanceId = [[Client1_43227]],  
                    Class = [[Position]],  
                    z = 74.296875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_43214]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_45017]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45015]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 3]],  
              Position = {
                y = -1007.734375,  
                x = 27545.5,  
                InstanceId = [[Client1_45018]],  
                Class = [[Position]],  
                z = 73.609375
              },  
              Angle = -1.46875,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45123]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45121]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 4]],  
              Position = {
                y = -1023.46875,  
                x = 27545.64063,  
                InstanceId = [[Client1_45124]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = 0.375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45127]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45125]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 5]],  
              Position = {
                y = -1022.140625,  
                x = 27527.0625,  
                InstanceId = [[Client1_45128]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = -0.234375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45131]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45129]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              Position = {
                y = -1019.734375,  
                x = 27519.25,  
                InstanceId = [[Client1_45132]],  
                Class = [[Position]],  
                z = 74.671875
              },  
              Angle = -0.046875,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45135]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45133]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chest 1]],  
              Position = {
                y = -1007.84375,  
                x = 27524.1875,  
                InstanceId = [[Client1_45136]],  
                Class = [[Position]],  
                z = 72.421875
              },  
              Angle = -0.046875,  
              Base = [[palette.entities.botobjects.chest]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45139]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45137]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -1000.9375,  
                x = 27536.5,  
                InstanceId = [[Client1_45140]],  
                Class = [[Position]],  
                z = 73.28125
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45143]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45141]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1004.03125,  
                x = 27542.40625,  
                InstanceId = [[Client1_45144]],  
                Class = [[Position]],  
                z = 73.65625
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45147]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45145]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[old chest 1]],  
              Position = {
                y = -1005,  
                x = 27545.90625,  
                InstanceId = [[Client1_45148]],  
                Class = [[Position]],  
                z = 74.0625
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.chest_old]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45151]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45149]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 1]],  
              Position = {
                y = -1021.890625,  
                x = 27537.01563,  
                InstanceId = [[Client1_45152]],  
                Class = [[Position]],  
                z = 74.921875
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.bag_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45155]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45153]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 2]],  
              Position = {
                y = -1028.03125,  
                x = 27546.23438,  
                InstanceId = [[Client1_45156]],  
                Class = [[Position]],  
                z = 74.890625
              },  
              Angle = 2.453125,  
              Base = [[palette.entities.botobjects.pack_4]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 6]],  
              InstanceId = [[Client1_45205]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45207]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1120.328125,  
                    x = 27848.64063,  
                    InstanceId = [[Client1_45208]],  
                    Class = [[Position]],  
                    z = 74.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45210]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1178.859375,  
                    x = 27857.45313,  
                    InstanceId = [[Client1_45211]],  
                    Class = [[Position]],  
                    z = 75.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45213]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1234.625,  
                    x = 27853.14063,  
                    InstanceId = [[Client1_45214]],  
                    Class = [[Position]],  
                    z = 76.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45216]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1266.0625,  
                    x = 27790.23438,  
                    InstanceId = [[Client1_45217]],  
                    Class = [[Position]],  
                    z = 72.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45219]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1262.84375,  
                    x = 27759.26563,  
                    InstanceId = [[Client1_45220]],  
                    Class = [[Position]],  
                    z = 73.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45222]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1213.71875,  
                    x = 27702.90625,  
                    InstanceId = [[Client1_45223]],  
                    Class = [[Position]],  
                    z = 77.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45225]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1208.90625,  
                    x = 27634.98438,  
                    InstanceId = [[Client1_45226]],  
                    Class = [[Position]],  
                    z = 75.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45228]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1200.515625,  
                    x = 27615.20313,  
                    InstanceId = [[Client1_45229]],  
                    Class = [[Position]],  
                    z = 72.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45231]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1202.59375,  
                    x = 27615.29688,  
                    InstanceId = [[Client1_45232]],  
                    Class = [[Position]],  
                    z = 73.03125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_45204]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 4]],  
              InstanceId = [[Client1_45268]],  
              Class = [[Region]],  
              Position = {
                y = -3.328125,  
                x = -2,  
                InstanceId = [[Client1_45267]],  
                Class = [[Position]],  
                z = 0.0625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45270]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1195.6875,  
                    x = 27604.59375,  
                    InstanceId = [[Client1_45271]],  
                    Class = [[Position]],  
                    z = 73.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45273]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1207.3125,  
                    x = 27603.70313,  
                    InstanceId = [[Client1_45274]],  
                    Class = [[Position]],  
                    z = 73.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45276]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1215.75,  
                    x = 27605.32813,  
                    InstanceId = [[Client1_45277]],  
                    Class = [[Position]],  
                    z = 75.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45279]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1219.859375,  
                    x = 27611.71875,  
                    InstanceId = [[Client1_45280]],  
                    Class = [[Position]],  
                    z = 73.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45282]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1214.390625,  
                    x = 27625.14063,  
                    InstanceId = [[Client1_45283]],  
                    Class = [[Position]],  
                    z = 73.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45285]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1201.5,  
                    x = 27626.90625,  
                    InstanceId = [[Client1_45286]],  
                    Class = [[Position]],  
                    z = 73.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45288]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1193.46875,  
                    x = 27621.32813,  
                    InstanceId = [[Client1_45289]],  
                    Class = [[Position]],  
                    z = 73
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_45365]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45363]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin mound 1]],  
              Position = {
                y = -1333.359375,  
                x = 27521.4375,  
                InstanceId = [[Client1_45366]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = 0.09375,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45369]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45367]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 1]],  
              Position = {
                y = -1340.953125,  
                x = 27523.15625,  
                InstanceId = [[Client1_45370]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = 0.890625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45373]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45371]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 2]],  
              Position = {
                y = -1330.125,  
                x = 27509.59375,  
                InstanceId = [[Client1_45374]],  
                Class = [[Position]],  
                z = 75.15625
              },  
              Angle = 0.890625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 7]],  
              InstanceId = [[Client1_45403]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_45402]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45405]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1210,  
                    x = 27617.1875,  
                    InstanceId = [[Client1_45406]],  
                    Class = [[Position]],  
                    z = 73.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45408]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1225.546875,  
                    x = 27618.04688,  
                    InstanceId = [[Client1_45409]],  
                    Class = [[Position]],  
                    z = 74.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45411]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1255.09375,  
                    x = 27603.34375,  
                    InstanceId = [[Client1_45412]],  
                    Class = [[Position]],  
                    z = 75.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45414]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1284.015625,  
                    x = 27575.70313,  
                    InstanceId = [[Client1_45415]],  
                    Class = [[Position]],  
                    z = 75.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45417]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1307.28125,  
                    x = 27553.32813,  
                    InstanceId = [[Client1_45418]],  
                    Class = [[Position]],  
                    z = 81.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45420]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1313.9375,  
                    x = 27547.40625,  
                    InstanceId = [[Client1_45421]],  
                    Class = [[Position]],  
                    z = 81.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45426]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1318.765625,  
                    x = 27544,  
                    InstanceId = [[Client1_45427]],  
                    Class = [[Position]],  
                    z = 80.90625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 8]],  
              InstanceId = [[Client1_45501]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_45500]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45503]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1318.234375,  
                    x = 27546.79688,  
                    InstanceId = [[Client1_45504]],  
                    Class = [[Position]],  
                    z = 81.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45506]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1305.796875,  
                    x = 27557.59375,  
                    InstanceId = [[Client1_45507]],  
                    Class = [[Position]],  
                    z = 81.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45509]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1276.5,  
                    x = 27589.84375,  
                    InstanceId = [[Client1_45510]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45512]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1236.65625,  
                    x = 27616.39063,  
                    InstanceId = [[Client1_45513]],  
                    Class = [[Position]],  
                    z = 75.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45515]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1218.984375,  
                    x = 27621.3125,  
                    InstanceId = [[Client1_45516]],  
                    Class = [[Position]],  
                    z = 74.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45518]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1209.671875,  
                    x = 27619.39063,  
                    InstanceId = [[Client1_45519]],  
                    Class = [[Position]],  
                    z = 73.953125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 5]],  
              InstanceId = [[Client1_45661]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45663]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1178.9375,  
                    x = 27628.4375,  
                    InstanceId = [[Client1_45664]],  
                    Class = [[Position]],  
                    z = 76.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45666]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1181.59375,  
                    x = 27629.04688,  
                    InstanceId = [[Client1_45667]],  
                    Class = [[Position]],  
                    z = 76.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45669]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1185.234375,  
                    x = 27631.4375,  
                    InstanceId = [[Client1_45670]],  
                    Class = [[Position]],  
                    z = 76.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45672]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1185.703125,  
                    x = 27633.09375,  
                    InstanceId = [[Client1_45673]],  
                    Class = [[Position]],  
                    z = 76.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45675]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1183.4375,  
                    x = 27632.92188,  
                    InstanceId = [[Client1_45676]],  
                    Class = [[Position]],  
                    z = 76.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45678]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1177.875,  
                    x = 27630.54688,  
                    InstanceId = [[Client1_45679]],  
                    Class = [[Position]],  
                    z = 76.953125
                  }
                }
              },  
              Position = {
                y = -2.65625,  
                x = 11.21875,  
                InstanceId = [[Client1_45660]],  
                Class = [[Position]],  
                z = 1.234375
              }
            },  
            {
              InstanceId = [[Client1_46164]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46162]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[butterflies 1]],  
              Position = {
                y = -1216.8125,  
                x = 27636.96875,  
                InstanceId = [[Client1_46165]],  
                Class = [[Position]],  
                z = 75.5
              },  
              Angle = 0.171875,  
              Base = [[palette.entities.botobjects.fx_ju_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46168]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46166]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[crickets 1]],  
              Position = {
                y = -1189.5625,  
                x = 27618.21875,  
                InstanceId = [[Client1_46169]],  
                Class = [[Position]],  
                z = 73.671875
              },  
              Angle = -2.40625,  
              Base = [[palette.entities.botobjects.fx_tr_cricket_aqua]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46180]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46178]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bats 1]],  
              Position = {
                y = -1374.796875,  
                x = 27529.01563,  
                InstanceId = [[Client1_46181]],  
                Class = [[Position]],  
                z = 74.71875
              },  
              Angle = 0.484375,  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46184]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46182]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bats 2]],  
              Position = {
                y = -1355.78125,  
                x = 27502.64063,  
                InstanceId = [[Client1_46185]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              Angle = 0.484375,  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46188]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46186]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bats 3]],  
              Position = {
                y = -1333.1875,  
                x = 27555.40625,  
                InstanceId = [[Client1_46189]],  
                Class = [[Position]],  
                z = 79.953125
              },  
              Angle = -1.8125,  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46200]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46198]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[mammal carrion 1]],  
              Position = {
                y = -1363.671875,  
                x = 27527.78125,  
                InstanceId = [[Client1_46201]],  
                Class = [[Position]],  
                z = 75.203125
              },  
              Angle = 0.03125,  
              Base = [[palette.entities.botobjects.carrion_mammal]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46204]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46202]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              Position = {
                y = -1378.515625,  
                x = 27527.82813,  
                InstanceId = [[Client1_46205]],  
                Class = [[Position]],  
                z = 74.609375
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46212]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46210]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bones 1]],  
              Position = {
                y = -1341.109375,  
                x = 27553.5625,  
                InstanceId = [[Client1_46213]],  
                Class = [[Position]],  
                z = 78.8125
              },  
              Angle = 1.796875,  
              Base = [[palette.entities.botobjects.bones]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46216]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46214]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bones 2]],  
              Position = {
                y = -1301.328125,  
                x = 27498.125,  
                InstanceId = [[Client1_46217]],  
                Class = [[Position]],  
                z = 74.359375
              },  
              Angle = -0.15625,  
              Base = [[palette.entities.botobjects.bones_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46220]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46218]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 2]],  
              Position = {
                y = -1206.71875,  
                x = 27627.98438,  
                InstanceId = [[Client1_46221]],  
                Class = [[Position]],  
                z = 74.40625
              },  
              Angle = -2.5,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46224]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46222]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 barrels 1]],  
              Position = {
                y = -1207.84375,  
                x = 27592.0625,  
                InstanceId = [[Client1_46225]],  
                Class = [[Position]],  
                z = 75.34375
              },  
              Angle = -0.921875,  
              Base = [[palette.entities.botobjects.barrels_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46232]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46230]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chest 3]],  
              Position = {
                y = -1205.859375,  
                x = 27633.92188,  
                InstanceId = [[Client1_46233]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = -2.8125,  
              Base = [[palette.entities.botobjects.chest]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46236]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46234]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 2]],  
              Position = {
                y = -1200.75,  
                x = 27620.0625,  
                InstanceId = [[Client1_46237]],  
                Class = [[Position]],  
                z = 73.484375
              },  
              Angle = -1.484375,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46240]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46238]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 2]],  
              Position = {
                y = -1184.0625,  
                x = 27612.79688,  
                InstanceId = [[Client1_46241]],  
                Class = [[Position]],  
                z = 74.5
              },  
              Angle = -1.65625,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46244]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46242]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[working chariot 1]],  
              Position = {
                y = -1226.1875,  
                x = 27608.46875,  
                InstanceId = [[Client1_46245]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              Angle = 1.734375,  
              Base = [[palette.entities.botobjects.chariot_working]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46248]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46246]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[old chest 2]],  
              Position = {
                y = -1196.015625,  
                x = 27592.39063,  
                InstanceId = [[Client1_46249]],  
                Class = [[Position]],  
                z = 75.75
              },  
              Angle = 0.1875,  
              Base = [[palette.entities.botobjects.chest_old]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46252]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46250]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 3]],  
              Position = {
                y = -1200.15625,  
                x = 27590.20313,  
                InstanceId = [[Client1_46253]],  
                Class = [[Position]],  
                z = 75.859375
              },  
              Angle = 0.1875,  
              Base = [[palette.entities.botobjects.pack_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46256]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46254]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 4]],  
              Position = {
                y = -1196.828125,  
                x = 27635.71875,  
                InstanceId = [[Client1_46257]],  
                Class = [[Position]],  
                z = 75.765625
              },  
              Angle = -3.109375,  
              Base = [[palette.entities.botobjects.pack_4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46260]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46258]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 5]],  
              Position = {
                y = -1193.171875,  
                x = 27634.32813,  
                InstanceId = [[Client1_46261]],  
                Class = [[Position]],  
                z = 75.90625
              },  
              Angle = -2.53125,  
              Base = [[palette.entities.botobjects.pack_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46264]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46262]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 2]],  
              Position = {
                y = -1200.46875,  
                x = 27621.98438,  
                InstanceId = [[Client1_46265]],  
                Class = [[Position]],  
                z = 73.65625
              },  
              Angle = -2.53125,  
              Base = [[palette.entities.botobjects.bag_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46268]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46266]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 3]],  
              Position = {
                y = -1188.640625,  
                x = 27624.09375,  
                InstanceId = [[Client1_46269]],  
                Class = [[Position]],  
                z = 74.484375
              },  
              Angle = -2.53125,  
              Base = [[palette.entities.botobjects.bag_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46272]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46270]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -1229.8125,  
                x = 27577.64063,  
                InstanceId = [[Client1_46273]],  
                Class = [[Position]],  
                z = 75.40625
              },  
              Angle = 0.65625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46280]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46278]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bats 4]],  
              Position = {
                y = -1132.015625,  
                x = 27813.60938,  
                InstanceId = [[Client1_46281]],  
                Class = [[Position]],  
                z = 77.40625
              },  
              Angle = -2.9375,  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46284]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46282]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bats 5]],  
              Position = {
                y = -1095.78125,  
                x = 27804.23438,  
                InstanceId = [[Client1_46285]],  
                Class = [[Position]],  
                z = 74.875
              },  
              Angle = -1.328125,  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46288]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46286]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[grey bugs 1]],  
              Position = {
                y = -1114.8125,  
                x = 27867.9375,  
                InstanceId = [[Client1_46289]],  
                Class = [[Position]],  
                z = 73.953125
              },  
              Angle = -2.484375,  
              Base = [[palette.entities.botobjects.fx_fo_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46300]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46298]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo infested carcass II 2]],  
              Position = {
                y = -1123.96875,  
                x = 27789.78125,  
                InstanceId = [[Client1_46301]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              Angle = 0.171875,  
              Base = [[palette.entities.botobjects.fx_goo_mamal]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 6]],  
              InstanceId = [[Client1_46889]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46891]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1184.40625,  
                    x = 27810.76563,  
                    InstanceId = [[Client1_46892]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46894]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1163.78125,  
                    x = 27841.98438,  
                    InstanceId = [[Client1_46895]],  
                    Class = [[Position]],  
                    z = 75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46897]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1166.75,  
                    x = 27848.14063,  
                    InstanceId = [[Client1_46898]],  
                    Class = [[Position]],  
                    z = 75.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46900]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1193.703125,  
                    x = 27843.34375,  
                    InstanceId = [[Client1_46901]],  
                    Class = [[Position]],  
                    z = 74.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46903]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1198.578125,  
                    x = 27838.25,  
                    InstanceId = [[Client1_46904]],  
                    Class = [[Position]],  
                    z = 74.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46906]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1187.84375,  
                    x = 27831.5,  
                    InstanceId = [[Client1_46907]],  
                    Class = [[Position]],  
                    z = 74.609375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_46888]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 9]],  
              InstanceId = [[Client1_46919]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46921]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1204.984375,  
                    x = 27604.89063,  
                    InstanceId = [[Client1_46922]],  
                    Class = [[Position]],  
                    z = 74.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46924]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1204.296875,  
                    x = 27607.09375,  
                    InstanceId = [[Client1_46925]],  
                    Class = [[Position]],  
                    z = 73.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46927]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1203,  
                    x = 27611.15625,  
                    InstanceId = [[Client1_46928]],  
                    Class = [[Position]],  
                    z = 73.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_46930]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1202.703125,  
                    x = 27613.78125,  
                    InstanceId = [[Client1_46931]],  
                    Class = [[Position]],  
                    z = 73.46875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_46918]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_47015]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47013]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 1]],  
              Position = {
                y = -1378.515625,  
                x = 27527.82813,  
                InstanceId = [[Client1_47016]],  
                Class = [[Position]],  
                z = 74.609375
              },  
              Angle = -0.78125,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47019]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47017]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo infested carcass II 3]],  
              Position = {
                y = -1326.484375,  
                x = 27506.65625,  
                InstanceId = [[Client1_47020]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.botobjects.fx_goo_mamal]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47023]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47021]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo infested carcass I 1]],  
              Position = {
                y = -1378.765625,  
                x = 27462.45313,  
                InstanceId = [[Client1_47024]],  
                Class = [[Position]],  
                z = 76.453125
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.botobjects.fx_goo_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47027]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47025]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo infested carcass I 2]],  
              Position = {
                y = -1304.65625,  
                x = 27514.64063,  
                InstanceId = [[Client1_47028]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = -0.25,  
              Base = [[palette.entities.botobjects.fx_goo_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47039]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47037]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[large stump 1]],  
              Position = {
                y = -1313.84375,  
                x = 27497.35938,  
                InstanceId = [[Client1_47040]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = 0,  
              Base = [[palette.entities.botobjects.stump]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47043]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47041]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[large stump 2]],  
              Position = {
                y = -1407.734375,  
                x = 27521.9375,  
                InstanceId = [[Client1_47044]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              Angle = 1,  
              Base = [[palette.entities.botobjects.stump]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47047]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47045]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bones 3]],  
              Position = {
                y = -1399.515625,  
                x = 27477.96875,  
                InstanceId = [[Client1_47048]],  
                Class = [[Position]],  
                z = 78.765625
              },  
              Angle = 1,  
              Base = [[palette.entities.botobjects.bones_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47051]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47049]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bones 4]],  
              Position = {
                y = -1318.9375,  
                x = 27515.375,  
                InstanceId = [[Client1_47052]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = 0.3125,  
              Base = [[palette.entities.botobjects.bones_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47055]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47053]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 2]],  
              Position = {
                y = -1319.796875,  
                x = 27516.60938,  
                InstanceId = [[Client1_47056]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = 0.125,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47059]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47057]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 3]],  
              Position = {
                y = -1299.09375,  
                x = 27496.35938,  
                InstanceId = [[Client1_47060]],  
                Class = [[Position]],  
                z = 74.078125
              },  
              Angle = -0.34375,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47063]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47061]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 4]],  
              Position = {
                y = -1381.265625,  
                x = 27459.375,  
                InstanceId = [[Client1_47064]],  
                Class = [[Position]],  
                z = 77.15625
              },  
              Angle = 0.6875,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47067]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47065]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[mammal carrion 2]],  
              Position = {
                y = -1036.515625,  
                x = 27614.04688,  
                InstanceId = [[Client1_47068]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -0.328125,  
              Base = [[palette.entities.botobjects.carrion_mammal]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47075]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47073]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bones 5]],  
              Position = {
                y = -1004.65625,  
                x = 27563.48438,  
                InstanceId = [[Client1_47076]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = -0.421875,  
              Base = [[palette.entities.botobjects.bones_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47083]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47081]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[insect carrion 2]],  
              Position = {
                y = -1032.40625,  
                x = 27564.85938,  
                InstanceId = [[Client1_47084]],  
                Class = [[Position]],  
                z = 74.84375
              },  
              Angle = -0.234375,  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47087]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47085]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[giant skull 1]],  
              Position = {
                y = -1001.5,  
                x = 27579.9375,  
                InstanceId = [[Client1_47088]],  
                Class = [[Position]],  
                z = 74.40625
              },  
              Angle = -1.1875,  
              Base = [[palette.entities.botobjects.giant_skull]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47091]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47089]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[giant skull 2]],  
              Position = {
                y = -1126.359375,  
                x = 27865.71875,  
                InstanceId = [[Client1_47092]],  
                Class = [[Position]],  
                z = 74.125
              },  
              Angle = 2.140625,  
              Base = [[palette.entities.botobjects.giant_skull]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47095]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47093]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bones 6]],  
              Position = {
                y = -1129.28125,  
                x = 27818.15625,  
                InstanceId = [[Client1_47096]],  
                Class = [[Position]],  
                z = 76.671875
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.bones_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47103]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47101]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fireflies 1]],  
              Position = {
                y = -1115.421875,  
                x = 27862.67188,  
                InstanceId = [[Client1_47104]],  
                Class = [[Position]],  
                z = 73.390625
              },  
              Angle = 2.65625,  
              Base = [[palette.entities.botobjects.fx_fo_bugsb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47107]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47105]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[grey bugs 2]],  
              Position = {
                y = -1106.625,  
                x = 27831.73438,  
                InstanceId = [[Client1_47108]],  
                Class = [[Position]],  
                z = 74.34375
              },  
              Angle = -2.484375,  
              Base = [[palette.entities.botobjects.fx_fo_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47111]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47109]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[grey bugs 3]],  
              Position = {
                y = -1126.546875,  
                x = 27839.5625,  
                InstanceId = [[Client1_47112]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              Angle = 2.3125,  
              Base = [[palette.entities.botobjects.fx_fo_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47119]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47117]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[grey bugs 4]],  
              Position = {
                y = -1020.796875,  
                x = 27564.01563,  
                InstanceId = [[Client1_47120]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -0.375,  
              Base = [[palette.entities.botobjects.fx_fo_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47123]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47121]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[grey bugs 5]],  
              Position = {
                y = -1026.390625,  
                x = 27558.60938,  
                InstanceId = [[Client1_47124]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              Angle = -0.109375,  
              Base = [[palette.entities.botobjects.fx_fo_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47127]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47125]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[grey bugs 6]],  
              Position = {
                y = -1003.171875,  
                x = 27502.4375,  
                InstanceId = [[Client1_47128]],  
                Class = [[Position]],  
                z = 70.984375
              },  
              Angle = -0.109375,  
              Base = [[palette.entities.botobjects.fx_fo_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47131]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47129]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[rotasects II 1]],  
              Position = {
                y = -1053.765625,  
                x = 27584.01563,  
                InstanceId = [[Client1_47132]],  
                Class = [[Position]],  
                z = 80.265625
              },  
              Angle = -1.140625,  
              Base = [[palette.entities.botobjects.fx_ju_rotasecteb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47135]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47133]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fireflies 2]],  
              Position = {
                y = -1229.078125,  
                x = 27593.3125,  
                InstanceId = [[Client1_47136]],  
                Class = [[Position]],  
                z = 75.484375
              },  
              Angle = 1.515625,  
              Base = [[palette.entities.botobjects.fx_fo_bugsb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47139]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47137]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[butterflies 2]],  
              Position = {
                y = -1193.453125,  
                x = 27623.34375,  
                InstanceId = [[Client1_47140]],  
                Class = [[Position]],  
                z = 73.9375
              },  
              Angle = 2.0625,  
              Base = [[palette.entities.botobjects.fx_ju_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47143]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47141]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[incandescent green pollen 1]],  
              Position = {
                y = -1171.359375,  
                x = 27607.51563,  
                InstanceId = [[Client1_47144]],  
                Class = [[Position]],  
                z = 76.453125
              },  
              Angle = -1.6875,  
              Base = [[palette.entities.botobjects.fx_ju_solbirthc]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47147]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47145]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fog II 1]],  
              Position = {
                y = -1023.3125,  
                x = 27549.17188,  
                InstanceId = [[Client1_47148]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -0.890625,  
              Base = [[palette.entities.botobjects.fx_fo_brumesb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47159]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47157]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fog 1]],  
              Position = {
                y = -1100.908081,  
                x = 27840.12109,  
                InstanceId = [[Client1_47160]],  
                Class = [[Position]],  
                z = 73.55606842
              },  
              Angle = -3.125,  
              Base = [[palette.entities.botobjects.fx_ju_solbirtha]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47167]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47165]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 3]],  
              Position = {
                y = -1215.890625,  
                x = 27636.17188,  
                InstanceId = [[Client1_47168]],  
                Class = [[Position]],  
                z = 75.390625
              },  
              Angle = -3.640625,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_42507]]
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_42505]],  
        Class = [[Position]],  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 0
    },  
    {
      InstanceId = [[Client1_42510]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_42578]],  
            Actions = {
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_42580]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_42581]],  
                Entity = r2.RefId([[Client1_42555]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_42582]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_42583]],  
                Entity = r2.RefId([[Client1_42559]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_42584]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_42585]],  
                Entity = r2.RefId([[Client1_42563]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[starts dialog]],  
                  InstanceId = [[Client1_42586]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_42587]],  
                Entity = r2.RefId([[Client1_42567]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_42579]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_43137]],  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_43139]],  
                  Value = r2.RefId([[Client1_43132]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_43140]],  
                Entity = r2.RefId([[Client1_43111]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_43138]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_43167]],  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_43202]],  
                  Value = r2.RefId([[Client1_43198]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_43203]],  
                Entity = r2.RefId([[Client1_43127]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_43168]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_43259]],  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_43261]],  
                  Value = r2.RefId([[Client1_43243]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_43262]],  
                Entity = r2.RefId([[Client1_42977]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_43260]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_45174]],  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_45176]],  
                  Value = r2.RefId([[Client1_45169]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_45177]],  
                Entity = r2.RefId([[Client1_44975]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_45195]],  
                  Value = r2.RefId([[Client1_45183]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_45196]],  
                Entity = r2.RefId([[Client1_45029]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_45197]],  
                  Value = r2.RefId([[Client1_45187]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_45198]],  
                Entity = r2.RefId([[Client1_45080]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_45175]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_45698]],  
            Actions = {
              {
                Action = {
                  Type = [[Deactivate]],  
                  InstanceId = [[Client1_45700]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_45701]],  
                Entity = r2.RefId([[Client1_45389]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_45699]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_45703]],  
            Actions = {
              {
                Action = {
                  Type = [[Deactivate]],  
                  InstanceId = [[Client1_45705]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_45706]],  
                Entity = r2.RefId([[Client1_45577]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_45704]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          }
        },  
        InstanceId = [[Client1_42508]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Name = [[Act 1:Act 1]],  
      Events = {
      },  
      WeatherValue = 122,  
      LocationId = [[Client1_42512]],  
      Title = [[]],  
      Version = 5,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_42555]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 3,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42553]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45440]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45441]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45442]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45443]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45403]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45527]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45528]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45707]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45708]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45661]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 0,  
              Speed = 1,  
              Angle = 3.25,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              Level = 0,  
              JacketModel = 5606446,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Thecaon]],  
              Position = {
                y = -1205,  
                x = 27618.35938,  
                InstanceId = [[Client1_42556]],  
                Class = [[Position]],  
                z = 73.609375
              },  
              ArmModel = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 2,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_42559]],  
              ActivitiesId = {
              },  
              HairType = 5623086,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 13,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 3,  
              GabaritHeight = 2,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 4,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42557]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45444]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45445]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45446]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45447]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45403]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45525]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45526]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45711]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45712]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45661]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 1,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 2,  
              Level = 0,  
              JacketModel = 0,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Dughan]],  
              Position = {
                y = -1205.171875,  
                x = 27613.04688,  
                InstanceId = [[Client1_42560]],  
                Class = [[Position]],  
                z = 73.640625
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 5,  
              Tattoo = 25
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_42563]],  
              ActivitiesId = {
              },  
              HairType = 5623854,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 3,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 2,  
              HandsColor = 1,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42561]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45431]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45432]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45433]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45434]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45403]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45523]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45524]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45709]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45710]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45661]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 5,  
              FeetModel = 0,  
              Speed = 1,  
              Angle = 1.90625,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              SheetClient = [[basic_zorai_female.creature]],  
              ArmColor = 0,  
              Level = 0,  
              JacketModel = 5618734,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ce-Ni]],  
              Position = {
                y = -1207.5625,  
                x = 27615.20313,  
                InstanceId = [[Client1_42564]],  
                Class = [[Position]],  
                z = 73.71875
              },  
              ArmModel = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 7,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_42591]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 0,  
              HandsModel = 5606958,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 1,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 9,  
              HandsColor = 5,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42589]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42762]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42763]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 7,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 3.015625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 5605166,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Ulydix]],  
              Position = {
                y = -1198.6875,  
                x = 27621.71875,  
                InstanceId = [[Client1_42592]],  
                Class = [[Position]],  
                z = 73.609375
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 5,  
              MorphTarget3 = 0,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_42662]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 8,  
              HandsModel = 5604142,  
              FeetColor = 2,  
              GabaritBreastSize = 4,  
              GabaritHeight = 13,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 14,  
              HandsColor = 1,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42660]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42811]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_42812]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42767]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 1.46875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 5605166,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_pierce_b3.creature]],  
              WeaponRightHand = 5635886,  
              Level = 2,  
              Name = [[Deups]],  
              Position = {
                y = -1216.59375,  
                x = 27606.5625,  
                InstanceId = [[Client1_42663]],  
                Class = [[Position]],  
                z = 74.5
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 5,  
              MorphTarget3 = 0,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_42674]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 5606958,  
              FeetColor = 0,  
              GabaritBreastSize = 8,  
              GabaritHeight = 8,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 6,  
              HandsColor = 1,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42672]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -0.015625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 5607726,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_slash_b3.creature]],  
              WeaponRightHand = 5595694,  
              Level = 2,  
              Name = [[Meron]],  
              Position = {
                y = -1195.25,  
                x = 27602.54688,  
                InstanceId = [[Client1_42675]],  
                Class = [[Position]],  
                z = 74.359375
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 6,  
              MorphTarget3 = 6,  
              Tattoo = 17
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_42682]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 3,  
              HandsModel = 5606958,  
              FeetColor = 0,  
              GabaritBreastSize = 13,  
              GabaritHeight = 10,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 0,  
              HandsColor = 5,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42680]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -2.25,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 5605166,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_b3.creature]],  
              WeaponRightHand = 5595950,  
              Level = 2,  
              Name = [[Xathus]],  
              Position = {
                y = -1189.78125,  
                x = 27614.5,  
                InstanceId = [[Client1_42683]],  
                Class = [[Position]],  
                z = 73.578125
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 1,  
              MorphTarget3 = 5,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_42861]],  
              ActivitiesId = {
              },  
              HairType = 5422,  
              TrouserColor = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 9,  
              HandsModel = 5609774,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42859]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 5653038,  
              Speed = 0,  
              Angle = 3,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 5610542,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Master of Wisdom]],  
              Position = {
                y = -1202.875,  
                x = 27708.98438,  
                InstanceId = [[Client1_42862]],  
                Class = [[Position]],  
                z = 76.859375
              },  
              ArmModel = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 0,  
              Tattoo = 25
            },  
            {
              InstanceId = [[Client1_42891]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42889]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42917]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42918]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Hard Kipee]],  
              Position = {
                y = -1105.625,  
                x = 27774.92188,  
                InstanceId = [[Client1_42892]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckhdb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42895]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42893]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42909]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42910]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Vulgar  Kipee]],  
              Position = {
                y = -1115.53125,  
                x = 27772.20313,  
                InstanceId = [[Client1_42896]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckhdb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42903]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42901]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42911]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42912]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Vigorous  Kipee]],  
              Position = {
                y = -1124.171875,  
                x = 27775.82813,  
                InstanceId = [[Client1_42904]],  
                Class = [[Position]],  
                z = 74.5
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckhdb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42907]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42905]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42915]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42916]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kinrey]],  
              Position = {
                y = -1111.8125,  
                x = 27786.76563,  
                InstanceId = [[Client1_42908]],  
                Class = [[Position]],  
                z = 74.828125
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckbib3]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_42977]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 8,  
              HandsModel = 5609774,  
              FeetColor = 1,  
              GabaritBreastSize = 8,  
              GabaritHeight = 6,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 7,  
              HandsColor = 2,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42975]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43288]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_43290]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_43291]],  
                        Entity = r2.RefId([[Client1_43233]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_43289]],  
                      Value = r2.RefId([[Client1_43245]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45263]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_45265]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45266]],  
                        Entity = r2.RefId([[Client1_45246]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Stand Up]],  
                          InstanceId = [[Client1_46649]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_46650]],  
                        Entity = r2.RefId([[Client1_42559]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Stand Up]],  
                          InstanceId = [[Client1_46651]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_46652]],  
                        Entity = r2.RefId([[Client1_42555]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Stand Up]],  
                          InstanceId = [[Client1_46653]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_46654]],  
                        Entity = r2.RefId([[Client1_42563]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45264]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45304]],  
                    Actions = {
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45305]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45309]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_45311]],  
                          Value = r2.RefId([[Client1_42762]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45312]],  
                        Entity = r2.RefId([[Client1_42591]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45310]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45314]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[stops dialog]],  
                          InstanceId = [[Client1_45316]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45317]],  
                        Entity = r2.RefId([[Client1_42817]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45315]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45319]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[stops dialog]],  
                          InstanceId = [[Client1_45321]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45322]],  
                        Entity = r2.RefId([[Client1_42567]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45320]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45552]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_45554]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45555]],  
                        Entity = r2.RefId([[Client1_45544]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45553]],  
                      Value = r2.RefId([[Client1_45529]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46012]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_46014]],  
                          Value = r2.RefId([[Client1_46008]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_46015]],  
                        Entity = r2.RefId([[Client1_46004]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_46013]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43243]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43244]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43245]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[6]],  
                        InstanceId = [[Client1_43246]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_43215]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45237]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45238]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45205]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45452]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45453]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45403]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45529]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45530]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45713]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45714]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45661]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 5653038,  
              Speed = 1,  
              Angle = -2.359375,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 5610542,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 6,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Princess Faneliah]],  
              Position = {
                y = -1106.921875,  
                x = 27856.51563,  
                InstanceId = [[Client1_42978]],  
                Class = [[Position]],  
                z = 73.59375
              },  
              ArmModel = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 5,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_43111]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 9,  
              HandsModel = 5606958,  
              FeetColor = 3,  
              GabaritBreastSize = 2,  
              GabaritHeight = 3,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 3,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43109]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43132]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43133]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43134]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43135]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_43077]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43281]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_43283]],  
                          Value = r2.RefId([[Client1_43245]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_43284]],  
                        Entity = r2.RefId([[Client1_42977]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[stops dialog]],  
                          InstanceId = [[Client1_43285]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_43286]],  
                        Entity = r2.RefId([[Client1_43005]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_43282]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 5606702,  
              Speed = 1,  
              Angle = -2.15625,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 2,  
              Level = 0,  
              JacketModel = 5606446,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              Sheet = [[ring_melee_damage_dealer_slash_b2.creature]],  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[Icarius]],  
              Position = {
                y = -1093.96875,  
                x = 27845.23438,  
                InstanceId = [[Client1_43112]],  
                Class = [[Position]],  
                z = 73.8125
              },  
              ArmModel = 5606190,  
              MorphTarget7 = 4,  
              MorphTarget3 = 1,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_43121]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 3,  
              HairColor = 3,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 7,  
              HandsColor = 5,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43119]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43123]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_43124]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_43077]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43130]],  
                    Actions = {
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_43131]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43210]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_43212]],  
                          Value = r2.RefId([[Client1_43200]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_43213]],  
                        Entity = r2.RefId([[Client1_43127]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_43211]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46981]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_46983]],  
                          Value = r2.RefId([[Client1_43134]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_46984]],  
                        Entity = r2.RefId([[Client1_43111]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_46982]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -2.5,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 3,  
              Level = 0,  
              JacketModel = 0,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              WeaponRightHand = 6933806,  
              WeaponLeftHand = 0,  
              Name = [[Deuron]],  
              Position = {
                y = -1108.359375,  
                x = 27852.84375,  
                InstanceId = [[Client1_43122]],  
                Class = [[Position]],  
                z = 73.6875
              },  
              ArmModel = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 7,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_43127]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 4,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 5,  
              HairColor = 5,  
              Aggro = 5,  
              EyesColor = 7,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43125]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43198]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43199]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43200]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43201]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_43170]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46988]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_46989]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              SheetClient = [[basic_fyros_male.creature]],  
              FeetModel = 5605422,  
              Speed = 1,  
              Angle = -0.9375,  
              Base = [[palette.entities.npcs.bandits.f_mage_atysian_curser_20]],  
              ArmColor = 0,  
              Level = 0,  
              BotAttackable = 0,  
              Sex = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6933806,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 2,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_blind_b2.creature]],  
              Name = [[Dyps]],  
              Position = {
                y = -1093.4375,  
                x = 27839.21875,  
                InstanceId = [[Client1_43128]],  
                Class = [[Position]],  
                z = 73.640625
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 1,  
              Tattoo = 29
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_45958]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 6,  
              HandsModel = 6699822,  
              FeetColor = 5,  
              GabaritBreastSize = 7,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 0,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 9,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45956]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45960]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_45961]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 6,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -1.09375,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              Level = 0,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]],  
              WeaponRightHand = 6755886,  
              WeaponLeftHand = 0,  
              Name = [[Ulydix]],  
              Position = {
                y = -1185.5,  
                x = 27607.53125,  
                InstanceId = [[Client1_45959]],  
                Class = [[Position]],  
                z = 74.5
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 5,  
              MorphTarget3 = 0,  
              Tattoo = 21
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45964]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 2,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 12,  
              GabaritHeight = 3,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 1,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45962]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45966]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_45967]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -3.015625,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6756654,  
              Level = 0,  
              Name = [[Boemus]],  
              Position = {
                y = -1195.859375,  
                x = 27618.40625,  
                InstanceId = [[Client1_45965]],  
                Class = [[Position]],  
                z = 73.296875
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 4,  
              MorphTarget3 = 2,  
              Tattoo = 14
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45970]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 9,  
              HandsModel = 6699822,  
              FeetColor = 1,  
              GabaritBreastSize = 6,  
              GabaritHeight = 10,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 5,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45968]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45972]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_45973]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -3.015625,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponRightHand = 6755630,  
              Level = 0,  
              Name = [[Ulycus]],  
              Position = {
                y = -1200.8125,  
                x = 27628.28125,  
                InstanceId = [[Client1_45971]],  
                Class = [[Position]],  
                z = 74.453125
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 0,  
              MorphTarget3 = 4,  
              Tattoo = 26
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_45976]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 2,  
              HandsModel = 6699822,  
              FeetColor = 3,  
              GabaritBreastSize = 11,  
              GabaritHeight = 6,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 11,  
              HandsColor = 2,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45974]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45978]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_45979]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42697]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 5,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = 2.578125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponRightHand = 6755630,  
              Level = 0,  
              Name = [[Zenix]],  
              Position = {
                y = -1209.0625,  
                x = 27631.70313,  
                InstanceId = [[Client1_45977]],  
                Class = [[Position]],  
                z = 74.8125
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 2
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45986]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 14,  
              HandsModel = 6699822,  
              FeetColor = 5,  
              GabaritBreastSize = 0,  
              GabaritHeight = 11,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 3,  
              HandsColor = 5,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45984]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45988]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_45989]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = 1.875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6756910,  
              Level = 0,  
              Name = [[Zela]],  
              Position = {
                y = -1224.328125,  
                x = 27626.17188,  
                InstanceId = [[Client1_45987]],  
                Class = [[Position]],  
                z = 75.359375
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 3,  
              MorphTarget3 = 2,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45992]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 1,  
              GabaritHeight = 2,  
              HairColor = 4,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 4,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45990]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45994]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_45995]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42767]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = 1.21875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6756654,  
              Level = 0,  
              Name = [[Apothus]],  
              Position = {
                y = -1228.859375,  
                x = 27618.21875,  
                InstanceId = [[Client1_45993]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 2,  
              MorphTarget3 = 4,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_45998]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 3,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 6,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 3,  
              HandsColor = 1,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45996]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46000]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_46001]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42697]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 5,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -0.6875,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]],  
              WeaponRightHand = 6756142,  
              Level = 0,  
              Name = [[Zeps]],  
              Position = {
                y = -1220.921875,  
                x = 27613.89063,  
                InstanceId = [[Client1_45999]],  
                Class = [[Position]],  
                z = 74.703125
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 7,  
              MorphTarget3 = 5,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_46004]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 2,  
              GabaritBreastSize = 5,  
              GabaritHeight = 9,  
              HairColor = 0,  
              EyesColor = 2,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 10,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46002]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46006]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_46007]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46008]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_46009]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_46919]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_46010]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -0.859375,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponRightHand = 6755886,  
              Level = 0,  
              Name = [[Deuxius]],  
              Position = {
                y = -1205.890625,  
                x = 27603.5,  
                InstanceId = [[Client1_46005]],  
                Class = [[Position]],  
                z = 74.34375
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 14
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_46038]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 5,  
              HandsModel = 6699822,  
              FeetColor = 3,  
              GabaritBreastSize = 11,  
              GabaritHeight = 11,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 1,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46036]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = 2.828125,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              Level = 0,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]],  
              WeaponRightHand = 6756142,  
              WeaponLeftHand = 0,  
              Name = [[Meron]],  
              Position = {
                y = -1219.203125,  
                x = 27628.70313,  
                InstanceId = [[Client1_46039]],  
                Class = [[Position]],  
                z = 75.203125
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 0,  
              MorphTarget3 = 3,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_46060]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 8,  
              HandsModel = 6700078,  
              FeetColor = 2,  
              GabaritBreastSize = 4,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 1,  
              HandsColor = 1,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46058]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46062]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_46063]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42767]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -0.65625,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]],  
              WeaponRightHand = 6755886,  
              Level = 0,  
              Name = [[Deseus]],  
              Position = {
                y = -1207.25,  
                x = 27597.04688,  
                InstanceId = [[Client1_46061]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 0,  
              MorphTarget3 = 7,  
              Tattoo = 29
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_46066]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 8,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 13,  
              GabaritHeight = 10,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 2,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46064]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46068]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_46069]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -0.65625,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]],  
              WeaponRightHand = 6756142,  
              Level = 0,  
              Name = [[Piion]],  
              Position = {
                y = -1193.6875,  
                x = 27596.67188,  
                InstanceId = [[Client1_46067]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 22
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_46072]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 12,  
              HandsModel = 6700078,  
              FeetColor = 0,  
              GabaritBreastSize = 1,  
              GabaritHeight = 9,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 1,  
              HandsColor = 1,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46070]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -2.4375,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]],  
              WeaponRightHand = 6755630,  
              Level = 0,  
              Name = [[Demus]],  
              Position = {
                y = -1188.40625,  
                x = 27632.65625,  
                InstanceId = [[Client1_46073]],  
                Class = [[Position]],  
                z = 76.125
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_46076]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 8,  
              GabaritHeight = 7,  
              HairColor = 0,  
              EyesColor = 6,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 4,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46074]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 5,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = 0.875,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]],  
              WeaponRightHand = 6756398,  
              Level = 0,  
              Name = [[Xallo]],  
              Position = {
                y = -1227.03125,  
                x = 27604.21875,  
                InstanceId = [[Client1_46077]],  
                Class = [[Position]],  
                z = 75.359375
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 22
            },  
            {
              InstanceId = [[Client1_46276]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46274]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              Position = {
                y = -1183.46875,  
                x = 27636.51563,  
                InstanceId = [[Client1_46277]],  
                Class = [[Position]],  
                z = 77.421875
              },  
              Angle = -2.25,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_42511]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_42567]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_42565]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 1]],  
          Position = {
            y = -1207.671875,  
            x = 27610.32813,  
            InstanceId = [[Client1_42566]],  
            Class = [[Position]],  
            z = 73.921875
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 3,  
              InstanceId = [[Client1_42568]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42569]],  
                  Who = r2.RefId([[Client1_42555]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42588]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_42570]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42571]],  
                  Who = r2.RefId([[Client1_42559]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42575]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_42573]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42574]],  
                  Who = r2.RefId([[Client1_42563]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42576]]
                }
              },  
              Name = [[]]
            }
          },  
          Repeating = 1
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_42817]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_42815]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 2]],  
          Position = {
            y = -1206.59375,  
            x = 27608.625,  
            InstanceId = [[Client1_42816]],  
            Class = [[Position]],  
            z = 73
          },  
          Repeating = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 10,  
              InstanceId = [[Client1_42818]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42819]],  
                  Who = r2.RefId([[Client1_42591]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42820]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_42821]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42822]],  
                  Who = r2.RefId([[Client1_45964]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42825]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_42823]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42824]],  
                  Who = r2.RefId([[]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_42826]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42827]],  
                  Who = r2.RefId([[Client1_45998]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_46302]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 1
        },  
        {
          InstanceId = [[Client1_42834]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_42865]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_42867]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_42868]],  
                    Entity = r2.RefId([[Client1_42853]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_42866]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_42835]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone trigger 1]],  
          Position = {
            y = -1203.921875,  
            x = 27711.6875,  
            InstanceId = [[Client1_42836]],  
            Class = [[Position]],  
            z = 77.03125
          },  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Places 1]],  
              InstanceId = [[Client1_42838]],  
              Deletable = 0,  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_42840]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -0.9375,  
                    x = 4.65625,  
                    InstanceId = [[Client1_42841]],  
                    Class = [[Position]],  
                    z = 0.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_42843]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 7.75,  
                    x = 3.65625,  
                    InstanceId = [[Client1_42844]],  
                    Class = [[Position]],  
                    z = -3.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_42846]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 1.203125,  
                    x = -6.609375,  
                    InstanceId = [[Client1_42847]],  
                    Class = [[Position]],  
                    z = -0.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_42849]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2.1875,  
                    x = -4.328125,  
                    InstanceId = [[Client1_42850]],  
                    Class = [[Position]],  
                    z = -0.09375
                  }
                }
              },  
              Position = {
                y = 0.390625,  
                x = -0.359375,  
                InstanceId = [[Client1_42837]],  
                Class = [[Position]],  
                z = -0.046875
              }
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_42838]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_42853]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_42851]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 3]],  
          Position = {
            y = -1207.28125,  
            x = 27709.57813,  
            InstanceId = [[Client1_42852]],  
            Class = [[Position]],  
            z = 77.328125
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_42854]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Quiet]],  
                  InstanceId = [[Client1_42855]],  
                  Who = r2.RefId([[Client1_42861]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_46974]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_43005]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_43003]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 4]],  
          Position = {
            y = -1097.609375,  
            x = 27862.6875,  
            InstanceId = [[Client1_43004]],  
            Class = [[Position]],  
            z = 74.890625
          },  
          Repeating = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_43006]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Imploring]],  
                  InstanceId = [[Client1_43007]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_46975]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_43009]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Hysterical]],  
                  InstanceId = [[Client1_43010]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_46976]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_46977]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Enraged]],  
                  InstanceId = [[Client1_46978]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_46979]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 1
        },  
        {
          InstanceId = [[Client1_43144]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_43145]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          _Zone = [[Client1_43148]],  
          Name = [[Zone trigger 2]],  
          Position = {
            y = -1106.390625,  
            x = 27857.60938,  
            InstanceId = [[Client1_43146]],  
            Class = [[Position]],  
            z = 73
          },  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          Active = 1,  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Places 2]],  
              InstanceId = [[Client1_43148]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_43147]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_43150]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 3.46875,  
                    x = 3.8125,  
                    InstanceId = [[Client1_43151]],  
                    Class = [[Position]],  
                    z = 0.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_43153]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 8.4375,  
                    x = -1.734375,  
                    InstanceId = [[Client1_43154]],  
                    Class = [[Position]],  
                    z = 0.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_43156]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -6.875,  
                    x = -11.34375,  
                    InstanceId = [[Client1_43157]],  
                    Class = [[Position]],  
                    z = 2.125
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_43159]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -11,  
                    x = -1.921875,  
                    InstanceId = [[Client1_43160]],  
                    Class = [[Position]],  
                    z = -0.171875
                  }
                }
              },  
              Deletable = 0,  
              Class = [[Region]]
            }
          },  
          Class = [[ZoneTrigger]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_43233]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_43293]],  
                Actions = {
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_43294]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45158]],  
                Actions = {
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45159]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45240]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45242]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45243]],  
                    Entity = r2.RefId([[Client1_42977]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45241]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_43231]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 5]],  
          Position = {
            y = -1098.125,  
            x = 27864.14063,  
            InstanceId = [[Client1_43232]],  
            Class = [[Position]],  
            z = 74.875
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_43234]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Ecstatic]],  
                  InstanceId = [[Client1_43235]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_43236]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 8,  
              InstanceId = [[Client1_43237]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Panick]],  
                  InstanceId = [[Client1_43238]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_43239]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 10,  
              InstanceId = [[Client1_43240]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Pointfront]],  
                  InstanceId = [[Client1_43241]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_43242]]
                }
              },  
              Name = [[]]
            }
          },  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_44975]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0.40625,  
            x = -8.484375,  
            InstanceId = [[Client1_44974]],  
            Class = [[Position]],  
            z = -1.28125
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_44967]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 13,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 5,  
              GabaritHeight = 1,  
              HairColor = 1,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44965]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45169]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45170]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45171]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45172]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 5606702,  
              Speed = 1,  
              Angle = -1.6875,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 5606446,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 6,  
              Sheet = [[ring_melee_damage_dealer_pierce_b4.creature]],  
              WeaponRightHand = 5635886,  
              Level = 3,  
              Name = [[Meps]],  
              Position = {
                y = -1006.140625,  
                x = 27551.20313,  
                InstanceId = [[Client1_44968]],  
                Class = [[Position]],  
                z = 75.40625
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 0,  
              MorphTarget3 = 2,  
              Tattoo = 15
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_44978]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 11,  
              GabaritHeight = 0,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44976]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -0.890625,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 5606446,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b4.creature]],  
              WeaponRightHand = 6933806,  
              Level = 3,  
              Name = [[Pirus]],  
              Position = {
                y = -1005.671875,  
                x = 27544.79688,  
                InstanceId = [[Client1_44980]],  
                Class = [[Position]],  
                z = 74.015625
              },  
              ArmModel = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 6,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45728]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 4,  
              HandsModel = 6703150,  
              FeetColor = 4,  
              GabaritBreastSize = 13,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 6697774,  
              GabaritLegsWidth = 4,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45726]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -0.609375,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 4,  
              Sheet = [[ring_magic_aoe_acid_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Ulyton]],  
              Position = {
                y = -1011.265625,  
                x = 27555.5625,  
                InstanceId = [[Client1_45730]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_45733]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 1,  
              HandsModel = 6702894,  
              FeetColor = 2,  
              GabaritBreastSize = 13,  
              GabaritHeight = 1,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 14,  
              HandsColor = 1,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45731]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = 1.8125,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 6,  
              Sheet = [[ring_magic_damage_dealer_acid_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Apomus]],  
              Position = {
                y = -1015.0625,  
                x = 27555.3125,  
                InstanceId = [[Client1_45735]],  
                Class = [[Position]],  
                z = 75.46875
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 2,  
              MorphTarget3 = 0,  
              Tattoo = 22
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45738]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 0,  
              HandsModel = 6702894,  
              FeetColor = 2,  
              GabaritBreastSize = 10,  
              GabaritHeight = 1,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 6698030,  
              GabaritLegsWidth = 12,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45736]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 0,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -2.859375,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 1,  
              Sheet = [[ring_magic_curser_fear_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Ibicaon]],  
              Position = {
                y = -1006.234375,  
                x = 27556.71875,  
                InstanceId = [[Client1_45740]],  
                Class = [[Position]],  
                z = 75.625
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 4,  
              MorphTarget3 = 0,  
              Tattoo = 6
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_45743]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 5,  
              HandsModel = 6705454,  
              FeetColor = 0,  
              GabaritBreastSize = 1,  
              GabaritHeight = 1,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45741]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = 0.265625,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 6753070,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_pierce_e2.creature]],  
              WeaponRightHand = 6753070,  
              Level = 0,  
              Name = [[Aekos]],  
              Position = {
                y = -1023.21875,  
                x = 27542.51563,  
                InstanceId = [[Client1_45745]],  
                Class = [[Position]],  
                z = 76.265625
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 7,  
              MorphTarget3 = 3,  
              Tattoo = 1
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45748]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 9,  
              HandsModel = 6703150,  
              FeetColor = 5,  
              GabaritBreastSize = 0,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6697774,  
              GabaritLegsWidth = 9,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45746]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -0.609375,  
              Base = [[palette.entities.npcs.bandits.f_mage_atysian_curser_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              Sheet = [[ring_magic_curser_blind_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Eullo]],  
              Position = {
                y = -1007.78125,  
                x = 27550.1875,  
                InstanceId = [[Client1_45750]],  
                Class = [[Position]],  
                z = 74.578125
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 23
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_45753]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 12,  
              HandsModel = 6699822,  
              FeetColor = 5,  
              GabaritBreastSize = 4,  
              GabaritHeight = 8,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 14,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45751]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 0,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = -0.609375,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 6773294,  
              JacketModel = 6702126,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              Sheet = [[ring_melee_tank_blunt_e2.creature]],  
              WeaponRightHand = 6752302,  
              Level = 0,  
              Name = [[Apollo]],  
              Position = {
                y = -1002.375,  
                x = 27549.89063,  
                InstanceId = [[Client1_45755]],  
                Class = [[Position]],  
                z = 75.359375
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_45758]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 11,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 0,  
              GabaritHeight = 8,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 14,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45756]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 7,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = -3.84375,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 7,  
              Sheet = [[ring_magic_damage_dealer_acid_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Tin]],  
              Position = {
                y = -1011.671875,  
                x = 27558.0625,  
                InstanceId = [[Client1_45760]],  
                Class = [[Position]],  
                z = 75.34375
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 2,  
              MorphTarget3 = 4,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_46080]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 12,  
              HandsModel = 6702894,  
              FeetColor = 0,  
              GabaritBreastSize = 4,  
              GabaritHeight = 8,  
              HairColor = 2,  
              EyesColor = 6,  
              TrouserModel = 6697774,  
              GabaritLegsWidth = 13,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46078]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 4,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -1.828125,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_120]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 2,  
              Sheet = [[ring_magic_aoe_acid_d2.creature]],  
              WeaponRightHand = 6934062,  
              Level = 0,  
              Name = [[Deion]],  
              Position = {
                y = -1003.0625,  
                x = 27550.59375,  
                InstanceId = [[Client1_46082]],  
                Class = [[Position]],  
                z = 75.359375
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 2,  
              MorphTarget3 = 6,  
              Tattoo = 30
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_46085]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 1,  
              HandsModel = 6702894,  
              FeetColor = 4,  
              GabaritBreastSize = 12,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 6,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46083]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 5,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -1.828125,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_120]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 6,  
              Sheet = [[ring_magic_aoe_acid_d2.creature]],  
              WeaponRightHand = 6934062,  
              Level = 0,  
              Name = [[Zenix]],  
              Position = {
                y = -1022.34375,  
                x = 27543.71875,  
                InstanceId = [[Client1_46087]],  
                Class = [[Position]],  
                z = 76.21875
              },  
              ArmModel = 5606190,  
              MorphTarget7 = 2,  
              MorphTarget3 = 6,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_46124]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 14,  
              HandsModel = 6703150,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 4,  
              TrouserModel = 6697774,  
              GabaritLegsWidth = 4,  
              HandsColor = 2,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46122]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -2.0625,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              Sheet = [[ring_magic_curser_fear_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Meps]],  
              Position = {
                y = -1003.109375,  
                x = 27552.39063,  
                InstanceId = [[Client1_46126]],  
                Class = [[Position]],  
                z = 75.59375
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_46134]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 5,  
              HandsModel = 6699822,  
              FeetColor = 1,  
              GabaritBreastSize = 6,  
              GabaritHeight = 2,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 13,  
              HandsColor = 5,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46132]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 6699310,  
              Speed = 0,  
              Angle = -2.0625,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 6773294,  
              JacketModel = 6701870,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_melee_tank_slash_e2.creature]],  
              WeaponRightHand = 6754862,  
              Level = 0,  
              Name = [[Ioros]],  
              Position = {
                y = -1008.15625,  
                x = 27551.39063,  
                InstanceId = [[Client1_46136]],  
                Class = [[Position]],  
                z = 74.625
              },  
              ArmModel = 6701614,  
              MorphTarget7 = 5,  
              MorphTarget3 = 2,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_46758]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 3,  
              HandsModel = 6705454,  
              FeetColor = 4,  
              GabaritBreastSize = 9,  
              GabaritHeight = 9,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 10,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46756]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6704942,  
              Speed = 0,  
              Angle = -1.828125,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 7,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              WeaponRightHand = 6756654,  
              Level = 0,  
              Name = [[Apotis]],  
              Position = {
                y = -1007.25,  
                x = 27548.70313,  
                InstanceId = [[Client1_46760]],  
                Class = [[Position]],  
                z = 74.34375
              },  
              ArmModel = 6706734,  
              MorphTarget7 = 6,  
              MorphTarget3 = 0,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_46763]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 5,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 13,  
              GabaritHeight = 7,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 10,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46761]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -1.828125,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              Sheet = [[ring_magic_damage_dealer_acid_f2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Boean]],  
              Position = {
                y = -1009.375,  
                x = 27550.23438,  
                InstanceId = [[Client1_46765]],  
                Class = [[Position]],  
                z = 74.546875
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 1,  
              MorphTarget3 = 6,  
              Tattoo = 11
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_44973]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_45029]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = -0.703125,  
            x = -0.09375,  
            InstanceId = [[Client1_45028]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_45021]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 8,  
              GabaritHeight = 0,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 8,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45019]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45183]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45184]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45185]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45186]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 6,  
              FeetModel = 5605422,  
              Speed = 1,  
              Angle = -2.9375,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 6,  
              Sheet = [[ring_magic_aoe_cold_b4.creature]],  
              WeaponRightHand = 6933806,  
              Level = 3,  
              Name = [[Aecaon]],  
              Position = {
                y = -1023.09375,  
                x = 27543,  
                InstanceId = [[Client1_45022]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_45047]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 2,  
              HandsModel = 5605678,  
              FeetColor = 5,  
              GabaritBreastSize = 11,  
              GabaritHeight = 9,  
              HairColor = 3,  
              EyesColor = 0,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45045]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -5.671875,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 5594158,  
              JacketModel = 0,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 3,  
              Sheet = [[ring_light_melee_pierce_b2.creature]],  
              WeaponRightHand = 5594158,  
              Level = 1,  
              Name = [[Xyllo]],  
              Position = {
                y = -1027.8125,  
                x = 27536.82813,  
                InstanceId = [[Client1_45049]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 2,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45768]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6705454,  
              FeetColor = 1,  
              GabaritBreastSize = 9,  
              GabaritHeight = 9,  
              HairColor = 5,  
              EyesColor = 6,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 5,  
              HandsColor = 3,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45766]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 6704942,  
              Speed = 0,  
              Angle = 1,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_blunt_e2.creature]],  
              WeaponRightHand = 6752046,  
              Level = 0,  
              Name = [[Lyan]],  
              Position = {
                y = -1019.921875,  
                x = 27547.23438,  
                InstanceId = [[Client1_45770]],  
                Class = [[Position]],  
                z = 74.859375
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_45783]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 8,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 13,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 3,  
              HandsColor = 1,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45781]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 0,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = -0.21875,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 2,  
              Sheet = [[ring_magic_damage_dealer_acid_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Gamus]],  
              Position = {
                y = -1022.859375,  
                x = 27536.70313,  
                InstanceId = [[Client1_45785]],  
                Class = [[Position]],  
                z = 75
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_45788]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 9,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 9,  
              GabaritHeight = 12,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6697774,  
              GabaritLegsWidth = 2,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45786]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 5,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = 1,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_magic_aoe_acid_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Iorus]],  
              Position = {
                y = -1026.421875,  
                x = 27542.10938,  
                InstanceId = [[Client1_45790]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 6,  
              MorphTarget3 = 7,  
              Tattoo = 2
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_45793]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 12,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 10,  
              GabaritHeight = 8,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 11,  
              HandsColor = 2,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45791]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 1,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -0.953125,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              Sheet = [[ring_magic_damage_dealer_acid_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Mello]],  
              Position = {
                y = -1020.3125,  
                x = 27544.46875,  
                InstanceId = [[Client1_45795]],  
                Class = [[Position]],  
                z = 74.875
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 4,  
              MorphTarget3 = 1,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_46099]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 1,  
              HandsModel = 6705454,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 2,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 8,  
              HandsColor = 5,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46097]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 6,  
              FeetModel = 6705198,  
              Speed = 0,  
              Angle = -2.703125,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 6,  
              Sheet = [[ring_melee_damage_dealer_slash_e2.creature]],  
              WeaponRightHand = 6756142,  
              Level = 0,  
              Name = [[Apolion]],  
              Position = {
                y = -1024.1875,  
                x = 27545.09375,  
                InstanceId = [[Client1_46101]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 7,  
              MorphTarget3 = 1,  
              Tattoo = 1
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_46104]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 9,  
              HandsModel = 6705454,  
              FeetColor = 0,  
              GabaritBreastSize = 2,  
              GabaritHeight = 5,  
              HairColor = 3,  
              EyesColor = 4,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 10,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46102]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = -2.703125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              Sheet = [[ring_light_melee_blunt_e2.creature]],  
              WeaponRightHand = 6752302,  
              Level = 0,  
              Name = [[Boean]],  
              Position = {
                y = -1026.953125,  
                x = 27538.76563,  
                InstanceId = [[Client1_46106]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 4,  
              MorphTarget3 = 2,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_46109]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 12,  
              HandsModel = 6705710,  
              FeetColor = 1,  
              GabaritBreastSize = 7,  
              GabaritHeight = 1,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 6,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46107]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 5,  
              FeetModel = 6704942,  
              Speed = 0,  
              Angle = -2.703125,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 0,  
              Sheet = [[ring_melee_damage_dealer_blunt_e2.creature]],  
              WeaponRightHand = 6755118,  
              Level = 0,  
              Name = [[Pecus]],  
              Position = {
                y = -1026.625,  
                x = 27536.40625,  
                InstanceId = [[Client1_46111]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 3,  
              MorphTarget3 = 0,  
              Tattoo = 15
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_46114]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 0,  
              HandsModel = 6702894,  
              FeetColor = 1,  
              GabaritBreastSize = 13,  
              GabaritHeight = 8,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6698030,  
              GabaritLegsWidth = 8,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46112]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = -0.40625,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 4,  
              Sheet = [[ring_magic_aoe_acid_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Eulion]],  
              Position = {
                y = -1023.71875,  
                x = 27539.78125,  
                InstanceId = [[Client1_46116]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 1,  
              MorphTarget3 = 5,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_46768]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 2,  
              HandsModel = 6705454,  
              FeetColor = 1,  
              GabaritBreastSize = 13,  
              GabaritHeight = 2,  
              HairColor = 0,  
              EyesColor = 0,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46766]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 2,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = -3.078125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_slash_f2.creature]],  
              WeaponRightHand = 6754350,  
              Level = 0,  
              Name = [[Apotheus]],  
              Position = {
                y = -1022.140625,  
                x = 27539.96875,  
                InstanceId = [[Client1_46770]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 2,  
              MorphTarget3 = 2,  
              Tattoo = 18
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45027]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_45080]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 3]],  
          Position = {
            y = -6.109375,  
            x = 10.25,  
            InstanceId = [[Client1_45079]],  
            Class = [[Position]],  
            z = 1.171875
          },  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45072]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 9,  
              HandsModel = 5606958,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 13,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 0,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45070]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45187]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45188]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45191]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45192]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 1,  
              FeetModel = 5606702,  
              Speed = 1,  
              Angle = -2.3125,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 5607726,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 4,  
              Sheet = [[ring_melee_damage_dealer_pierce_b4.creature]],  
              WeaponRightHand = 5635886,  
              Level = 3,  
              Name = [[Merius]],  
              Position = {
                y = -1012.28125,  
                x = 27519.42188,  
                InstanceId = [[Client1_45073]],  
                Class = [[Position]],  
                z = 73.234375
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 5,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45093]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 5,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 6,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 12,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45091]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = 0.390625,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              Sheet = [[ring_magic_aoe_cold_b4.creature]],  
              WeaponRightHand = 6933806,  
              Level = 3,  
              Name = [[Gan]],  
              Position = {
                y = -1012.9375,  
                x = 27518.45313,  
                InstanceId = [[Client1_45095]],  
                Class = [[Position]],  
                z = 73.34375
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 6,  
              Tattoo = 30
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45813]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 4,  
              HandsModel = 6705710,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 9,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 0,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45811]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = 9.828125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 0,  
              Sheet = [[ring_light_melee_slash_e2.creature]],  
              WeaponRightHand = 6754606,  
              Level = 0,  
              Name = [[Lydix]],  
              Position = {
                y = -1009.109375,  
                x = 27521.32813,  
                InstanceId = [[Client1_45815]],  
                Class = [[Position]],  
                z = 72.609375
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 1,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_45818]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 12,  
              HandsModel = 6700078,  
              FeetColor = 5,  
              GabaritBreastSize = 14,  
              GabaritHeight = 9,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 1,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45816]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 6699566,  
              Speed = 0,  
              Angle = 0.34375,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 6773038,  
              JacketModel = 6702126,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 6,  
              Sheet = [[ring_melee_tank_blunt_e2.creature]],  
              WeaponRightHand = 6752046,  
              Level = 0,  
              Name = [[Apotis]],  
              Position = {
                y = -1012.6875,  
                x = 27514.10938,  
                InstanceId = [[Client1_45820]],  
                Class = [[Position]],  
                z = 73.046875
              },  
              ArmModel = 6701358,  
              MorphTarget7 = 2,  
              MorphTarget3 = 0,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45828]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 6,  
              HandsModel = 6705454,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 3,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 7,  
              HandsColor = 1,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45826]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 1,  
              FeetModel = 6705198,  
              Speed = 0,  
              Angle = 1.21875,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              Sheet = [[ring_light_melee_slash_e2.creature]],  
              WeaponRightHand = 6754350,  
              Level = 0,  
              Name = [[Xylaus]],  
              Position = {
                y = -1015.296875,  
                x = 27519.0625,  
                InstanceId = [[Client1_45830]],  
                Class = [[Position]],  
                z = 73.84375
              },  
              ArmModel = 6703918,  
              MorphTarget7 = 2,  
              MorphTarget3 = 1,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45833]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 12,  
              HandsModel = 6705454,  
              FeetColor = 2,  
              GabaritBreastSize = 12,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 1,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45831]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6704942,  
              Speed = 0,  
              Angle = 0.890625,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 7,  
              Sheet = [[ring_melee_damage_dealer_slash_e2.creature]],  
              WeaponRightHand = 6756654,  
              Level = 0,  
              Name = [[Eulus]],  
              Position = {
                y = -1008.828125,  
                x = 27518.60938,  
                InstanceId = [[Client1_45835]],  
                Class = [[Position]],  
                z = 72.5
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 5,  
              Tattoo = 26
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45838]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6702894,  
              FeetColor = 2,  
              GabaritBreastSize = 5,  
              GabaritHeight = 13,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 10,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45836]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 2,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = 0.890625,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_170]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              Sheet = [[ring_magic_damage_dealer_acid_e2.creature]],  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Dyrius]],  
              Position = {
                y = -1014.109375,  
                x = 27516.17188,  
                InstanceId = [[Client1_45840]],  
                Class = [[Position]],  
                z = 73.5
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 7,  
              MorphTarget3 = 2,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_46753]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 6,  
              HandsModel = 6705710,  
              FeetColor = 4,  
              GabaritBreastSize = 1,  
              GabaritHeight = 13,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 14,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46751]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6704942,  
              Speed = 0,  
              Angle = -1.25,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              JacketModel = 6701870,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 4,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              WeaponRightHand = 6756654,  
              Level = 0,  
              Name = [[Deun]],  
              Position = {
                y = -1011.328125,  
                x = 27522.95313,  
                InstanceId = [[Client1_46755]],  
                Class = [[Position]],  
                z = 73.203125
              },  
              ArmModel = 6706478,  
              MorphTarget7 = 0,  
              MorphTarget3 = 5,  
              Tattoo = 6
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45078]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45162]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_45160]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 6]],  
          Position = {
            y = -1021.03125,  
            x = 27524.71875,  
            InstanceId = [[Client1_45161]],  
            Class = [[Position]],  
            z = 74.828125
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 0,  
              InstanceId = [[Client1_45163]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Cruel]],  
                  InstanceId = [[Client1_45164]],  
                  Who = r2.RefId([[Client1_43127]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45165]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45246]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_45244]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 7]],  
          Position = {
            y = -1208,  
            x = 27612.60938,  
            InstanceId = [[Client1_45245]],  
            Class = [[Position]],  
            z = 73
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_47169]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_47170]],  
                  Who = r2.RefId([[Client1_42555]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42977]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 0,  
              InstanceId = [[Client1_47171]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_47172]],  
                  Who = r2.RefId([[Client1_42563]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42977]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 2,  
              InstanceId = [[Client1_45247]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Cheer]],  
                  InstanceId = [[Client1_45248]],  
                  Who = r2.RefId([[Client1_42559]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42977]]),  
                  Says = [[Client1_45249]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 4,  
              InstanceId = [[Client1_45250]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45251]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42559]]),  
                  Says = [[Client1_45252]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 6,  
              InstanceId = [[Client1_45253]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45254]],  
                  Who = r2.RefId([[Client1_42563]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42977]]),  
                  Says = [[Client1_46646]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 6,  
              InstanceId = [[Client1_45255]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Panick]],  
                  InstanceId = [[Client1_45256]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42563]]),  
                  Says = [[Client1_46647]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 9,  
              InstanceId = [[Client1_45259]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Dance]],  
                  InstanceId = [[Client1_45260]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42563]]),  
                  Says = [[Client1_46648]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45342]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45436]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45438]],  
                      Value = r2.RefId([[Client1_45433]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45439]],  
                    Entity = r2.RefId([[Client1_42563]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45448]],  
                      Value = r2.RefId([[Client1_45446]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45449]],  
                    Entity = r2.RefId([[Client1_42559]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45450]],  
                      Value = r2.RefId([[Client1_45442]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45451]],  
                    Entity = r2.RefId([[Client1_42555]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45454]],  
                      Value = r2.RefId([[Client1_45452]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45455]],  
                    Entity = r2.RefId([[Client1_42977]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45437]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_45340]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 8]],  
          Position = {
            y = -1204.859375,  
            x = 27602.5,  
            InstanceId = [[Client1_45341]],  
            Class = [[Position]],  
            z = 75
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45343]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45344]],  
                  Who = r2.RefId([[Client1_46004]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42977]]),  
                  Says = [[Client1_46017]]
                }
              },  
              Name = [[]]
            }
          },  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_45389]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 4]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_45388]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              InstanceId = [[Client1_45377]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45375]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45484]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45559]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45560]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45561]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Guard Zone]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Assault Kirosta]],  
              Position = {
                y = -1343.640625,  
                x = 27526.82813,  
                InstanceId = [[Client1_45378]],  
                Class = [[Position]],  
                z = 75.15625
              },  
              Angle = 0.8926824927,  
              Base = [[palette.entities.creatures.ckfrb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46956]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46954]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Reaper Kizoar]],  
              Position = {
                y = -1331.546875,  
                x = 27523.57813,  
                InstanceId = [[Client1_46958]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              Angle = 0.9686824083,  
              Base = [[palette.entities.creatures.ckiie1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46961]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46959]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Reaper Kipucker]],  
              Position = {
                y = -1334.171875,  
                x = 27534.4375,  
                InstanceId = [[Client1_46963]],  
                Class = [[Position]],  
                z = 76.859375
              },  
              Angle = 2.005727053,  
              Base = [[palette.entities.creatures.ckeie1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46966]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46964]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Reaper Kipee]],  
              Position = {
                y = -1343.875,  
                x = 27530.9375,  
                InstanceId = [[Client1_46968]],  
                Class = [[Position]],  
                z = 75.609375
              },  
              Angle = 2.005727053,  
              Base = [[palette.entities.creatures.ckhie1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47011]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47009]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipee]],  
              Position = {
                y = -1354.125,  
                x = 27527.5625,  
                InstanceId = [[Client1_47012]],  
                Class = [[Position]],  
                z = 75.203125
              },  
              Angle = -1.905639052,  
              Base = [[palette.entities.creatures.ckhif4]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45387]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45458]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45496]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_45498]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45499]],  
                    Entity = r2.RefId([[Client1_45488]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45497]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_45456]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 9]],  
          Position = {
            y = -1321.28125,  
            x = 27544.21875,  
            InstanceId = [[Client1_45457]],  
            Class = [[Position]],  
            z = 81
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45459]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Surprised]],  
                  InstanceId = [[Client1_45460]],  
                  Who = r2.RefId([[Client1_42555]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45463]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 2,  
              InstanceId = [[Client1_45461]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Scared]],  
                  InstanceId = [[Client1_45462]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45466]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45488]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45480]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_45482]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45483]],  
                    Entity = r2.RefId([[Client1_45389]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_45481]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45532]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45534]],  
                      Value = r2.RefId([[Client1_45529]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45535]],  
                    Entity = r2.RefId([[Client1_42977]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45536]],  
                      Value = r2.RefId([[Client1_45525]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45537]],  
                    Entity = r2.RefId([[Client1_42559]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45538]],  
                      Value = r2.RefId([[Client1_45527]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45539]],  
                    Entity = r2.RefId([[Client1_42555]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45540]],  
                      Value = r2.RefId([[Client1_45523]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45541]],  
                    Entity = r2.RefId([[Client1_42563]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45533]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45557]],  
                Actions = {
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45558]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45563]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45565]],  
                      Value = r2.RefId([[Client1_45560]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45566]],  
                    Entity = r2.RefId([[Client1_45389]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45564]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45650]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_45652]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45653]],  
                    Entity = r2.RefId([[Client1_45577]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45657]],  
                      Value = r2.RefId([[Client1_45647]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45658]],  
                    Entity = r2.RefId([[Client1_45577]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45651]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_45486]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 10]],  
          Position = {
            y = -1321.46875,  
            x = 27545.48438,  
            InstanceId = [[Client1_45487]],  
            Class = [[Position]],  
            z = 81
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45489]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45490]],  
                  Who = r2.RefId([[Client1_42559]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45493]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_45491]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45492]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45494]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45544]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45681]],  
                Actions = {
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45682]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45716]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45718]],  
                      Value = r2.RefId([[Client1_45713]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45719]],  
                    Entity = r2.RefId([[Client1_42977]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45720]],  
                      Value = r2.RefId([[Client1_45711]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45721]],  
                    Entity = r2.RefId([[Client1_42559]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45722]],  
                      Value = r2.RefId([[Client1_45709]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45723]],  
                    Entity = r2.RefId([[Client1_42563]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45724]],  
                      Value = r2.RefId([[Client1_45707]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45725]],  
                    Entity = r2.RefId([[Client1_42555]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45717]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_45542]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 11]],  
          Position = {
            y = -1208.109375,  
            x = 27621.10938,  
            InstanceId = [[Client1_45543]],  
            Class = [[Position]],  
            z = 73
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45545]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Imploring]],  
                  InstanceId = [[Client1_45546]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45547]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_45548]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45549]],  
                  Who = r2.RefId([[]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          InstanceId = [[Client1_45577]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 5]],  
          Position = {
            y = 1.6875,  
            x = 0.890625,  
            InstanceId = [[Client1_45576]],  
            Class = [[Position]],  
            z = -0.640625
          },  
          Components = {
            {
              InstanceId = [[Client1_45618]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45616]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45645]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45646]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45647]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45648]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Guard Zone]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Reaper Kincher]],  
              Position = {
                y = -1372.953125,  
                x = 27501.35938,  
                InstanceId = [[Client1_45619]],  
                Class = [[Position]],  
                z = 74.578125
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckdie2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45896]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45894]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Reaper Kizarak]],  
              Position = {
                y = -1398.703125,  
                x = 27450.78125,  
                InstanceId = [[Client1_45898]],  
                Class = [[Position]],  
                z = 80.359375
              },  
              Angle = 0.8493987918,  
              Base = [[palette.entities.creatures.ckcie4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45911]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45909]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Reaper Kipesta]],  
              Position = {
                y = -1398.484375,  
                x = 27495.01563,  
                InstanceId = [[Client1_45913]],  
                Class = [[Position]],  
                z = 75.890625
              },  
              Angle = 0.8493987918,  
              Base = [[palette.entities.creatures.ckjie4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45930]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45928]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Reaper Kirosta]],  
              Position = {
                y = -1372.84375,  
                x = 27508.76563,  
                InstanceId = [[Client1_45932]],  
                Class = [[Position]],  
                z = 75.1875
              },  
              Angle = 0.917637825,  
              Base = [[palette.entities.creatures.ckfie1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45953]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45951]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Reaper Kizoar]],  
              Position = {
                y = -1365.859375,  
                x = 27512,  
                InstanceId = [[Client1_45955]],  
                Class = [[Position]],  
                z = 75.5625
              },  
              Angle = 0.7751168013,  
              Base = [[palette.entities.creatures.ckiie1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46937]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46935]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Reaper Kidinak]],  
              Position = {
                y = -1363.625,  
                x = 27505.5625,  
                InstanceId = [[Client1_46938]],  
                Class = [[Position]],  
                z = 75.28125
              },  
              Angle = 1.334124207,  
              Base = [[palette.entities.creatures.ckaie2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46941]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46939]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Reaper Kirosta]],  
              Position = {
                y = -1373.953125,  
                x = 27485.17188,  
                InstanceId = [[Client1_46943]],  
                Class = [[Position]],  
                z = 73.78125
              },  
              Angle = 6.193125248,  
              Base = [[palette.entities.creatures.ckfie1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46946]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46944]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Reaper Kinrey]],  
              Position = {
                y = -1382.875,  
                x = 27491.32813,  
                InstanceId = [[Client1_46948]],  
                Class = [[Position]],  
                z = 73.953125
              },  
              Angle = -5.229468822,  
              Base = [[palette.entities.creatures.ckbie1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46951]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46949]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Reaper Kiban]],  
              Position = {
                y = -1376.046875,  
                x = 27491.20313,  
                InstanceId = [[Client1_46953]],  
                Class = [[Position]],  
                z = 73.765625
              },  
              Angle = -0.02838751674,  
              Base = [[palette.entities.creatures.ckgie1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46999]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46997]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kirosta]],  
              Position = {
                y = -1364.71875,  
                x = 27493.53125,  
                InstanceId = [[Client1_47000]],  
                Class = [[Position]],  
                z = 74.53125
              },  
              Angle = -0.3800510764,  
              Base = [[palette.entities.creatures.ckfif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47003]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47001]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Overlord Kidinak]],  
              Position = {
                y = -1376.859375,  
                x = 27507.28125,  
                InstanceId = [[Client1_47004]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              Angle = 0.4914812446,  
              Base = [[palette.entities.creatures.ckaif3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_47007]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_47005]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Overlord Kidinak]],  
              Position = {
                y = -1358.5625,  
                x = 27494.03125,  
                InstanceId = [[Client1_47008]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = -0.3976939619,  
              Base = [[palette.entities.creatures.ckaif4]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45575]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_46872]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 6]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_46871]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              InstanceId = [[Client1_46864]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46862]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_46916]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_46917]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_46889]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Guard Zone]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kipucker]],  
              Position = {
                y = -1174.21875,  
                x = 27836.98438,  
                InstanceId = [[Client1_46865]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = 1.076104045,  
              Base = [[palette.entities.creatures.ckeib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46880]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46878]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Warrior Kidinak]],  
              Position = {
                y = -1183.546875,  
                x = 27838.39063,  
                InstanceId = [[Client1_46882]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              Angle = 1.076104045,  
              Base = [[palette.entities.creatures.ckaid2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46885]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_46883]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Warrior Kipesta]],  
              Position = {
                y = -1177.671875,  
                x = 27826.34375,  
                InstanceId = [[Client1_46887]],  
                Class = [[Position]],  
                z = 74.5625
              },  
              Angle = 1.076104045,  
              Base = [[palette.entities.creatures.ckjid3]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_46870]],  
            Activities = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_46985]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45179]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45181]],  
                      Value = r2.RefId([[Client1_45171]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45182]],  
                    Entity = r2.RefId([[Client1_44975]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45189]],  
                      Value = r2.RefId([[Client1_45185]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45190]],  
                    Entity = r2.RefId([[Client1_45029]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45193]],  
                      Value = r2.RefId([[Client1_45191]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45194]],  
                    Entity = r2.RefId([[Client1_45080]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_46990]],  
                      Value = r2.RefId([[Client1_46988]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_46991]],  
                    Entity = r2.RefId([[Client1_43127]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_47189]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_47190]],  
                    Entity = r2.RefId([[Client1_45162]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_45180]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_46986]]
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[Bandits attack]],  
          Position = {
            y = -1017.5625,  
            x = 27546.125,  
            InstanceId = [[Client1_46987]],  
            Class = [[Position]],  
            z = 75
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.user_event]]
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_46994]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45475]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_45477]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45478]],  
                    Entity = r2.RefId([[Client1_45458]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_45476]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_46995]]
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[Kittins Attack]],  
          Position = {
            y = -1345.25,  
            x = 27548.9375,  
            InstanceId = [[Client1_46996]],  
            Class = [[Position]],  
            z = 78.015625
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.user_event]]
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_47173]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_47177]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_47179]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_47180]],  
                    Entity = r2.RefId([[Client1_45246]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_47178]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_47174]]
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[Girls dialogs]],  
          Position = {
            y = -1200.390625,  
            x = 27611.125,  
            InstanceId = [[Client1_47175]],  
            Class = [[Position]],  
            z = 73
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.user_event]]
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_47181]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_47185]],  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_47186]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_47187]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_47188]],  
                    Entity = r2.RefId([[Client1_45342]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_47182]]
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[Girls, escape!!]],  
          Position = {
            y = -1202.944946,  
            x = 27608.97852,  
            z = 73,  
            Class = [[Position]],  
            InstanceId = [[Client1_47183]]
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.user_event]]
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_42509]],  
        Class = [[Position]],  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 1
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_42500]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_42572]],  
        Class = [[TextManagerEntry]],  
        Text = [[Princess Faneliah was kidnapped by bandits two years ago, I worry so much for her!]]
      },  
      {
        Count = 8,  
        InstanceId = [[Client1_42575]],  
        Class = [[TextManagerEntry]],  
        Text = [[Me too, I know they bring her to the northeast of here. ]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_42576]],  
        Class = [[TextManagerEntry]],  
        Text = [[We need a strong warrior to deliver her and defeat the bandits!]]
      },  
      {
        Count = 18,  
        InstanceId = [[Client1_42588]],  
        Class = [[TextManagerEntry]],  
        Text = [[Princess Faneliah was kidnapped by bandits two days ago, I worry so much for her!]]
      },  
      {
        Count = 8,  
        InstanceId = [[Client1_42820]],  
        Class = [[TextManagerEntry]],  
        Text = [[I hope the Princess is safe]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_42825]],  
        Class = [[TextManagerEntry]],  
        Text = [[She was kidnapped when we were escorting Thecaon to the Gingo's valley]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_42828]],  
        Class = [[TextManagerEntry]],  
        Text = [[We need to be more carefull]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_42829]],  
        Class = [[TextManagerEntry]],  
        Text = [[Everybody worry about the Princess]]
      },  
      {
        Count = 8,  
        InstanceId = [[Client1_43008]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please! Help me!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_43011]],  
        Class = [[TextManagerEntry]],  
        Text = [[Kill them! Save me!]]
      },  
      {
        Count = 10,  
        InstanceId = [[Client1_43236]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh! You did it! You're a Hero!]]
      },  
      {
        Count = 13,  
        InstanceId = [[Client1_43239]],  
        Class = [[TextManagerEntry]],  
        Text = [[But when you were fighting those horrible bandits, I saw the last of them running away to the west of here. He went to alert his friends!]]
      },  
      {
        Count = 10,  
        InstanceId = [[Client1_43242]],  
        Class = [[TextManagerEntry]],  
        Text = [[I go back to the village to tell the guards to be carefull and ready to fight. You can escort me or follow the bandit to see what will happen]]
      },  
      {
        Count = 16,  
        InstanceId = [[Client1_45165]],  
        Class = [[TextManagerEntry]],  
        Text = [[Someone mysterious delivered the Princess! Two of us have been killed! Let's take revenge and kill all this silly village!!]]
      },  
      {
        Count = 9,  
        InstanceId = [[Client1_45249]],  
        Class = [[TextManagerEntry]],  
        Text = [[Princess Faneliah you're safe!]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_45252]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yeah, you can thanks our Hero!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45258]],  
        Class = [[TextManagerEntry]],  
        Text = [[No! A lot of them are hiding in the moutains, and one of my kidmapper managed to escape and alert them!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_45345]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bandits are attacking us as you thought Princess!! Go with the girl and hide far from here!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45357]],  
        Class = [[TextManagerEntry]],  
        Text = [[The bandits are attacking us now! Please Princess, go and hide with the]]
      },  
      {
        Count = 8,  
        InstanceId = [[Client1_45463]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh! What is this brown thing?]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_45466]],  
        Class = [[TextManagerEntry]],  
        Text = [[A kittin mound!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_45493]],  
        Class = [[TextManagerEntry]],  
        Text = [[we've woken up the kittins! ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45494]],  
        Class = [[TextManagerEntry]],  
        Text = [[They have seen us! Let's go back to the village it will be safer than here, ESCAPE GIRLS!!!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_45547]],  
        Class = [[TextManagerEntry]],  
        Text = [[We woke up a lot of kittins! They attack the village! Please protect us!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_45550]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh! OK girls, go and hide behind the tents!]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_46017]],  
        Class = [[TextManagerEntry]],  
        Text = [[The bandits are attacking us as you thought Princess! Escape to the south of here with the girls! Hurry!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_46302]],  
        Class = [[TextManagerEntry]],  
        Text = [[I saw a lot of kittins around here]]
      },  
      {
        Count = 7,  
        InstanceId = [[Client1_46646]],  
        Class = [[TextManagerEntry]],  
        Text = [[All the bandits have been killed?]]
      },  
      {
        Count = 7,  
        InstanceId = [[Client1_46647]],  
        Class = [[TextManagerEntry]],  
        Text = [[No! A lot of them are hiding in the moutains, and one of my kidnapper managed to escape and alert them!]]
      },  
      {
        Count = 9,  
        InstanceId = [[Client1_46648]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hopefully the guards will protect us, cause soon or later they will attack our little town!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_46974]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bandits managed to discover a safe path between the kittins, you must find it too]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_46975]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please! Let me go!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_46976]],  
        Class = [[TextManagerEntry]],  
        Text = [[I want to see my friends!]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_46979]],  
        Class = [[TextManagerEntry]],  
        Text = [[I need to come back home! ]]
      }
    }
  }
}