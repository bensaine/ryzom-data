scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts09]],  
      Time = 0,  
      Name = [[Two Ponds (Desert 09)]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    },  
    {
      InstanceId = [[Client1_2001]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts09]],  
      Time = 0,  
      Name = [[Two Pond (Desert 09)]],  
      Season = [[fall]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2NorthEastEntryPoint]]
    },  
    {
      InstanceId = [[Client1_8287]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts09]],  
      Time = 0,  
      Name = [[Two Pons (Desert 09)]],  
      Season = [[winter]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_3]]
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_4]],  
    Class = [[Position]],  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    ZoneTrigger = 1,  
    NpcCustom = 0,  
    MapDescription = 0,  
    TextManager = 0,  
    LogicEntityAction = 0,  
    BanditCamp = 1,  
    Npc = 0,  
    TextManagerEntry = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    DefaultFeature = 0,  
    ChatSequence = 0,  
    EventType = 0,  
    Creature = 0,  
    Region = 0,  
    ActivityStep = 1,  
    ActionType = 0,  
    ActionStep = 0,  
    Fauna = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 1,  
    WayPoint = 0,  
    Position = 0,  
    Location = 0,  
    NpcCreature = 0,  
    LogicEntityBehavior = 1,  
    Road = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 3,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_8]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_6]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 0,  
      LocationId = [[]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_670]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_668]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1241.953125,  
                x = 27691.98438,  
                InstanceId = [[Client1_671]],  
                Class = [[Position]],  
                z = 77.25
              },  
              Angle = 3.421875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_674]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_672]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1249.90625,  
                x = 27696.48438,  
                InstanceId = [[Client1_675]],  
                Class = [[Position]],  
                z = 78.953125
              },  
              Angle = 3.171875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_678]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_676]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 3]],  
              Position = {
                y = -1258.5,  
                x = 27692.45313,  
                InstanceId = [[Client1_679]],  
                Class = [[Position]],  
                z = 76.984375
              },  
              Angle = 2.546875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_682]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_680]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              Position = {
                y = -1195.71875,  
                x = 27677.85938,  
                InstanceId = [[Client1_683]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = -1.0625,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_686]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_684]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              Position = {
                y = -1205.015625,  
                x = 27686.6875,  
                InstanceId = [[Client1_687]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_690]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_688]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 3]],  
              Position = {
                y = -1221.125,  
                x = 27680.95313,  
                InstanceId = [[Client1_691]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = -3.015625,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_694]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_692]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 4]],  
              Position = {
                y = -1216.40625,  
                x = 27668.79688,  
                InstanceId = [[Client1_695]],  
                Class = [[Position]],  
                z = 74.640625
              },  
              Angle = 0.203125,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_698]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_696]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 1]],  
              Position = {
                y = -1253.765625,  
                x = 27642.25,  
                InstanceId = [[Client1_699]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              Angle = 1.0625,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_702]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_700]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 2]],  
              Position = {
                y = -1229.0625,  
                x = 27625.70313,  
                InstanceId = [[Client1_703]],  
                Class = [[Position]],  
                z = 75.546875
              },  
              Angle = -0.015625,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_706]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_704]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 3]],  
              Position = {
                y = -1223.859375,  
                x = 27646.46875,  
                InstanceId = [[Client1_707]],  
                Class = [[Position]],  
                z = 75.84375
              },  
              Angle = -1.21875,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_714]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_712]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 4]],  
              Position = {
                y = -1244.234375,  
                x = 27629.10938,  
                InstanceId = [[Client1_715]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = 0.046875,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_718]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_716]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              Position = {
                y = -1220.84375,  
                x = 27652.34375,  
                InstanceId = [[Client1_719]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = 5.078125,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_722]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_720]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 1]],  
              Position = {
                y = -1268.953125,  
                x = 27646.89063,  
                InstanceId = [[Client1_723]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              Angle = -6.25,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_726]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_724]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 2]],  
              Position = {
                y = -1285.875,  
                x = 27653.59375,  
                InstanceId = [[Client1_727]],  
                Class = [[Position]],  
                z = 74.734375
              },  
              Angle = -5.234375,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_730]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_728]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 3]],  
              Position = {
                y = -1286.21875,  
                x = 27667.4375,  
                InstanceId = [[Client1_731]],  
                Class = [[Position]],  
                z = 74.890625
              },  
              Angle = 0.25,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_734]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_732]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 4]],  
              Position = {
                y = -1285.71875,  
                x = 27628.9375,  
                InstanceId = [[Client1_735]],  
                Class = [[Position]],  
                z = 74.671875
              },  
              Angle = 0.25,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_738]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_736]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1238.78125,  
                x = 27661.73438,  
                InstanceId = [[Client1_739]],  
                Class = [[Position]],  
                z = 75.28125
              },  
              Angle = 3.109375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_742]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_740]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -1250.453125,  
                x = 27646.09375,  
                InstanceId = [[Client1_743]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = 2.4375,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_746]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_744]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              Position = {
                y = -1250.3125,  
                x = 27671.96875,  
                InstanceId = [[Client1_747]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              Angle = 2.984375,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_750]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_748]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[crate 1]],  
              Position = {
                y = -1254.125,  
                x = 27688.5625,  
                InstanceId = [[Client1_751]],  
                Class = [[Position]],  
                z = 76.265625
              },  
              Angle = 2.34375,  
              Base = [[palette.entities.botobjects.crate1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_754]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_752]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 crates 1]],  
              Position = {
                y = -1241.75,  
                x = 27687,  
                InstanceId = [[Client1_755]],  
                Class = [[Position]],  
                z = 75.875
              },  
              Angle = 2.34375,  
              Base = [[palette.entities.botobjects.crate2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_758]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_756]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 barrels 1]],  
              Position = {
                y = -1278.515625,  
                x = 27662.625,  
                InstanceId = [[Client1_759]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              Angle = 2.34375,  
              Base = [[palette.entities.botobjects.barrels_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_762]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_760]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[counter 1]],  
              Position = {
                y = -1265.640625,  
                x = 27655.67188,  
                InstanceId = [[Client1_763]],  
                Class = [[Position]],  
                z = 74.609375
              },  
              Angle = 0.796875,  
              Base = [[palette.entities.botobjects.counter]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_766]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_764]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[counter 2]],  
              Position = {
                y = -1246,  
                x = 27683.5625,  
                InstanceId = [[Client1_767]],  
                Class = [[Position]],  
                z = 75.265625
              },  
              Angle = 3.40625,  
              Base = [[palette.entities.botobjects.counter]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_770]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_768]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[counter 3]],  
              Position = {
                y = -1223.5,  
                x = 27660.03125,  
                InstanceId = [[Client1_771]],  
                Class = [[Position]],  
                z = 74.859375
              },  
              Angle = -1.78125,  
              Base = [[palette.entities.botobjects.counter]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_774]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_772]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1240.625,  
                x = 27661.45313,  
                InstanceId = [[Client1_775]],  
                Class = [[Position]],  
                z = 75.265625
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_778]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_776]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[old chest 1]],  
              Position = {
                y = -1228.375,  
                x = 27645.96875,  
                InstanceId = [[Client1_779]],  
                Class = [[Position]],  
                z = 76.171875
              },  
              Angle = -1.25,  
              Base = [[palette.entities.botobjects.chest_old]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_782]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_780]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp off 1]],  
              Position = {
                y = -1238.953125,  
                x = 27686.65625,  
                InstanceId = [[Client1_783]],  
                Class = [[Position]],  
                z = 75.765625
              },  
              Angle = 3.03125,  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_786]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_784]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp off 2]],  
              Position = {
                y = -1220.046875,  
                x = 27674.5625,  
                InstanceId = [[Client1_787]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              Angle = -2.21875,  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_790]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_788]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp off 3]],  
              Position = {
                y = -1273.25,  
                x = 27659.67188,  
                InstanceId = [[Client1_791]],  
                Class = [[Position]],  
                z = 74.875
              },  
              Angle = 1.359375,  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_794]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_792]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp off 4]],  
              Position = {
                y = -1248.203125,  
                x = 27638.26563,  
                InstanceId = [[Client1_795]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = 1.359375,  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_798]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_796]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 2]],  
              Position = {
                y = -1225.34375,  
                x = 27654.15625,  
                InstanceId = [[Client1_799]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              Angle = 0.0625,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_802]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_800]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 2]],  
              Position = {
                y = -1230.203125,  
                x = 27681.14063,  
                InstanceId = [[Client1_803]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = -2.71875,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_806]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_804]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 3]],  
              Position = {
                y = -1235.28125,  
                x = 27683.21875,  
                InstanceId = [[Client1_807]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              Angle = -2.84375,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_810]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_808]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -1256.96875,  
                x = 27673.875,  
                InstanceId = [[Client1_811]],  
                Class = [[Position]],  
                z = 74.921875
              },  
              Angle = -4.0625,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_814]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_812]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -1201.96875,  
                x = 27613.98438,  
                InstanceId = [[Client1_815]],  
                Class = [[Position]],  
                z = 73.421875
              },  
              Angle = -0.984375,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_818]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_816]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 2]],  
              Position = {
                y = -1278.390625,  
                x = 27613.01563,  
                InstanceId = [[Client1_819]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = 0.734375,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_822]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_820]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 3]],  
              Position = {
                y = -1230.46875,  
                x = 27710.57813,  
                InstanceId = [[Client1_823]],  
                Class = [[Position]],  
                z = 80.28125
              },  
              Angle = 3.15625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_826]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_824]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              Position = {
                y = -1243.953125,  
                x = 27722.3125,  
                InstanceId = [[Client1_827]],  
                Class = [[Position]],  
                z = 81.78125
              },  
              Angle = 2.75,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_841]],  
              ActivitiesId = {
              },  
              HairType = 5623854,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 13,  
              HandsModel = 5617966,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 1,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 4,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_839]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 2,  
              FeetModel = 5617710,  
              Speed = 0,  
              Angle = 3.890625,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Tattoo = 3,  
              MorphTarget3 = 5,  
              MorphTarget7 = 6,  
              ArmModel = 0,  
              Position = {
                y = -1236.21875,  
                x = 27662.8125,  
                InstanceId = [[Client1_843]],  
                Class = [[Position]],  
                z = 75.265625
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 7,  
              JacketModel = 0,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Shuai-Chon]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 5,  
              Level = 0,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_834]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 5613870,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 8,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_832]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 0.9177268147,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 2,  
              MorphTarget3 = 2,  
              MorphTarget7 = 7,  
              ArmModel = 0,  
              Position = {
                y = -1240.0625,  
                x = 27660.25,  
                InstanceId = [[Client1_835]],  
                Class = [[Position]],  
                z = 75.3125
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 3,  
              JacketModel = 5614638,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Ba'Mba]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 5,  
              Level = 0,  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_830]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 8,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 11,  
              GabaritHeight = 9,  
              HairColor = 1,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 4,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_828]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 2.433843374,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 3,  
              MorphTarget3 = 4,  
              MorphTarget7 = 2,  
              ArmModel = 0,  
              Position = {
                y = -1239.984375,  
                x = 27664.07813,  
                InstanceId = [[Client1_831]],  
                Class = [[Position]],  
                z = 75.203125
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 2,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Layla]],  
              Sex = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 0,  
              Level = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              InstanceId = [[Client1_921]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_919]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[butterflies 1]],  
              Position = {
                y = -1244.515625,  
                x = 27638.95313,  
                InstanceId = [[Client1_922]],  
                Class = [[Position]],  
                z = 75.640625
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.botobjects.fx_ju_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_961]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 5606958,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 12,  
              HairColor = 5,  
              Aggro = 15,  
              EyesColor = 6,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 12,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_959]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_995]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_996]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_964]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -2.203125,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 2,  
              MorphTarget3 = 5,  
              MorphTarget7 = 3,  
              ArmModel = 5604910,  
              Position = {
                y = -1230.46875,  
                x = 27662.96875,  
                InstanceId = [[Client1_962]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              WeaponRightHand = 5595182,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              JacketModel = 5605166,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Xakos]],  
              Sex = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              ArmColor = 4,  
              Level = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_964]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_963]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_966]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1229.578125,  
                    x = 27655.75,  
                    InstanceId = [[Client1_967]],  
                    Class = [[Position]],  
                    z = 75.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_969]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1231.765625,  
                    x = 27648.03125,  
                    InstanceId = [[Client1_970]],  
                    Class = [[Position]],  
                    z = 76.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_972]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1238.703125,  
                    x = 27645.14063,  
                    InstanceId = [[Client1_973]],  
                    Class = [[Position]],  
                    z = 76.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_975]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1249.015625,  
                    x = 27651.67188,  
                    InstanceId = [[Client1_976]],  
                    Class = [[Position]],  
                    z = 74.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_978]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1251.21875,  
                    x = 27665.73438,  
                    InstanceId = [[Client1_979]],  
                    Class = [[Position]],  
                    z = 74.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_981]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1243.421875,  
                    x = 27676.53125,  
                    InstanceId = [[Client1_982]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_984]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1229.859375,  
                    x = 27675.84375,  
                    InstanceId = [[Client1_985]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_987]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1226.015625,  
                    x = 27667.45313,  
                    InstanceId = [[Client1_988]],  
                    Class = [[Position]],  
                    z = 74.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_990]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1229.625,  
                    x = 27655.75,  
                    InstanceId = [[Client1_991]],  
                    Class = [[Position]],  
                    z = 75.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_993]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1229.578125,  
                    x = 27655.75,  
                    InstanceId = [[Client1_994]],  
                    Class = [[Position]],  
                    z = 75.390625
                  }
                }
              }
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_999]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 14,  
              HandsModel = 5606958,  
              FeetColor = 3,  
              GabaritBreastSize = 5,  
              GabaritHeight = 4,  
              HairColor = 5,  
              EyesColor = 6,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_997]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1031]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[14]],  
                        InstanceId = [[Client1_1032]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1006]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[25]],  
                        InstanceId = [[Client1_1056]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1036]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -2.84375,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 0,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              ArmModel = 5604910,  
              Position = {
                y = -1230.71875,  
                x = 27642.6875,  
                InstanceId = [[Client1_1000]],  
                Class = [[Position]],  
                z = 76.390625
              },  
              WeaponRightHand = 5635886,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              JacketModel = 5607726,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Icaps]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              ArmColor = 4,  
              Level = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_1003]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 5,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 10,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 12,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1001]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1033]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[14]],  
                        InstanceId = [[Client1_1034]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1006]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[25]],  
                        InstanceId = [[Client1_1055]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1036]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 1,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -2.84375,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 7,  
              MorphTarget3 = 5,  
              MorphTarget7 = 5,  
              ArmModel = 5604910,  
              Position = {
                y = -1236.734375,  
                x = 27640.98438,  
                InstanceId = [[Client1_1004]],  
                Class = [[Position]],  
                z = 76.3125
              },  
              WeaponRightHand = 5635886,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 6,  
              JacketModel = 5605166,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Xacus]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              ArmColor = 4,  
              Level = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_1006]],  
              Class = [[Region]],  
              Position = {
                y = -4.875,  
                x = -3.171875,  
                InstanceId = [[Client1_1005]],  
                Class = [[Position]],  
                z = 0.0625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1008]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1240.75,  
                    x = 27679.875,  
                    InstanceId = [[Client1_1009]],  
                    Class = [[Position]],  
                    z = 75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1011]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1225.8125,  
                    x = 27676.9375,  
                    InstanceId = [[Client1_1012]],  
                    Class = [[Position]],  
                    z = 74.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1014]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1221.125,  
                    x = 27671.32813,  
                    InstanceId = [[Client1_1015]],  
                    Class = [[Position]],  
                    z = 74.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1017]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1228,  
                    x = 27643.5,  
                    InstanceId = [[Client1_1018]],  
                    Class = [[Position]],  
                    z = 76.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1020]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1235.109375,  
                    x = 27638.46875,  
                    InstanceId = [[Client1_1021]],  
                    Class = [[Position]],  
                    z = 76.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1023]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1244.1875,  
                    x = 27640.67188,  
                    InstanceId = [[Client1_1024]],  
                    Class = [[Position]],  
                    z = 75.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1026]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1254.46875,  
                    x = 27653.32813,  
                    InstanceId = [[Client1_1027]],  
                    Class = [[Position]],  
                    z = 74.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1029]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1253.875,  
                    x = 27669.10938,  
                    InstanceId = [[Client1_1030]],  
                    Class = [[Position]],  
                    z = 74.8125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client1_1036]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1035]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1038]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1244.578125,  
                    x = 27658.42188,  
                    InstanceId = [[Client1_1039]],  
                    Class = [[Position]],  
                    z = 75.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1041]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1242.328125,  
                    x = 27655.0625,  
                    InstanceId = [[Client1_1042]],  
                    Class = [[Position]],  
                    z = 75.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1044]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1238.859375,  
                    x = 27654.65625,  
                    InstanceId = [[Client1_1045]],  
                    Class = [[Position]],  
                    z = 75.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1047]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1233.96875,  
                    x = 27657.65625,  
                    InstanceId = [[Client1_1048]],  
                    Class = [[Position]],  
                    z = 75.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1050]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1233.140625,  
                    x = 27662.59375,  
                    InstanceId = [[Client1_1051]],  
                    Class = [[Position]],  
                    z = 75.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1053]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1233.703125,  
                    x = 27659.39063,  
                    InstanceId = [[Client1_1054]],  
                    Class = [[Position]],  
                    z = 75.375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_1077]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1075]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bats 1]],  
              Position = {
                y = -1071.59375,  
                x = 27753.85938,  
                InstanceId = [[Client1_1078]],  
                Class = [[Position]],  
                z = 74.34375
              },  
              Angle = -2.484375,  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1081]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1079]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bats 2]],  
              Position = {
                y = -1105.234375,  
                x = 27715.20313,  
                InstanceId = [[Client1_1082]],  
                Class = [[Position]],  
                z = 75.125
              },  
              Angle = -2.484375,  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1085]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1083]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bats 3]],  
              Position = {
                y = -1120.625,  
                x = 27759.09375,  
                InstanceId = [[Client1_1086]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = -2.484375,  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1097]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1095]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wall ruin 1]],  
              Position = {
                y = -1104.5625,  
                x = 27730.85938,  
                InstanceId = [[Client1_1098]],  
                Class = [[Position]],  
                z = 74.578125
              },  
              Angle = -2.328125,  
              Base = [[palette.entities.botobjects.ruin_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1101]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1099]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wall ruin 2]],  
              Position = {
                y = -1063.328125,  
                x = 27744.67188,  
                InstanceId = [[Client1_1102]],  
                Class = [[Position]],  
                z = 74.796875
              },  
              Angle = -2.328125,  
              Base = [[palette.entities.botobjects.ruin_wall_b]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_1120]],  
              Class = [[Region]],  
              Position = {
                y = -12.4375,  
                x = -2.34375,  
                InstanceId = [[Client1_1119]],  
                Class = [[Position]],  
                z = -0.875
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1122]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1113.84375,  
                    x = 27737.60938,  
                    InstanceId = [[Client1_1123]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1125]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1111.6875,  
                    x = 27729.76563,  
                    InstanceId = [[Client1_1126]],  
                    Class = [[Position]],  
                    z = 74.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1128]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1115.234375,  
                    x = 27722.28125,  
                    InstanceId = [[Client1_1129]],  
                    Class = [[Position]],  
                    z = 74.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1131]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1121.078125,  
                    x = 27720.21875,  
                    InstanceId = [[Client1_1132]],  
                    Class = [[Position]],  
                    z = 75.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1134]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1136.875,  
                    x = 27722.3125,  
                    InstanceId = [[Client1_1135]],  
                    Class = [[Position]],  
                    z = 75.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1137]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1141.953125,  
                    x = 27736.78125,  
                    InstanceId = [[Client1_1138]],  
                    Class = [[Position]],  
                    z = 72.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1140]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1134.890625,  
                    x = 27747.57813,  
                    InstanceId = [[Client1_1141]],  
                    Class = [[Position]],  
                    z = 73.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1143]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1122.203125,  
                    x = 27752.875,  
                    InstanceId = [[Client1_1144]],  
                    Class = [[Position]],  
                    z = 74.78125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 3]],  
              InstanceId = [[Client1_1194]],  
              Class = [[Region]],  
              Position = {
                y = -14.78125,  
                x = 11.640625,  
                InstanceId = [[Client1_1193]],  
                Class = [[Position]],  
                z = 1.140625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1196]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1071.046875,  
                    x = 27759.45313,  
                    InstanceId = [[Client1_1197]],  
                    Class = [[Position]],  
                    z = 74.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1199]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1062.953125,  
                    x = 27745.28125,  
                    InstanceId = [[Client1_1200]],  
                    Class = [[Position]],  
                    z = 74.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1202]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1058.125,  
                    x = 27735.5,  
                    InstanceId = [[Client1_1203]],  
                    Class = [[Position]],  
                    z = 74.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1205]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1081.390625,  
                    x = 27736.9375,  
                    InstanceId = [[Client1_1206]],  
                    Class = [[Position]],  
                    z = 71.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1208]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1092.796875,  
                    x = 27734.1875,  
                    InstanceId = [[Client1_1209]],  
                    Class = [[Position]],  
                    z = 73.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1211]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1103.9375,  
                    x = 27730.8125,  
                    InstanceId = [[Client1_1212]],  
                    Class = [[Position]],  
                    z = 74.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1214]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1103.65625,  
                    x = 27739.1875,  
                    InstanceId = [[Client1_1215]],  
                    Class = [[Position]],  
                    z = 74.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1217]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1100.796875,  
                    x = 27749.96875,  
                    InstanceId = [[Client1_1218]],  
                    Class = [[Position]],  
                    z = 74.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1220]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1089.359375,  
                    x = 27762.54688,  
                    InstanceId = [[Client1_1221]],  
                    Class = [[Position]],  
                    z = 74.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1223]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1082.5625,  
                    x = 27764.04688,  
                    InstanceId = [[Client1_1224]],  
                    Class = [[Position]],  
                    z = 74.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1226]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1071.3125,  
                    x = 27761.35938,  
                    InstanceId = [[Client1_1227]],  
                    Class = [[Position]],  
                    z = 74.203125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 4]],  
              InstanceId = [[Client1_1239]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1241]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1122.359375,  
                    x = 27759.875,  
                    InstanceId = [[Client1_1242]],  
                    Class = [[Position]],  
                    z = 74.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1244]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1128.296875,  
                    x = 27760.3125,  
                    InstanceId = [[Client1_1245]],  
                    Class = [[Position]],  
                    z = 73.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1247]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1129.515625,  
                    x = 27775.09375,  
                    InstanceId = [[Client1_1248]],  
                    Class = [[Position]],  
                    z = 73.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1250]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1136.640625,  
                    x = 27793.46875,  
                    InstanceId = [[Client1_1251]],  
                    Class = [[Position]],  
                    z = 75.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1253]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1126.46875,  
                    x = 27792.40625,  
                    InstanceId = [[Client1_1254]],  
                    Class = [[Position]],  
                    z = 75.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1256]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1116.515625,  
                    x = 27776.75,  
                    InstanceId = [[Client1_1257]],  
                    Class = [[Position]],  
                    z = 75.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1259]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1122.25,  
                    x = 27760.01563,  
                    InstanceId = [[Client1_1260]],  
                    Class = [[Position]],  
                    z = 74.796875
                  }
                }
              },  
              Position = {
                y = 0.84375,  
                x = -0.8125,  
                InstanceId = [[Client1_1238]],  
                Class = [[Position]],  
                z = 0.046875
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 5]],  
              InstanceId = [[Client1_1275]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1277]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1170.6875,  
                    x = 27811,  
                    InstanceId = [[Client1_1278]],  
                    Class = [[Position]],  
                    z = 76.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1280]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1159.59375,  
                    x = 27805.32813,  
                    InstanceId = [[Client1_1281]],  
                    Class = [[Position]],  
                    z = 79.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1283]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1147.671875,  
                    x = 27797.48438,  
                    InstanceId = [[Client1_1284]],  
                    Class = [[Position]],  
                    z = 79.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1286]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1146.359375,  
                    x = 27791.46875,  
                    InstanceId = [[Client1_1287]],  
                    Class = [[Position]],  
                    z = 76.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1289]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1151.796875,  
                    x = 27787.45313,  
                    InstanceId = [[Client1_1290]],  
                    Class = [[Position]],  
                    z = 74.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1292]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1159.96875,  
                    x = 27787.1875,  
                    InstanceId = [[Client1_1293]],  
                    Class = [[Position]],  
                    z = 75.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1295]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1166.234375,  
                    x = 27790.625,  
                    InstanceId = [[Client1_1296]],  
                    Class = [[Position]],  
                    z = 76.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1298]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1177.84375,  
                    x = 27796.85938,  
                    InstanceId = [[Client1_1299]],  
                    Class = [[Position]],  
                    z = 75.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1301]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1182.015625,  
                    x = 27804.34375,  
                    InstanceId = [[Client1_1302]],  
                    Class = [[Position]],  
                    z = 76.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1304]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1183.90625,  
                    x = 27812.48438,  
                    InstanceId = [[Client1_1305]],  
                    Class = [[Position]],  
                    z = 74.78125
                  }
                }
              },  
              Position = {
                y = 2.75,  
                x = -0.046875,  
                InstanceId = [[Client1_1274]],  
                Class = [[Position]],  
                z = -0.5
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 6]],  
              InstanceId = [[Client1_1345]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1347]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1208.578125,  
                    x = 27844.03125,  
                    InstanceId = [[Client1_1348]],  
                    Class = [[Position]],  
                    z = 75.28125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1350]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1192.421875,  
                    x = 27834.14063,  
                    InstanceId = [[Client1_1351]],  
                    Class = [[Position]],  
                    z = 74.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1353]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1183.046875,  
                    x = 27835.54688,  
                    InstanceId = [[Client1_1354]],  
                    Class = [[Position]],  
                    z = 74.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1356]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1179.4375,  
                    x = 27849.32813,  
                    InstanceId = [[Client1_1357]],  
                    Class = [[Position]],  
                    z = 75.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1359]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1179.5625,  
                    x = 27860.03125,  
                    InstanceId = [[Client1_1360]],  
                    Class = [[Position]],  
                    z = 75.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1362]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1193.078125,  
                    x = 27862.8125,  
                    InstanceId = [[Client1_1363]],  
                    Class = [[Position]],  
                    z = 76.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1365]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1207.96875,  
                    x = 27852.70313,  
                    InstanceId = [[Client1_1366]],  
                    Class = [[Position]],  
                    z = 76.875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1344]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 3]],  
              InstanceId = [[Client1_1588]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1587]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1590]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1096.890625,  
                    x = 27848.64063,  
                    InstanceId = [[Client1_1591]],  
                    Class = [[Position]],  
                    z = 72.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2020]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1100.09375,  
                    x = 27854.73438,  
                    InstanceId = [[Client1_2021]],  
                    Class = [[Position]],  
                    z = 73
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1593]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1104.328125,  
                    x = 27860.8125,  
                    InstanceId = [[Client1_1594]],  
                    Class = [[Position]],  
                    z = 74.34375
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_1808]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1806]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              Position = {
                y = -1095.078125,  
                x = 27843.20313,  
                InstanceId = [[Client1_1809]],  
                Class = [[Position]],  
                z = 73.578125
              },  
              Angle = -1.421875,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1857]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1855]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1927]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1928]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1194]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Assault Kirosta]],  
              Position = {
                y = -1097.21875,  
                x = 27760.28125,  
                InstanceId = [[Client1_1858]],  
                Class = [[Position]],  
                z = 74.296875
              },  
              Angle = -0.3505425453,  
              Base = [[palette.entities.creatures.ckfrb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1861]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1859]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1929]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1930]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1194]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Assault Kirosta]],  
              Position = {
                y = -1097.203125,  
                x = 27771.15625,  
                InstanceId = [[Client1_1862]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              Angle = -0.3505425453,  
              Base = [[palette.entities.creatures.ckfrb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1865]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1863]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1925]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1926]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1194]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kipesta]],  
              Position = {
                y = -1107.265625,  
                x = 27762.23438,  
                InstanceId = [[Client1_1866]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              Angle = -0.3505425453,  
              Base = [[palette.entities.creatures.ckjib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1869]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1867]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1923]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1924]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1120]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Trooper Kipee]],  
              Position = {
                y = -1128.84375,  
                x = 27729.01563,  
                InstanceId = [[Client1_1870]],  
                Class = [[Position]],  
                z = 74.5625
              },  
              Angle = 0.8529254794,  
              Base = [[palette.entities.creatures.ckhib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1873]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1871]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1919]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1920]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1120]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kipee]],  
              Position = {
                y = -1136.734375,  
                x = 27739.14063,  
                InstanceId = [[Client1_1874]],  
                Class = [[Position]],  
                z = 73.125
              },  
              Angle = 0.8529254794,  
              Base = [[palette.entities.creatures.ckhib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1877]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1875]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1921]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1922]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1120]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kipee]],  
              Position = {
                y = -1141.953125,  
                x = 27729.10938,  
                InstanceId = [[Client1_1878]],  
                Class = [[Position]],  
                z = 74
              },  
              Angle = 0.8529254794,  
              Base = [[palette.entities.creatures.ckhib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1881]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1879]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1933]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1934]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1239]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1964]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1239]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kizarak]],  
              Position = {
                y = -1127.9375,  
                x = 27781.39063,  
                InstanceId = [[Client1_1882]],  
                Class = [[Position]],  
                z = 73.90625
              },  
              Angle = 2.418059587,  
              Base = [[palette.entities.creatures.ckcib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1885]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1883]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1931]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1932]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1239]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                y = -1123.65625,  
                x = 27764.15625,  
                InstanceId = [[Client1_1886]],  
                Class = [[Position]],  
                z = 74.5625
              },  
              Angle = 2.418059587,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1889]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1887]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1935]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1936]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1239]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Trooper Kizoar]],  
              Position = {
                y = -1120.171875,  
                x = 27770.875,  
                InstanceId = [[Client1_1890]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              Angle = 2.418059587,  
              Base = [[palette.entities.creatures.ckiib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1905]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1903]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1960]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1961]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1345]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Assault Kirosta]],  
              Position = {
                y = -1191.390625,  
                x = 27855.375,  
                InstanceId = [[Client1_1906]],  
                Class = [[Position]],  
                z = 75.703125
              },  
              Angle = 2.571155548,  
              Base = [[palette.entities.creatures.ckfrb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1913]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1911]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1958]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1959]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1345]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                y = -1182.9375,  
                x = 27843.48438,  
                InstanceId = [[Client1_1914]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              Angle = 2.571155548,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1917]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1915]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1962]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1963]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1345]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                y = -1203.1875,  
                x = 27852.39063,  
                InstanceId = [[Client1_1918]],  
                Class = [[Position]],  
                z = 76.484375
              },  
              Angle = 2.571155548,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_2057]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 10,  
              HandsModel = 5604142,  
              FeetColor = 1,  
              GabaritBreastSize = 13,  
              GabaritHeight = 2,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 9,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2055]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2082]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_2083]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2060]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2093]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_2094]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 6,  
              FeetModel = 5606702,  
              Speed = 1,  
              Angle = -0.578125,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_20]],  
              Tattoo = 15,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              ArmModel = 5606190,  
              Position = {
                y = -1093.34375,  
                x = 27841.23438,  
                InstanceId = [[Client1_2058]],  
                Class = [[Position]],  
                z = 73.65625
              },  
              WeaponRightHand = 5594414,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 0,  
              JacketModel = 5607726,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Cetis]],  
              Sex = 1,  
              Sheet = [[ring_melee_tank_pierce_b2.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 5637166,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InheritPos = 1,  
              Name = [[Route 4]],  
              InstanceId = [[Client1_2060]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2062]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1095.46875,  
                    x = 27839.71875,  
                    InstanceId = [[Client1_2063]],  
                    Class = [[Position]],  
                    z = 73.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2065]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1093.15625,  
                    x = 27778.17188,  
                    InstanceId = [[Client1_2066]],  
                    Class = [[Position]],  
                    z = 75.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2068]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1085.96875,  
                    x = 27723.39063,  
                    InstanceId = [[Client1_2069]],  
                    Class = [[Position]],  
                    z = 76.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2071]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1107.53125,  
                    x = 27660.0625,  
                    InstanceId = [[Client1_2072]],  
                    Class = [[Position]],  
                    z = 75.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2074]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1128.765625,  
                    x = 27552.26563,  
                    InstanceId = [[Client1_2075]],  
                    Class = [[Position]],  
                    z = 75.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2077]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1155.25,  
                    x = 27512.42188,  
                    InstanceId = [[Client1_2078]],  
                    Class = [[Position]],  
                    z = 75.953125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_2059]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 5]],  
              InstanceId = [[Client1_2745]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_2744]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2747]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1104.046875,  
                    x = 27742.76563,  
                    InstanceId = [[Client1_2748]],  
                    Class = [[Position]],  
                    z = 74.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2750]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1110.34375,  
                    x = 27736.98438,  
                    InstanceId = [[Client1_2751]],  
                    Class = [[Position]],  
                    z = 74.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2753]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1113.0625,  
                    x = 27734,  
                    InstanceId = [[Client1_2754]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2756]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1118.453125,  
                    x = 27727.73438,  
                    InstanceId = [[Client1_2757]],  
                    Class = [[Position]],  
                    z = 74.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2759]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1128.34375,  
                    x = 27715.78125,  
                    InstanceId = [[Client1_2760]],  
                    Class = [[Position]],  
                    z = 76.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2762]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1140.515625,  
                    x = 27705.98438,  
                    InstanceId = [[Client1_2763]],  
                    Class = [[Position]],  
                    z = 78.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2765]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1158,  
                    x = 27695.23438,  
                    InstanceId = [[Client1_2766]],  
                    Class = [[Position]],  
                    z = 75.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2768]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1174.390625,  
                    x = 27689.76563,  
                    InstanceId = [[Client1_2769]],  
                    Class = [[Position]],  
                    z = 74.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2771]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1188.53125,  
                    x = 27686.4375,  
                    InstanceId = [[Client1_2772]],  
                    Class = [[Position]],  
                    z = 75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2774]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1207.578125,  
                    x = 27679.03125,  
                    InstanceId = [[Client1_2775]],  
                    Class = [[Position]],  
                    z = 74.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2777]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1227.609375,  
                    x = 27666.8125,  
                    InstanceId = [[Client1_2778]],  
                    Class = [[Position]],  
                    z = 74.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_2780]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1236.109375,  
                    x = 27659.15625,  
                    InstanceId = [[Client1_2781]],  
                    Class = [[Position]],  
                    z = 75.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_3315]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1237.4375,  
                    x = 27660.23438,  
                    InstanceId = [[Client1_3316]],  
                    Class = [[Position]],  
                    z = 75
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_7204]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7202]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier(T) 1]],  
              Position = {
                y = -1108.3125,  
                x = 27860.8125,  
                InstanceId = [[Client1_7205]],  
                Class = [[Position]],  
                z = 73.53125
              },  
              Angle = -0.953125,  
              Base = [[palette.entities.botobjects.barrier_T]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_7208]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7206]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 1]],  
              Position = {
                y = -1105.25,  
                x = 27863.40625,  
                InstanceId = [[Client1_7209]],  
                Class = [[Position]],  
                z = 73.8125
              },  
              Angle = -0.8125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_7212]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7210]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 2]],  
              Position = {
                y = -1110.359375,  
                x = 27863.39063,  
                InstanceId = [[Client1_7213]],  
                Class = [[Position]],  
                z = 73.484375
              },  
              Angle = -2.03125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_7216]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7214]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 3]],  
              Position = {
                y = -1110.65625,  
                x = 27866.34375,  
                InstanceId = [[Client1_7217]],  
                Class = [[Position]],  
                z = 73.703125
              },  
              Angle = -1.046875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_7220]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7218]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 4]],  
              Position = {
                y = -1105.3125,  
                x = 27866.15625,  
                InstanceId = [[Client1_7221]],  
                Class = [[Position]],  
                z = 73.984375
              },  
              Angle = -2.25,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_7224]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_7222]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier(T) 2]],  
              Position = {
                y = -1107.34375,  
                x = 27868.48438,  
                InstanceId = [[Client1_7225]],  
                Class = [[Position]],  
                z = 74.171875
              },  
              Angle = -3.953125,  
              Base = [[palette.entities.botobjects.barrier_T]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8306]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8304]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin mound 1]],  
              Position = {
                y = -1536.375,  
                x = 27589.39063,  
                InstanceId = [[Client1_8307]],  
                Class = [[Position]],  
                z = 90.921875
              },  
              Angle = 3.8125,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8310]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8308]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 1]],  
              Position = {
                y = -1543.203125,  
                x = 27575.35938,  
                InstanceId = [[Client1_8311]],  
                Class = [[Position]],  
                z = 90.375
              },  
              Angle = 1.390625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8314]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8312]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 2]],  
              Position = {
                y = -1531.671875,  
                x = 27583.46875,  
                InstanceId = [[Client1_8315]],  
                Class = [[Position]],  
                z = 91.03125
              },  
              Angle = 1.390625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8318]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8316]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 3]],  
              Position = {
                y = -1530.9375,  
                x = 27600.95313,  
                InstanceId = [[Client1_8319]],  
                Class = [[Position]],  
                z = 91.078125
              },  
              Angle = 1.390625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8322]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8320]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 4]],  
              Position = {
                y = -1538.625,  
                x = 27600.67188,  
                InstanceId = [[Client1_8323]],  
                Class = [[Position]],  
                z = 91.0625
              },  
              Angle = 1.390625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 7]],  
              InstanceId = [[Client1_8373]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_8375]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1551.5625,  
                    x = 27614.75,  
                    InstanceId = [[Client1_8376]],  
                    Class = [[Position]],  
                    z = 90.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_8378]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1513.3125,  
                    x = 27614.875,  
                    InstanceId = [[Client1_8379]],  
                    Class = [[Position]],  
                    z = 93.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_8381]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1502.359375,  
                    x = 27610.28125,  
                    InstanceId = [[Client1_8382]],  
                    Class = [[Position]],  
                    z = 93.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_8384]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1505.40625,  
                    x = 27588.04688,  
                    InstanceId = [[Client1_8385]],  
                    Class = [[Position]],  
                    z = 87.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_8387]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1520.875,  
                    x = 27549.10938,  
                    InstanceId = [[Client1_8388]],  
                    Class = [[Position]],  
                    z = 90.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_8390]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1550.9375,  
                    x = 27536.48438,  
                    InstanceId = [[Client1_8391]],  
                    Class = [[Position]],  
                    z = 90.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_8393]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1551.71875,  
                    x = 27566.75,  
                    InstanceId = [[Client1_8394]],  
                    Class = [[Position]],  
                    z = 90.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_8396]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1552.546875,  
                    x = 27603.28125,  
                    InstanceId = [[Client1_8397]],  
                    Class = [[Position]],  
                    z = 91.4375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_8372]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_9474]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9472]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1]],  
              Position = {
                y = -1095.6875,  
                x = 27786.64063,  
                InstanceId = [[Client1_9475]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -1.803035975,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9478]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9476]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 2]],  
              Position = {
                y = -1096.078125,  
                x = 27790.54688,  
                InstanceId = [[Client1_9479]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -1.872849107,  
              Base = [[palette.entities.botobjects.tomb_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9482]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9480]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 3]],  
              Position = {
                y = -1100.546875,  
                x = 27788.625,  
                InstanceId = [[Client1_9483]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              Angle = -1.960115671,  
              Base = [[palette.entities.botobjects.tomb_4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9486]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9484]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 4]],  
              Position = {
                y = -1099.203125,  
                x = 27785.4375,  
                InstanceId = [[Client1_9487]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = -1.785582662,  
              Base = [[palette.entities.botobjects.tomb_5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9490]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9488]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo infested carcass II 1]],  
              Position = {
                y = -1107.109375,  
                x = 27797.21875,  
                InstanceId = [[Client1_9491]],  
                Class = [[Position]],  
                z = 74.734375
              },  
              Angle = -2.765625,  
              Base = [[palette.entities.botobjects.fx_goo_mamal]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9494]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9492]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo infested carcass I 1]],  
              Position = {
                y = -1122.90625,  
                x = 27811.09375,  
                InstanceId = [[Client1_9495]],  
                Class = [[Position]],  
                z = 76.125
              },  
              Angle = -3.046875,  
              Base = [[palette.entities.botobjects.fx_goo_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9498]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9496]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo infested carcass II 2]],  
              Position = {
                y = -1146.78125,  
                x = 27811.65625,  
                InstanceId = [[Client1_9499]],  
                Class = [[Position]],  
                z = 79.296875
              },  
              Angle = 1.078125,  
              Base = [[palette.entities.botobjects.fx_goo_mamal]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9502]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9500]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo infested carcass I 2]],  
              Position = {
                y = -1177.234375,  
                x = 27828.17188,  
                InstanceId = [[Client1_9503]],  
                Class = [[Position]],  
                z = 74.65625
              },  
              Angle = 1.703125,  
              Base = [[palette.entities.botobjects.fx_goo_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9510]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9508]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 1]],  
              Position = {
                y = -1147.609375,  
                x = 27813.95313,  
                InstanceId = [[Client1_9511]],  
                Class = [[Position]],  
                z = 78.84375
              },  
              Angle = 2.0625,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9514]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9512]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 2]],  
              Position = {
                y = -1171.65625,  
                x = 27827.20313,  
                InstanceId = [[Client1_9515]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = 2.0625,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_9518]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_9516]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[goo smoke 3]],  
              Position = {
                y = -1112.828125,  
                x = 27798.60938,  
                InstanceId = [[Client1_9519]],  
                Class = [[Position]],  
                z = 74.765625
              },  
              Angle = -0.4375,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_10828]],  
              Name = [[Route 6]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_10830]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_10831]],  
                    x = 27513.125,  
                    y = -1160.21875,  
                    z = 76.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_10833]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_10834]],  
                    x = 27551.70313,  
                    y = -1187.609375,  
                    z = 74.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_10836]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_10837]],  
                    x = 27596.84375,  
                    y = -1208.3125,  
                    z = 74.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_10839]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_10840]],  
                    x = 27641.15625,  
                    y = -1230.921875,  
                    z = 76.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_10842]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_10843]],  
                    x = 27647.67188,  
                    y = -1237.53125,  
                    z = 76.046875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_10827]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_9]]
        },  
        {
          CarnivoreRace = [[Vigorous Gingo]],  
          Ghosts = {
          },  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_5156]]
          },  
          Class = [[Fauna]],  
          CarnivoresCount = [[3]],  
          HerbivoreBase = [[palette.entities.creatures.chcdb4]],  
          _HerbCount = 7,  
          Base = [[palette.entities.botobjects.user_event]],  
          _CarnCount = 3,  
          _CarnId = [[Client1_5221]],  
          HerbivoresCount = [[7]],  
          CarnivoreBase = [[palette.entities.creatures.ccadb1]],  
          HerbivoreRace = [[Sprightly Capryni]],  
          InheritPos = 1,  
          Name = [[Fauna Feature 1]],  
          _HerbId = [[Client1_5218]],  
          Position = {
            y = -1317.765625,  
            x = 27772.73438,  
            InstanceId = [[Client1_5157]],  
            Class = [[Position]],  
            z = 73
          },  
          InstanceId = [[Client1_5155]],  
          HerbivoresName = [[Sprightly Capryni]],  
          Components = {
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 1]],  
              InstanceId = [[Client1_5159]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5161]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2.46875,  
                    x = 10.109375,  
                    InstanceId = [[Client1_5162]],  
                    Class = [[Position]],  
                    z = 3.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5164]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 14.359375,  
                    x = -1.203125,  
                    InstanceId = [[Client1_5165]],  
                    Class = [[Position]],  
                    z = -2.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5167]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 7.09375,  
                    x = -6,  
                    InstanceId = [[Client1_5168]],  
                    Class = [[Position]],  
                    z = -3.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5170]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -6.671875,  
                    x = -6.296875,  
                    InstanceId = [[Client1_5171]],  
                    Class = [[Position]],  
                    z = 0.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5173]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -19.21875,  
                    x = -1.546875,  
                    InstanceId = [[Client1_5174]],  
                    Class = [[Position]],  
                    z = 2.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5176]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -30.09375,  
                    x = 10.484375,  
                    InstanceId = [[Client1_5177]],  
                    Class = [[Position]],  
                    z = 10.1875
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1322.71875,  
                x = 27784.625,  
                InstanceId = [[Client1_5158]],  
                Class = [[Position]],  
                z = 74.21875
              }
            },  
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 2]],  
              InstanceId = [[Client1_5179]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5181]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_5182]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5184]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_5185]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5187]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_5188]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5190]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_5191]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5193]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_5194]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5196]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_5197]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1324.765625,  
                x = 27765.73438,  
                InstanceId = [[Client1_5178]],  
                Class = [[Position]],  
                z = 73
              }
            },  
            {
              InheritPos = 0,  
              Name = [[Food Zone 1]],  
              InstanceId = [[Client1_5199]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5201]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_5202]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5204]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 15.4375,  
                    x = 7.390625,  
                    InstanceId = [[Client1_5205]],  
                    Class = [[Position]],  
                    z = 0.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5207]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 24.578125,  
                    x = -7.828125,  
                    InstanceId = [[Client1_5208]],  
                    Class = [[Position]],  
                    z = 2.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5210]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 7.78125,  
                    x = -14.5,  
                    InstanceId = [[Client1_5211]],  
                    Class = [[Position]],  
                    z = 1.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5213]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_5214]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5216]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_5217]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1307.765625,  
                x = 27772.73438,  
                InstanceId = [[Client1_5198]],  
                Class = [[Position]],  
                z = 73
              }
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_5218]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_5219]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.chcdb4]],  
              RaceName = [[Sprightly Capryni]],  
              CrittersCount = [[7]],  
              _CrittersCount = 7,  
              SleepZone = [[Client1_5159]],  
              FoodZone = [[Client1_5199]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Herbivores]],  
              Position = {
                y = -1307.765625,  
                x = 27782.73438,  
                InstanceId = [[Client1_5220]],  
                Class = [[Position]],  
                z = 71
              },  
              Ghosts = {
              },  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              _Seed = 1145022090
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_5221]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_5222]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.ccadb1]],  
              RaceName = [[Vigorous Gingo]],  
              CrittersCount = [[3]],  
              _CrittersCount = 3,  
              SleepZone = [[Client1_5179]],  
              FoodZone = [[Client1_5159]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Carnivores]],  
              Position = {
                y = -1327.765625,  
                x = 27762.73438,  
                InstanceId = [[Client1_5223]],  
                Class = [[Position]],  
                z = 73
              },  
              Ghosts = {
              },  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              _Seed = 1145022090
            }
          },  
          _Seed = 1145022090
        }
      },  
      Version = 5,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Permanent]]
    },  
    {
      InstanceId = [[Client1_12]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_845]],  
            Class = [[LogicEntityAction]],  
            Conditions = {
            },  
            Actions = {
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_847]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_848]],  
                Entity = r2.RefId([[Client1_834]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_849]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_850]],  
                Entity = r2.RefId([[Client1_830]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_851]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_852]],  
                Entity = r2.RefId([[Client1_841]]),  
                Class = [[ActionStep]]
              }
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_846]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            }
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_1719]],  
            Class = [[LogicEntityAction]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_1720]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            }
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_1724]],  
            Class = [[LogicEntityAction]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_1725]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            }
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_2085]],  
            Class = [[LogicEntityAction]],  
            Conditions = {
            },  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_2087]],  
                  Value = r2.RefId([[Client1_2093]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_2088]],  
                Entity = r2.RefId([[Client1_2057]]),  
                Class = [[ActionStep]]
              }
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_2086]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            }
          }
        },  
        InstanceId = [[Client1_10]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 1,  
      LocationId = [[Client1_14]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_911]],  
              ActivitiesId = {
              },  
              HairType = 5624366,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 10,  
              HandsModel = 5617966,  
              FeetColor = 5,  
              GabaritBreastSize = 14,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 11,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_909]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_917]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_918]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 5617710,  
              Speed = 0,  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Tattoo = 12,  
              MorphTarget3 = 2,  
              MorphTarget7 = 4,  
              ArmModel = 0,  
              Position = {
                y = -1244.5625,  
                x = 27638.0625,  
                InstanceId = [[Client1_912]],  
                Class = [[Position]],  
                z = 75.59375
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 6,  
              JacketModel = 0,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Mu]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_934]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 14,  
              HandsModel = 5613870,  
              FeetColor = 0,  
              GabaritBreastSize = 12,  
              GabaritHeight = 2,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 12,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_932]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -2.109375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 7,  
              MorphTarget3 = 5,  
              MorphTarget7 = 1,  
              ArmModel = 0,  
              Position = {
                y = -1222.484375,  
                x = 27660.32813,  
                InstanceId = [[Client1_935]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              JacketModel = 5614638,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Ba'caunin]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 1,  
              Level = 0,  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_938]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 5,  
              HandsModel = 5613870,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 10,  
              HairColor = 1,  
              EyesColor = 4,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 14,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_936]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 6,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 6.65625,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 29,  
              MorphTarget3 = 6,  
              MorphTarget7 = 5,  
              ArmModel = 0,  
              Position = {
                y = -1229,  
                x = 27674.39063,  
                InstanceId = [[Client1_939]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 2,  
              JacketModel = 0,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Laughan]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 4,  
              Level = 0,  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_915]],  
              ActivitiesId = {
              },  
              HairType = 8494,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 4,  
              HandsModel = 5617966,  
              FeetColor = 5,  
              GabaritBreastSize = 10,  
              GabaritHeight = 13,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_913]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 5617710,  
              Speed = 0,  
              Angle = -3.0625,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Tattoo = 28,  
              MorphTarget3 = 7,  
              MorphTarget7 = 1,  
              ArmModel = 0,  
              Position = {
                y = -1228.96875,  
                x = 27676.95313,  
                InstanceId = [[Client1_916]],  
                Class = [[Position]],  
                z = 75
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              JacketModel = 5618734,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Pu-Fu]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_945]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 11,  
              HandsModel = 5605678,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 8,  
              HairColor = 5,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 10,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_943]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 3.703125,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 18,  
              MorphTarget3 = 0,  
              MorphTarget7 = 6,  
              ArmModel = 0,  
              Position = {
                y = -1246.0625,  
                x = 27684.90625,  
                InstanceId = [[Client1_946]],  
                Class = [[Position]],  
                z = 75.484375
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 3,  
              JacketModel = 0,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Ioros]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 5,  
              Level = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_949]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 13,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 12,  
              GabaritHeight = 6,  
              HairColor = 5,  
              EyesColor = 6,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 12,  
              HandsColor = 3,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_947]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 5653038,  
              Speed = 0,  
              Angle = -0.40625,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Tattoo = 24,  
              MorphTarget3 = 3,  
              MorphTarget7 = 7,  
              ArmModel = 0,  
              Position = {
                y = -1266.390625,  
                x = 27655.40625,  
                InstanceId = [[Client1_950]],  
                Class = [[Position]],  
                z = 74.640625
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Nirni]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 3,  
              Level = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_953]],  
              ActivitiesId = {
              },  
              HairType = 5622318,  
              TrouserColor = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 1,  
              HandsModel = 5609774,  
              FeetColor = 2,  
              GabaritBreastSize = 10,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 4,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_951]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = -0.0625,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Tattoo = 25,  
              MorphTarget3 = 6,  
              MorphTarget7 = 7,  
              ArmModel = 0,  
              Position = {
                y = -1274.328125,  
                x = 27660.14063,  
                InstanceId = [[Client1_954]],  
                Class = [[Position]],  
                z = 74.921875
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 4,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Liccio]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 4,  
              Level = 0,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_1796]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 5,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 13,  
              HandsModel = 5605678,  
              FeetColor = 2,  
              GabaritBreastSize = 8,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 0,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 13,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1794]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2022]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_2023]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1588]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1816]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1818]],  
                          Value = r2.RefId([[Client1_2046]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1819]],  
                        Entity = r2.RefId([[Client1_1804]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_1817]],  
                      Value = r2.RefId([[Client1_2022]]),  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2049]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_2050]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2096]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_2098]],  
                          Value = r2.RefId([[Client1_2082]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_2099]],  
                        Entity = r2.RefId([[Client1_2057]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_2097]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 4,  
              FeetModel = 5605422,  
              Speed = 1,  
              Angle = 2.78125,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              Tattoo = 26,  
              MorphTarget3 = 7,  
              MorphTarget7 = 1,  
              ArmModel = 0,  
              Position = {
                y = -1096.140625,  
                x = 27847.73438,  
                InstanceId = [[Client1_1797]],  
                Class = [[Position]],  
                z = 73.765625
              },  
              WeaponRightHand = 6933806,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 6,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Tirius]],  
              Sex = 1,  
              Sheet = [[ring_magic_aoe_cold_b2.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_1804]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 11,  
              HandsModel = 5604142,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 11,  
              HandsColor = 1,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1802]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2046]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_2047]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1588]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 6,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = -0.59375,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_20]],  
              Tattoo = 26,  
              MorphTarget3 = 1,  
              MorphTarget7 = 6,  
              ArmModel = 5606190,  
              Position = {
                y = -1091.921875,  
                x = 27844.53125,  
                InstanceId = [[Client1_1805]],  
                Class = [[Position]],  
                z = 74.078125
              },  
              WeaponRightHand = 5594926,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Eukos]],  
              Sex = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 5637166,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InstanceId = [[Client1_1950]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1948]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1952]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1953]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1275]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kiban]],  
              Position = {
                y = -1150.890625,  
                x = 27792.46875,  
                InstanceId = [[Client1_1951]],  
                Class = [[Position]],  
                z = 77.421875
              },  
              Angle = 1.167310715,  
              Base = [[palette.entities.creatures.ckgib4]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_2013]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5609774,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 3,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2011]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 5653038,  
              Speed = 0,  
              Angle = 4.140625,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Tattoo = 3,  
              MorphTarget3 = 3,  
              MorphTarget7 = 7,  
              ArmModel = 0,  
              Position = {
                y = -1108.421875,  
                x = 27864.8125,  
                InstanceId = [[Client1_2014]],  
                Class = [[Position]],  
                z = 73.65625
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Faneliah]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 3,  
              Level = 0,  
              SheetClient = [[basic_matis_female.creature]]
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_866]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_868]],  
                Class = [[LogicEntityAction]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_869]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                }
              }
            },  
            InstanceId = [[Client1_864]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 1]],  
          Position = {
            y = -1238.609375,  
            x = 27663.75,  
            InstanceId = [[Client1_865]],  
            Class = [[Position]],  
            z = 75
          },  
          Repeating = 1,  
          Components = {
            {
              Time = 6,  
              InstanceId = [[Client1_870]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_871]],  
                  Who = r2.RefId([[Client1_830]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_896]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 8,  
              InstanceId = [[Client1_887]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_888]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_894]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 8,  
              InstanceId = [[Client1_890]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_891]],  
                  Who = r2.RefId([[Client1_841]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_895]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 8,  
              InstanceId = [[Client1_897]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_898]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_899]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 9,  
              InstanceId = [[Client1_900]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_901]],  
                  Who = r2.RefId([[Client1_830]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_904]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 8,  
              InstanceId = [[Client1_902]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_903]],  
                  Who = r2.RefId([[Client1_841]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_905]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 9,  
              InstanceId = [[Client1_906]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_907]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_908]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Active = 1
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_925]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_923]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 2]],  
          Position = {
            y = -1238.609375,  
            x = 27641.20313,  
            InstanceId = [[Client1_924]],  
            Class = [[Position]],  
            z = 76.21875
          },  
          Active = 1,  
          Components = {
            {
              Time = 5,  
              InstanceId = [[Client1_926]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Excited]],  
                  InstanceId = [[Client1_927]],  
                  Who = r2.RefId([[Client1_911]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_928]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 7,  
              InstanceId = [[Client1_929]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Dreamy]],  
                  InstanceId = [[Client1_930]],  
                  Who = r2.RefId([[Client1_911]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_931]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_1059]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_1057]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 3]],  
          Position = {
            y = -1231.859375,  
            x = 27639.51563,  
            InstanceId = [[Client1_1058]],  
            Class = [[Position]],  
            z = 77
          },  
          Active = 1,  
          Components = {
            {
              Time = 44,  
              InstanceId = [[Client1_1060]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Innocent]],  
                  InstanceId = [[Client1_1061]],  
                  Who = r2.RefId([[Client1_999]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_1003]]),  
                  Says = [[Client1_1062]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 8,  
              InstanceId = [[Client1_1063]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Confused]],  
                  InstanceId = [[Client1_1064]],  
                  Who = r2.RefId([[Client1_1003]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_999]]),  
                  Says = [[Client1_1065]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 10,  
              InstanceId = [[Client1_1068]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_1069]],  
                  Who = r2.RefId([[Client1_999]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_1072]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_1070]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Discreet]],  
                  InstanceId = [[Client1_1071]],  
                  Who = r2.RefId([[Client1_961]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_1003]]),  
                  Says = [[Client1_1074]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 1
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_1431]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_1436]],  
                Class = [[LogicEntityAction]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_1437]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                }
              }
            },  
            InstanceId = [[Client1_1429]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 4]],  
          Position = {
            y = -1104.5625,  
            x = 27867.75,  
            InstanceId = [[Client1_1430]],  
            Class = [[Position]],  
            z = 74.234375
          },  
          Active = 1,  
          Components = {
            {
              Time = 5,  
              InstanceId = [[Client1_1438]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Imploring]],  
                  InstanceId = [[Client1_1439]],  
                  Who = r2.RefId([[Client1_2013]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2892]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_1441]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Disgusted]],  
                  InstanceId = [[Client1_1442]],  
                  Who = r2.RefId([[Client1_2013]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2893]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 9,  
              InstanceId = [[Client1_1444]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Hysterical]],  
                  InstanceId = [[Client1_1445]],  
                  Who = r2.RefId([[Client1_2013]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2894]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 1
        },  
        {
          InstanceId = [[Client1_2024]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_2042]],  
                Class = [[LogicEntityAction]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_2044]],  
                      Value = r2.RefId([[Client1_2022]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_2045]],  
                    Entity = r2.RefId([[Client1_1796]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_2043]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                }
              }
            },  
            InstanceId = [[Client1_2025]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          _Zone = [[Client1_2028]],  
          Name = [[Zone trigger 1]],  
          Position = {
            y = -1107.1875,  
            x = 27863.5,  
            InstanceId = [[Client1_2026]],  
            Class = [[Position]],  
            z = 73.65625
          },  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          Active = 1,  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Places 1]],  
              InstanceId = [[Client1_2028]],  
              Position = {
                y = -0.890625,  
                x = 0.734375,  
                InstanceId = [[Client1_2027]],  
                Class = [[Position]],  
                z = -1.234375
              },  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2030]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -0.203125,  
                    x = 5.984375,  
                    InstanceId = [[Client1_2031]],  
                    Class = [[Position]],  
                    z = 2.75
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2033]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 7.78125,  
                    x = -1.234375,  
                    InstanceId = [[Client1_2034]],  
                    Class = [[Position]],  
                    z = 2.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2036]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2.234375,  
                    x = -6.328125,  
                    InstanceId = [[Client1_2037]],  
                    Class = [[Position]],  
                    z = 0.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2039]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -6.703125,  
                    x = -0.265625,  
                    InstanceId = [[Client1_2040]],  
                    Class = [[Position]],  
                    z = 0.5625
                  }
                }
              },  
              Deletable = 0
            }
          },  
          Class = [[ZoneTrigger]]
        },  
        {
          CarnivoreRace = [[Vigorous Goari]],  
          Ghosts = {
          },  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_5443]]
          },  
          Class = [[Fauna]],  
          CarnivoresCount = [[3]],  
          HerbivoreBase = [[palette.entities.creatures.chddb4]],  
          _HerbCount = 7,  
          Base = [[palette.entities.botobjects.user_event]],  
          _CarnCount = 3,  
          _CarnId = [[Client1_5508]],  
          HerbivoresCount = [[7]],  
          _Seed = 1145022240,  
          HerbivoreRace = [[Sprightly Yubo]],  
          InheritPos = 1,  
          _HerbId = [[Client1_5505]],  
          Name = [[Fauna Feature 2]],  
          Position = {
            y = -1455.6875,  
            x = 27656.59375,  
            InstanceId = [[Client1_5444]],  
            Class = [[Position]],  
            z = 75
          },  
          HerbivoresName = [[Sprightly Yubo]],  
          InstanceId = [[Client1_5442]],  
          Components = {
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 3]],  
              InstanceId = [[Client1_5446]],  
              Position = {
                y = -1455.6875,  
                x = 27666.59375,  
                InstanceId = [[Client1_5445]],  
                Class = [[Position]],  
                z = 75
              },  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5448]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_5449]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5451]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_5452]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5454]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_5455]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5457]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_5458]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5460]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_5461]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5463]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_5464]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1
            },  
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 4]],  
              InstanceId = [[Client1_5466]],  
              Position = {
                y = -1462.6875,  
                x = 27649.59375,  
                InstanceId = [[Client1_5465]],  
                Class = [[Position]],  
                z = 75
              },  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5468]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_5469]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5471]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_5472]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5474]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_5475]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5477]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_5478]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5480]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_5481]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5483]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_5484]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1
            },  
            {
              InheritPos = 0,  
              Name = [[Food Zone 2]],  
              InstanceId = [[Client1_5486]],  
              Position = {
                y = -1445.6875,  
                x = 27656.59375,  
                InstanceId = [[Client1_5485]],  
                Class = [[Position]],  
                z = 75
              },  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5488]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_5489]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5491]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_5492]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5494]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_5495]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5497]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_5498]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5500]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_5501]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5503]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_5504]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_5505]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_5506]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.chddb4]],  
              RaceName = [[Sprightly Yubo]],  
              CrittersCount = [[7]],  
              _CrittersCount = 7,  
              SleepZone = [[Client1_5446]],  
              FoodZone = [[Client1_5486]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Herbivores]],  
              Position = {
                y = -1445.6875,  
                x = 27666.59375,  
                InstanceId = [[Client1_5507]],  
                Class = [[Position]],  
                z = 75
              },  
              _Seed = 1145022240,  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              Ghosts = {
              }
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_5508]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_5509]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.cccdb1]],  
              RaceName = [[Vigorous Goari]],  
              CrittersCount = [[3]],  
              _CrittersCount = 3,  
              SleepZone = [[Client1_5466]],  
              FoodZone = [[Client1_5446]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Carnivores]],  
              Position = {
                y = -1465.6875,  
                x = 27646.59375,  
                InstanceId = [[Client1_5510]],  
                Class = [[Position]],  
                z = 75
              },  
              _Seed = 1145022240,  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              Ghosts = {
              }
            }
          },  
          CarnivoreBase = [[palette.entities.creatures.cccdb1]]
        },  
        {
          CarnivoreRace = [[Vigorous Gingo]],  
          Ghosts = {
          },  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_5814]]
          },  
          Class = [[Fauna]],  
          CarnivoresCount = [[3]],  
          HerbivoreBase = [[palette.entities.creatures.chcdb3]],  
          _HerbCount = 12,  
          Base = [[palette.entities.botobjects.user_event]],  
          _CarnCount = 3,  
          _CarnId = [[Client1_5879]],  
          HerbivoresCount = [[12]],  
          CarnivoreBase = [[palette.entities.creatures.ccadb1]],  
          HerbivoreRace = [[Scampering Capryni]],  
          InheritPos = 1,  
          Name = [[Fauna Feature 3]],  
          _HerbId = [[Client1_5876]],  
          Position = {
            y = -1461.640625,  
            x = 27558.3125,  
            InstanceId = [[Client1_5815]],  
            Class = [[Position]],  
            z = 75
          },  
          InstanceId = [[Client1_5813]],  
          HerbivoresName = [[Scampering Capryni]],  
          Components = {
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 5]],  
              InstanceId = [[Client1_5817]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5819]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0.109375,  
                    x = 14.890625,  
                    InstanceId = [[Client1_5820]],  
                    Class = [[Position]],  
                    z = 0.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5822]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 15.6875,  
                    x = 15.359375,  
                    InstanceId = [[Client1_5823]],  
                    Class = [[Position]],  
                    z = 1.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5825]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 6.828125,  
                    x = -17.484375,  
                    InstanceId = [[Client1_5826]],  
                    Class = [[Position]],  
                    z = -0.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5828]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -5.9375,  
                    x = -17.84375,  
                    InstanceId = [[Client1_5829]],  
                    Class = [[Position]],  
                    z = -0.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5831]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -14.78125,  
                    x = -5.609375,  
                    InstanceId = [[Client1_5832]],  
                    Class = [[Position]],  
                    z = -3.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5834]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -3.640625,  
                    x = 14.34375,  
                    InstanceId = [[Client1_5835]],  
                    Class = [[Position]],  
                    z = -0.625
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1453.5,  
                x = 27588.5625,  
                InstanceId = [[Client1_5816]],  
                Class = [[Position]],  
                z = 74.265625
              }
            },  
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 6]],  
              InstanceId = [[Client1_5837]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5839]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_5840]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5842]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_5843]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5845]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_5846]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5848]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_5849]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5851]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_5852]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5854]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_5855]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1468.640625,  
                x = 27551.3125,  
                InstanceId = [[Client1_5836]],  
                Class = [[Position]],  
                z = 75
              }
            },  
            {
              InheritPos = 0,  
              Name = [[Food Zone 3]],  
              InstanceId = [[Client1_5857]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5859]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 2.984375,  
                    x = 10.03125,  
                    InstanceId = [[Client1_5860]],  
                    Class = [[Position]],  
                    z = -0.28125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5862]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 32.03125,  
                    x = 1.265625,  
                    InstanceId = [[Client1_5863]],  
                    Class = [[Position]],  
                    z = 0.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5865]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 29.34375,  
                    x = -13.84375,  
                    InstanceId = [[Client1_5866]],  
                    Class = [[Position]],  
                    z = 2.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5868]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 15.796875,  
                    x = -19.9375,  
                    InstanceId = [[Client1_5869]],  
                    Class = [[Position]],  
                    z = -0.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5871]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -7.578125,  
                    x = -2.09375,  
                    InstanceId = [[Client1_5872]],  
                    Class = [[Position]],  
                    z = 0.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_5874]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -7.78125,  
                    x = 1.875,  
                    InstanceId = [[Client1_5875]],  
                    Class = [[Position]],  
                    z = 0.28125
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1446.75,  
                x = 27455.07813,  
                InstanceId = [[Client1_5856]],  
                Class = [[Position]],  
                z = 82.5
              }
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_5876]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_5877]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.chcdb3]],  
              RaceName = [[Scampering Capryni]],  
              CrittersCount = [[12]],  
              _CrittersCount = 12,  
              SleepZone = [[Client1_5817]],  
              FoodZone = [[Client1_5857]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Herbivores]],  
              Position = {
                y = -1451.640625,  
                x = 27568.3125,  
                InstanceId = [[Client1_5878]],  
                Class = [[Position]],  
                z = 75
              },  
              Ghosts = {
              },  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              _Seed = 1145022376
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_5879]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_5880]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.ccadb1]],  
              RaceName = [[Vigorous Gingo]],  
              CrittersCount = [[3]],  
              _CrittersCount = 3,  
              SleepZone = [[Client1_5837]],  
              FoodZone = [[Client1_5817]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Carnivores]],  
              Position = {
                y = -1471.640625,  
                x = 27548.3125,  
                InstanceId = [[Client1_5881]],  
                Class = [[Position]],  
                z = 77
              },  
              Ghosts = {
              },  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              _Seed = 1145022376
            }
          },  
          _Seed = 1145022376
        },  
        {
          CarnivoreRace = [[Vigorous Gingo]],  
          Ghosts = {
          },  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_6670]]
          },  
          Class = [[Fauna]],  
          CarnivoresCount = [[1]],  
          HerbivoreBase = [[palette.entities.creatures.chddb4]],  
          _HerbCount = 7,  
          Base = [[palette.entities.botobjects.user_event]],  
          _CarnCount = 1,  
          _CarnId = [[Client1_6735]],  
          HerbivoresCount = [[7]],  
          CarnivoreBase = [[palette.entities.creatures.ccadb1]],  
          HerbivoreRace = [[Sprightly Yubo]],  
          InheritPos = 1,  
          Name = [[Fauna Feature 4]],  
          _HerbId = [[Client1_6732]],  
          Position = {
            y = -1299.734375,  
            x = 27647.625,  
            InstanceId = [[Client1_6671]],  
            Class = [[Position]],  
            z = 73
          },  
          InstanceId = [[Client1_6669]],  
          HerbivoresName = [[Sprightly Yubo]],  
          Components = {
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 7]],  
              InstanceId = [[Client1_6673]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6675]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_6676]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6678]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_6679]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6681]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_6682]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6684]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_6685]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6687]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_6688]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6690]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_6691]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1299.734375,  
                x = 27657.625,  
                InstanceId = [[Client1_6672]],  
                Class = [[Position]],  
                z = 73
              }
            },  
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 8]],  
              InstanceId = [[Client1_6693]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6695]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_6696]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6698]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_6699]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6701]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_6702]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6704]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_6705]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6707]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_6708]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6710]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_6711]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1259.140625,  
                x = 27609.92188,  
                InstanceId = [[Client1_6692]],  
                Class = [[Position]],  
                z = 74.890625
              }
            },  
            {
              InheritPos = 0,  
              Name = [[Food Zone 4]],  
              InstanceId = [[Client1_6713]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6715]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_6716]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6718]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_6719]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6721]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_6722]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6724]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_6725]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6727]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_6728]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_6730]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_6731]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Position = {
                y = -1284.140625,  
                x = 27646.03125,  
                InstanceId = [[Client1_6712]],  
                Class = [[Position]],  
                z = 73.640625
              }
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_6732]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_6733]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.chddb4]],  
              RaceName = [[Sprightly Yubo]],  
              CrittersCount = [[7]],  
              _CrittersCount = 7,  
              SleepZone = [[Client1_6673]],  
              FoodZone = [[Client1_6713]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Herbivores]],  
              Position = {
                y = -1289.734375,  
                x = 27657.625,  
                InstanceId = [[Client1_6734]],  
                Class = [[Position]],  
                z = 75
              },  
              Ghosts = {
              },  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              _Seed = 1145022701
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_6735]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_6736]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.ccadb1]],  
              RaceName = [[Vigorous Gingo]],  
              CrittersCount = [[1]],  
              _CrittersCount = 1,  
              SleepZone = [[Client1_6693]],  
              FoodZone = [[Client1_6673]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Carnivores]],  
              Position = {
                y = -1298.859375,  
                x = 27645.35938,  
                InstanceId = [[Client1_6737]],  
                Class = [[Position]],  
                z = 72.796875
              },  
              Ghosts = {
              },  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              _Seed = 1145022701
            }
          },  
          _Seed = 1145022701
        }
      },  
      Version = 5,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_11]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Act 1:Act 1]]
    },  
    {
      InstanceId = [[Client1_1999]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_2296]],  
            Class = [[LogicEntityAction]],  
            Conditions = {
            },  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_2298]],  
                  Value = r2.RefId([[Client1_2520]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_2299]],  
                Entity = r2.RefId([[Client1_2017]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[starts dialog]],  
                  InstanceId = [[Client1_2527]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_2528]],  
                Entity = r2.RefId([[Client1_2004]]),  
                Class = [[ActionStep]]
              }
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_2297]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            }
          }
        },  
        InstanceId = [[Client1_1997]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 21,  
      LocationId = [[Client1_2001]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_1998]],  
        Class = [[Position]],  
        z = 0
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_2017]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 7,  
              HandsModel = 5609774,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 3,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2015]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2636]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Sit Down]],  
                          InstanceId = [[Client1_2638]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_2639]],  
                        Entity = r2.RefId([[Client1_2017]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_2637]],  
                      Value = r2.RefId([[Client1_2522]]),  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_3754]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_3756]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_3757]],  
                        Entity = r2.RefId([[Client1_3423]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_3755]],  
                      Value = r2.RefId([[Client1_2522]]),  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_10389]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_10390]],  
                      Value = r2.RefId([[Client1_2522]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_10391]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_10392]],  
                        Entity = r2.RefId([[Client1_3423]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2520]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_2521]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2522]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_2787]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2745]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 5653038,  
              Speed = 1,  
              Angle = 2.671875,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Tattoo = 3,  
              MorphTarget3 = 4,  
              MorphTarget7 = 7,  
              ArmModel = 0,  
              Position = {
                y = -1100.796875,  
                x = 27745.79688,  
                InstanceId = [[Client1_2018]],  
                Class = [[Position]],  
                z = 73.96875
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[Faneliah]],  
              Sex = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 1,  
              Level = 0,  
              SheetClient = [[basic_matis_female.creature]]
            }
          },  
          InstanceId = [[Client1_2000]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_2004]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_2514]],  
                Class = [[LogicEntityAction]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_2515]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_2525]],  
                Class = [[LogicEntityAction]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_2529]],  
                      Value = r2.RefId([[Client1_2522]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_2530]],  
                    Entity = r2.RefId([[Client1_2017]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_2526]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                }
              }
            },  
            InstanceId = [[Client1_2002]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 1]],  
          Position = {
            y = -1102.078125,  
            x = 27743.76563,  
            InstanceId = [[Client1_2003]],  
            Class = [[Position]],  
            z = 74.109375
          },  
          Repeating = 0,  
          Components = {
            {
              Time = 3,  
              InstanceId = [[Client1_2005]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Friendly]],  
                  InstanceId = [[Client1_2006]],  
                  Who = r2.RefId([[Client1_2017]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2159]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_2007]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Pointleft]],  
                  InstanceId = [[Client1_2008]],  
                  Who = r2.RefId([[Client1_2017]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2160]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Active = 1
        },  
        {
          _BanditsLevel = 0,  
          InstanceId = [[Client1_2100]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_2101]]
          },  
          Class = [[BanditCamp]],  
          _BanditsCount = 12,  
          _Zone = [[Client1_2104]],  
          Race = [[Fyros]],  
          ZoneSize = [[nil]],  
          Base = [[palette.entities.botobjects.campfire]],  
          BanditsCount = [[12]],  
          _Race = 0,  
          BanditsLevel = [[20]],  
          InheritPos = 1,  
          Cycle = 30,  
          Name = [[Bandit's camp 1]],  
          Position = {
            y = -1156.921875,  
            x = 27509.48438,  
            InstanceId = [[Client1_2102]],  
            Class = [[Position]],  
            z = 77
          },  
          _Seed = 1144931278,  
          Active = 1,  
          Components = {
            {
              InheritPos = 0,  
              Name = [[Wander zone 1]],  
              InstanceId = [[Client1_2104]],  
              Position = {
                y = -1156.921875,  
                x = 27509.48438,  
                InstanceId = [[Client1_2103]],  
                Class = [[Position]],  
                z = 77
              },  
              Class = [[Region]],  
              Deletable = 0,  
              Points = {
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2106]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_2107]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2109]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_2110]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2112]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_2113]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2115]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_2116]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2118]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_2119]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_2121]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_2122]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              }
            }
          },  
          Ghosts = {
          }
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_3423]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_3421]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 2]],  
          Position = {
            y = -1237.796875,  
            x = 27659.125,  
            InstanceId = [[Client1_3422]],  
            Class = [[Position]],  
            z = 75
          },  
          Repeating = 1,  
          Components = {
            {
              Time = 5,  
              InstanceId = [[Client1_3424]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_3425]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_2017]]),  
                  Says = [[Client1_3426]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_3427]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_3428]],  
                  Who = r2.RefId([[Client1_830]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_2017]]),  
                  Says = [[Client1_3429]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 4,  
              InstanceId = [[Client1_3430]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_3431]],  
                  Who = r2.RefId([[Client1_2017]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_3434]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 4,  
              InstanceId = [[Client1_3432]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_3433]],  
                  Who = r2.RefId([[Client1_841]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_2017]]),  
                  Says = [[Client1_3435]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Active = 1
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 2:Act 2]],  
      Version = 5
    },  
    {
      InstanceId = [[Client1_8285]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_8283]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 1,  
      LocationId = [[Client1_8287]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_8290]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 5,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 5609774,  
              FeetColor = 5,  
              GabaritBreastSize = 6,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 3,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 6,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8288]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 5653038,  
              Speed = 0,  
              Angle = 2.046875,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Tattoo = 3,  
              MorphTarget3 = 3,  
              MorphTarget7 = 3,  
              ArmModel = 0,  
              Position = {
                y = -1237.984375,  
                x = 27659.15625,  
                InstanceId = [[Client1_8291]],  
                Class = [[Position]],  
                z = 75.375
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Faneliah]],  
              Sex = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              SheetClient = [[basic_matis_female.creature]]
            }
          },  
          InstanceId = [[Client1_8286]]
        },  
        {
          InstanceId = [[Client1_8354]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_8353]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              InstanceId = [[Client1_8334]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8332]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_8398]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_8399]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_8373]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_8400]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_8373]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kipee]],  
              Position = {
                y = -1511.4375,  
                x = 27605.57813,  
                InstanceId = [[Client1_8335]],  
                Class = [[Position]],  
                z = 93.703125
              },  
              Angle = 0.9094356298,  
              Base = [[palette.entities.creatures.ckhib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8330]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8328]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Assault Kirosta]],  
              Position = {
                y = -1517.359375,  
                x = 27587.98438,  
                InstanceId = [[Client1_8331]],  
                Class = [[Position]],  
                z = 90.390625
              },  
              Angle = 0.9094356298,  
              Base = [[palette.entities.creatures.ckfrb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8326]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8324]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Assault Kirosta]],  
              Position = {
                y = -1532.828125,  
                x = 27610.48438,  
                InstanceId = [[Client1_8327]],  
                Class = [[Position]],  
                z = 91.109375
              },  
              Angle = 0.9094356298,  
              Base = [[palette.entities.creatures.ckfrb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8346]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8344]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kizarak]],  
              Position = {
                y = -1534.296875,  
                x = 27604.89063,  
                InstanceId = [[Client1_8347]],  
                Class = [[Position]],  
                z = 91.015625
              },  
              Angle = 0.9094356298,  
              Base = [[palette.entities.creatures.ckcib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8342]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8340]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                y = -1519.421875,  
                x = 27577,  
                InstanceId = [[Client1_8343]],  
                Class = [[Position]],  
                z = 90.109375
              },  
              Angle = 0.9094356298,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8338]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8336]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                y = -1524.171875,  
                x = 27570.79688,  
                InstanceId = [[Client1_8339]],  
                Class = [[Position]],  
                z = 90.546875
              },  
              Angle = 0.9094356298,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_8350]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8348]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kizoar]],  
              Position = {
                y = -1536.015625,  
                x = 27561.26563,  
                InstanceId = [[Client1_8351]],  
                Class = [[Position]],  
                z = 91
              },  
              Angle = 0.9094356298,  
              Base = [[palette.entities.creatures.ckiib2]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_8352]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        }
      },  
      Events = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_8284]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Name = [[Act 3:Act 3]],  
      Version = 5,  
      Title = [[]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 7,  
        InstanceId = [[Client1_894]],  
        Class = [[TextManagerEntry]],  
        Text = [[I worry about her...bandits are so terrible.]]
      },  
      {
        Count = 10,  
        InstanceId = [[Client1_895]],  
        Class = [[TextManagerEntry]],  
        Text = [[I'm sure they bring her to the north east of here.]]
      },  
      {
        Count = 14,  
        InstanceId = [[Client1_896]],  
        Class = [[TextManagerEntry]],  
        Text = [[Who will help Princess Faneliah? ]]
      },  
      {
        Count = 7,  
        InstanceId = [[Client1_899]],  
        Class = [[TextManagerEntry]],  
        Text = [[Maybe a GUARD could do his job??? Ahem...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_904]],  
        Class = [[TextManagerEntry]],  
        Text = [[They act as if they don't hear us]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_905]],  
        Class = [[TextManagerEntry]],  
        Text = [[It would be faster if we go ourselves]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_908]],  
        Class = [[TextManagerEntry]],  
        Text = [[I'm afraid of the kittins...]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_928]],  
        Class = [[TextManagerEntry]],  
        Text = [[Woow, beautifull butterflies!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_931]],  
        Class = [[TextManagerEntry]],  
        Text = [[I want to be a butterfly too...]]
      },  
      {
        Count = 10,  
        InstanceId = [[Client1_1062]],  
        Class = [[TextManagerEntry]],  
        Text = [[I can't help the princess, I have to feed my yubo...]]
      },  
      {
        Count = 7,  
        InstanceId = [[Client1_1065]],  
        Class = [[TextManagerEntry]],  
        Text = [[I can't help the princess, my wife will be jealous...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1072]],  
        Class = [[TextManagerEntry]],  
        Text = [[I can't help the princess, I'm allergic to the kittins...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1073]],  
        Class = [[TextManagerEntry]],  
        Text = [[I can't help the princess, she frighten me...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1074]],  
        Class = [[TextManagerEntry]],  
        Text = [[I can't help the princess, she frightens me...]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1440]],  
        Class = [[TextManagerEntry]],  
        Text = [[Help!!!!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_1443]],  
        Class = [[TextManagerEntry]],  
        Text = [[I can't bear their stupid jokes anymore!!!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_1446]],  
        Class = [[TextManagerEntry]],  
        Text = [[Come here and help me!!!!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1610]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hey!!! Get out!!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1677]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hey!!! Get out!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_2009]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you! I was afraid nobody would come to save me!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_2010]],  
        Class = [[TextManagerEntry]],  
        Text = [[Let's return to the village]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_2159]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh thanks!! You're my hero!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_2160]],  
        Class = [[TextManagerEntry]],  
        Text = [[Let's return to the village I miss my friends!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_2892]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please help me!!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_2893]],  
        Class = [[TextManagerEntry]],  
        Text = [[They smell so bad, I can't stay here anymore!!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_2894]],  
        Class = [[TextManagerEntry]],  
        Text = [[Come here and help me!!]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_3426]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh Princess, you're safe! I'm so happy!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_3429]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are the bandits all dead?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_3434]],  
        Class = [[TextManagerEntry]],  
        Text = [[No! I heard they're hiding somewhere near the village and the cliffs]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_3435]],  
        Class = [[TextManagerEntry]],  
        Text = [[Maybe we should hide somewhere?]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}