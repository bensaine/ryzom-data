scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_367]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 32,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_369]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_370]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_372]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_371]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_373]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_374]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_376]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_375]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_380]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_381]],  
                x = 29504.82813,  
                y = -2047.3125,  
                z = -21.75
              },  
              Angle = 1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_378]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 14,  
              HairType = 4910,  
              HairColor = 2,  
              Tattoo = 13,  
              EyesColor = 6,  
              MorphTarget1 = 5,  
              MorphTarget2 = 0,  
              MorphTarget3 = 2,  
              MorphTarget4 = 6,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              Sex = 1,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 4,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[matis-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_377]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_368]]
  }
}