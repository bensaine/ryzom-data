scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.2]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 2,  
    ChatSequence = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    ActivityStep = 0,  
    TimerFeature = 0,  
    NpcCustom = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    Behavior = 0,  
    EventType = 0,  
    Position = 0,  
    LogicEntityReaction = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    ActionStep = 0
  },  
  Acts = {
    {
      Cost = 6,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_279]],  
            Actions = {
              {
                Action = {
                  Type = [[Desactivate]],  
                  InstanceId = [[Client1_284]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_285]],  
                Entity = r2.RefId([[Client1_282]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Scenario Started]],  
              InstanceId = [[Client1_278]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Title = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Permanent]],  
      Version = 2,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_22]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_20]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -1540.71875,  
                x = 22394.39063,  
                InstanceId = [[Client1_23]],  
                Class = [[Position]],  
                z = 84.234375
              },  
              Angle = 2.34375,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_26]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_24]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              Position = {
                y = -1543.46875,  
                x = 22388.96875,  
                InstanceId = [[Client1_27]],  
                Class = [[Position]],  
                z = 83.96875
              },  
              Angle = 2.34375,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_30]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_28]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_31]],  
                x = 22388.45313,  
                y = -1537.953125,  
                z = 84
              },  
              Angle = 2.0625,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_38]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_36]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_202]],  
                    Name = [[]],  
                    InstanceId = [[Client1_205]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_204]]
                  },  
                  {
                    LogicEntityAction = [[Client1_274]],  
                    Name = [[]],  
                    InstanceId = [[Client1_277]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_276]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Pizza]],  
                    InstanceId = [[Client1_190]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_191]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_192]],  
                            Who = [[Client1_38]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_193]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_212]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_213]],  
                          Value = r2.RefId([[Client1_216]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_214]],  
                        Entity = r2.RefId([[Client1_46]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_211]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              ActivitiesId = {
              },  
              Name = [[Paf le yubo]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = 2,  
              Base = [[palette.entities.creatures.chddb2]],  
              Position = {
                y = -1541.46875,  
                x = 22387.3125,  
                InstanceId = [[Client1_39]],  
                Class = [[Position]],  
                z = 83.921875
              }
            },  
            {
              InstanceId = [[Client1_282]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_280]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_279]],  
                    Name = [[]],  
                    InstanceId = [[Client1_286]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_285]]
                  },  
                  {
                    LogicEntityAction = [[Client1_241]],  
                    Name = [[]],  
                    InstanceId = [[Client1_292]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_291]]
                  },  
                  {
                    LogicEntityAction = [[Client1_294]],  
                    Name = [[]],  
                    InstanceId = [[Client1_297]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_296]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              ActivitiesId = {
              },  
              Name = [[Hard Kipee]],  
              Position = {
                y = -1542.890625,  
                x = 22384.32813,  
                InstanceId = [[Client1_283]],  
                Class = [[Position]],  
                z = 83.8125
              },  
              BotAttackable = 0,  
              Angle = 1.375,  
              Base = [[palette.entities.creatures.ckhdb4]],  
              PlayerAttackable = 0
            },  
            {
              InstanceId = [[Client1_317]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_315]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tombe du precedent barbie yubo]],  
              Position = {
                y = -1538.796875,  
                x = 22400.25,  
                InstanceId = [[Client1_318]],  
                Class = [[Position]],  
                z = 84.84375
              },  
              Angle = 2.480549335,  
              Base = [[palette.entities.botobjects.tomb_5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_325]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_323]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[olao 1]],  
              Position = {
                y = -1540.234375,  
                x = 22398.51563,  
                InstanceId = [[Client1_326]],  
                Class = [[Position]],  
                z = 84.578125
              },  
              Angle = 2.53125,  
              Base = [[palette.entities.botobjects.ju_s3_bush_tree]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_337]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_335]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bamboo II 1]],  
              Position = {
                y = -1546.375,  
                x = 22386.54688,  
                InstanceId = [[Client1_338]],  
                Class = [[Position]],  
                z = 84.0625
              },  
              Angle = -1.28125,  
              Base = [[palette.entities.botobjects.ju_s3_bamboo]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_341]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_339]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bamboo II 2]],  
              Position = {
                y = -1547.734375,  
                x = 22386.3125,  
                InstanceId = [[Client1_342]],  
                Class = [[Position]],  
                z = 84.15625
              },  
              Angle = -0.296875,  
              Base = [[palette.entities.botobjects.ju_s3_bamboo]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_345]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_343]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bamboo II 3]],  
              Position = {
                y = -1547.34375,  
                x = 22385.26563,  
                InstanceId = [[Client1_346]],  
                Class = [[Position]],  
                z = 84.15625
              },  
              Angle = -2.421875,  
              Base = [[palette.entities.botobjects.ju_s3_bamboo]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_349]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_347]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami hut 1]],  
              Position = {
                y = -1545.53125,  
                x = 22393.6875,  
                InstanceId = [[Client1_350]],  
                Class = [[Position]],  
                z = 83.8125
              },  
              Angle = 2.109375,  
              Base = [[palette.entities.botobjects.kami_hut]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_353]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_354]],  
                x = 22388.65625,  
                y = -1542.859375,  
                z = 83.90625
              },  
              Angle = 2.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_351]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[street lamp 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_357]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_358]],  
                x = 22394.0625,  
                y = -1540.09375,  
                z = 84.171875
              },  
              Angle = 1.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_355]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[street lamp 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_365]],  
              Base = [[palette.entities.botobjects.fx_ju_colibrisb]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_366]],  
                x = 22387.3125,  
                y = -1538.984375,  
                z = 83.9375
              },  
              Angle = 2.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_363]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[hummingbird 1]]
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_294]],  
            Actions = {
              {
                Action = {
                  Type = [[Desactivate]],  
                  InstanceId = [[Client1_295]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_296]],  
                Entity = r2.RefId([[Client1_282]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_293]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
        [[Client1_80]]
      },  
      Events = {
      },  
      Title = [[Act 1]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Act 1]],  
      Version = 2,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_42]],  
              ActivitiesId = {
                [[Client1_80]]
              },  
              HairType = 2862,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 6,  
              HandsModel = 5605678,  
              FeetColor = 5,  
              GabaritBreastSize = 3,  
              GabaritHeight = 9,  
              HairColor = 2,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_179]],  
                    Name = [[]],  
                    InstanceId = [[Client1_182]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_181]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Dis]],  
                    InstanceId = [[Client1_64]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_65]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_66]],  
                            Who = [[Client1_42]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_46]]),  
                            Says = [[Client1_272]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[mort]],  
                    InstanceId = [[Client1_172]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 5,  
                        InstanceId = [[Client1_173]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_174]],  
                            Who = [[Client1_42]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_46]]),  
                            Says = [[Client1_175]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_166]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_167]],  
                          Value = r2.RefId([[Client1_68]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_168]],  
                        Entity = r2.RefId([[Client1_46]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_165]],  
                      Value = r2.RefId([[Client1_64]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_202]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[Kill]],  
                          InstanceId = [[Client1_203]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_204]],  
                        Entity = r2.RefId([[Client1_38]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_201]],  
                      Value = r2.RefId([[Client1_172]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_80]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_81]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_64]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 0,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_43]],  
                x = 22391.42188,  
                y = -1537.59375,  
                z = 84.21875
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[Ken]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_46]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 4,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 2,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 4,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_166]],  
                    Name = [[]],  
                    InstanceId = [[Client1_169]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_168]]
                  },  
                  {
                    LogicEntityAction = [[Client1_212]],  
                    Name = [[]],  
                    InstanceId = [[Client1_215]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_214]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[ben]],  
                    InstanceId = [[Client1_68]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 2,  
                        InstanceId = [[Client1_69]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_70]],  
                            Who = [[Client1_46]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_82]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[Veto]],  
                    InstanceId = [[Client1_83]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_84]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_85]],  
                            Who = [[Client1_46]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_86]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[ouais]],  
                    InstanceId = [[Client1_216]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 5,  
                        InstanceId = [[Client1_217]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_218]],  
                            Who = [[Client1_46]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_222]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_148]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_149]],  
                          Value = r2.RefId([[Client1_83]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_150]],  
                        Entity = r2.RefId([[Client1_46]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_147]],  
                      Value = r2.RefId([[Client1_68]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_179]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_180]],  
                          Value = r2.RefId([[Client1_172]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_181]],  
                        Entity = r2.RefId([[Client1_42]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_178]],  
                      Value = r2.RefId([[Client1_83]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 0,  
              Angle = -2,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 5610542,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_47]],  
                x = 22389.3125,  
                y = -1535.078125,  
                z = 84.125
              },  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Barbie va a la plage]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 7,  
              Tattoo = 25
            }
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          Secondes = 40,  
          Behavior = {
            InstanceId = [[Client1_253]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_256]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Start Act]],  
                      InstanceId = [[Client1_257]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_258]],  
                    Entity = r2.RefId([[Client1_14]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[On Trigger]],  
                  InstanceId = [[Client1_255]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[TimerFeature]],  
          InheritPos = 1,  
          Minutes = 0,  
          Name = [[Passage acte 2]],  
          Position = {
            y = -1537.8125,  
            x = 22384.20313,  
            InstanceId = [[Client1_254]],  
            Class = [[Position]],  
            z = 83.71875
          },  
          InstanceId = [[Client1_252]],  
          Components = {
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          Cyclic = 0
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_10]]
    },  
    {
      Cost = 3,  
      Behavior = {
        InstanceId = [[Client1_12]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_274]],  
            Actions = {
              {
                Action = {
                  Type = [[Desactivate]],  
                  InstanceId = [[Client1_275]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_276]],  
                Entity = r2.RefId([[Client1_38]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_273]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          }
        },  
        Reactions = {
          {
            LogicEntityAction = [[Client1_256]],  
            Name = [[]],  
            InstanceId = [[Client1_259]],  
            Class = [[LogicEntityReaction]],  
            ActionStep = [[Client1_258]]
          }
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
        [[Client1_228]]
      },  
      InstanceId = [[Client1_14]],  
      Name = [[Act 2]],  
      Title = [[Act 2]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_50]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 11,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 1,  
              GabaritHeight = 6,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 14,  
              HandsColor = 2,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_241]],  
                    Name = [[]],  
                    InstanceId = [[Client1_266]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_265]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[burne]],  
                    InstanceId = [[Client1_260]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 4,  
                        InstanceId = [[Client1_261]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_262]],  
                            Who = [[Client1_50]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_58]]),  
                            Says = [[Client1_263]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_268]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_269]],  
                          Value = r2.RefId([[Client1_248]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_270]],  
                        Entity = r2.RefId([[Client1_58]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_267]],  
                      Value = r2.RefId([[Client1_260]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 5605422,  
              Angle = -2.140625,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 5606446,  
              Position = {
                y = -1537.390625,  
                x = 22390.79688,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = 84.1875
              },  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Ken]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 3,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_54]],  
              ActivitiesId = {
              },  
              HairType = 5622318,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 3,  
              GabaritBreastSize = 10,  
              GabaritHeight = 4,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 10,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_52]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 6,  
              FeetModel = 5653038,  
              Angle = -2.140625,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 5610542,  
              Position = {
                y = -1535.53125,  
                x = 22388.54688,  
                InstanceId = [[Client1_55]],  
                Class = [[Position]],  
                z = 84.078125
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[Barbie va a la plage]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 1,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_58]],  
              ActivitiesId = {
                [[Client1_228]]
              },  
              HairType = 9006,  
              TrouserColor = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 12,  
              HandsModel = 5617966,  
              FeetColor = 1,  
              GabaritBreastSize = 11,  
              GabaritHeight = 7,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 12,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_56]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_268]],  
                    Name = [[]],  
                    InstanceId = [[Client1_271]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_270]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[probl]],  
                    InstanceId = [[Client1_230]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 5,  
                        InstanceId = [[Client1_231]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_232]],  
                            Who = [[Client1_58]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_54]]),  
                            Says = [[Client1_233]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[Certe]],  
                    InstanceId = [[Client1_248]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_249]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_250]],  
                            Who = [[Client1_58]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_251]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_241]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_264]],  
                          Value = r2.RefId([[Client1_260]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_265]],  
                        Entity = r2.RefId([[Client1_50]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_290]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_291]],  
                        Entity = r2.RefId([[Client1_282]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_240]],  
                      Value = r2.RefId([[Client1_230]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_228]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_229]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_230]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Angle = -0.109375,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              SheetClient = [[basic_zorai_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Position = {
                y = -1538.265625,  
                x = 22384.67188,  
                InstanceId = [[Client1_59]],  
                Class = [[Position]],  
                z = 83.765625
              },  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[Le Barbie Vendeur de yubo]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 6,  
              Tattoo = 0
            }
          },  
          InstanceId = [[Client1_15]]
        }
      },  
      Version = 2,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_13]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Events = {
      },  
      ManualWeather = 0,  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 12,  
        InstanceId = [[Client1_63]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_67]],  
        Class = [[TextManagerEntry]],  
        Text = [[Dis Barbie je crois que le yubo a bouff� un truc louhe ... ]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_71]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oui Ken, j'ai cru comprendre j'ai march� dans un truc louche aussi ce matin ...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_72]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oui Ken, j'ai cru comprendre j'ai march� dans un truc louche aussi ce matin ...]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_82]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oui Ken, j'ai cru comprendre ... j'ai march� dans un truc louche aussi ce matin ...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_86]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bouge pas j'appelle le Barbie Veto ... *sort son portable rose et compose le numero du barbie veto*]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_175]],  
        Class = [[TextManagerEntry]],  
        Text = [[Ha mon avis vu la tete du yubo, c'est plus un barbie veto qu'il lui faut mais un barbie taxidermiste ...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_193]],  
        Class = [[TextManagerEntry]],  
        Text = [[<J'aurais jamais du bouffer la derniere part de pizza tartiflette ... je crois que je vais ...>]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_219]],  
        Class = [[TextManagerEntry]],  
        Text = [[...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_220]],  
        Class = [[TextManagerEntry]],  
        Text = [[...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_221]],  
        Class = [[TextManagerEntry]],  
        Text = [[...\n ha ouais ... \n effectivement il vient de claquer le con ...]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_222]],  
        Class = [[TextManagerEntry]],  
        Text = [[...ha ouais ... effectivement il vient de claquer le con ... Bon ben faut appeler un barbie vendeur de yubo ...]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_233]],  
        Class = [[TextManagerEntry]],  
        Text = [[Un probl�me de yubo ? Pas de probleme je vous le remplace immediatement !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_251]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bon okay il est pas tres ressemblant mais bon il est gratuit vous allez pas me casser les noix !]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_263]],  
        Class = [[TextManagerEntry]],  
        Text = [[Heu ... j'crois qu'il y a une burne dans l'potage la quand meme ...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_272]],  
        Class = [[TextManagerEntry]],  
        Text = [[Dis Barbie je crois que le yubo a bouff� un truc louche ... ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_301]],  
        Class = [[TextManagerEntry]],  
        Text = [[*prout*]]
      }
    }
  }
}