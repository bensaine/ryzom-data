#!/bin/bash

if [[ "$OSTYPE" == "msys" ]]
then
	winpty /r/python27/python $*
else
	python $*
fi
