scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1132]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 18,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1134]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_1135]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1141]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1142]],  
                x = 27940.65625,  
                y = -1967.8125,  
                z = -3.140625
              },  
              Angle = 1.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1139]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Exit]]
            }
          },  
          InstanceId = [[Client1_1136]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1133]]
  }
}