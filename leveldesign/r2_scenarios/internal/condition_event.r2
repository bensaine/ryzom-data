scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 2,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_24]],  
        [[Client1_25]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_10]],  
              ActivitiesId = {
                [[Client1_24]]
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 3,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 11,  
              GabaritHeight = 6,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_34]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_35]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_36]],  
                            Who = r2.RefId([[Client1_10]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_37]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) fyros-dressed civilian 1 says nono...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_24]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Angle = 0.328125,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Position = {
                y = -1519.3125,  
                x = 22377.35938,  
                InstanceId = [[Client1_11]],  
                Class = [[Position]],  
                z = 79.984375
              },  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 5,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[fyros-dressed civilian 1]],  
              Sex = 0,  
              WeaponRightHand = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 1,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_14]],  
              ActivitiesId = {
                [[Client1_25]]
              },  
              HairType = 5622830,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 3,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 9,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_28]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of activity step Activity 1 : Stand Still for 5sec' event of matis-dressed civilian 1]],  
                    InstanceId = [[Client1_31]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_30]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_16]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_17]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_18]],  
                            Who = r2.RefId([[Client1_14]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_23]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) matis-dressed civilian 1 says chat...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of activity step Activity 1 : Stand Still for 5sec' event]],  
                    InstanceId = [[Client1_28]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_27]],  
                      Value = r2.RefId([[Client1_26]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_29]],  
                          Value = r2.RefId([[Client1_16]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_30]],  
                        Entity = r2.RefId([[Client1_14]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                      {
                        InstanceId = [[Client1_39]],  
                        Entity = r2.RefId([[Client1_10]]),  
                        Class = [[ConditionStep]],  
                        Condition = {
                          Type = [[is in chat sequence]],  
                          InstanceId = [[Client1_38]],  
                          Value = r2.RefId([[Client1_34]]),  
                          Class = [[ConditionType]]
                        }
                      }
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_25]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[5]],  
                        InstanceId = [[Client1_26]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Stand Still for 5sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 0,  
              Angle = 0.328125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Position = {
                y = -1518.5625,  
                x = 22374.14063,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = 79.09375
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[matis-dressed civilian 1]],  
              Sex = 0,  
              WeaponRightHand = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 7,  
              Tattoo = 1
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_6]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_19]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_20]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_21]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_22]],  
        Class = [[TextManagerEntry]],  
        Text = [[chat1
]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_23]],  
        Class = [[TextManagerEntry]],  
        Text = [[chat1]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_37]],  
        Class = [[TextManagerEntry]],  
        Text = [[nonononono]]
      }
    }
  }
}