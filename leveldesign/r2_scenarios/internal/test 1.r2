scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 1,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_27]],  
              Base = [[palette.entities.botobjects.fx_goo_mamal]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_28]],  
                x = 23432.89063,  
                y = -1078.4375,  
                z = 32.703125
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_25]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[goo infested carcass II 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_43]],  
              Base = [[palette.entities.botobjects.fx_ju_bugsa]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_44]],  
                x = 23453.51563,  
                y = -1052.859375,  
                z = 34.265625
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_41]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[butterflies 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_47]],  
              Base = [[palette.entities.botobjects.tomb_5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_48]],  
                x = 23445.76563,  
                y = -1053.65625,  
                z = 34.40625
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_45]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tomb stone 5 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_51]],  
              Base = [[palette.entities.botobjects.merchant_armor_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_52]],  
                x = 23454.875,  
                y = -1053.046875,  
                z = 34.109375
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_49]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker armor sign 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_55]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_56]],  
                x = 23437,  
                y = -1083.484375,  
                z = 31.953125
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_53]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[house ruin 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_59]],  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_60]],  
                x = 23449.04688,  
                y = -1050.515625,  
                z = 34.703125
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_57]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client2_102]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_101]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_104]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_105]],  
                    x = 23511.23438,  
                    y = -1093.203125,  
                    z = 32.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_107]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_108]],  
                    x = 23511.6875,  
                    y = -1088.5,  
                    z = 33.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_110]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_111]],  
                    x = 23512.42188,  
                    y = -1080.984375,  
                    z = 33.703125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client2_115]],  
              Name = [[Route 2]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_114]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_117]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_118]],  
                    x = 23522.79688,  
                    y = -1119.3125,  
                    z = 33.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_120]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_121]],  
                    x = 23518.45313,  
                    y = -1126.296875,  
                    z = 33.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_123]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_124]],  
                    x = 23531.48438,  
                    y = -1136.546875,  
                    z = 33.390625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client2_128]],  
              Name = [[Place 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_127]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_130]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_131]],  
                    x = 23464.96875,  
                    y = -1135.609375,  
                    z = 34.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_133]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_134]],  
                    x = 23464.96875,  
                    y = -1135.609375,  
                    z = 34.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_136]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_137]],  
                    x = 23480.40625,  
                    y = -1140.265625,  
                    z = 33.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_139]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_140]],  
                    x = 23481.0625,  
                    y = -1203.21875,  
                    z = 31.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_142]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_143]],  
                    x = 23464.96875,  
                    y = -1147.234375,  
                    z = 35.515625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client2_160]],  
              Name = [[Route 3]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_159]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_162]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_163]],  
                    x = 23437,  
                    y = -1067.984375,  
                    z = 33.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_165]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_166]],  
                    x = 23430.14063,  
                    y = -1059.359375,  
                    z = 33.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client2_168]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_169]],  
                    x = 23421.75,  
                    y = -1061.125,  
                    z = 33.09375
                  },  
                  InheritPos = 1
                }
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_4]],  
      ManualWeather = 0,  
      WeatherValue = 0
    },  
    {
      Cost = 21,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client2_112]],  
        [[Client2_125]],  
        [[Client2_144]],  
        [[Client2_157]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_3]],  
              Base = [[palette.entities.creatures.cpefd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_4]],  
                x = 23453.35938,  
                y = -1059.03125,  
                z = 34.6875
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_1]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Noxious Cratcha]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_11]],  
              Base = [[palette.entities.creatures.chnfd1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_12]],  
                x = 23440.3125,  
                y = -1066.171875,  
                z = 33.8125
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_9]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_157]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_170]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client2_160]],  
                        Name = [[Activity 1 : Follow Route Route 3 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Gruff Bawaab]],  
              ActivitiesId = {
                [[Client2_157]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_15]],  
              Base = [[palette.entities.creatures.cbajd3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_16]],  
                x = 23444.34375,  
                y = -1055.234375,  
                z = 34.390625
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_13]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Mocking Igara]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_19]],  
              Base = [[palette.entities.creatures.ckdid4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_20]],  
                x = 23453.39063,  
                y = -1127.296875,  
                z = 33.765625
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_17]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_144]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_145]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client2_128]],  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Warrior Kincher]],  
              ActivitiesId = {
                [[Client2_144]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_23]],  
              Base = [[palette.entities.creatures.cdbjd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_24]],  
                x = 23459.60938,  
                y = -1084.90625,  
                z = 29.9375
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_21]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Lurking Gibbai]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_31]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_32]],  
                x = 23455.46875,  
                y = -1057.265625,  
                z = 34.40625
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_29]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burning Salamander 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_35]],  
              Base = [[palette.entities.creatures.chddb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_36]],  
                x = 23456.875,  
                y = -1094.5,  
                z = 30.109375
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_33]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scampering Yubo]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_39]],  
              Base = [[palette.entities.creatures.ccadb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_40]],  
                x = 23460.01563,  
                y = -1099.390625,  
                z = 30.796875
              },  
              Angle = 1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_37]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_63]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_64]],  
                x = 23518.32813,  
                y = -1108.328125,  
                z = 32.921875
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_61]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 4,  
              HairType = 3118,  
              HairColor = 0,  
              Tattoo = 14,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 6,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 2,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[fyros-dressed civilian 1]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_67]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_68]],  
                x = 23518.65625,  
                y = -1111.828125,  
                z = 32.921875
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_65]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 14,  
              HairType = 5622574,  
              HairColor = 1,  
              Tattoo = 7,  
              EyesColor = 6,  
              MorphTarget1 = 5,  
              MorphTarget2 = 2,  
              MorphTarget3 = 3,  
              MorphTarget4 = 6,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5610030,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 2,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 5,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[matis-dressed civilian 1]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_71]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_72]],  
                x = 23514.60938,  
                y = -1108.765625,  
                z = 32.84375
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_69]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 8,  
              HairType = 7470,  
              HairColor = 3,  
              Tattoo = 13,  
              EyesColor = 6,  
              MorphTarget1 = 1,  
              MorphTarget2 = 4,  
              MorphTarget3 = 1,  
              MorphTarget4 = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 7,  
              MorphTarget8 = 6,  
              Sex = 0,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 5653550,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[tryker-dressed civilian 1]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_75]],  
              Base = [[palette.entities.npcs.civils.z_civil_120]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_76]],  
                x = 23511.82813,  
                y = -1107.71875,  
                z = 32.765625
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_73]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 6,  
              HairType = 8238,  
              HairColor = 4,  
              Tattoo = 29,  
              EyesColor = 0,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 2,  
              MorphTarget4 = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 1,  
              MorphTarget7 = 5,  
              MorphTarget8 = 0,  
              Sex = 1,  
              JacketModel = 6736686,  
              TrouserModel = 6735662,  
              FeetModel = 5617710,  
              HandsModel = 5617966,  
              ArmModel = 5618478,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 3,  
              FeetColor = 2,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[zorai-dressed civilian 1]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_zorai_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_d2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_79]],  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_80]],  
                x = 23510.40625,  
                y = -1103.46875,  
                z = 32.640625
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_77]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 11,  
              HairType = 2606,  
              HairColor = 2,  
              Tattoo = 0,  
              EyesColor = 0,  
              MorphTarget1 = 2,  
              MorphTarget2 = 4,  
              MorphTarget3 = 5,  
              MorphTarget4 = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6706990,  
              TrouserModel = 6706222,  
              FeetModel = 6704942,  
              HandsModel = 6705710,  
              ArmModel = 6706478,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 1,  
              FeetColor = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6756910,  
              Name = [[desert armsman 1]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_83]],  
              Base = [[palette.entities.npcs.bandits.m_mage_celestial_curser_120]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_84]],  
                x = 23510.73438,  
                y = -1098.671875,  
                z = 32.703125
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_81]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_112]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_113]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client2_102]],  
                        Name = [[Activity 1 : Repeat Route Route 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Repeat Road]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 5622830,  
              HairColor = 2,  
              Tattoo = 11,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 6,  
              MorphTarget4 = 6,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 4,  
              MorphTarget8 = 3,  
              Sex = 0,  
              JacketModel = 6715182,  
              TrouserModel = 6708526,  
              FeetModel = 5653038,  
              HandsModel = 6713646,  
              ArmModel = 5610286,  
              JacketColor = 3,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6934830,  
              Name = [[forest illusionist 1]],  
              ActivitiesId = {
                [[Client2_112]]
              },  
              SheetClient = [[basic_matis_male.creature]],  
              Sheet = [[ring_magic_curser_fear_d2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client2_87]],  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_120]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_88]],  
                x = 23506.95313,  
                y = -1102.796875,  
                z = 32.421875
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_85]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 2,  
              HairType = 6958,  
              HairColor = 2,  
              Tattoo = 23,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 0,  
              MorphTarget3 = 1,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 6725934,  
              TrouserModel = 5614126,  
              FeetModel = 6723886,  
              HandsModel = 6724398,  
              ArmModel = 6725422,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 2,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6935598,  
              Name = [[lakeland elementalist 1]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_magic_damage_dealer_acid_d2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_91]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_d]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_92]],  
                x = 23503.03125,  
                y = -1101.828125,  
                z = 32.140625
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_89]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Ardent Genius 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_95]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_d]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_96]],  
                x = 23528.875,  
                y = -1116.734375,  
                z = 33.078125
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_93]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_125]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_126]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client2_115]],  
                        Name = [[Activity 1 : Follow Route Route 2 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Hairy Colossus 1]],  
              ActivitiesId = {
                [[Client2_125]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_99]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_d]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_100]],  
                x = 23514.01563,  
                y = -1098.375,  
                z = 32.859375
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_97]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_10]],  
              Base = [[palette.entities.npcs.bandits.f_light_melee_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_11]],  
                x = 23612.45313,  
                y = -1111.890625,  
                z = 33.09375
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_8]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 4,  
              HairType = 2350,  
              HairColor = 4,  
              Tattoo = 29,  
              EyesColor = 4,  
              MorphTarget1 = 3,  
              MorphTarget2 = 6,  
              MorphTarget3 = 2,  
              MorphTarget4 = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              Sex = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 4,  
              WeaponRightHand = 5594414,  
              Name = [[desert milicia 1]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_light_melee_pierce_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_14]],  
              Base = [[palette.entities.npcs.bandits.f_mage_atysian_curser_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_15]],  
                x = 23613.40625,  
                y = -1113.703125,  
                z = 33.109375
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_12]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 0,  
              HairType = 2350,  
              HairColor = 4,  
              Tattoo = 2,  
              EyesColor = 3,  
              MorphTarget1 = 2,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 5,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5658414,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 5,  
              WeaponRightHand = 6933806,  
              Name = [[desert tormentor 1]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_magic_curser_blind_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_18]],  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19]],  
                x = 23614.73438,  
                y = -1112.65625,  
                z = 33.09375
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_16]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 2862,  
              HairColor = 3,  
              Tattoo = 9,  
              EyesColor = 5,  
              MorphTarget1 = 0,  
              MorphTarget2 = 6,  
              MorphTarget3 = 5,  
              MorphTarget4 = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5606190,  
              JacketColor = 4,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 4,  
              WeaponRightHand = 5593646,  
              Name = [[desert warrior 1]],  
              WeaponLeftHand = 5637166,  
              ActivitiesId = {
              },  
              Sheet = [[ring_melee_tank_blunt_b2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_6]],  
      ManualWeather = 0,  
      WeatherValue = 0
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
    }
  },  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    TextManager = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcCustom = 0,  
    ActivityStep = 0,  
    Position = 0,  
    Region = 0,  
    Road = 0,  
    WayPoint = 0,  
    DefaultFeature = 0
  }
}