#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SCRIPT_WDIR=$(cygpath.exe  -w -a "$SCRIPT_DIR/..")

PATH="$PATH:$SCRIPT_DIR/../useful_stuff/_tools"

while read -r line
do
	if [[ ${line:0:2} == "R:" ]]
	then
		if [[ "${line:8:-1}" != "$SCRIPT_WDIR\\" ]]
		then
			zenity --question --text="R: has been allready affected\n\nDo you want re-affect them?"
			if [[ "$?" == "0" ]]
			then
				subst //D R:
				subst "R:" "$SCRIPT_WDIR\\"
				exit
			fi
		fi
	fi
done < <(subst)

subst //D "R:"
subst "R:" "$SCRIPT_WDIR\\"
