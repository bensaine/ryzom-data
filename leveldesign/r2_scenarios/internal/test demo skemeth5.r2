scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_1023]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes36]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Camp des guardes]],  
      Season = [[Spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint01]]
    },  
    {
      InstanceId = [[Client1_1826]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes04]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Camp de bandits]],  
      Season = [[Summer]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint02]]
    }
  },  
  InstanceId = [[Client1_1014]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_1012]]
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
    {
      SheetId = 8657710,  
      Name = [[Bouteille]],  
      InstanceId = [[Client1_2566]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    }
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_1013]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    ChatAction = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    TextManager = 0,  
    LogicEntityAction = 0,  
    EasterEgg = 0,  
    RequestItem = 0,  
    TextManagerEntry = 0,  
    ChatStep = 0,  
    ChatSequence = 0,  
    DefaultFeature = 0,  
    RegionVertex = 0,  
    Region = 0,  
    ZoneTrigger = 1,  
    PlotItem = 0,  
    Road = 0,  
    WayPoint = 0,  
    ActionStep = 1,  
    ActivityStep = 1,  
    TalkTo = 0,  
    NpcGrpFeature = 1,  
    EventType = 0,  
    Position = 0,  
    Location = 1,  
    ActionType = 0,  
    LogicEntityBehavior = 1,  
    Npc = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1010]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_1017]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1015]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 0,  
      LocationId = [[]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1030]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1031]],  
                x = 34996.92188,  
                y = -3816.03125,  
                z = -18.421875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1028]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1034]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1035]],  
                x = 34976.14063,  
                y = -3839.21875,  
                z = -18.53125
              },  
              Angle = 1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1032]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1038]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1039]],  
                x = 34999.92188,  
                y = -3741.59375,  
                z = -12.453125
              },  
              Angle = 2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1036]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1046]],  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1047]],  
                x = 34973.04688,  
                y = -3784.390625,  
                z = -16.265625
              },  
              Angle = 1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1044]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[bamboo I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1050]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1051]],  
                x = 34987.42188,  
                y = -3810.125,  
                z = -18.6875
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1048]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1054]],  
              Base = [[palette.entities.botobjects.chariot]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1055]],  
                x = 34992.23438,  
                y = -3817.328125,  
                z = -18.5625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1052]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1058]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1059]],  
                x = 34960.60938,  
                y = -3837.734375,  
                z = -19.015625
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1056]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1062]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1063]],  
                x = 34994.39063,  
                y = -3826.515625,  
                z = -18.453125
              },  
              Angle = 2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1060]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1066]],  
              Base = [[palette.entities.botobjects.tr_s2_lokness_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1067]],  
                x = 34944.64063,  
                y = -3756.453125,  
                z = -17.640625
              },  
              Angle = -1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1064]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[weeding I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1074]],  
              Base = [[palette.entities.botobjects.tr_s3_trumpet_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1075]],  
                x = 34999.20313,  
                y = -3765.078125,  
                z = -13.390625
              },  
              Angle = -1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1072]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[trumperer III 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1082]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1083]],  
                x = 34955.07813,  
                y = -3809.625,  
                z = -18.328125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1080]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1086]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1088]],  
                x = 34985.73438,  
                y = -3827.90625,  
                z = -19.265625
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1087]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1091]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1093]],  
                x = 34964.76563,  
                y = -3811,  
                z = -18.71875
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1092]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1096]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1097]],  
                x = 34991.21875,  
                y = -3768.28125,  
                z = -14.9375
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1094]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1100]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1101]],  
                x = 34981.95313,  
                y = -3766.59375,  
                z = -15.546875
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1098]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1104]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1105]],  
                x = 34922.9375,  
                y = -3765.53125,  
                z = -19.5
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1102]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1108]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1109]],  
                x = 34931.5625,  
                y = -3764.265625,  
                z = -19.5625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1106]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1116]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1117]],  
                x = 34960.25,  
                y = -3809.859375,  
                z = -18.5625
              },  
              Angle = -1.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1114]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1120]],  
              Base = [[palette.entities.botobjects.barrels_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1121]],  
                x = 34992.34375,  
                y = -3813.65625,  
                z = -18.515625
              },  
              Angle = 2.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1118]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 barrels 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1223]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1222]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1225]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1226]],  
                    x = 35143.95313,  
                    y = -3770.796875,  
                    z = -20.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1228]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1229]],  
                    x = 35120.89063,  
                    y = -3775.5625,  
                    z = -16.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1231]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1232]],  
                    x = 35100.46875,  
                    y = -3788.078125,  
                    z = -17.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1234]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1235]],  
                    x = 35035.29688,  
                    y = -3788.734375,  
                    z = -19.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1237]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1238]],  
                    x = 34993.9375,  
                    y = -3807.28125,  
                    z = -18.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1240]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1241]],  
                    x = 34976.76563,  
                    y = -3827.453125,  
                    z = -19.109375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1317]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1319]],  
                x = 34955.07813,  
                y = -3809.625,  
                z = -18.328125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1318]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1322]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1323]],  
                x = 35167.03125,  
                y = -3764.71875,  
                z = -18.65625
              },  
              Angle = 2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1320]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1326]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1327]],  
                x = 35156.0625,  
                y = -3794.203125,  
                z = -18.1875
              },  
              Angle = 1.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1324]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1330]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1331]],  
                x = 35153.125,  
                y = -3751.359375,  
                z = -19.671875
              },  
              Angle = 4.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1328]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1334]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1335]],  
                x = 35025.1875,  
                y = -3808.765625,  
                z = -17.078125
              },  
              Angle = -2.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1332]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1338]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1339]],  
                x = 35023.20313,  
                y = -3804.078125,  
                z = -17.109375
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1336]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1346]],  
              Base = [[palette.entities.botobjects.vegetable_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1347]],  
                x = 35020.71875,  
                y = -3795.265625,  
                z = -17.078125
              },  
              Angle = -2.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1344]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living gateway 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1350]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1351]],  
                x = 35018.89063,  
                y = -3786.265625,  
                z = -16.703125
              },  
              Angle = -2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1348]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1354]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1355]],  
                x = 35017.73438,  
                y = -3781.4375,  
                z = -16.171875
              },  
              Angle = 0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1352]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1358]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1359]],  
                x = 35016.79688,  
                y = -3776.265625,  
                z = -15.53125
              },  
              Angle = -3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1356]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1362]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1363]],  
                x = 35016.17188,  
                y = -3771.375,  
                z = -14.984375
              },  
              Angle = 0.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1360]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1366]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1367]],  
                x = 35015.45313,  
                y = -3766.40625,  
                z = -15.046875
              },  
              Angle = -2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1364]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1370]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1371]],  
                x = 35014.125,  
                y = -3761.578125,  
                z = -14.984375
              },  
              Angle = 0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1368]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1374]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1375]],  
                x = 35012.79688,  
                y = -3756.5625,  
                z = -14.03125
              },  
              Angle = -3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1372]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 9]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1378]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1379]],  
                x = 35012.04688,  
                y = -3751.625,  
                z = -13.796875
              },  
              Angle = 0.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1376]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 10]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1382]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1383]],  
                x = 35011.89063,  
                y = -3746.65625,  
                z = -14.015625
              },  
              Angle = 3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1380]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 11]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1386]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1387]],  
                x = 35011.4375,  
                y = -3741.734375,  
                z = -13.640625
              },  
              Angle = 0.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1384]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 12]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1390]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1391]],  
                x = 35010.0625,  
                y = -3736.921875,  
                z = -13.515625
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1388]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 13]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1394]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1395]],  
                x = 35007.32813,  
                y = -3732.875,  
                z = -13.453125
              },  
              Angle = 0.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1392]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 14]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1398]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1399]],  
                x = 35002.96875,  
                y = -3730.3125,  
                z = -13.15625
              },  
              Angle = -1.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1396]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 15]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1402]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1403]],  
                x = 34998.14063,  
                y = -3729.484375,  
                z = -13.25
              },  
              Angle = 1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1400]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 16]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1406]],  
              Base = [[palette.entities.botobjects.vegetable_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1407]],  
                x = 34982.60938,  
                y = -3729.078125,  
                z = -15.609375
              },  
              Angle = 1.546875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1404]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living gateway 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1410]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1411]],  
                x = 34970.625,  
                y = -3727.9375,  
                z = -13.625
              },  
              Angle = -1.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1408]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 17]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1414]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1415]],  
                x = 34965.54688,  
                y = -3727.4375,  
                z = -12.828125
              },  
              Angle = 1.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1412]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 18]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1418]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1420]],  
                x = 34989.96875,  
                y = -3729.3125,  
                z = -15.375
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1419]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1423]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1425]],  
                x = 34976.20313,  
                y = -3728.671875,  
                z = -14.96875
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1424]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1432]],  
              Base = [[palette.entities.botobjects.tr_s2_palmtree_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1433]],  
                x = 34961.4375,  
                y = -3727.125,  
                z = -15.46875
              },  
              Angle = 0.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1430]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[olansi VI 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1436]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1437]],  
                x = 34957.17188,  
                y = -3726.953125,  
                z = -17
              },  
              Angle = 4.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1434]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 19]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1440]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1441]],  
                x = 34952.14063,  
                y = -3726.890625,  
                z = -17.46875
              },  
              Angle = 1.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1438]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 20]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1444]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1445]],  
                x = 34947.0625,  
                y = -3726.71875,  
                z = -18.390625
              },  
              Angle = -1.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1442]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 21]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1448]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1449]],  
                x = 34942.0625,  
                y = -3726.765625,  
                z = -18.734375
              },  
              Angle = 1.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1446]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 22]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1452]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1453]],  
                x = 34936.89063,  
                y = -3726.796875,  
                z = -19.078125
              },  
              Angle = -1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1450]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 23]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1456]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1457]],  
                x = 34931.92188,  
                y = -3726.21875,  
                z = -19.140625
              },  
              Angle = 1.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1454]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 24]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1460]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1461]],  
                x = 34926.9375,  
                y = -3724.53125,  
                z = -18.96875
              },  
              Angle = -1.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1458]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 25]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1464]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1465]],  
                x = 34922.375,  
                y = -3722.421875,  
                z = -18.71875
              },  
              Angle = 1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1462]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 26]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1468]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1469]],  
                x = 34917.78125,  
                y = -3719.9375,  
                z = -18.3125
              },  
              Angle = -2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1466]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 27]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1472]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1473]],  
                x = 34913.67188,  
                y = -3717.34375,  
                z = -18.0625
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1470]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 28]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1476]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1477]],  
                x = 34909.54688,  
                y = -3714.203125,  
                z = -17.96875
              },  
              Angle = -2.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1474]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 29]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1480]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1481]],  
                x = 34905.875,  
                y = -3710.859375,  
                z = -17.4375
              },  
              Angle = 0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1478]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 30]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1484]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1485]],  
                x = 34901.53125,  
                y = -3708.75,  
                z = -16.796875
              },  
              Angle = -1.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1482]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 31]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1488]],  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1490]],  
                x = 34999.23438,  
                y = -3776.65625,  
                z = -14.703125
              },  
              Angle = 1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1489]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[bamboo I 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1585]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1586]],  
                x = 35158.8125,  
                y = -3758.78125,  
                z = -19.25
              },  
              Angle = -2.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1583]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1666]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1667]],  
                x = 34967.67188,  
                y = -3831.984375,  
                z = -19.875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1664]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1700]],  
              Name = [[Route 2]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1699]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1702]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1703]],  
                    x = 35154.45313,  
                    y = -3760.40625,  
                    z = -19.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1705]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1706]],  
                    x = 35115.54688,  
                    y = -3774.65625,  
                    z = -16.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1708]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1709]],  
                    x = 35055.20313,  
                    y = -3792.1875,  
                    z = -20.71875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1711]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1712]],  
                    x = 35020.35938,  
                    y = -3794.5,  
                    z = -17.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1714]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1715]],  
                    x = 34972.875,  
                    y = -3814.875,  
                    z = -19.109375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1720]],  
              Name = [[Route 3]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1719]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1722]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1723]],  
                    x = 35150.1875,  
                    y = -3776.6875,  
                    z = -19.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1725]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1726]],  
                    x = 35113.65625,  
                    y = -3792.359375,  
                    z = -16.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1728]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1729]],  
                    x = 35017.79688,  
                    y = -3792.109375,  
                    z = -16.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1731]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1732]],  
                    x = 34983.60938,  
                    y = -3805.53125,  
                    z = -18.21875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1794]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1793]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1796]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1797]],  
                    x = 35146.10938,  
                    y = -3773.03125,  
                    z = -19.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1799]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1800]],  
                    x = 35095.28125,  
                    y = -3791.734375,  
                    z = -17.75
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1802]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1803]],  
                    x = 35026.875,  
                    y = -3796.046875,  
                    z = -17.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1805]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1806]],  
                    x = 34982.64063,  
                    y = -3815.015625,  
                    z = -18.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1808]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1809]],  
                    x = 34975.8125,  
                    y = -3831.734375,  
                    z = -19.59375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2120]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2121]],  
                x = 34054.59375,  
                y = -1145.578125,  
                z = -19.28125
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2118]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2128]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2129]],  
                x = 34071.21875,  
                y = -1109.53125,  
                z = -21.265625
              },  
              Angle = -1.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2126]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[hut 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2132]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2133]],  
                x = 34089.125,  
                y = -1114.875,  
                z = -21.0625
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2130]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2136]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2137]],  
                x = 34078.09375,  
                y = -1169.671875,  
                z = -21.140625
              },  
              Angle = 1.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2134]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2140]],  
              Base = [[palette.entities.botobjects.tent_matis]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2141]],  
                x = 34056.96875,  
                y = -1129.484375,  
                z = -20.6875
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2138]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[matis tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2144]],  
              Base = [[palette.entities.botobjects.tent_matis]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2145]],  
                x = 34097.51563,  
                y = -1166.171875,  
                z = -21.15625
              },  
              Angle = 2.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2142]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[matis tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2152]],  
              Base = [[palette.entities.botobjects.tent_matis]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2153]],  
                x = 34062.60938,  
                y = -1117.484375,  
                z = -21.078125
              },  
              Angle = -0.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2150]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[matis tent 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2333]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2334]],  
                x = 34089.07813,  
                y = -1153.8125,  
                z = -21.03125
              },  
              Angle = 1.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2331]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2337]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2338]],  
                x = 34063.71875,  
                y = -1134.984375,  
                z = -20.4375
              },  
              Angle = -0.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2335]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2341]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2342]],  
                x = 34076.01563,  
                y = -1115.15625,  
                z = -21.171875
              },  
              Angle = -1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2339]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2345]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2346]],  
                x = 34078.04688,  
                y = -1135.890625,  
                z = -20.953125
              },  
              Angle = 0.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2343]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 9]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2349]],  
              Base = [[palette.entities.botobjects.chariot_working]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2350]],  
                x = 34065.9375,  
                y = -1111.953125,  
                z = -21.171875
              },  
              Angle = -1,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2347]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[working chariot 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_matis]],  
              Angle = -1.961982846,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_2531]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34104.77344,  
                y = -1119.441406,  
                z = -21.00548935,  
                InstanceId = [[Client1_2534]]
              },  
              InheritPos = 1,  
              DisplayMode = 0,  
              InstanceId = [[Client1_2533]],  
              Name = [[matis tent 5]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent]],  
              Angle = 2.418078184,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_2535]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34192.30078,  
                y = -1167.841187,  
                z = 0.9048390388,  
                InstanceId = [[Client1_2538]]
              },  
              InheritPos = 1,  
              DisplayMode = 0,  
              InstanceId = [[Client1_2537]],  
              Name = [[tent 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Angle = 4.136430327,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_2539]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34193.76563,  
                y = -1119.273315,  
                z = 2.98626709,  
                InstanceId = [[Client1_2542]]
              },  
              InheritPos = 1,  
              DisplayMode = 0,  
              InstanceId = [[Client1_2541]],  
              Name = [[tryker tent 6]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Angle = 3.630284844,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_2543]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34200.1875,  
                y = -1126.570313,  
                z = 2.606955051,  
                InstanceId = [[Client1_2546]]
              },  
              InheritPos = 1,  
              DisplayMode = 0,  
              InstanceId = [[Client1_2545]],  
              Name = [[tryker tent 7]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Angle = -2.161106825,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_2547]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34184.64844,  
                y = -1115.714355,  
                z = 3.916532993,  
                InstanceId = [[Client1_2550]]
              },  
              InheritPos = 1,  
              DisplayMode = 0,  
              InstanceId = [[Client1_2549]],  
              Name = [[tryker tent 8]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.campfire_out]],  
              Angle = -1.533402681,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_2551]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34179.63281,  
                y = -1125.892578,  
                z = 3.120289803,  
                InstanceId = [[Client1_2554]]
              },  
              InheritPos = 1,  
              DisplayMode = 0,  
              InstanceId = [[Client1_2553]],  
              Name = [[dead camp fire 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.campfire_out]],  
              Angle = -2.018115759,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_2555]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34192.92578,  
                y = -1139.437256,  
                z = 3.983988762,  
                InstanceId = [[Client1_2558]]
              },  
              InheritPos = 1,  
              DisplayMode = 0,  
              InstanceId = [[Client1_2557]],  
              Name = [[dead camp fire 2]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.barrels_3]],  
              Angle = 2.412213802,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_2559]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34193.59375,  
                y = -1163.308105,  
                z = 1.506181717,  
                InstanceId = [[Client1_2562]]
              },  
              InheritPos = 1,  
              DisplayMode = 0,  
              InstanceId = [[Client1_2561]],  
              Name = [[3 barrels 2]]
            }
          },  
          InstanceId = [[Client1_1018]]
        }
      },  
      Version = 5,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1016]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Permanent]]
    },  
    {
      InstanceId = [[Client1_1021]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1019]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 1,  
      LocationId = [[Client1_1023]],  
      WeatherValue = 78,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1042]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1043]],  
                x = 34968.17188,  
                y = -3744.34375,  
                z = -12.328125
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1040]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1145]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1146]],  
                x = 34977.01563,  
                y = -3831.953125,  
                z = -19.484375
              },  
              Angle = 1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1143]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 7,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 25,  
              EyesColor = 6,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 3,  
              MorphTarget4 = 7,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6721070,  
              HandsModel = 6721326,  
              ArmModel = 6723118,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 5,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6766638,  
              Level = 0,  
              Name = [[Rosira]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1168]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1169]],  
                x = 35147.79688,  
                y = -3772.0625,  
                z = -19.8125
              },  
              Angle = 3.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1166]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1492]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1493]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1262]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1263]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1264]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1265]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1223]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 29,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              Level = 0,  
              Name = [[Kridix]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            }
          },  
          InstanceId = [[Client1_1022]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1132]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1124]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1125]],  
                x = 34973.32813,  
                y = -3827.296875,  
                z = -19.765625
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1122]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 6723630,  
              TrouserModel = 6722350,  
              FeetModel = 6721070,  
              HandsModel = 6721582,  
              ArmModel = 6723118,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 1,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6765870,  
              Level = 0,  
              Name = [[Ba'Massey]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1128]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1129]],  
                x = 34977.59375,  
                y = -3828.40625,  
                z = -19.5
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1126]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 6721838,  
              HairColor = 3,  
              Tattoo = 15,  
              EyesColor = 3,  
              MorphTarget1 = 1,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721582,  
              ArmModel = 6722862,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6765614,  
              Level = 0,  
              Name = [[Be'Daghan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1135]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1137]],  
                x = 34977.51563,  
                y = -3830.75,  
                z = -19.734375
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1133]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 12,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 7,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721326,  
              ArmModel = 6722862,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6765614,  
              Level = 0,  
              Name = [[Be'Gany]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1140]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1142]],  
                x = 34975.73438,  
                y = -3831.875,  
                z = -19.890625
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1138]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 1,  
              HairType = 6721838,  
              HairColor = 1,  
              Tattoo = 30,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721582,  
              ArmModel = 6722862,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6765870,  
              Level = 0,  
              Name = [[Mac'Jorn]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1651]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1653]],  
                x = 34973.6875,  
                y = -3831.0625,  
                z = -19.890625
              },  
              Angle = 2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1649]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 10,  
              HairType = 5612590,  
              HairColor = 1,  
              Tattoo = 30,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5615918,  
              TrouserModel = 5612846,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5636398,  
              Level = 0,  
              Name = [[Rosira]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1656]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1658]],  
                x = 34976.09375,  
                y = -3826.765625,  
                z = -19.546875
              },  
              Angle = 2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1654]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 9,  
              HairType = 5623086,  
              HairColor = 3,  
              Tattoo = 13,  
              EyesColor = 0,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              MorphTarget7 = 2,  
              MorphTarget8 = 7,  
              JacketModel = 5613358,  
              TrouserModel = 5612846,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5600814,  
              Level = 0,  
              Name = [[Mac'Rippsey]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1661]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1663]],  
                x = 34972.46875,  
                y = -3829.453125,  
                z = -19.96875
              },  
              Angle = 2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1659]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 0,  
              HairType = 5623086,  
              HairColor = 4,  
              Tattoo = 6,  
              EyesColor = 7,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 0,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5636398,  
              Level = 0,  
              Name = [[Mac'Cautty]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1131]],  
            x = 10.453125,  
            y = 1.34375,  
            z = 0.484375
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1130]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1199]],  
          Name = [[Group 2]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1191]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1192]],  
                x = 35168.125,  
                y = -3731.796875,  
                z = -14.84375
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1189]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1258]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1259]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1260]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1261]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1223]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 14,  
              HairType = 3118,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 1,  
              MorphTarget1 = 7,  
              MorphTarget2 = 3,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Apothus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1195]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1196]],  
                x = 35169.35938,  
                y = -3730.140625,  
                z = -15.03125
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1193]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 5604398,  
              HairColor = 0,  
              Tattoo = 30,  
              EyesColor = 1,  
              MorphTarget1 = 5,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 4,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Dekos]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1202]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1204]],  
                x = 35170.03125,  
                y = -3732.6875,  
                z = -14.4375
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1200]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 12,  
              HairType = 2606,  
              HairColor = 4,  
              Tattoo = 5,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              MorphTarget7 = 2,  
              MorphTarget8 = 5,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5635886,  
              Level = 0,  
              Name = [[Kyron]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1268]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1270]],  
                x = 35171.57813,  
                y = -3730.578125,  
                z = -14.734375
              },  
              Angle = -2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1266]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 1,  
              Tattoo = 18,  
              EyesColor = 3,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              JacketModel = 5607726,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1273]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1275]],  
                x = 35167.0625,  
                y = -3728.65625,  
                z = -15.109375
              },  
              Angle = -2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1271]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 6,  
              HairType = 5604398,  
              HairColor = 0,  
              Tattoo = 29,  
              EyesColor = 5,  
              MorphTarget1 = 5,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Aktius]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1539]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1541]],  
                x = 35168.9375,  
                y = -3728.03125,  
                z = -15.0625
              },  
              Angle = -2.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1537]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 3,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 18,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 7,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 3,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756142,  
              Level = 0,  
              Name = [[Meps]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1198]],  
            x = -17.453125,  
            y = -35.875,  
            z = -4.953125
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1197]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1552]],  
          Name = [[Group 4]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1544]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1545]],  
                x = 35160.29688,  
                y = -3757.375,  
                z = -18.84375
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1542]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1745]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1746]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1747]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1748]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 1,  
              HairType = 6700590,  
              HairColor = 3,  
              Tattoo = 30,  
              EyesColor = 0,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6756910,  
              Level = 0,  
              Name = [[Eukos]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1548]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1549]],  
                x = 35158.20313,  
                y = -3756.28125,  
                z = -19.203125
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1546]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 6700334,  
              HairColor = 0,  
              Tattoo = 23,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              MorphTarget7 = 6,  
              MorphTarget8 = 0,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 5,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              Level = 0,  
              Name = [[Kridix]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1565]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1567]],  
                x = 35156.29688,  
                y = -3758.078125,  
                z = -19.703125
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1563]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 13,  
              HairType = 6700590,  
              HairColor = 2,  
              Tattoo = 15,  
              EyesColor = 3,  
              MorphTarget1 = 4,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 5,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6755118,  
              Level = 0,  
              Name = [[Abyla]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1570]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1572]],  
                x = 35161.5625,  
                y = -3759.5625,  
                z = -18.828125
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1568]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 2,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 15,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756654,  
              Level = 0,  
              Name = [[Aeseus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1575]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1577]],  
                x = 35159.60938,  
                y = -3761.484375,  
                z = -19.21875
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1573]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 12,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 8,  
              EyesColor = 4,  
              MorphTarget1 = 1,  
              MorphTarget2 = 3,  
              MorphTarget3 = 7,  
              MorphTarget4 = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              MorphTarget7 = 6,  
              MorphTarget8 = 6,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701358,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              Level = 0,  
              Name = [[Theros]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1580]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1582]],  
                x = 35157.0625,  
                y = -3760.90625,  
                z = -19.640625
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1578]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 8,  
              HairType = 6700334,  
              HairColor = 3,  
              Tattoo = 17,  
              EyesColor = 5,  
              MorphTarget1 = 4,  
              MorphTarget2 = 6,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 1,  
              MorphTarget8 = 4,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6756910,  
              Level = 0,  
              Name = [[Diorius]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1551]],  
            x = 0.078125,  
            y = 0.125,  
            z = 0.015625
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1550]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1597]],  
          Name = [[Group 5]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1589]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1590]],  
                x = 35153.8125,  
                y = -3777.296875,  
                z = -18.8125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1587]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1749]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1750]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1751]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1752]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 4,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 15,  
              EyesColor = 4,  
              MorphTarget1 = 7,  
              MorphTarget2 = 0,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 3,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Kritheus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1593]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1594]],  
                x = 35156.3125,  
                y = -3775,  
                z = -18.578125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1591]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 3,  
              HairType = 5604398,  
              HairColor = 2,  
              Tattoo = 25,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 1,  
              MorphTarget4 = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 2,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Iokos]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1600]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1602]],  
                x = 35155.70313,  
                y = -3780.140625,  
                z = -18.5625
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1598]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 7,  
              HairType = 5604398,  
              HairColor = 3,  
              Tattoo = 20,  
              EyesColor = 6,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 6,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Iosse]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1605]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1607]],  
                x = 35156.875,  
                y = -3777.734375,  
                z = -18.359375
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1603]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 6,  
              HairType = 5621806,  
              HairColor = 2,  
              Tattoo = 8,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 6,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 0,  
              ArmColor = 3,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Boekos]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1610]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1612]],  
                x = 35156.29688,  
                y = -3776.28125,  
                z = -18.515625
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1608]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 1,  
              HairType = 5604398,  
              HairColor = 4,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 7,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Lyton]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1615]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1617]],  
                x = 35155.95313,  
                y = -3778.765625,  
                z = -18.515625
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1613]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 10,  
              HairType = 5621550,  
              HairColor = 2,  
              Tattoo = 22,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 2,  
              MorphTarget8 = 5,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5607470,  
              JacketColor = 5,  
              ArmColor = 0,  
              HandsColor = 1,  
              TrouserColor = 0,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Iodix]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1596]],  
            x = -1.109375,  
            y = 2.21875,  
            z = -0.21875
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1595]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1628]],  
          Name = [[Group 6]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1620]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1621]],  
                x = 34962.09375,  
                y = -3809.890625,  
                z = -18.59375
              },  
              Angle = -0.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1618]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 5623598,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5615918,  
              TrouserModel = 5612846,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5600302,  
              Level = 0,  
              Name = [[Be'laury]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1624]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1625]],  
                x = 34967.51563,  
                y = -3812.046875,  
                z = -18.8125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1622]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 7470,  
              HairColor = 0,  
              Tattoo = 28,  
              EyesColor = 0,  
              MorphTarget1 = 1,  
              MorphTarget2 = 2,  
              MorphTarget3 = 4,  
              MorphTarget4 = 6,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 1,  
              MorphTarget8 = 0,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 1,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              Level = 0,  
              Name = [[Mac'Nary]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1631]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1633]],  
                x = 34967.54688,  
                y = -3808.890625,  
                z = -18.734375
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1629]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 7,  
              HairType = 5623086,  
              HairColor = 1,  
              Tattoo = 6,  
              EyesColor = 3,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 2,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 4,  
              ArmColor = 2,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              Level = 0,  
              Name = [[Mac'Arlly]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1636]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1638]],  
                x = 34964.78125,  
                y = -3814.015625,  
                z = -18.890625
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1634]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 31,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 6,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 3,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5600302,  
              Level = 0,  
              Name = [[Mac'Jorn]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1641]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1643]],  
                x = 34964.35938,  
                y = -3807.6875,  
                z = -18.703125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1639]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 14,  
              HairType = 5612590,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 0,  
              MorphTarget1 = 4,  
              MorphTarget2 = 3,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5613102,  
              JacketColor = 5,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              Level = 0,  
              Name = [[Be'Toolly]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1646]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1648]],  
                x = 34962.28125,  
                y = -3812.859375,  
                z = -18.703125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1644]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 13,  
              HairType = 6958,  
              HairColor = 5,  
              Tattoo = 16,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 4,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5636398,  
              Level = 0,  
              Name = [[O'Arty]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1627]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1626]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1678]],  
          Name = [[Group 7]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1670]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1671]],  
                x = 34966.15625,  
                y = -3833.890625,  
                z = -19.46875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1668]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 13,  
              HairType = 7214,  
              HairColor = 5,  
              Tattoo = 2,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5600814,  
              Level = 0,  
              Name = [[Be'Keeer]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1674]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1675]],  
                x = 34967.10938,  
                y = -3829.453125,  
                z = -20.25
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1672]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 12,  
              HairType = 7470,  
              HairColor = 3,  
              Tattoo = 6,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              Level = 0,  
              Name = [[Mac'Laughan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1681]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1683]],  
                x = 34970.45313,  
                y = -3831.875,  
                z = -19.78125
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1679]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 2,  
              HairType = 5623342,  
              HairColor = 3,  
              Tattoo = 28,  
              EyesColor = 0,  
              MorphTarget1 = 6,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 5613358,  
              TrouserModel = 5612846,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 0,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5601070,  
              Level = 0,  
              Name = [[O'Darghan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1686]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1688]],  
                x = 34968.53125,  
                y = -3833.828125,  
                z = -19.453125
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1684]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 6702,  
              HairColor = 5,  
              Tattoo = 0,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 6,  
              MorphTarget3 = 2,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 0,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              Level = 0,  
              Name = [[Ba'Roley]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1691]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1693]],  
                x = 34965.34375,  
                y = -3831.59375,  
                z = -19.953125
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1689]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 12,  
              HairType = 5623598,  
              HairColor = 5,  
              Tattoo = 26,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 3,  
              MorphTarget3 = 2,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              Level = 0,  
              Name = [[Be'Yle]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1696]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1698]],  
                x = 34969.57813,  
                y = -3829.78125,  
                z = -20.15625
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1694]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 3,  
              HairType = 5612590,  
              HairColor = 4,  
              Tattoo = 7,  
              EyesColor = 4,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              Level = 0,  
              Name = [[Mac'Rippsey]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1677]],  
            x = -0.234375,  
            y = -0.15625,  
            z = 0.03125
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1676]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          MissionText = [[Find <mission_target> for me]],  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1779]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1782]],  
                    Entity = r2.RefId([[]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1781]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1780]],  
                  Type = [[activation]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1784]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1787]],  
                    Entity = r2.RefId([[]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1786]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1785]],  
                  Type = [[activation]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1789]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1792]],  
                    Entity = r2.RefId([[]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1791]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1790]],  
                  Type = [[activation]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            InstanceId = [[Client1_1776]]
          },  
          WaitValidationText = [[Did <mission_giver> ask you to talk to me?]],  
          Base = [[palette.entities.botobjects.bot_chat]],  
          Repeatable = 0,  
          ContextualText = [[Talk to <mission_target>]],  
          MissionGiver = r2.RefId([[Client1_1145]]),  
          MissionTarget = r2.RefId([[Client1_1168]]),  
          InheritPos = 1,  
          MissionSucceedText = [[Yes I am <mission_target>]],  
          Name = [[Mission: Talk to 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1777]],  
            x = 34975.5,  
            y = -3830.0625,  
            z = -19.84375
          },  
          Class = [[TalkTo]],  
          InstanceId = [[Client1_1775]],  
          Components = {
          },  
          _Seed = 1147177673
        }
      },  
      Version = 5,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1020]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Act 1:Act 1]]
    },  
    {
      InstanceId = [[Client1_1829]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1827]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Title = [[]],  
      Version = 5,  
      WeatherValue = 317,  
      LocationId = [[Client1_1826]],  
      Name = [[Act 2:Act 2]],  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1943]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1945]],  
                x = 34265.40625,  
                y = -1201.328125,  
                z = -18.484375
              },  
              Angle = 1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1944]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 7,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 25,  
              EyesColor = 6,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 3,  
              MorphTarget4 = 7,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6721070,  
              HandsModel = 6721326,  
              ArmModel = 6723118,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 5,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6766638,  
              Name = [[Be'Doyley]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2104]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2113]],  
                x = 34186.90625,  
                y = -1150.296875,  
                z = 3.296875
              },  
              Angle = 3.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2105]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 29,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              Name = [[Dycaon]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2124]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2125]],  
                x = 34051.21875,  
                y = -1168.59375,  
                z = -17.71875
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2122]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[wind turbine 2]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_1830]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1833]],  
          Name = [[Group 8]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1838]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1840]],  
                x = -4,  
                y = 2.171875,  
                z = 0.15625
              },  
              Angle = -0.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1839]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 5623598,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5615918,  
              TrouserModel = 5612846,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Aggro = 115,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5600302,  
              Name = [[Mac'Darssey]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1843]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1845]],  
                x = -2.921875,  
                y = -3.125,  
                z = 0.171875
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1844]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 7470,  
              HairColor = 0,  
              Tattoo = 28,  
              EyesColor = 0,  
              MorphTarget1 = 1,  
              MorphTarget2 = 2,  
              MorphTarget3 = 4,  
              MorphTarget4 = 6,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 1,  
              MorphTarget8 = 0,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 1,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              Name = [[Mac'Rippsey]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1848]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1850]],  
                x = -5.03125,  
                y = 5.875,  
                z = 0.078125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1849]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 7,  
              HairType = 5623086,  
              HairColor = 1,  
              Tattoo = 6,  
              EyesColor = 3,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 2,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 4,  
              ArmColor = 2,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              Name = [[Rosira]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1853]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1855]],  
                x = -3.265625,  
                y = -1.296875,  
                z = 0.171875
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1854]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 31,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 6,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 3,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5600302,  
              Name = [[Mac'Darssey]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1858]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1860]],  
                x = -4.40625,  
                y = 4.046875,  
                z = 0.171875
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1859]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 14,  
              HairType = 5612590,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 0,  
              MorphTarget1 = 4,  
              MorphTarget2 = 3,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5613102,  
              JacketColor = 5,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              Name = [[Be'teghan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1863]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1865]],  
                x = -3.671875,  
                y = 0.328125,  
                z = 0.125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1864]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 13,  
              HairType = 6958,  
              HairColor = 5,  
              Tattoo = 16,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 4,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5636398,  
              Name = [[O'Cauty]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1835]],  
            x = 34178.3125,  
            y = -1145.71875,  
            z = 1.765625
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1834]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1868]],  
          Name = [[Group 9]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1873]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1875]],  
                x = -1.921875,  
                y = -0.9375,  
                z = -0.40625
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1874]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 13,  
              HairType = 7214,  
              HairColor = 5,  
              Tattoo = 2,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Aggro = 115,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5600814,  
              Name = [[Be'Keeer]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1878]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1880]],  
                x = -4.359375,  
                y = 2.546875,  
                z = -0.40625
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1879]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 12,  
              HairType = 7470,  
              HairColor = 3,  
              Tattoo = 6,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              Name = [[Be'Daghan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1883]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1885]],  
                x = -0.453125,  
                y = -4.84375,  
                z = -0.46875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1884]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 2,  
              HairType = 5623342,  
              HairColor = 3,  
              Tattoo = 28,  
              EyesColor = 0,  
              MorphTarget1 = 6,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 5613358,  
              TrouserModel = 5612846,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 0,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5601070,  
              Name = [[Be'Daghan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1888]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1890]],  
                x = -1.03125,  
                y = -3.015625,  
                z = -0.390625
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1889]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 6702,  
              HairColor = 5,  
              Tattoo = 0,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 6,  
              MorphTarget3 = 2,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 0,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              Name = [[Be'Ledacan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1893]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1895]],  
                x = -3.109375,  
                y = 1.0625,  
                z = -0.375
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1894]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 12,  
              HairType = 5623598,  
              HairColor = 5,  
              Tattoo = 26,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 3,  
              MorphTarget3 = 2,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              Name = [[Ba'Neppy]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1898]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1900]],  
                x = -5.46875,  
                y = 3.953125,  
                z = -0.34375
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1899]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 3,  
              HairType = 5612590,  
              HairColor = 4,  
              Tattoo = 7,  
              EyesColor = 4,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              Name = [[Mac'Arlly]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1870]],  
            x = 34173.01563,  
            y = -1133.078125,  
            z = 2.25
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1869]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1903]],  
          Name = [[Group 10]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1907]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1909]],  
                x = -9.96875,  
                y = 9.65625,  
                z = 0.015625
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1908]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 6723630,  
              TrouserModel = 6722350,  
              FeetModel = 6721070,  
              HandsModel = 6721582,  
              ArmModel = 6723118,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 1,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Aggro = 115,  
              Speed = 1,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6766638,  
              Name = [[Mac'Laughan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1912]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1914]],  
                x = -11.109375,  
                y = 13.078125,  
                z = 0
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1913]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 6721838,  
              HairColor = 3,  
              Tattoo = 15,  
              EyesColor = 3,  
              MorphTarget1 = 1,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721582,  
              ArmModel = 6722862,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6765614,  
              Name = [[Be'Nan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1917]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1919]],  
                x = -10.921875,  
                y = 14.828125,  
                z = 0.078125
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1918]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 12,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 7,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721326,  
              ArmModel = 6722862,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6765614,  
              Name = [[Be'Riplan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1922]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1924]],  
                x = -9.546875,  
                y = 7.34375,  
                z = 0.015625
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1923]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 1,  
              HairType = 6721838,  
              HairColor = 1,  
              Tattoo = 30,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721582,  
              ArmModel = 6722862,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6765870,  
              Name = [[Mac'darrroy]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1927]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1929]],  
                x = -8.984375,  
                y = 5.6875,  
                z = 0
              },  
              Angle = 2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1928]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 10,  
              HairType = 5612590,  
              HairColor = 1,  
              Tattoo = 30,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5615918,  
              TrouserModel = 5612846,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5636398,  
              Name = [[Be'Gany]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1932]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1934]],  
                x = -10.4375,  
                y = 11.203125,  
                z = 0.015625
              },  
              Angle = 2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1933]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 9,  
              HairType = 5623086,  
              HairColor = 3,  
              Tattoo = 13,  
              EyesColor = 0,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              MorphTarget7 = 2,  
              MorphTarget8 = 7,  
              JacketModel = 5613358,  
              TrouserModel = 5612846,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5600814,  
              Name = [[Ba'Roley]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1937]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1939]],  
                x = -12.078125,  
                y = 6.75,  
                z = -0.3125
              },  
              Angle = 2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1938]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 0,  
              HairType = 5623086,  
              HairColor = 4,  
              Tattoo = 6,  
              EyesColor = 7,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 0,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5636398,  
              Name = [[Mac'Nary]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1904]],  
            x = 34187.90625,  
            y = -1157.453125,  
            z = 2.296875
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1940]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1987]],  
          Name = [[Group 12]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1991]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1997]],  
                x = -1.046875,  
                y = -1.46875,  
                z = 0.015625
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1992]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 14,  
              HairType = 3118,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 1,  
              MorphTarget1 = 7,  
              MorphTarget2 = 3,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Aggro = 115,  
              Speed = 1,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              Name = [[Ioron]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2000]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2002]],  
                x = -1.484375,  
                y = 0.0625,  
                z = -0.109375
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2001]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 5604398,  
              HairColor = 0,  
              Tattoo = 30,  
              EyesColor = 1,  
              MorphTarget1 = 5,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 4,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              Name = [[Icaps]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2005]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2007]],  
                x = -0.484375,  
                y = -2.96875,  
                z = -0.140625
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2006]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 12,  
              HairType = 2606,  
              HairColor = 4,  
              Tattoo = 5,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              MorphTarget7 = 2,  
              MorphTarget8 = 5,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 1,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5635886,  
              Name = [[Tin]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2010]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2012]],  
                x = -0.21875,  
                y = -4.703125,  
                z = -0.125
              },  
              Angle = -2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2011]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 1,  
              Tattoo = 18,  
              EyesColor = 3,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              JacketModel = 5607726,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[Melus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2015]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2017]],  
                x = -2.109375,  
                y = 1.65625,  
                z = -0.234375
              },  
              Angle = -2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2016]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 6,  
              HairType = 5604398,  
              HairColor = 0,  
              Tattoo = 29,  
              EyesColor = 5,  
              MorphTarget1 = 5,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[Debus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2020]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2022]],  
                x = -3,  
                y = 2.875,  
                z = -0.0625
              },  
              Angle = -2.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2021]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 3,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 18,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 7,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 3,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756142,  
              Name = [[Zeps]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1988]],  
            x = 34177.0625,  
            y = -1136.34375,  
            z = 2.578125
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2023]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2026]],  
          Name = [[Group 13]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2031]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2037]],  
                x = -7.078125,  
                y = -2.328125,  
                z = -1.125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2032]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 4,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 15,  
              EyesColor = 4,  
              MorphTarget1 = 7,  
              MorphTarget2 = 0,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 3,  
              FeetColor = 4,  
              Aggro = 115,  
              Speed = 1,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595694,  
              Name = [[Pyan]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2040]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2042]],  
                x = -7.890625,  
                y = 1.1875,  
                z = -1.109375
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2041]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 3,  
              HairType = 5604398,  
              HairColor = 2,  
              Tattoo = 25,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 1,  
              MorphTarget4 = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 2,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595950,  
              Name = [[Zelus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2045]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2047]],  
                x = -6.6875,  
                y = -4.21875,  
                z = -1.109375
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2046]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 7,  
              HairType = 5604398,  
              HairColor = 3,  
              Tattoo = 20,  
              EyesColor = 6,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 6,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 5595182,  
              Name = [[Dyla]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2055]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2057]],  
                x = -7.5,  
                y = -0.515625,  
                z = -1.125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2056]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 1,  
              HairType = 5604398,  
              HairColor = 4,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 7,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595182,  
              Name = [[Deseus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2060]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2062]],  
                x = -6.671875,  
                y = -5.9375,  
                z = -1.15625
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2061]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 10,  
              HairType = 5621550,  
              HairColor = 2,  
              Tattoo = 22,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 2,  
              MorphTarget8 = 5,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5607470,  
              JacketColor = 5,  
              ArmColor = 0,  
              HandsColor = 1,  
              TrouserColor = 0,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              Name = [[Lydix]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2028]],  
            x = 34185.82813,  
            y = -1139.296875,  
            z = 3.6875
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2027]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2065]],  
          Name = [[Group 14]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2070]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2076]],  
                x = -6.0625,  
                y = 3.609375,  
                z = -0.078125
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2071]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 1,  
              HairType = 6700590,  
              HairColor = 3,  
              Tattoo = 30,  
              EyesColor = 0,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 2,  
              Aggro = 115,  
              Speed = 1,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6756910,  
              Name = [[Krirus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2079]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2081]],  
                x = -6.859375,  
                y = 4.6875,  
                z = 0.015625
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2080]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 6700334,  
              HairColor = 0,  
              Tattoo = 23,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              MorphTarget7 = 6,  
              MorphTarget8 = 0,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 5,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              Name = [[Ulydix]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2084]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2086]],  
                x = -7.59375,  
                y = 5.75,  
                z = 0.109375
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2085]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 13,  
              HairType = 6700590,  
              HairColor = 2,  
              Tattoo = 15,  
              EyesColor = 3,  
              MorphTarget1 = 4,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 5,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6755118,  
              Name = [[Ulyseus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2089]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2091]],  
                x = -5.28125,  
                y = 2.625,  
                z = -0.109375
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2090]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 2,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 15,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756654,  
              Name = [[Meros]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2094]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2096]],  
                x = -4.625,  
                y = 1.59375,  
                z = -0.15625
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2095]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 12,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 8,  
              EyesColor = 4,  
              MorphTarget1 = 1,  
              MorphTarget2 = 3,  
              MorphTarget3 = 7,  
              MorphTarget4 = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              MorphTarget7 = 6,  
              MorphTarget8 = 6,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701358,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              Name = [[Xarus]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2099]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2101]],  
                x = -4.078125,  
                y = 0.171875,  
                z = -0.234375
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2100]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 8,  
              HairType = 6700334,  
              HairColor = 3,  
              Tattoo = 17,  
              EyesColor = 5,  
              MorphTarget1 = 4,  
              MorphTarget2 = 6,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 1,  
              MorphTarget8 = 4,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6756910,  
              Name = [[Min]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2067]],  
            x = 34177.34375,  
            y = -1132.328125,  
            z = 2.734375
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2066]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2168]],  
          Name = [[Group 15]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2160]],  
              Base = [[palette.entities.npcs.bandits.m_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2161]],  
                x = 34083.29688,  
                y = -1123.5,  
                z = -21.046875
              },  
              Angle = -0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2158]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 7,  
              HairType = 5422,  
              HairColor = 1,  
              Tattoo = 5,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              JacketModel = 6715182,  
              TrouserModel = 6714414,  
              FeetModel = 6713390,  
              HandsModel = 6713646,  
              ArmModel = 6714926,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 1,  
              TrouserColor = 5,  
              FeetColor = 3,  
              Aggro = 115,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Antogio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_poison_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2164]],  
              Base = [[palette.entities.npcs.bandits.m_mage_celestial_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2165]],  
                x = 34079.39063,  
                y = -1123.859375,  
                z = -21
              },  
              Angle = -0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2162]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 2,  
              HairType = 5934,  
              HairColor = 5,  
              Tattoo = 17,  
              EyesColor = 3,  
              MorphTarget1 = 3,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 4,  
              MorphTarget8 = 6,  
              JacketModel = 6715438,  
              TrouserModel = 6708526,  
              FeetModel = 6713390,  
              HandsModel = 6713902,  
              ArmModel = 6714670,  
              JacketColor = 5,  
              ArmColor = 3,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Perni]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_fear_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2171]],  
              Base = [[palette.entities.npcs.bandits.m_mage_atysian_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2173]],  
                x = 34080.96875,  
                y = -1120.9375,  
                z = -21
              },  
              Angle = -0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2169]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 10,  
              HairType = 5622830,  
              HairColor = 1,  
              Tattoo = 29,  
              EyesColor = 4,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              MorphTarget7 = 6,  
              MorphTarget8 = 0,  
              JacketModel = 6715438,  
              TrouserModel = 6708526,  
              FeetModel = 6713390,  
              HandsModel = 6713902,  
              ArmModel = 6714670,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Peccio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_blind_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2176]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2178]],  
                x = 34079.875,  
                y = -1122.265625,  
                z = -21.015625
              },  
              Angle = -0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2174]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 2,  
              HairType = 5622830,  
              HairColor = 1,  
              Tattoo = 27,  
              EyesColor = 2,  
              MorphTarget1 = 1,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 6,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 7,  
              JacketModel = 6715182,  
              TrouserModel = 6708782,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Roero]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2181]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2183]],  
                x = 34083.09375,  
                y = -1125.421875,  
                z = -21
              },  
              Angle = -0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2179]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 2,  
              HairType = 6711086,  
              HairColor = 2,  
              Tattoo = 9,  
              EyesColor = 6,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 5,  
              MorphTarget4 = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              JacketModel = 6712622,  
              TrouserModel = 6711854,  
              FeetModel = 6710318,  
              HandsModel = 6710574,  
              ArmModel = 6712366,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 6774062,  
              Sex = 1,  
              WeaponRightHand = 6758702,  
              Level = 0,  
              Name = [[Sirgio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_pierce_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2186]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2188]],  
                x = 34084.39063,  
                y = -1122.140625,  
                z = -21.03125
              },  
              Angle = -0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2184]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 8,  
              HairType = 6711086,  
              HairColor = 5,  
              Tattoo = 15,  
              EyesColor = 5,  
              MorphTarget1 = 2,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 5,  
              MorphTarget7 = 6,  
              MorphTarget8 = 6,  
              JacketModel = 6712878,  
              TrouserModel = 6711598,  
              FeetModel = 6710318,  
              HandsModel = 6710574,  
              ArmModel = 6712366,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 1,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 6774318,  
              Sex = 1,  
              WeaponRightHand = 6757422,  
              Level = 0,  
              Name = [[Gicha]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_blunt_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2167]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2166]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2195]],  
          Name = [[Group 16]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2116]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2117]],  
                x = 34084.0625,  
                y = -1145.859375,  
                z = -21.0625
              },  
              Angle = -0.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2114]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 5622318,  
              HairColor = 5,  
              Tattoo = 16,  
              EyesColor = 7,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 6715438,  
              TrouserModel = 6708782,  
              FeetModel = 6713390,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 4,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Girni]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2191]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2192]],  
                x = 34081.23438,  
                y = -1147.859375,  
                z = -21.015625
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2189]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 6,  
              HairType = 5934,  
              HairColor = 3,  
              Tattoo = 30,  
              EyesColor = 0,  
              MorphTarget1 = 1,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 5,  
              MorphTarget7 = 0,  
              MorphTarget8 = 0,  
              JacketModel = 6712878,  
              TrouserModel = 6716974,  
              FeetModel = 6710062,  
              HandsModel = 6716462,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6761262,  
              Level = 0,  
              Name = [[Lini]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2198]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2200]],  
                x = 34081.03125,  
                y = -1144.40625,  
                z = -21.015625
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2196]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 1,  
              HairType = 5934,  
              HairColor = 2,  
              Tattoo = 24,  
              EyesColor = 5,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 6712622,  
              TrouserModel = 6716718,  
              FeetModel = 6710318,  
              HandsModel = 6716206,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6760238,  
              Level = 0,  
              Name = [[Liccio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2203]],  
              Base = [[palette.entities.npcs.bandits.m_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2204]],  
                x = 34081.03125,  
                y = -1145.9375,  
                z = -21
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2201]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 10,  
              HairType = 5622830,  
              HairColor = 4,  
              Tattoo = 31,  
              EyesColor = 0,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 6715438,  
              TrouserModel = 6714158,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Piche]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_poison_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2207]],  
              Base = [[palette.entities.npcs.bandits.m_mage_atysian_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2209]],  
                x = 34083.04688,  
                y = -1143.96875,  
                z = -21.015625
              },  
              Angle = -0.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2205]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 4,  
              HairType = 5622830,  
              HairColor = 0,  
              Tattoo = 27,  
              EyesColor = 5,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 2,  
              MorphTarget4 = 6,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 6715438,  
              TrouserModel = 6708782,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Miane]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_blind_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2212]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2214]],  
                x = 34084.3125,  
                y = -1148,  
                z = -21
              },  
              Angle = 0.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2210]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 2,  
              HairType = 6711086,  
              HairColor = 5,  
              Tattoo = 13,  
              EyesColor = 4,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 1,  
              MorphTarget4 = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              MorphTarget7 = 3,  
              MorphTarget8 = 5,  
              JacketModel = 6712878,  
              TrouserModel = 6711854,  
              FeetModel = 6710318,  
              HandsModel = 6710830,  
              ArmModel = 6712110,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 2,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 6774318,  
              Sex = 0,  
              WeaponRightHand = 6759982,  
              Level = 0,  
              Name = [[Anibre]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2194]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2193]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2225]],  
          Name = [[Group 17]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2217]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2218]],  
                x = 34068.95313,  
                y = -1133.140625,  
                z = -20.71875
              },  
              Angle = -0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2215]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 11,  
              HairType = 6711342,  
              HairColor = 1,  
              Tattoo = 1,  
              EyesColor = 3,  
              MorphTarget1 = 0,  
              MorphTarget2 = 6,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 6712622,  
              TrouserModel = 6711854,  
              FeetModel = 6710062,  
              HandsModel = 6710574,  
              ArmModel = 6712110,  
              JacketColor = 0,  
              ArmColor = 3,  
              HandsColor = 2,  
              TrouserColor = 5,  
              FeetColor = 1,  
              Aggro = 115,  
              Speed = 1,  
              WeaponLeftHand = 6774062,  
              Sex = 1,  
              WeaponRightHand = 6759726,  
              Level = 0,  
              Name = [[Tinaera]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2221]],  
              Base = [[palette.entities.npcs.bandits.m_mage_atysian_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2222]],  
                x = 34067.76563,  
                y = -1130.59375,  
                z = -20.859375
              },  
              Angle = -0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2219]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 9,  
              HairType = 5622574,  
              HairColor = 3,  
              Tattoo = 23,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 6,  
              MorphTarget3 = 1,  
              MorphTarget4 = 5,  
              MorphTarget5 = 6,  
              MorphTarget6 = 7,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 6715182,  
              TrouserModel = 6708526,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Varo]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_madness_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2228]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2230]],  
                x = 34066.09375,  
                y = -1134.71875,  
                z = -20.578125
              },  
              Angle = -0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2226]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 5622830,  
              HairColor = 0,  
              Tattoo = 19,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 2,  
              MorphTarget3 = 4,  
              MorphTarget4 = 6,  
              MorphTarget5 = 5,  
              MorphTarget6 = 3,  
              MorphTarget7 = 6,  
              MorphTarget8 = 3,  
              JacketModel = 6715438,  
              TrouserModel = 6708526,  
              FeetModel = 6713134,  
              HandsModel = 6713646,  
              ArmModel = 6714670,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Vara]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2224]],  
            x = -2.765625,  
            y = -1.78125,  
            z = 0.125
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2223]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2241]],  
          Name = [[Group 18]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2233]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2234]],  
                x = 34097.57813,  
                y = -1124.703125,  
                z = -21
              },  
              Angle = -1.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2231]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 13,  
              HairType = 6711086,  
              HairColor = 0,  
              Tattoo = 13,  
              EyesColor = 7,  
              MorphTarget1 = 2,  
              MorphTarget2 = 6,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              JacketModel = 6712622,  
              TrouserModel = 6711598,  
              FeetModel = 6710062,  
              HandsModel = 6710574,  
              ArmModel = 6712110,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 5,  
              Aggro = 115,  
              Speed = 1,  
              WeaponLeftHand = 6774062,  
              Sex = 0,  
              WeaponRightHand = 6759726,  
              Level = 0,  
              Name = [[Ciero]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2237]],  
              Base = [[palette.entities.npcs.bandits.m_mage_atysian_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2238]],  
                x = 34094.1875,  
                y = -1123.03125,  
                z = -21
              },  
              Angle = -1.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2235]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 5,  
              HairType = 5422,  
              HairColor = 0,  
              Tattoo = 6,  
              EyesColor = 7,  
              MorphTarget1 = 3,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 6715438,  
              TrouserModel = 6708526,  
              FeetModel = 6713390,  
              HandsModel = 6713902,  
              ArmModel = 6714670,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Lichi]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_blind_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2244]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2246]],  
                x = 34097.73438,  
                y = -1121.1875,  
                z = -21.015625
              },  
              Angle = -1.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2242]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 2,  
              HairType = 6711086,  
              HairColor = 1,  
              Tattoo = 22,  
              EyesColor = 4,  
              MorphTarget1 = 0,  
              MorphTarget2 = 3,  
              MorphTarget3 = 4,  
              MorphTarget4 = 7,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              MorphTarget7 = 3,  
              MorphTarget8 = 0,  
              JacketModel = 6712878,  
              TrouserModel = 6711598,  
              FeetModel = 6710318,  
              HandsModel = 6710830,  
              ArmModel = 6712366,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 6774318,  
              Sex = 0,  
              WeaponRightHand = 6759982,  
              Level = 0,  
              Name = [[Varo]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2240]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2239]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2257]],  
          Name = [[Group 19]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2249]],  
              Base = [[palette.entities.npcs.bandits.m_mage_atysian_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2250]],  
                x = 34082.51563,  
                y = -1160.984375,  
                z = -21.0625
              },  
              Angle = 1.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2247]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 6,  
              HairType = 5934,  
              HairColor = 2,  
              Tattoo = 20,  
              EyesColor = 7,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 7,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 6715438,  
              TrouserModel = 6708526,  
              FeetModel = 6713390,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 2,  
              TrouserColor = 0,  
              FeetColor = 5,  
              Aggro = 115,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6934318,  
              Level = 0,  
              Name = [[Nirni]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_madness_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2253]],  
              Base = [[palette.entities.npcs.bandits.m_mage_celestial_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2254]],  
                x = 34083.34375,  
                y = -1163.890625,  
                z = -21.046875
              },  
              Angle = 1.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2251]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 4,  
              HairType = 5166,  
              HairColor = 2,  
              Tattoo = 6,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 6715182,  
              TrouserModel = 6708526,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Peero]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_fear_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2260]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2262]],  
                x = 34079.5625,  
                y = -1162.328125,  
                z = -21.078125
              },  
              Angle = 1.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2258]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 11,  
              HairType = 4910,  
              HairColor = 3,  
              Tattoo = 29,  
              EyesColor = 6,  
              MorphTarget1 = 6,  
              MorphTarget2 = 4,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              MorphTarget7 = 1,  
              MorphTarget8 = 5,  
              JacketModel = 6717998,  
              TrouserModel = 6716974,  
              FeetModel = 6710318,  
              HandsModel = 6716462,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 5,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6761262,  
              Level = 0,  
              Name = [[Robre]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2256]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2255]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2273]],  
          Name = [[Group 20]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2265]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2266]],  
                x = 34096.8125,  
                y = -1159.5,  
                z = -20.953125
              },  
              Angle = 1.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2263]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 0,  
              HairType = 6711342,  
              HairColor = 3,  
              Tattoo = 9,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 6712622,  
              TrouserModel = 6711854,  
              FeetModel = 6710318,  
              HandsModel = 6710574,  
              ArmModel = 6712366,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 2,  
              FeetColor = 3,  
              Aggro = 115,  
              Speed = 1,  
              WeaponLeftHand = 6774318,  
              Sex = 0,  
              WeaponRightHand = 6759982,  
              Level = 0,  
              Name = [[Gine]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2269]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2270]],  
                x = 34099.125,  
                y = -1162.46875,  
                z = -21.03125
              },  
              Angle = 0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2267]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 3,  
              HairType = 6711086,  
              HairColor = 5,  
              Tattoo = 6,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 6,  
              MorphTarget3 = 5,  
              MorphTarget4 = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              JacketModel = 6712622,  
              TrouserModel = 6711854,  
              FeetModel = 6710318,  
              HandsModel = 6710830,  
              ArmModel = 6712110,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 3,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 6774318,  
              Sex = 1,  
              WeaponRightHand = 6758958,  
              Level = 0,  
              Name = [[Chiani]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_pierce_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2276]],  
              Base = [[palette.entities.npcs.bandits.m_mage_atysian_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2278]],  
                x = 34094.70313,  
                y = -1162.421875,  
                z = -21
              },  
              Angle = 0.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2274]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 10,  
              HairType = 5422,  
              HairColor = 3,  
              Tattoo = 13,  
              EyesColor = 3,  
              MorphTarget1 = 6,  
              MorphTarget2 = 6,  
              MorphTarget3 = 1,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 3,  
              JacketModel = 6715182,  
              TrouserModel = 6708782,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Gisse]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_blind_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2272]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2271]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2289]],  
          Name = [[Group 21]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2281]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2282]],  
                x = 34098.25,  
                y = -1144.546875,  
                z = -21.3125
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2279]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 12,  
              HairType = 6711086,  
              HairColor = 2,  
              Tattoo = 25,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              MorphTarget7 = 1,  
              MorphTarget8 = 6,  
              JacketModel = 6712622,  
              TrouserModel = 6711598,  
              FeetModel = 6710318,  
              HandsModel = 6710830,  
              ArmModel = 6712366,  
              JacketColor = 1,  
              ArmColor = 5,  
              HandsColor = 1,  
              TrouserColor = 5,  
              FeetColor = 4,  
              Aggro = 115,  
              Speed = 1,  
              WeaponLeftHand = 6774318,  
              Sex = 1,  
              WeaponRightHand = 6759470,  
              Level = 0,  
              Name = [[Stassi]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2285]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2286]],  
                x = 34096.26563,  
                y = -1143.09375,  
                z = -21.234375
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2283]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 13,  
              HairType = 5934,  
              HairColor = 4,  
              Tattoo = 27,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 5,  
              MorphTarget3 = 0,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 6717998,  
              TrouserModel = 6714414,  
              FeetModel = 6715950,  
              HandsModel = 6716206,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6759726,  
              Level = 0,  
              Name = [[Aninne]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_slash_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2292]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2294]],  
                x = 34095.8125,  
                y = -1146.8125,  
                z = -21.203125
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2290]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 14,  
              HairType = 4910,  
              HairColor = 0,  
              Tattoo = 5,  
              EyesColor = 5,  
              MorphTarget1 = 4,  
              MorphTarget2 = 5,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 7,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              JacketModel = 6717998,  
              TrouserModel = 6714414,  
              FeetModel = 6715950,  
              HandsModel = 6716206,  
              ArmModel = 6717486,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6757422,  
              Level = 0,  
              Name = [[Liche]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_blunt_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2297]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2299]],  
                x = 34094.96875,  
                y = -1144.734375,  
                z = -21.203125
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2295]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 0,  
              HairType = 5166,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 5,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 6717998,  
              TrouserModel = 6716718,  
              FeetModel = 6715694,  
              HandsModel = 6716462,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6760494,  
              Level = 0,  
              Name = [[Pegio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2288]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2287]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2310]],  
          Name = [[Group 22]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2302]],  
              Base = [[palette.entities.npcs.bandits.m_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2303]],  
                x = 34067.76563,  
                y = -1151.25,  
                z = -20.109375
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2300]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 11,  
              HairType = 4910,  
              HairColor = 4,  
              Tattoo = 5,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 4,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 6715438,  
              TrouserModel = 6714414,  
              FeetModel = 6713390,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Aggro = 115,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Level = 0,  
              Name = [[Niro]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_poison_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2313]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2315]],  
                x = 34068.26563,  
                y = -1148.359375,  
                z = -20.21875
              },  
              Angle = 0.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2311]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 7,  
              HairType = 5622574,  
              HairColor = 1,  
              Tattoo = 7,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 6,  
              JacketModel = 6712622,  
              TrouserModel = 6716718,  
              FeetModel = 6710062,  
              HandsModel = 6716206,  
              ArmModel = 6717486,  
              JacketColor = 5,  
              ArmColor = 2,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6761262,  
              Level = 0,  
              Name = [[Sirgio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2318]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2320]],  
                x = 34064.71875,  
                y = -1151.203125,  
                z = -19.6875
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2316]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 2,  
              HairType = 6711342,  
              HairColor = 3,  
              Tattoo = 10,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 3,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 2,  
              MorphTarget8 = 2,  
              JacketModel = 6712878,  
              TrouserModel = 6711854,  
              FeetModel = 6710318,  
              HandsModel = 6710574,  
              ArmModel = 6712110,  
              JacketColor = 2,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponLeftHand = 6774062,  
              Sex = 1,  
              WeaponRightHand = 6759726,  
              Level = 0,  
              Name = [[Ciero]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2323]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2325]],  
                x = 34066.4375,  
                y = -1152.5625,  
                z = -19.84375
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2321]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 11,  
              HairType = 5622830,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 1,  
              MorphTarget1 = 7,  
              MorphTarget2 = 4,  
              MorphTarget3 = 7,  
              MorphTarget4 = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              JacketModel = 6717998,  
              TrouserModel = 6716974,  
              FeetModel = 6715694,  
              HandsModel = 6716206,  
              ArmModel = 6714926,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponLeftHand = 6758446,  
              Sex = 1,  
              WeaponRightHand = 6758446,  
              Level = 0,  
              Name = [[Anini]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_pierce_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2328]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2330]],  
                x = 34066.71875,  
                y = -1149.96875,  
                z = -19.984375
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2326]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 7,  
              HairType = 4910,  
              HairColor = 3,  
              Tattoo = 25,  
              EyesColor = 2,  
              MorphTarget1 = 0,  
              MorphTarget2 = 4,  
              MorphTarget3 = 6,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              MorphTarget7 = 6,  
              MorphTarget8 = 3,  
              JacketModel = 6715438,  
              TrouserModel = 6716974,  
              FeetModel = 6715950,  
              HandsModel = 6716206,  
              ArmModel = 6714926,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6757166,  
              Level = 0,  
              Name = [[Ciogio]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_blunt_f2.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2309]],  
            x = -0.078125,  
            y = 0.015625,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2308]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_2351]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_2352]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone trigger 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2353]],  
            x = 34142.32813,  
            y = -1183.796875,  
            z = -23.9375
          },  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_2355]],  
              Name = [[Places 1]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2357]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2358]],  
                    x = 15.765625,  
                    y = 4.515625,  
                    z = 2.140625
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2360]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2361]],  
                    x = 13.65625,  
                    y = 20.015625,  
                    z = 4
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2372]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2373]],  
                    x = 3.4375,  
                    y = 23.703125,  
                    z = 0.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2369]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2370]],  
                    x = -8.75,  
                    y = 14.234375,  
                    z = -0.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2363]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2364]],  
                    x = -5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_2366]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2367]],  
                    x = 3.359375,  
                    y = 4.234375,  
                    z = -1.078125
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              },  
              Deletable = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2354]],  
                x = 2.4375,  
                y = -0.3125,  
                z = -0.328125
              }
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_2355]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2395]],  
          Name = [[Group 23]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2387]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2388]],  
                x = 34098.64063,  
                y = -1133.046875,  
                z = -21.15625
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2385]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 10,  
              HairType = 6711086,  
              HairColor = 4,  
              Tattoo = 0,  
              EyesColor = 7,  
              MorphTarget1 = 1,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 7,  
              MorphTarget8 = 6,  
              JacketModel = 6712622,  
              TrouserModel = 6711854,  
              FeetModel = 6710318,  
              HandsModel = 6710830,  
              ArmModel = 6712366,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6759470,  
              Name = [[Lini]],  
              WeaponLeftHand = 6774318,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2391]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2392]],  
                x = 34095.76563,  
                y = -1133.9375,  
                z = -21.140625
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2389]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 1,  
              HairType = 6711086,  
              HairColor = 5,  
              Tattoo = 8,  
              EyesColor = 7,  
              MorphTarget1 = 2,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              JacketModel = 6712622,  
              TrouserModel = 6711854,  
              FeetModel = 6710318,  
              HandsModel = 6710830,  
              ArmModel = 6712110,  
              JacketColor = 5,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 3,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6758958,  
              Name = [[Roni]],  
              WeaponLeftHand = 6774318,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_pierce_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2398]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2400]],  
                x = 34098.23438,  
                y = -1130.125,  
                z = -21.125
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2396]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 13,  
              HairType = 6711342,  
              HairColor = 4,  
              Tattoo = 20,  
              EyesColor = 7,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 3,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 6712622,  
              TrouserModel = 6711598,  
              FeetModel = 6710062,  
              HandsModel = 6710830,  
              ArmModel = 6712366,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6759214,  
              Name = [[Cuinni]],  
              WeaponLeftHand = 6774062,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2403]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2405]],  
                x = 34095.79688,  
                y = -1132.515625,  
                z = -21.09375
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2401]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 11,  
              HairType = 5622574,  
              HairColor = 2,  
              Tattoo = 3,  
              EyesColor = 0,  
              MorphTarget1 = 1,  
              MorphTarget2 = 0,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 6717998,  
              TrouserModel = 6716974,  
              FeetModel = 6710062,  
              HandsModel = 6716462,  
              ArmModel = 6717230,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6760494,  
              Name = [[Frergio]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2408]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2410]],  
                x = 34096.73438,  
                y = -1130.390625,  
                z = -21.078125
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2406]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 1,  
              HairType = 4910,  
              HairColor = 0,  
              Tattoo = 25,  
              EyesColor = 1,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              MorphTarget7 = 1,  
              MorphTarget8 = 1,  
              JacketModel = 6717998,  
              TrouserModel = 6716718,  
              FeetModel = 6715694,  
              HandsModel = 6716462,  
              ArmModel = 6717486,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 3,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6760238,  
              Name = [[Aninne]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2393]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Activities = {
            },  
            Actions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2394]],  
            x = 0,  
            y = 0,  
            z = 0
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2426]],  
          Name = [[Group 24]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2418]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2419]],  
                x = 34101.65625,  
                y = -1152.25,  
                z = -21.015625
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2416]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 2,  
              HairType = 5166,  
              HairColor = 3,  
              Tattoo = 20,  
              EyesColor = 6,  
              MorphTarget1 = 2,  
              MorphTarget2 = 1,  
              MorphTarget3 = 1,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 6,  
              JacketModel = 6712622,  
              TrouserModel = 6716974,  
              FeetModel = 6710062,  
              HandsModel = 6716206,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 1,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Aggro = 115,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6760494,  
              Name = [[Niro]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2422]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2423]],  
                x = 34099.125,  
                y = -1154.734375,  
                z = -20.96875
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2420]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 1,  
              HairType = 5934,  
              HairColor = 5,  
              Tattoo = 30,  
              EyesColor = 0,  
              MorphTarget1 = 4,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 6712622,  
              TrouserModel = 6716718,  
              FeetModel = 6710062,  
              HandsModel = 6716462,  
              ArmModel = 6717230,  
              JacketColor = 5,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6761774,  
              Name = [[Triera]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2429]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2431]],  
                x = 34098.17188,  
                y = -1150.90625,  
                z = -21.125
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2427]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 4,  
              HairType = 5934,  
              HairColor = 2,  
              Tattoo = 24,  
              EyesColor = 2,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 6,  
              MorphTarget8 = 5,  
              JacketModel = 6712878,  
              TrouserModel = 6716974,  
              FeetModel = 6715694,  
              HandsModel = 6716462,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6761262,  
              Name = [[Vara]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2434]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2436]],  
                x = 34098.39063,  
                y = -1152.96875,  
                z = -21.03125
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2432]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 4,  
              HairType = 5422,  
              HairColor = 3,  
              Tattoo = 1,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 0,  
              MorphTarget3 = 5,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 3,  
              JacketModel = 6715438,  
              TrouserModel = 6708782,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 2,  
              ArmColor = 1,  
              HandsColor = 2,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Name = [[Vagia]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2439]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2441]],  
                x = 34098.17188,  
                y = -1154.109375,  
                z = -20.984375
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2437]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 11,  
              HairType = 5622830,  
              HairColor = 0,  
              Tattoo = 29,  
              EyesColor = 5,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 2,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 6715438,  
              TrouserModel = 6708782,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Name = [[Anigio]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2444]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2446]],  
                x = 34098,  
                y = -1152.078125,  
                z = -21.0625
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2442]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 4,  
              HairType = 5166,  
              HairColor = 3,  
              Tattoo = 4,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 3,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              JacketModel = 6715438,  
              TrouserModel = 6708526,  
              FeetModel = 6713390,  
              HandsModel = 6713902,  
              ArmModel = 6714670,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 5,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Name = [[Anini]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2449]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2451]],  
                x = 34100.96875,  
                y = -1153.890625,  
                z = -20.921875
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2447]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 4,  
              HairType = 5934,  
              HairColor = 5,  
              Tattoo = 22,  
              EyesColor = 0,  
              MorphTarget1 = 2,  
              MorphTarget2 = 1,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 5,  
              MorphTarget8 = 2,  
              JacketModel = 6715438,  
              TrouserModel = 6708526,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714670,  
              JacketColor = 5,  
              ArmColor = 2,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Name = [[Cuinni]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2454]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2456]],  
                x = 34100.64063,  
                y = -1150.8125,  
                z = -21.125
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2452]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 12,  
              HairType = 5622318,  
              HairColor = 1,  
              Tattoo = 25,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 0,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              JacketModel = 6715438,  
              TrouserModel = 6708782,  
              FeetModel = 6713390,  
              HandsModel = 6713646,  
              ArmModel = 6714670,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Name = [[Nirni]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2424]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Activities = {
            },  
            Actions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2425]],  
            x = 0,  
            y = 0,  
            z = 0
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2467]],  
          Name = [[Group 25]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2459]],  
              Base = [[palette.entities.npcs.bandits.m_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2460]],  
                x = 34079.5625,  
                y = -1132.875,  
                z = -20.96875
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2457]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 11,  
              HairType = 5622574,  
              HairColor = 4,  
              Tattoo = 9,  
              EyesColor = 3,  
              MorphTarget1 = 7,  
              MorphTarget2 = 3,  
              MorphTarget3 = 1,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              MorphTarget7 = 0,  
              MorphTarget8 = 0,  
              JacketModel = 6715438,  
              TrouserModel = 6714414,  
              FeetModel = 6713134,  
              HandsModel = 6713646,  
              ArmModel = 6714926,  
              JacketColor = 5,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 1,  
              Aggro = 115,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Name = [[Miagio]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_poison_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2463]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2464]],  
                x = 34076.9375,  
                y = -1134.265625,  
                z = -20.96875
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2461]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 5622830,  
              HairColor = 5,  
              Tattoo = 26,  
              EyesColor = 5,  
              MorphTarget1 = 5,  
              MorphTarget2 = 2,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 1,  
              MorphTarget7 = 5,  
              MorphTarget8 = 0,  
              JacketModel = 6717742,  
              TrouserModel = 6716718,  
              FeetModel = 6715694,  
              HandsModel = 6716206,  
              ArmModel = 6714670,  
              JacketColor = 5,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6758190,  
              Name = [[Girni]],  
              WeaponLeftHand = 6758190,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2470]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2472]],  
                x = 34077.53125,  
                y = -1130.140625,  
                z = -20.96875
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2468]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 8,  
              HairType = 5422,  
              HairColor = 1,  
              Tattoo = 29,  
              EyesColor = 4,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 5,  
              MorphTarget8 = 4,  
              JacketModel = 6717742,  
              TrouserModel = 6716718,  
              FeetModel = 6715694,  
              HandsModel = 6716462,  
              ArmModel = 6717230,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6759982,  
              Name = [[Peccio]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_slash_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2475]],  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2477]],  
                x = 34075.96875,  
                y = -1131.921875,  
                z = -20.953125
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2473]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 14,  
              HairType = 6711086,  
              HairColor = 5,  
              Tattoo = 20,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 2,  
              MorphTarget4 = 7,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 6712622,  
              TrouserModel = 6711854,  
              FeetModel = 6710318,  
              HandsModel = 6710830,  
              ArmModel = 6712110,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6758702,  
              Name = [[Robre]],  
              WeaponLeftHand = 6774062,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_pierce_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2480]],  
              Base = [[palette.entities.npcs.bandits.m_mage_atysian_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2482]],  
                x = 34080.32813,  
                y = -1134.5625,  
                z = -20.984375
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2478]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 12,  
              HairType = 5622318,  
              HairColor = 0,  
              Tattoo = 25,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 4,  
              MorphTarget8 = 3,  
              JacketModel = 6715182,  
              TrouserModel = 6708526,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714670,  
              JacketColor = 4,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Name = [[Trira]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_blind_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2485]],  
              Base = [[palette.entities.npcs.bandits.m_mage_celestial_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2487]],  
                x = 34081,  
                y = -1131.4375,  
                z = -20.96875
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2483]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 3,  
              HairType = 5622830,  
              HairColor = 0,  
              Tattoo = 29,  
              EyesColor = 5,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 2,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 2,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 6715182,  
              TrouserModel = 6708526,  
              FeetModel = 6713390,  
              HandsModel = 6713646,  
              ArmModel = 6714926,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Name = [[Sirgia]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_fear_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2490]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2492]],  
                x = 34082.25,  
                y = -1133.5625,  
                z = -20.953125
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2488]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 1,  
              HairType = 5622574,  
              HairColor = 0,  
              Tattoo = 6,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 6715182,  
              TrouserModel = 6708526,  
              FeetModel = 6713134,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Name = [[Gicha]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2465]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Activities = {
            },  
            Actions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2466]],  
            x = 0,  
            y = 0,  
            z = 0
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2503]],  
          Name = [[Group 26]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2495]],  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2496]],  
                x = 34089.9375,  
                y = -1138.84375,  
                z = -21.078125
              },  
              Angle = 0.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2493]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 0,  
              HairType = 5622318,  
              HairColor = 4,  
              Tattoo = 13,  
              EyesColor = 7,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 4,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 2,  
              MorphTarget7 = 3,  
              MorphTarget8 = 4,  
              JacketModel = 6712878,  
              TrouserModel = 6716974,  
              FeetModel = 6715950,  
              HandsModel = 6716462,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 5,  
              FeetColor = 4,  
              Aggro = 115,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6761518,  
              Name = [[Pegio]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2499]],  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2500]],  
                x = 34089.70313,  
                y = -1140.453125,  
                z = -21.0625
              },  
              Angle = 0.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2497]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 11,  
              HairType = 5934,  
              HairColor = 1,  
              Tattoo = 30,  
              EyesColor = 4,  
              MorphTarget1 = 4,  
              MorphTarget2 = 6,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              JacketModel = 6715438,  
              TrouserModel = 6708782,  
              FeetModel = 6713390,  
              HandsModel = 6713902,  
              ArmModel = 6714926,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 3,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6935086,  
              Name = [[Chiani]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_poison_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2506]],  
              Base = [[palette.entities.npcs.bandits.m_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2508]],  
                x = 34089.82813,  
                y = -1142.171875,  
                z = -21.0625
              },  
              Angle = 0.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2504]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 14,  
              HairType = 5622830,  
              HairColor = 0,  
              Tattoo = 3,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 7,  
              MorphTarget4 = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              MorphTarget7 = 5,  
              MorphTarget8 = 0,  
              JacketModel = 6715438,  
              TrouserModel = 6714414,  
              FeetModel = 6713134,  
              HandsModel = 6713646,  
              ArmModel = 6714670,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Name = [[Sirgio]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_poison_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2511]],  
              Base = [[palette.entities.npcs.bandits.m_mage_celestial_curser_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2513]],  
                x = 34090.35938,  
                y = -1137.015625,  
                z = -21.0625
              },  
              Angle = 0.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2509]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 2,  
              HairType = 5622574,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 2,  
              MorphTarget1 = 2,  
              MorphTarget2 = 4,  
              MorphTarget3 = 5,  
              MorphTarget4 = 5,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              MorphTarget7 = 7,  
              MorphTarget8 = 7,  
              JacketModel = 6715438,  
              TrouserModel = 6708526,  
              FeetModel = 6713390,  
              HandsModel = 6713646,  
              ArmModel = 6714926,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6935086,  
              Name = [[Lichi]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_curser_fear_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2516]],  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2518]],  
                x = 34090.71875,  
                y = -1135.25,  
                z = -21.0625
              },  
              Angle = 0.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2514]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 9,  
              HairType = 5622574,  
              HairColor = 3,  
              Tattoo = 24,  
              EyesColor = 2,  
              MorphTarget1 = 1,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 6,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              MorphTarget7 = 5,  
              MorphTarget8 = 5,  
              JacketModel = 6717742,  
              TrouserModel = 6714158,  
              FeetModel = 6715694,  
              HandsModel = 6716462,  
              ArmModel = 6717486,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6757166,  
              Name = [[Pegio]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_blunt_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2501]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Activities = {
            },  
            Actions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2502]],  
            x = 0,  
            y = 0,  
            z = 0
          }
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_2524]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_2525]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_2526]],  
                  Emote = [[Stop]],  
                  Who = r2.RefId([[Client1_1943]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2530]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_2527]],  
              Time = 6,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_2528]],  
                  Emote = [[Stop]],  
                  Who = r2.RefId([[Client1_1943]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_2529]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_2522]]
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2523]],  
            x = 34264.64063,  
            y = -1199.640625,  
            z = -17
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]]
        },  
        {
          MissionText = [[Search <qt1> <item1> for me, please!]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[Thank you]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_2568]]
          },  
          WaitValidationText = [[I beg you to give me your <qt1> <item1>.]],  
          DisplayMode = 0,  
          Item1Qty = 1,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.bot_request_item]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[Give <mission_giver> some items]],  
          MissionGiver = r2.RefId([[Client1_1943]]),  
          Item1Id = r2.RefId([[Client1_2566]]),  
          _Seed = 1147275205,  
          InheritPos = 1,  
          InstanceId = [[Client1_2567]],  
          Name = [[Mission: Request Item 1]],  
          Position = {
            y = -1204.059326,  
            x = 34265.44141,  
            z = -19,  
            Class = [[Position]],  
            InstanceId = [[Client1_2569]]
          },  
          Class = [[RequestItem]],  
          Active = 1,  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        },  
        {
          Item2Id = r2.RefId([[]]),  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_2571]]
          },  
          Class = [[EasterEgg]],  
          ItemQty = 1,  
          DisplayMode = 0,  
          Item1Qty = 1,  
          Item3Qty = 1,  
          Base = [[palette.entities.botobjects.chest_wisdom_std_sel]],  
          Item2Qty = 1,  
          Item1Id = r2.RefId([[Client1_2566]]),  
          InheritPos = 1,  
          Name = [[Item Chest 1]],  
          Position = {
            y = -1126.157837,  
            x = 34054.69922,  
            z = -21,  
            Class = [[Position]],  
            InstanceId = [[Client1_2572]]
          },  
          _Seed = 1147275347,  
          InstanceId = [[Client1_2570]],  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        }
      },  
      Counters = {
      },  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1828]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 1
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1155]],  
        Count = 1,  
        Text = [[hey mec, va nous cherchez des potes!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2529]],  
        Count = 2,  
        Text = [[va attaquer les gens et magne toi le cul]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2530]],  
        Count = 1,  
        Text = [[va attaquer les gens et magne toi le cul!]]
      }
    },  
    InstanceId = [[Client1_1011]]
  }
}