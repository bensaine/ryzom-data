scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_686]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 8,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_688]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 17,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_699]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_697]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 1]],  
              Position = {
                y = -2318.09375,  
                x = 30729.46875,  
                InstanceId = [[Client1_700]],  
                Class = [[Position]],  
                z = 71
              },  
              Angle = -2.96875,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_703]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_701]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 2]],  
              Position = {
                y = -2329.015625,  
                x = 30734,  
                InstanceId = [[Client1_704]],  
                Class = [[Position]],  
                z = 72.125
              },  
              Angle = -2.96875,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_707]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_705]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              Position = {
                y = -2307.203125,  
                x = 30755.95313,  
                InstanceId = [[Client1_708]],  
                Class = [[Position]],  
                z = 67.90625
              },  
              Angle = -1.796875,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_711]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_709]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              Position = {
                y = -2314.515625,  
                x = 30764.04688,  
                InstanceId = [[Client1_712]],  
                Class = [[Position]],  
                z = 68.0625
              },  
              Angle = -2.96875,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_715]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_713]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 3]],  
              Position = {
                y = -2322.390625,  
                x = 30756.60938,  
                InstanceId = [[Client1_716]],  
                Class = [[Position]],  
                z = 70.046875
              },  
              Angle = -4.375,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_727]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_725]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[butterflies 1]],  
              Position = {
                y = -2308.59375,  
                x = 30747.20313,  
                InstanceId = [[Client1_728]],  
                Class = [[Position]],  
                z = 67.484375
              },  
              Angle = -2.390625,  
              Base = [[palette.entities.botobjects.fx_ju_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_735]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_733]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fireflies 1]],  
              Position = {
                y = -2310.234375,  
                x = 30739.03125,  
                InstanceId = [[Client1_736]],  
                Class = [[Position]],  
                z = 68.265625
              },  
              Angle = -2.390625,  
              Base = [[palette.entities.botobjects.fx_fo_bugsb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_766]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_764]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -2315.4375,  
                x = 30753.73438,  
                InstanceId = [[Client1_767]],  
                Class = [[Position]],  
                z = 69
              },  
              Angle = -2.875,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_806]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_811]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2303.21875,  
                    x = 30695.375,  
                    InstanceId = [[Client1_812]],  
                    Class = [[Position]],  
                    z = 76.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_814]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2306.28125,  
                    x = 30709.67188,  
                    InstanceId = [[Client1_815]],  
                    Class = [[Position]],  
                    z = 73.15625
                  }
                }
              },  
              Position = {
                y = -0.734375,  
                x = 3.5625,  
                InstanceId = [[Client1_805]],  
                Class = [[Position]],  
                z = 0.40625
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client1_908]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_910]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2315.765625,  
                    x = 30746.57813,  
                    InstanceId = [[Client1_911]],  
                    Class = [[Position]],  
                    z = 68.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_913]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2315.765625,  
                    x = 30746.57813,  
                    InstanceId = [[Client1_914]],  
                    Class = [[Position]],  
                    z = 68.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_916]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2307.609375,  
                    x = 30715.65625,  
                    InstanceId = [[Client1_917]],  
                    Class = [[Position]],  
                    z = 72.921875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_907]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 3]],  
              InstanceId = [[Client1_954]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_956]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2304.171875,  
                    x = 30715.26563,  
                    InstanceId = [[Client1_957]],  
                    Class = [[Position]],  
                    z = 72.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_962]],  
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_963]],  
                    x = 30791.73438,  
                    y = -2165.453125,  
                    z = 79.09375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_953]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_982]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_980]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -2139.640625,  
                x = 30809.89063,  
                InstanceId = [[Client1_983]],  
                Class = [[Position]],  
                z = 73.1875
              },  
              Angle = -1.6875,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_986]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_984]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -2159.265625,  
                x = 30825.875,  
                InstanceId = [[Client1_987]],  
                Class = [[Position]],  
                z = 71.953125
              },  
              Angle = -2.6875,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_990]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_988]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -2172.328125,  
                x = 30820.39063,  
                InstanceId = [[Client1_991]],  
                Class = [[Position]],  
                z = 72.96875
              },  
              Angle = 2.90625,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1352]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1350]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[yellow pollen 1]],  
              Position = {
                y = -2307.15625,  
                x = 30700.39063,  
                InstanceId = [[Client1_1353]],  
                Class = [[Position]],  
                z = 77.296875
              },  
              Angle = -0.296875,  
              Base = [[palette.entities.botobjects.fx_tr_pollen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1360]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1358]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[light pollen 1]],  
              Position = {
                y = -2262.53125,  
                x = 30706.35938,  
                InstanceId = [[Client1_1361]],  
                Class = [[Position]],  
                z = 68.90625
              },  
              Angle = -1.875,  
              Base = [[palette.entities.botobjects.fx_ju_solbirthb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1364]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1362]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[light pollen 2]],  
              Position = {
                y = -2279.40625,  
                x = 30715.1875,  
                InstanceId = [[Client1_1365]],  
                Class = [[Position]],  
                z = 69.59375
              },  
              Angle = -2.96875,  
              Base = [[palette.entities.botobjects.fx_ju_solbirthb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1368]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1366]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[light pollen 3]],  
              Position = {
                y = -2250.453125,  
                x = 30729.40625,  
                InstanceId = [[Client1_1369]],  
                Class = [[Position]],  
                z = 68.671875
              },  
              Angle = -2.625,  
              Base = [[palette.entities.botobjects.fx_ju_solbirthb]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1372]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1370]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[incandescent green pollen 1]],  
              Position = {
                y = -2265.796875,  
                x = 30726.14063,  
                InstanceId = [[Client1_1373]],  
                Class = [[Position]],  
                z = 68.34375
              },  
              Angle = -2.625,  
              Base = [[palette.entities.botobjects.fx_ju_solbirthc]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1382]],  
              Name = [[Route 4]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1384]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1385]],  
                    x = 30792.48438,  
                    y = -2159.53125,  
                    z = 77.3125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1387]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1388]],  
                    x = 30780.71875,  
                    y = -2155.125,  
                    z = 78.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1390]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1391]],  
                    x = 30767.375,  
                    y = -2147.546875,  
                    z = 75.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1393]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1394]],  
                    x = 30766.01563,  
                    y = -2100.203125,  
                    z = 76.421875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1396]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1397]],  
                    x = 30731.60938,  
                    y = -2062.25,  
                    z = 74.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1399]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1400]],  
                    x = 30697.65625,  
                    y = -1994.125,  
                    z = 76.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1402]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1403]],  
                    x = 30700.15625,  
                    y = -1929.640625,  
                    z = 72.640625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1405]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1406]],  
                    x = 30667.67188,  
                    y = -1871.28125,  
                    z = 62.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1408]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1409]],  
                    x = 30604.64063,  
                    y = -1861.828125,  
                    z = 60.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1411]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1412]],  
                    x = 30527.98438,  
                    y = -1880.953125,  
                    z = 55
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1414]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1415]],  
                    x = 30450.96875,  
                    y = -1854.953125,  
                    z = 52.671875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1381]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1417]],  
              Name = [[Place 1]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1419]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1420]],  
                    x = 30505.75,  
                    y = -1865.765625,  
                    z = 57.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1422]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1423]],  
                    x = 30509.95313,  
                    y = -1891.828125,  
                    z = 55.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1425]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1426]],  
                    x = 30551.71875,  
                    y = -1885.734375,  
                    z = 57.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1428]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1429]],  
                    x = 30565.65625,  
                    y = -1851.875,  
                    z = 58.3125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1416]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1467]],  
              Name = [[Place 2]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1469]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1470]],  
                    x = 30695.26563,  
                    y = -2001.21875,  
                    z = 76.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1472]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1473]],  
                    x = 30708.73438,  
                    y = -1987.359375,  
                    z = 76.90625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1475]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1476]],  
                    x = 30722.14063,  
                    y = -2011.8125,  
                    z = 79.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1478]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1479]],  
                    x = 30706.89063,  
                    y = -2022.46875,  
                    z = 78.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1481]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1482]],  
                    x = 30696.75,  
                    y = -2015.65625,  
                    z = 75.78125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1466]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1490]],  
              Name = [[Place 3]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1492]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1493]],  
                    x = 30672.73438,  
                    y = -1893.015625,  
                    z = 63.71875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1495]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1496]],  
                    x = 30694,  
                    y = -1895.828125,  
                    z = 66.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1498]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1499]],  
                    x = 30696.28125,  
                    y = -1931.484375,  
                    z = 71.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1501]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1502]],  
                    x = 30682.0625,  
                    y = -1926.953125,  
                    z = 69.5625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1489]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_690]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_689]],  
      Events = {
      }
    },  
    {
      Cost = 30,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_786]],  
        [[Client1_822]],  
        [[Client1_824]],  
        [[Client1_831]],  
        [[Client1_847]],  
        [[Client1_918]],  
        [[Client1_1030]],  
        [[Client1_1032]],  
        [[Client1_1172]],  
        [[Client1_1175]],  
        [[Client1_1219]],  
        [[Client1_1483]],  
        [[Client1_1485]],  
        [[Client1_1487]],  
        [[Client1_1503]],  
        [[Client1_1505]],  
        [[Client1_1519]],  
        [[Client1_1521]],  
        [[Client1_1523]],  
        [[Client1_1525]],  
        [[Client1_1527]],  
        [[Client1_1529]],  
        [[Client1_1531]],  
        [[Client1_1533]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_695]],  
              ActivitiesId = {
                [[Client1_822]],  
                [[Client1_824]],  
                [[Client1_831]],  
                [[Client1_1032]],  
                [[Client1_1172]]
              },  
              HairType = 6702,  
              TrouserColor = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 3,  
              HandsModel = 5613870,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 0,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 0,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_693]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[SEQ1]],  
                    InstanceId = [[Client1_822]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_823]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[Client1_788]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Stand Still for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq2]],  
                    InstanceId = [[Client1_824]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1312]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_806]],  
                        Name = [[Activity 1 : Follow Route Route 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq3]],  
                    InstanceId = [[Client1_831]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_832]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[Client1_833]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq4]],  
                    InstanceId = [[Client1_1032]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1174]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[Client1_944]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Stand Still for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq5]],  
                    InstanceId = [[Client1_1172]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1173]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_954]],  
                        Name = [[Activity 1 : Follow Route Route 3 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_788]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_789]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Bow]],  
                            InstanceId = [[Client1_790]],  
                            Who = [[Client1_695]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_793]]
                          }
                        },  
                        Name = [[Chat 1 : (after  0sec) Xavier's Wife says Hi P...]]
                      },  
                      {
                        Time = 1,  
                        InstanceId = [[Client1_795]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_796]],  
                            Who = [[Client1_695]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_797]]
                          }
                        },  
                        Name = [[Chat 2 : (after  1sec) Xavier's Wife says I ne...]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_799]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_800]],  
                            Who = [[Client1_695]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_802]]
                          }
                        },  
                        Name = [[Chat 3 : (after  3sec) Xavier's Wife says Foll...]]
                      }
                    }
                  },  
                  {
                    Name = [[Chat2]],  
                    InstanceId = [[Client1_833]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_834]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_835]],  
                            Who = [[Client1_695]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_1349]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Xavier's Wife says Aw !...]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_838]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_839]],  
                            Who = [[Client1_695]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_841]]
                          }
                        },  
                        Name = [[Chat 2 : (after  3sec) Xavier's Wife says Lets...]]
                      }
                    }
                  },  
                  {
                    Name = [[Chat3]],  
                    InstanceId = [[Client1_944]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_945]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_946]],  
                            Who = [[Client1_695]],  
                            Class = [[ChatAction]],  
                            Facing = [[Client1_894]],  
                            Says = [[Client1_947]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) Xavier's Wife says You ...]]
                      },  
                      {
                        Time = 1,  
                        InstanceId = [[Client1_949]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_950]],  
                            Who = [[Client1_695]],  
                            Class = [[ChatAction]],  
                            Facing = [[Client1_894]],  
                            Says = [[Client1_952]]
                          }
                        },  
                        Name = [[Chat 2 : (after  1sec) Xavier's Wife says We s...]]
                      }
                    }
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1544]],  
                    Name = [[Chat4]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1545]],  
                        Time = 1,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1546]],  
                            Emote = [[]],  
                            Who = [[Client1_695]],  
                            Facing = [[Client1_994]],  
                            Says = [[Client1_1547]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Xavier's Wife says ...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of chat sequence Chat1' event]],  
                    InstanceId = [[Client1_1308]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1313]],  
                          Value = [[Client1_824]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1314]],  
                        Entity = [[Client1_695]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_1307]],  
                      Value = [[Client1_788]],  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[Event 2 : on 'end of activity step Activity 1 : Follow Route Route 1 without time limit' event]],  
                    InstanceId = [[Client1_1317]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_1316]],  
                      Value = [[Client1_1312]],  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1318]],  
                          Value = [[Client1_831]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1319]],  
                        Entity = [[Client1_695]],  
                        Class = [[ActionStep]]
                      }
                    }
                  },  
                  {
                    Name = [[Event 3 : on 'end of chat sequence Chat3' event]],  
                    InstanceId = [[Client1_1339]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_1338]],  
                      Value = [[Client1_944]],  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1340]],  
                          Value = [[Client1_1172]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1341]],  
                        Entity = [[Client1_695]],  
                        Class = [[ActionStep]]
                      }
                    }
                  },  
                  {
                    Name = [[Event 4 : on 'end of chat sequence Chat3' event]],  
                    InstanceId = [[Client1_1345]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_1344]],  
                      Value = [[Client1_944]],  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1346]],  
                          Value = [[Client1_1219]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1347]],  
                        Entity = [[Client1_906]],  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1308]],  
                    Name = [[Trigger 1 : Begin activity Seq2 on 'End of chat Chat1' event of Xavier's Wife]],  
                    InstanceId = [[Client1_1315]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1314]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1317]],  
                    Name = [[Trigger 2 : Begin activity Seq3 on 'End of activity step Activity 1 : Follow Route Route 1 without time limit' event of Xavier's Wife]],  
                    InstanceId = [[Client1_1320]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1319]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1335]],  
                    Name = [[Trigger 3 : Begin activity Seq4 on 'End of chat Chat1' event of Tryker]],  
                    InstanceId = [[Client1_1333]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1337]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1339]],  
                    Name = [[Trigger 4 : Begin activity Seq5 on 'End of chat Chat3' event of Xavier's Wife]],  
                    InstanceId = [[Client1_1342]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1341]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              SheetClient = [[basic_tryker_female.creature]],  
              FeetModel = 5653550,  
              Speed = [[run]],  
              Angle = 2.625,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              ArmModel = 0,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              BotAttackable = 1,  
              ArmColor = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_b1.creature]],  
              WeaponRightHand = 0,  
              JacketModel = 5614638,  
              Name = [[Xavier's Wife]],  
              Position = {
                y = -2303.046875,  
                x = 30697.51563,  
                InstanceId = [[Client1_696]],  
                Class = [[Position]],  
                z = 76.90625
              },  
              Sex = 1,  
              MorphTarget7 = 1,  
              MorphTarget3 = 3,  
              Tattoo = 18
            },  
            {
              InstanceId = [[Client1_966]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_964]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              Position = {
                y = -2140.453125,  
                x = 30792.0625,  
                InstanceId = [[Client1_967]],  
                Class = [[Position]],  
                z = 74.140625
              },  
              Angle = -2.15625,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_970]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_968]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 2]],  
              Position = {
                y = -2144.75,  
                x = 30786.64063,  
                InstanceId = [[Client1_971]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              Angle = -1.09375,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_974]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_972]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 3]],  
              Position = {
                y = -2150.375,  
                x = 30783.07813,  
                InstanceId = [[Client1_975]],  
                Class = [[Position]],  
                z = 76.703125
              },  
              Angle = -1.640625,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_978]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_976]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 4]],  
              Position = {
                y = -2136.296875,  
                x = 30797.23438,  
                InstanceId = [[Client1_979]],  
                Class = [[Position]],  
                z = 73.625
              },  
              Angle = -1.640625,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_994]],  
              ActivitiesId = {
                [[Client1_1533]]
              },  
              HairType = 7470,  
              TrouserColor = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 10,  
              HandsModel = 5613870,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 5,  
              HandsColor = 1,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_992]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1533]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1534]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[Client1_1535]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Stand Still for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1535]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1536]],  
                        Time = 1,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1537]],  
                            Emote = [[]],  
                            Who = [[Client1_994]],  
                            Facing = [[Client1_695]],  
                            Says = [[Client1_1538]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Chief says Miss...]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1540]],  
                        Time = 1,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1541]],  
                            Emote = [[]],  
                            Who = [[Client1_994]],  
                            Facing = [[Client1_695]],  
                            Says = [[Client1_1543]]
                          }
                        },  
                        Name = [[Chat 2 : (after  1sec) Chief says Your...]]
                      }
                    }
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1549]],  
                    Name = [[Chat2]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1550]],  
                        Time = 1,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1551]],  
                            Emote = [[]],  
                            Who = [[Client1_994]],  
                            Facing = [[]],  
                            Says = [[]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Chief says ...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 6,  
              FeetModel = 5653550,  
              SheetClient = [[basic_tryker_male.creature]],  
              Angle = 3.109375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              ArmModel = 0,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              BotAttackable = 0,  
              ArmColor = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              JacketModel = 5614638,  
              Name = [[Chief]],  
              Position = {
                y = -2165.125,  
                x = 30794.09375,  
                InstanceId = [[Client1_995]],  
                Class = [[Position]],  
                z = 78.4375
              },  
              Sex = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 7,  
              Tattoo = 12
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1432]],  
              Base = [[palette.entities.creatures.ckhdb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1433]],  
                x = 30531.17188,  
                y = -1865.765625,  
                z = 53.9375
              },  
              Angle = 2.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1430]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1525]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1526]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1417]],  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1525]]
              },  
              InheritPos = 1,  
              Name = [[Vulgar  Kipee]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1436]],  
              Base = [[palette.entities.creatures.ckhdb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1437]],  
                x = 30511.09375,  
                y = -1870.625,  
                z = 56.140625
              },  
              Angle = 2.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1434]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1531]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1532]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1417]],  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1531]]
              },  
              InheritPos = 1,  
              Name = [[Vigorous  Kipee]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1440]],  
              Base = [[palette.entities.creatures.ckhdb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1441]],  
                x = 30544.09375,  
                y = -1866.6875,  
                z = 56.40625
              },  
              Angle = 2.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1438]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1523]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1524]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1417]],  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1523]]
              },  
              InheritPos = 1,  
              Name = [[Moderate  Kipee]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1444]],  
              Base = [[palette.entities.creatures.ckhdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1445]],  
                x = 30513.6875,  
                y = -1880.46875,  
                z = 55.578125
              },  
              Angle = 2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1442]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1529]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1530]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1417]],  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1529]]
              },  
              InheritPos = 1,  
              Name = [[Hard Kipee]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1448]],  
              Base = [[palette.entities.creatures.cccdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1449]],  
                x = 30682.54688,  
                y = -1911.171875,  
                z = 66.765625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1446]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1503]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1504]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1490]],  
                        Name = [[Activity 1 : Wander Place 3 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1503]]
              },  
              InheritPos = 1,  
              Name = [[Baying Goari]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1452]],  
              Base = [[palette.entities.creatures.cccdb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1453]],  
                x = 30688.60938,  
                y = -1900.125,  
                z = 66.328125
              },  
              Angle = 2.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1450]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1505]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1506]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1490]],  
                        Name = [[Activity 1 : Wander Place 3 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1505]]
              },  
              InheritPos = 1,  
              Name = [[Scowling Goari]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1456]],  
              Base = [[palette.entities.creatures.ccadb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1457]],  
                x = 30710.40625,  
                y = -2012.015625,  
                z = 78.421875
              },  
              Angle = 2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1454]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1487]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1488]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1467]],  
                        Name = [[Activity 1 : Wander Place 2 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1487]]
              },  
              InheritPos = 1,  
              Name = [[Growling Gingo]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1460]],  
              Base = [[palette.entities.creatures.ccadb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1461]],  
                x = 30702,  
                y = -2012.25,  
                z = 76.9375
              },  
              Angle = 2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1458]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1483]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1484]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1467]],  
                        Name = [[Activity 1 : Wander Place 2 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1483]]
              },  
              InheritPos = 1,  
              Name = [[Vigorous Gingo]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1464]],  
              Base = [[palette.entities.creatures.ccadb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1465]],  
                x = 30703.34375,  
                y = -2009.15625,  
                z = 77.21875
              },  
              Angle = 2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1462]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1485]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1486]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1467]],  
                        Name = [[Activity 1 : Wander Place 2 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1485]]
              },  
              InheritPos = 1,  
              Name = [[Scowling Gingo]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1509]],  
              Base = [[palette.entities.creatures.ckhib3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1510]],  
                x = 30548.3125,  
                y = -1881.375,  
                z = 57.203125
              },  
              Angle = -0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1507]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1519]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1520]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1417]],  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1519]]
              },  
              InheritPos = 1,  
              Name = [[Great Trooper Kipee]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1513]],  
              Base = [[palette.entities.creatures.ckbib1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1514]],  
                x = 30529.34375,  
                y = -1875.734375,  
                z = 54.6875
              },  
              Angle = -0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1511]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1527]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1528]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1417]],  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1527]]
              },  
              InheritPos = 1,  
              Name = [[Trooper Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1517]],  
              Base = [[palette.entities.creatures.ckcib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1518]],  
                x = 30547.03125,  
                y = -1873.46875,  
                z = 57.125
              },  
              Angle = -0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1515]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1521]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1522]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_1417]],  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1521]]
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kizarak]]
            }
          },  
          InstanceId = [[Client1_692]]
        },  
        {
          InstanceId = [[Client1_891]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Gingos]],  
          Position = {
            y = 19.234375,  
            x = 1.84375,  
            InstanceId = [[Client1_890]],  
            Class = [[Position]],  
            z = -3.546875
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_889]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_879]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_877]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'group death' event]],  
                    InstanceId = [[Client1_1055]],  
                    Event = {
                      Type = [[group death]],  
                      InstanceId = [[Client1_1054]],  
                      Value = [[]],  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1226]],  
                    Event = {
                      Type = [[group death]],  
                      InstanceId = [[Client1_1225]],  
                      Value = [[]],  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1323]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[group death]],  
                      InstanceId = [[Client1_1322]],  
                      Value = [[]],  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1324]],  
                          Value = [[Client1_1030]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1325]],  
                        Entity = [[Client1_906]],  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1380]],  
                    LogicEntityAction = [[Client1_1377]],  
                    ActionStep = [[Client1_1379]],  
                    Name = [[]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                y = -2337.125,  
                x = 30742.04688,  
                InstanceId = [[Client1_880]],  
                Class = [[Position]],  
                z = 72.796875
              },  
              ActivitiesId = {
              },  
              Angle = -3.109375,  
              Base = [[palette.entities.creatures.ccadb4]],  
              BotAttackable = 0
            },  
            {
              InstanceId = [[Client1_887]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_885]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                y = -2328.296875,  
                x = 30745.14063,  
                InstanceId = [[Client1_888]],  
                Class = [[Position]],  
                z = 71.140625
              },  
              Angle = -2.6875,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_883]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_881]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Baying Gingo]],  
              Position = {
                y = -2342.75,  
                x = 30748.34375,  
                InstanceId = [[Client1_884]],  
                Class = [[Position]],  
                z = 73.71875
              },  
              Angle = -2.671875,  
              Base = [[palette.entities.creatures.ccadb4]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_906]],  
          ActivitiesId = {
            [[Client1_918]],  
            [[Client1_1175]],  
            [[Client1_1219]]
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Trykers]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_905]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_904]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_894]],  
              ActivitiesId = {
                [[Client1_1030]]
              },  
              HairType = 5623598,  
              TrouserColor = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 5613870,  
              FeetColor = 3,  
              GabaritBreastSize = 4,  
              GabaritHeight = 6,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 9,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_892]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_918]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1220]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq2]],  
                    InstanceId = [[Client1_1030]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1221]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_908]],  
                        Name = [[Activity 1 : Follow Route Route 2 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq3]],  
                    InstanceId = [[Client1_1175]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1222]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[Client1_930]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq4]],  
                    InstanceId = [[Client1_1219]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1223]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_954]],  
                        Name = [[Activity 1 : Follow Route Route 3 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_930]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_931]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Cheer]],  
                            InstanceId = [[Client1_932]],  
                            Who = [[Client1_898]],  
                            Class = [[ChatAction]],  
                            Facing = [[Client1_695]],  
                            Says = [[Client1_933]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Speaker Tryker says Than...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of activity step Activity 1 : Follow Route Route 2 without time limit' event]],  
                    InstanceId = [[Client1_1327]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_1326]],  
                      Value = [[Client1_1221]],  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1328]],  
                          Value = [[Client1_1175]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1329]],  
                        Entity = [[Client1_906]],  
                        Class = [[ActionStep]]
                      }
                    }
                  },  
                  {
                    Name = [[Event 2 : on 'end of chat sequence Chat1' event]],  
                    InstanceId = [[Client1_1335]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_1334]],  
                      Value = [[Client1_930]],  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_1336]],  
                          Value = [[Client1_1032]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_1337]],  
                        Entity = [[Client1_695]],  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1323]],  
                    Name = [[Trigger 1 : Begin activity Seq2 on 'Group death' event of Baying Gingo]],  
                    InstanceId = [[Client1_1321]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1325]]
                  },  
                  {
                    LogicEntityAction = [[Client1_1345]],  
                    Name = [[Trigger 2 : Begin activity Seq4 on 'End of chat Chat3' event of Xavier's Wife]],  
                    InstanceId = [[Client1_1343]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1347]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 5653550,  
              SheetClient = [[basic_tryker_male.creature]],  
              Angle = 2.765625,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              ArmModel = 0,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              BotAttackable = 1,  
              ArmColor = 3,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              JacketModel = 5614638,  
              Name = [[Tryker]],  
              Position = {
                y = -2316.609375,  
                x = 30748.59375,  
                InstanceId = [[Client1_895]],  
                Class = [[Position]],  
                z = 68.953125
              },  
              Sex = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 4,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_902]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 0,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 10,  
              GabaritHeight = 5,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 4,  
              HandsColor = 4,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_900]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5653550,  
              Angle = 2.875,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              PlayerAttackable = 0,  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -2318.375,  
                x = 30749.40625,  
                InstanceId = [[Client1_903]],  
                Class = [[Position]],  
                z = 69.28125
              },  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[Tryker]],  
              Sex = 0,  
              JacketModel = 5614638,  
              MorphTarget7 = 3,  
              MorphTarget3 = 0,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_898]],  
              ActivitiesId = {
              },  
              HairType = 5623598,  
              TrouserColor = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 12,  
              HandsModel = 5613870,  
              FeetColor = 3,  
              GabaritBreastSize = 4,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 1,  
              HandsColor = 1,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_896]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 7,  
              FeetModel = 0,  
              Angle = 2.875,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              PlayerAttackable = 0,  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -2315.59375,  
                x = 30750.53125,  
                InstanceId = [[Client1_899]],  
                Class = [[Position]],  
                z = 68.875
              },  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Speaker Tryker]],  
              Sex = 0,  
              JacketModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 5,  
              Tattoo = 9
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_1022]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 6]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1021]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1020]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_1010]],  
              ActivitiesId = {
              },  
              HairType = 6958,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 14,  
              HandsModel = 5615150,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 8,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 5612846,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1008]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1191]],  
                    Event = {
                      Type = [[group death]],  
                      InstanceId = [[Client1_1190]],  
                      Value = [[]],  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                    },  
                    Conditions = {
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_1327]],  
                    Name = [[]],  
                    InstanceId = [[Client1_1330]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_1329]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 5,  
              SheetClient = [[basic_tryker_male.creature]],  
              FeetModel = 5614894,  
              Speed = [[run]],  
              Angle = -2.875,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              ArmModel = 5615662,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              BotAttackable = 1,  
              ArmColor = 4,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 4,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5600814,  
              JacketModel = 5615918,  
              Name = [[Guard]],  
              Position = {
                y = -2164.6875,  
                x = 30796.84375,  
                InstanceId = [[Client1_1011]],  
                Class = [[Position]],  
                z = 77.59375
              },  
              Sex = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 2,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_1018]],  
              ActivitiesId = {
              },  
              HairType = 7214,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 13,  
              HandsModel = 5615150,  
              FeetColor = 4,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5612846,  
              GabaritLegsWidth = 8,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1016]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 5614894,  
              SheetClient = [[basic_tryker_female.creature]],  
              Angle = -2.875,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              ArmModel = 5615662,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              BotAttackable = 0,  
              ArmColor = 4,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5600814,  
              JacketModel = 5615918,  
              Name = [[Guard]],  
              Position = {
                y = -2167.03125,  
                x = 30799.96875,  
                InstanceId = [[Client1_1019]],  
                Class = [[Position]],  
                z = 77.140625
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 4,  
              Tattoo = 19
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_1006]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 14,  
              HandsModel = 5615150,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 5,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5612846,  
              GabaritLegsWidth = 1,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1004]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 5614894,  
              Angle = -2.875,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmModel = 5615662,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -2164.703125,  
                x = 30801.10938,  
                InstanceId = [[Client1_1007]],  
                Class = [[Position]],  
                z = 76.484375
              },  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              WeaponRightHand = 5601070,  
              ArmColor = 4,  
              Name = [[Guard]],  
              Sex = 1,  
              JacketModel = 5615918,  
              MorphTarget7 = 2,  
              MorphTarget3 = 1,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_1002]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 1,  
              HandsModel = 5615150,  
              FeetColor = 4,  
              GabaritBreastSize = 9,  
              GabaritHeight = 12,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5612846,  
              GabaritLegsWidth = 9,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1000]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 5614894,  
              Angle = -2.875,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 5615662,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -2162.5,  
                x = 30801.5,  
                InstanceId = [[Client1_1003]],  
                Class = [[Position]],  
                z = 75.96875
              },  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              WeaponRightHand = 5600302,  
              ArmColor = 4,  
              Name = [[Guard]],  
              Sex = 0,  
              JacketModel = 5615918,  
              MorphTarget7 = 0,  
              MorphTarget3 = 7,  
              Tattoo = 30
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_1014]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 5,  
              HandsModel = 5615150,  
              FeetColor = 4,  
              GabaritBreastSize = 2,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 5612846,  
              GabaritLegsWidth = 1,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1012]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 5614894,  
              Angle = -2.875,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmModel = 5615662,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -2160.828125,  
                x = 30800.89063,  
                InstanceId = [[Client1_1015]],  
                Class = [[Position]],  
                z = 75.78125
              },  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              WeaponRightHand = 5636398,  
              ArmColor = 4,  
              Name = [[Guard]],  
              Sex = 1,  
              JacketModel = 5615918,  
              MorphTarget7 = 1,  
              MorphTarget3 = 7,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_998]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 10,  
              HandsModel = 5615150,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 6,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 5612846,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_996]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5614894,  
              Angle = -2.875,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmModel = 5615662,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -2160.296875,  
                x = 30798.75,  
                InstanceId = [[Client1_999]],  
                Class = [[Position]],  
                z = 76.125
              },  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              WeaponRightHand = 5636398,  
              ArmColor = 4,  
              Name = [[Guard]],  
              Sex = 1,  
              JacketModel = 5615918,  
              MorphTarget7 = 1,  
              MorphTarget3 = 7,  
              Tattoo = 4
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_691]],  
      Events = {
      }
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1375]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_1374]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 31,  
        InstanceId = [[Client1_791]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_792]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hi Pioneer ! 
]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_793]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hi Pioneer ! ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_794]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hi Pioneer ! ]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_797]],  
        Class = [[TextManagerEntry]],  
        Text = [[I need your help to save my friends !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_798]],  
        Class = [[TextManagerEntry]],  
        Text = [[I need your help to save my friends !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_801]],  
        Class = [[TextManagerEntry]],  
        Text = [[Follow me ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_802]],  
        Class = [[TextManagerEntry]],  
        Text = [[Follow me ]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_836]],  
        Class = [[TextManagerEntry]],  
        Text = [[Aw ! They are prisonners by Kipees ! ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_837]],  
        Class = [[TextManagerEntry]],  
        Text = [[Aw ! They are prisonners by Kipees ! ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_840]],  
        Class = [[TextManagerEntry]],  
        Text = [[Lets kill them !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_841]],  
        Class = [[TextManagerEntry]],  
        Text = [[Lets kill them !]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_933]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thanks a lot Miss Antoviaque and Pioneer !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_934]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thanks a lot Miss Antoviaque and Pioneer !]]
      },  
      {
        Count = 7,  
        InstanceId = [[Client1_947]],  
        Class = [[TextManagerEntry]],  
        Text = [[You are welcome !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_948]],  
        Class = [[TextManagerEntry]],  
        Text = [[You are welcome !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_951]],  
        Class = [[TextManagerEntry]],  
        Text = [[We should move to the camp !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_952]],  
        Class = [[TextManagerEntry]],  
        Text = [[We should move to the camp !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1348]],  
        Class = [[TextManagerEntry]],  
        Text = [[Aw ! They are prisonners by Gingos ! ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_1349]],  
        Class = [[TextManagerEntry]],  
        Text = [[Aw ! They are prisonners by Gingos ! ]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1538]],  
        Count = 2,  
        Text = [[Miss Antoviaque...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1539]],  
        Count = 1,  
        Text = [[Miss Antoviaque...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1542]],  
        Count = 1,  
        Text = [[Your husband is prisonner of Kitins...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1543]],  
        Count = 1,  
        Text = [[Your husband is prisonner of Kitins...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1547]],  
        Count = 2,  
        Text = [[What ?! Oh noes !!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1548]],  
        Count = 1,  
        Text = [[What ?! Oh noes !!!]]
      }
    },  
    InstanceId = [[Client1_687]]
  }
}