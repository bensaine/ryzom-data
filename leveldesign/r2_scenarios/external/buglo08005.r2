scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 10,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    Road = 0,  
    RegionVertex = 0,  
    Position = 0,  
    NpcCustom = 0,  
    TextManager = 0,  
    WayPoint = 0,  
    ActivityStep = 0
  },  
  Acts = {
    {
      Cost = 39,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client1_184]],  
        [[Client1_186]],  
        [[Client1_188]],  
        [[Client2_92]],  
        [[Client2_94]],  
        [[Client2_96]],  
        [[Client1_230]]
      },  
      Title = [[Act 0]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_26]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_24]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[LORI hut 1]],  
              Position = {
                y = -1980.171875,  
                x = 22484.51563,  
                InstanceId = [[Client1_27]],  
                Class = [[Position]],  
                z = 38.25
              },  
              Angle = -0.203125,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_15]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_13]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_510]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client2_511]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_482]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Scowling Gingo]],  
              Position = {
                y = -1971.078125,  
                x = 22612.1875,  
                InstanceId = [[Client2_16]],  
                Class = [[Position]],  
                z = 22.875
              },  
              Angle = -2.4375,  
              Base = [[palette.entities.creatures.ccadb3]],  
              ActivitiesId = {
                [[Client2_510]]
              }
            },  
            {
              InstanceId = [[Client1_129]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_127]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1990.265625,  
                x = 22494.79688,  
                InstanceId = [[Client1_130]],  
                Class = [[Position]],  
                z = 37.421875
              },  
              Angle = 0.234375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_133]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_131]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1999.375,  
                x = 22498.125,  
                InstanceId = [[Client1_134]],  
                Class = [[Position]],  
                z = 35.84375
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_149]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_147]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              Position = {
                y = -1961.09375,  
                x = 22495.9375,  
                InstanceId = [[Client1_150]],  
                Class = [[Position]],  
                z = 37.078125
              },  
              Angle = -2.640625,  
              Base = [[palette.entities.creatures.chhdd2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_151]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              Position = {
                y = -1958.3125,  
                x = 22494.59375,  
                InstanceId = [[Client1_154]],  
                Class = [[Position]],  
                z = 36.765625
              },  
              Angle = -0.109375,  
              Base = [[palette.entities.creatures.chhdd2]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Region]],  
              InstanceId = [[Client2_66]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_68]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1809.359375,  
                    x = 22354.60938,  
                    InstanceId = [[Client2_69]],  
                    Class = [[Position]],  
                    z = -12.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_71]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1838.515625,  
                    x = 22404.71875,  
                    InstanceId = [[Client2_72]],  
                    Class = [[Position]],  
                    z = -9.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_74]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1861.5,  
                    x = 22308.78125,  
                    InstanceId = [[Client2_75]],  
                    Class = [[Position]],  
                    z = -16.09375
                  }
                }
              },  
              Position = {
                y = 0.3125,  
                x = -0.96875,  
                InstanceId = [[Client2_65]],  
                Class = [[Position]],  
                z = -0.15625
              }
            },  
            {
              InstanceId = [[Client1_157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_155]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_186]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_187]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1962.3125,  
                x = 22527.07813,  
                InstanceId = [[Client1_158]],  
                Class = [[Position]],  
                z = 32.09375
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_186]]
              }
            },  
            {
              InstanceId = [[Client1_161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_159]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_188]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_189]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1960.59375,  
                x = 22530.4375,  
                InstanceId = [[Client1_162]],  
                Class = [[Position]],  
                z = 31.984375
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_188]]
              }
            },  
            {
              InstanceId = [[Client1_165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_163]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_184]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_185]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_168]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Raspal]],  
              Position = {
                y = -1958.96875,  
                x = 22529.23438,  
                InstanceId = [[Client1_166]],  
                Class = [[Position]],  
                z = 32.046875
              },  
              Angle = -1.59375,  
              Base = [[palette.entities.creatures.chmdd2]],  
              ActivitiesId = {
                [[Client1_184]]
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime Raspal Region]],  
              InstanceId = [[Client1_168]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_170]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1953.609375,  
                    x = 22517.57813,  
                    InstanceId = [[Client1_171]],  
                    Class = [[Position]],  
                    z = 32.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_173]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1969.796875,  
                    x = 22523.5,  
                    InstanceId = [[Client1_174]],  
                    Class = [[Position]],  
                    z = 32.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_176]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1962.265625,  
                    x = 22541.09375,  
                    InstanceId = [[Client1_177]],  
                    Class = [[Position]],  
                    z = 31.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_179]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1952.75,  
                    x = 22536.78125,  
                    InstanceId = [[Client1_180]],  
                    Class = [[Position]],  
                    z = 31.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_182]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1952.640625,  
                    x = 22526.15625,  
                    InstanceId = [[Client1_183]],  
                    Class = [[Position]],  
                    z = 32.34375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_167]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client2_82]],  
              ActivitiesId = {
                [[Client2_92]]
              },  
              HairType = 5621806,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 7,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 12,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 5,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_80]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_92]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_93]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 0,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Position = {
                y = -1824.65625,  
                x = 22365.84375,  
                InstanceId = [[Client2_83]],  
                Class = [[Position]],  
                z = -7.3125
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 0,  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[Kaethus]],  
              Sex = 1,  
              WeaponRightHand = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 6,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client2_86]],  
              ActivitiesId = {
                [[Client2_96]]
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 11,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 10,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 5,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_84]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_96]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_374]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_66]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 5605422,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Position = {
                y = -1829.953125,  
                x = 22369.46875,  
                InstanceId = [[Client2_87]],  
                Class = [[Position]],  
                z = -6.859375
              },  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              JacketModel = 0,  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[Kaethon]],  
              Sex = 1,  
              WeaponRightHand = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 3,  
              Tattoo = 25
            },  
            {
              InheritPos = 1,  
              Name = [[Hime Yubo Region]],  
              InstanceId = [[Client1_214]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_213]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_216]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2005.296875,  
                    x = 22514.92188,  
                    InstanceId = [[Client1_217]],  
                    Class = [[Position]],  
                    z = 31.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_219]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1986.8125,  
                    x = 22506.90625,  
                    InstanceId = [[Client1_220]],  
                    Class = [[Position]],  
                    z = 36.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_222]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1985.78125,  
                    x = 22533.75,  
                    InstanceId = [[Client1_223]],  
                    Class = [[Position]],  
                    z = 32.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_225]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2011.84375,  
                    x = 22534.125,  
                    InstanceId = [[Client1_226]],  
                    Class = [[Position]],  
                    z = 32.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_228]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2020.296875,  
                    x = 22520.67188,  
                    InstanceId = [[Client1_229]],  
                    Class = [[Position]],  
                    z = 32.84375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Goari Region]],  
              InstanceId = [[Client2_159]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_161]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1786.953125,  
                    x = 22509.51563,  
                    InstanceId = [[Client2_162]],  
                    Class = [[Position]],  
                    z = 5.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_164]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1806.578125,  
                    x = 22497.70313,  
                    InstanceId = [[Client2_165]],  
                    Class = [[Position]],  
                    z = 5.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_167]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1805.515625,  
                    x = 22524.42188,  
                    InstanceId = [[Client2_168]],  
                    Class = [[Position]],  
                    z = 9.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_170]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1784.84375,  
                    x = 22530.84375,  
                    InstanceId = [[Client2_171]],  
                    Class = [[Position]],  
                    z = 8.328125
                  }
                }
              },  
              Position = {
                y = -2.09375,  
                x = -2.1875,  
                InstanceId = [[Client2_158]],  
                Class = [[Position]],  
                z = -3.8125
              }
            },  
            {
              InstanceId = [[Client2_184]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_182]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              Position = {
                y = -1814.75,  
                x = 22463.85938,  
                InstanceId = [[Client2_185]],  
                Class = [[Position]],  
                z = 1.78125
              },  
              Angle = 1.5625,  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime to Kaeno]],  
              InstanceId = [[Client1_283]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_282]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_285]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1987.40625,  
                    x = 22501,  
                    InstanceId = [[Client1_286]],  
                    Class = [[Position]],  
                    z = 37.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_288]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1993.265625,  
                    x = 22577.40625,  
                    InstanceId = [[Client1_289]],  
                    Class = [[Position]],  
                    z = 27.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_291]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1960.734375,  
                    x = 22621.92188,  
                    InstanceId = [[Client1_292]],  
                    Class = [[Position]],  
                    z = 23.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_294]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1854.234375,  
                    x = 22653.51563,  
                    InstanceId = [[Client1_295]],  
                    Class = [[Position]],  
                    z = 15.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_297]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1813.578125,  
                    x = 22611.90625,  
                    InstanceId = [[Client1_298]],  
                    Class = [[Position]],  
                    z = 9.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_300]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1838.359375,  
                    x = 22552.375,  
                    InstanceId = [[Client1_301]],  
                    Class = [[Position]],  
                    z = 4.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_303]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1801.96875,  
                    x = 22521.51563,  
                    InstanceId = [[Client1_304]],  
                    Class = [[Position]],  
                    z = 4.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_306]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1805.78125,  
                    x = 22486.5625,  
                    InstanceId = [[Client1_307]],  
                    Class = [[Position]],  
                    z = 1.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_309]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1874.234375,  
                    x = 22486.40625,  
                    InstanceId = [[Client1_310]],  
                    Class = [[Position]],  
                    z = 0.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_312]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1874.03125,  
                    x = 22432.10938,  
                    InstanceId = [[Client1_313]],  
                    Class = [[Position]],  
                    z = -3.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_315]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1831.140625,  
                    x = 22378.46875,  
                    InstanceId = [[Client1_316]],  
                    Class = [[Position]],  
                    z = -7.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_318]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1835.859375,  
                    x = 22325.10938,  
                    InstanceId = [[Client1_319]],  
                    Class = [[Position]],  
                    z = -11.84375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Tunnel Region]],  
              InstanceId = [[Client2_225]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_227]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1878.390625,  
                    x = 22645.875,  
                    InstanceId = [[Client2_228]],  
                    Class = [[Position]],  
                    z = 19.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_230]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1875.3125,  
                    x = 22656.0625,  
                    InstanceId = [[Client2_231]],  
                    Class = [[Position]],  
                    z = 19.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_233]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1843.90625,  
                    x = 22674.07813,  
                    InstanceId = [[Client2_234]],  
                    Class = [[Position]],  
                    z = 14.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_236]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1821,  
                    x = 22634.26563,  
                    InstanceId = [[Client2_237]],  
                    Class = [[Position]],  
                    z = 14.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_239]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1826.15625,  
                    x = 22628.46875,  
                    InstanceId = [[Client2_240]],  
                    Class = [[Position]],  
                    z = 13.109375
                  }
                }
              },  
              Position = {
                y = -1.5625,  
                x = -2,  
                InstanceId = [[Client2_224]],  
                Class = [[Position]],  
                z = -0.59375
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Messenger To Guard]],  
              InstanceId = [[Client2_346]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_348]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1879.59375,  
                    x = 22541.5,  
                    InstanceId = [[Client2_349]],  
                    Class = [[Position]],  
                    z = 9.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_351]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1866,  
                    x = 22545,  
                    InstanceId = [[Client2_352]],  
                    Class = [[Position]],  
                    z = 8.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_354]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1832.515625,  
                    x = 22542,  
                    InstanceId = [[Client2_355]],  
                    Class = [[Position]],  
                    z = 4.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_357]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1804.71875,  
                    x = 22521.5,  
                    InstanceId = [[Client2_358]],  
                    Class = [[Position]],  
                    z = 5.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_360]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1808.890625,  
                    x = 22462.625,  
                    InstanceId = [[Client2_361]],  
                    Class = [[Position]],  
                    z = 0.71875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_345]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Tower To Manager]],  
              InstanceId = [[Client2_418]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_417]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_420]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1851.78125,  
                    x = 22481.09375,  
                    InstanceId = [[Client2_421]],  
                    Class = [[Position]],  
                    z = 1.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_423]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1873.859375,  
                    x = 22475.53125,  
                    InstanceId = [[Client2_424]],  
                    Class = [[Position]],  
                    z = 0.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_426]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1873.609375,  
                    x = 22445.09375,  
                    InstanceId = [[Client2_427]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_429]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1854.796875,  
                    x = 22401.9375,  
                    InstanceId = [[Client2_430]],  
                    Class = [[Position]],  
                    z = -6.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_432]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1836.203125,  
                    x = 22325.14063,  
                    InstanceId = [[Client2_433]],  
                    Class = [[Position]],  
                    z = -11.90625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kaeno Izam Region]],  
              InstanceId = [[Client2_444]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_446]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1809.390625,  
                    x = 22491.8125,  
                    InstanceId = [[Client2_447]],  
                    Class = [[Position]],  
                    z = 1.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_449]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1819.28125,  
                    x = 22488.89063,  
                    InstanceId = [[Client2_450]],  
                    Class = [[Position]],  
                    z = 3.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_452]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1832.921875,  
                    x = 22492.85938,  
                    InstanceId = [[Client2_453]],  
                    Class = [[Position]],  
                    z = 1.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_455]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1848.671875,  
                    x = 22467.26563,  
                    InstanceId = [[Client2_456]],  
                    Class = [[Position]],  
                    z = 1.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_458]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1817.328125,  
                    x = 22462.57813,  
                    InstanceId = [[Client2_459]],  
                    Class = [[Position]],  
                    z = 2.40625
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_443]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Hime Gingo Region]],  
              InstanceId = [[Client2_482]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_484]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1966.390625,  
                    x = 22608.53125,  
                    InstanceId = [[Client2_485]],  
                    Class = [[Position]],  
                    z = 24.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_487]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1973.25,  
                    x = 22632.95313,  
                    InstanceId = [[Client2_488]],  
                    Class = [[Position]],  
                    z = 21.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_490]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1984.140625,  
                    x = 22625.17188,  
                    InstanceId = [[Client2_491]],  
                    Class = [[Position]],  
                    z = 22.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_493]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1988.25,  
                    x = 22607.20313,  
                    InstanceId = [[Client2_494]],  
                    Class = [[Position]],  
                    z = 23.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_496]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1994.4375,  
                    x = 22603.0625,  
                    InstanceId = [[Client2_497]],  
                    Class = [[Position]],  
                    z = 24.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_499]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2007.828125,  
                    x = 22603.1875,  
                    InstanceId = [[Client2_500]],  
                    Class = [[Position]],  
                    z = 25.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_502]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2008.453125,  
                    x = 22594.39063,  
                    InstanceId = [[Client2_503]],  
                    Class = [[Position]],  
                    z = 25.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_505]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1971.796875,  
                    x = 22570.01563,  
                    InstanceId = [[Client2_506]],  
                    Class = [[Position]],  
                    z = 28.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_508]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1973.390625,  
                    x = 22602.9375,  
                    InstanceId = [[Client2_509]],  
                    Class = [[Position]],  
                    z = 23.109375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_481]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client1_369]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
      },  
      Version = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_370]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Title = [[Act 1]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 18,  
        InstanceId = [[Client1_328]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 10,  
        InstanceId = [[Client1_329]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_330]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_331]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_332]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_344]],  
        Class = [[TextManagerEntry]],  
        Text = [[These are dire times, I fear that we might face hard times to come.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_345]],  
        Class = [[TextManagerEntry]],  
        Text = [[I fear that the others might not do well now, I wonder how they are...]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_349]],  
        Class = [[TextManagerEntry]],  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_350]],  
        Class = [[TextManagerEntry]],  
        Text = [[Then why do we not send someone there to see after them ?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_355]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_356]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_357]],  
        Class = [[TextManagerEntry]],  
        Text = [[Daghn can do it, I know he can!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_293]],  
        Class = [[TextManagerEntry]],  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_294]],  
        Class = [[TextManagerEntry]],  
        Text = [[I think I can hear KITINS!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_365]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_366]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yes, please Daghn, go see that they are safe then return to us and tell us !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_295]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_296]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are those Kitins that I'm hearing?]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_364]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_365]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Who the hell are YOU?!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_369]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_370]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will go and see to the others that they are doing well. ("I will go, and see if the others are doing well." would be more correct.)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_371]],  
        Class = [[TextManagerEntry]],  
        Text = [[I have come too see what aid you need... (should be "to see")]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_372]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_373]],  
        Class = [[TextManagerEntry]],  
        Text = [[I see, to hadn over such materials I need a helping hand. Can you spare anyone ? (Language Invalid :p)]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client2_381]],  
        Class = [[TextManagerEntry]],  
        Text = [[The Kitins are coming!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_384]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought, please go to the town for me, and warn the people!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_385]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought, please go to the town for me, and warn the people!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_386]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_387]],  
        Class = [[TextManagerEntry]],  
        Text = [[Just as I thought.]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_390]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please go to the town for me, and warn the people!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_391]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please go to the town for me, and warn the people!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_394]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will do that.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_395]],  
        Class = [[TextManagerEntry]],  
        Text = [[I will do that.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_399]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this place.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_400]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this place.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_401]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this guard position.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_402]],  
        Class = [[TextManagerEntry]],  
        Text = [[Thank you, I will take care of this guard position.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_407]],  
        Class = [[TextManagerEntry]],  
        Text = [[I keep hearing something from the tunnel...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_408]],  
        Class = [[TextManagerEntry]],  
        Text = [[I keep hearing something from the tunnel...]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_439]],  
        Class = [[TextManagerEntry]],  
        Text = [[We have a huge problem!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_440]],  
        Class = [[TextManagerEntry]],  
        Text = [[We have a huge problem!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_462]],  
        Class = [[TextManagerEntry]],  
        Text = [[What happened?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_463]],  
        Class = [[TextManagerEntry]],  
        Text = [[What happened?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_466]],  
        Class = [[TextManagerEntry]],  
        Text = [[There might be Kitins incoming!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_467]],  
        Class = [[TextManagerEntry]],  
        Text = [[There might be Kitins incoming!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_470]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Not the Kitins!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_471]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh my KAMI! Not the Kitins!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_475]],  
        Class = [[TextManagerEntry]],  
        Text = [[*I keep hearing something coming from the tunnel...*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_476]],  
        Class = [[TextManagerEntry]],  
        Text = [[*I keep hearing something coming from the tunnel...*]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_479]],  
        Class = [[TextManagerEntry]],  
        Text = [[*Could it be ... ?!*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_480]],  
        Class = [[TextManagerEntry]],  
        Text = [[*Could it be ... ?!*]]
      }
    }
  }
}