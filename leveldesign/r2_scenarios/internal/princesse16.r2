scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_42512]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts09]],  
      Time = 0,  
      Name = [[Two Ponds (Desert 09)]],  
      Season = [[fall]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_42503]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_42501]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_42502]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_42499]]
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    UserTrigger = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManager = 0,  
    TextManagerEntry = 0,  
    EventType = 0,  
    ZoneTrigger = 1,  
    DefaultFeature = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    ChatSequence = 0,  
    Region = 0,  
    Road = 0,  
    NpcCreature = 0,  
    ActivityStep = 1,  
    NpcCustom = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 1,  
    ActionStep = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 1,  
    WayPoint = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_42506]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_42504]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Events = {
      },  
      Title = [[]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Version = 5,  
      Name = [[Permanent]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_42505]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_42515]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42513]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 1]],  
              Position = {
                y = -1204.328125,  
                x = 27631.82813,  
                InstanceId = [[Client1_42516]],  
                Class = [[Position]],  
                z = 74.84375
              },  
              Angle = 2.5,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42519]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42517]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 2]],  
              Position = {
                y = -1214.234375,  
                x = 27635.71875,  
                InstanceId = [[Client1_42520]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              Angle = 2.5,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42523]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42521]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1223.5625,  
                x = 27611.03125,  
                InstanceId = [[Client1_42524]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              Angle = 1.59375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42527]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42525]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1216.546875,  
                x = 27602.25,  
                InstanceId = [[Client1_42528]],  
                Class = [[Position]],  
                z = 74.609375
              },  
              Angle = 0.984375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42531]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42529]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              Position = {
                y = -1199.34375,  
                x = 27597.95313,  
                InstanceId = [[Client1_42532]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              Angle = 0.078125,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42535]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42533]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              Position = {
                y = -1191.890625,  
                x = 27599.01563,  
                InstanceId = [[Client1_42536]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              Angle = -0.671875,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42539]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42537]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 1]],  
              Position = {
                y = -1184.84375,  
                x = 27612.6875,  
                InstanceId = [[Client1_42540]],  
                Class = [[Position]],  
                z = 74.359375
              },  
              Angle = -1.828125,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42543]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42541]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 2]],  
              Position = {
                y = -1185.40625,  
                x = 27626.48438,  
                InstanceId = [[Client1_42544]],  
                Class = [[Position]],  
                z = 75.28125
              },  
              Angle = -2.4375,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42551]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42549]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1202.59375,  
                x = 27615.04688,  
                InstanceId = [[Client1_42552]],  
                Class = [[Position]],  
                z = 73.421875
              },  
              Angle = 2.359375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_42697]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_42696]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42699]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1209.015625,  
                    x = 27629.28125,  
                    InstanceId = [[Client1_42700]],  
                    Class = [[Position]],  
                    z = 74.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42702]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1212.078125,  
                    x = 27628.25,  
                    InstanceId = [[Client1_42703]],  
                    Class = [[Position]],  
                    z = 74.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42705]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1216.046875,  
                    x = 27625.6875,  
                    InstanceId = [[Client1_42706]],  
                    Class = [[Position]],  
                    z = 74.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42708]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1219.171875,  
                    x = 27619.89063,  
                    InstanceId = [[Client1_42709]],  
                    Class = [[Position]],  
                    z = 74.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42711]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1217.0625,  
                    x = 27614.34375,  
                    InstanceId = [[Client1_42712]],  
                    Class = [[Position]],  
                    z = 74.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42714]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1217.671875,  
                    x = 27611.65625,  
                    InstanceId = [[Client1_42715]],  
                    Class = [[Position]],  
                    z = 74.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42717]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1213.109375,  
                    x = 27610.60938,  
                    InstanceId = [[Client1_42718]],  
                    Class = [[Position]],  
                    z = 74.234375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_42723]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_42722]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42725]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1205.328125,  
                    x = 27627.10938,  
                    InstanceId = [[Client1_42726]],  
                    Class = [[Position]],  
                    z = 74.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42728]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1198.390625,  
                    x = 27629.75,  
                    InstanceId = [[Client1_42729]],  
                    Class = [[Position]],  
                    z = 74.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42731]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1192.421875,  
                    x = 27625.15625,  
                    InstanceId = [[Client1_42732]],  
                    Class = [[Position]],  
                    z = 74.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42734]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1193.609375,  
                    x = 27615.40625,  
                    InstanceId = [[Client1_42735]],  
                    Class = [[Position]],  
                    z = 73.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42737]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1190.546875,  
                    x = 27611.9375,  
                    InstanceId = [[Client1_42738]],  
                    Class = [[Position]],  
                    z = 73.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42740]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1191.890625,  
                    x = 27604.65625,  
                    InstanceId = [[Client1_42741]],  
                    Class = [[Position]],  
                    z = 74.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42743]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1201.015625,  
                    x = 27603.48438,  
                    InstanceId = [[Client1_42744]],  
                    Class = [[Position]],  
                    z = 74.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42746]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1209.9375,  
                    x = 27604.625,  
                    InstanceId = [[Client1_42747]],  
                    Class = [[Position]],  
                    z = 74.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42749]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1229.578125,  
                    x = 27606.78125,  
                    InstanceId = [[Client1_42750]],  
                    Class = [[Position]],  
                    z = 75.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42752]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1228.734375,  
                    x = 27621.26563,  
                    InstanceId = [[Client1_42753]],  
                    Class = [[Position]],  
                    z = 75.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42755]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1219.3125,  
                    x = 27633.14063,  
                    InstanceId = [[Client1_42756]],  
                    Class = [[Position]],  
                    z = 75.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42758]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1201.71875,  
                    x = 27635.96875,  
                    InstanceId = [[Client1_42759]],  
                    Class = [[Position]],  
                    z = 75.421875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client1_42767]],  
              Class = [[Road]],  
              Position = {
                y = 2.5,  
                x = -5.921875,  
                InstanceId = [[Client1_42766]],  
                Class = [[Position]],  
                z = 0.109375
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42769]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1219.515625,  
                    x = 27609.51563,  
                    InstanceId = [[Client1_42770]],  
                    Class = [[Position]],  
                    z = 74.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42772]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1207.578125,  
                    x = 27606.70313,  
                    InstanceId = [[Client1_42773]],  
                    Class = [[Position]],  
                    z = 74.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42775]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1198.09375,  
                    x = 27606.32813,  
                    InstanceId = [[Client1_42776]],  
                    Class = [[Position]],  
                    z = 73.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42778]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1195.640625,  
                    x = 27607.4375,  
                    InstanceId = [[Client1_42779]],  
                    Class = [[Position]],  
                    z = 73.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42781]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1194.296875,  
                    x = 27611.0625,  
                    InstanceId = [[Client1_42782]],  
                    Class = [[Position]],  
                    z = 73.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42784]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1200.328125,  
                    x = 27623.90625,  
                    InstanceId = [[Client1_42785]],  
                    Class = [[Position]],  
                    z = 73.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42787]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1201.21875,  
                    x = 27631,  
                    InstanceId = [[Client1_42788]],  
                    Class = [[Position]],  
                    z = 74.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42790]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1227.046875,  
                    x = 27659.98438,  
                    InstanceId = [[Client1_42791]],  
                    Class = [[Position]],  
                    z = 75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42793]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1224.21875,  
                    x = 27632.20313,  
                    InstanceId = [[Client1_42794]],  
                    Class = [[Position]],  
                    z = 75.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42796]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1235.140625,  
                    x = 27619.375,  
                    InstanceId = [[Client1_42797]],  
                    Class = [[Position]],  
                    z = 75.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42799]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1230.8125,  
                    x = 27605.3125,  
                    InstanceId = [[Client1_42800]],  
                    Class = [[Position]],  
                    z = 75.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42802]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1219.703125,  
                    x = 27609.53125,  
                    InstanceId = [[Client1_42803]],  
                    Class = [[Position]],  
                    z = 74.640625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_42870]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_42869]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42872]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1132.4375,  
                    x = 27780.82813,  
                    InstanceId = [[Client1_42873]],  
                    Class = [[Position]],  
                    z = 72.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42875]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1123.828125,  
                    x = 27800.59375,  
                    InstanceId = [[Client1_42876]],  
                    Class = [[Position]],  
                    z = 75.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42878]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1105.625,  
                    x = 27807.82813,  
                    InstanceId = [[Client1_42879]],  
                    Class = [[Position]],  
                    z = 74.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42881]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1097.109375,  
                    x = 27787.82813,  
                    InstanceId = [[Client1_42882]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42884]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1095.71875,  
                    x = 27775.5625,  
                    InstanceId = [[Client1_42885]],  
                    Class = [[Position]],  
                    z = 75.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42887]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1118.265625,  
                    x = 27762.09375,  
                    InstanceId = [[Client1_42888]],  
                    Class = [[Position]],  
                    z = 75
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 3]],  
              InstanceId = [[Client1_42920]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_42919]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42922]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1208.4375,  
                    x = 27831.26563,  
                    InstanceId = [[Client1_42923]],  
                    Class = [[Position]],  
                    z = 73.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42925]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1201.703125,  
                    x = 27848.54688,  
                    InstanceId = [[Client1_42926]],  
                    Class = [[Position]],  
                    z = 75.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42928]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1181.34375,  
                    x = 27847.59375,  
                    InstanceId = [[Client1_42929]],  
                    Class = [[Position]],  
                    z = 75.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42931]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1169.828125,  
                    x = 27828.65625,  
                    InstanceId = [[Client1_42932]],  
                    Class = [[Position]],  
                    z = 75.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42934]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1163.796875,  
                    x = 27799.84375,  
                    InstanceId = [[Client1_42935]],  
                    Class = [[Position]],  
                    z = 79.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42937]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1169.15625,  
                    x = 27797.82813,  
                    InstanceId = [[Client1_42938]],  
                    Class = [[Position]],  
                    z = 77.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42940]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1178.5,  
                    x = 27799.9375,  
                    InstanceId = [[Client1_42941]],  
                    Class = [[Position]],  
                    z = 76.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42943]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1185.984375,  
                    x = 27807.40625,  
                    InstanceId = [[Client1_42944]],  
                    Class = [[Position]],  
                    z = 75.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_42946]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1196.34375,  
                    x = 27828.23438,  
                    InstanceId = [[Client1_42947]],  
                    Class = [[Position]],  
                    z = 73.46875
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_42981]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42979]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 1]],  
              Position = {
                y = -1110.09375,  
                x = 27854.40625,  
                InstanceId = [[Client1_42982]],  
                Class = [[Position]],  
                z = 73.640625
              },  
              Angle = -2.0625,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42985]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42983]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 2]],  
              Position = {
                y = -1109.53125,  
                x = 27857.15625,  
                InstanceId = [[Client1_42986]],  
                Class = [[Position]],  
                z = 73.5
              },  
              Angle = -3.71875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42989]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42987]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 3]],  
              Position = {
                y = -1104.015625,  
                x = 27858.625,  
                InstanceId = [[Client1_42990]],  
                Class = [[Position]],  
                z = 73.765625
              },  
              Angle = -2.3125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42993]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42991]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 4]],  
              Position = {
                y = -1106.296875,  
                x = 27859.51563,  
                InstanceId = [[Client1_42994]],  
                Class = [[Position]],  
                z = 73.625
              },  
              Angle = -3.53125,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42997]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42995]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[barrier 5]],  
              Position = {
                y = -1105,  
                x = 27856.25,  
                InstanceId = [[Client1_42998]],  
                Class = [[Position]],  
                z = 73.671875
              },  
              Angle = -0.546875,  
              Base = [[palette.entities.botobjects.barrier]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_43018]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43016]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 1]],  
              Position = {
                y = -1094,  
                x = 27851.70313,  
                InstanceId = [[Client1_43019]],  
                Class = [[Position]],  
                z = 74.421875
              },  
              Angle = -2.25,  
              Base = [[palette.entities.botobjects.pack_2]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 3]],  
              InstanceId = [[Client1_43077]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43079]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1112.25,  
                    x = 27850.84375,  
                    InstanceId = [[Client1_43080]],  
                    Class = [[Position]],  
                    z = 73.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43082]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1114.078125,  
                    x = 27853.9375,  
                    InstanceId = [[Client1_43083]],  
                    Class = [[Position]],  
                    z = 73.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43085]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1110.859375,  
                    x = 27858.90625,  
                    InstanceId = [[Client1_43086]],  
                    Class = [[Position]],  
                    z = 73.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43088]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1105.890625,  
                    x = 27861.39063,  
                    InstanceId = [[Client1_43089]],  
                    Class = [[Position]],  
                    z = 72.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43091]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1101.484375,  
                    x = 27862.01563,  
                    InstanceId = [[Client1_43092]],  
                    Class = [[Position]],  
                    z = 74.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43094]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1100.625,  
                    x = 27857.1875,  
                    InstanceId = [[Client1_43095]],  
                    Class = [[Position]],  
                    z = 74.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43097]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1098.203125,  
                    x = 27854.07813,  
                    InstanceId = [[Client1_43098]],  
                    Class = [[Position]],  
                    z = 73.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43100]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1108.828125,  
                    x = 27849.96875,  
                    InstanceId = [[Client1_43101]],  
                    Class = [[Position]],  
                    z = 73.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43103]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1112.25,  
                    x = 27850.84375,  
                    InstanceId = [[Client1_43104]],  
                    Class = [[Position]],  
                    z = 73.953125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_43076]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_43117]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43115]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              Position = {
                y = -1095.953125,  
                x = 27842.32813,  
                InstanceId = [[Client1_43118]],  
                Class = [[Position]],  
                z = 73.484375
              },  
              Angle = -1.75,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 4]],  
              InstanceId = [[Client1_43170]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_43169]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43172]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1093.859375,  
                    x = 27838.54688,  
                    InstanceId = [[Client1_43173]],  
                    Class = [[Position]],  
                    z = 73
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43175]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1098.265625,  
                    x = 27820.07813,  
                    InstanceId = [[Client1_43176]],  
                    Class = [[Position]],  
                    z = 74.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43178]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1096.109375,  
                    x = 27795.5,  
                    InstanceId = [[Client1_43179]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43181]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1090.78125,  
                    x = 27771.17188,  
                    InstanceId = [[Client1_43182]],  
                    Class = [[Position]],  
                    z = 74.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43184]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1068.140625,  
                    x = 27743.65625,  
                    InstanceId = [[Client1_43185]],  
                    Class = [[Position]],  
                    z = 74.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43187]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1038.984375,  
                    x = 27673.32813,  
                    InstanceId = [[Client1_43188]],  
                    Class = [[Position]],  
                    z = 75.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43190]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1022.4375,  
                    x = 27605.32813,  
                    InstanceId = [[Client1_43191]],  
                    Class = [[Position]],  
                    z = 75.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43193]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1016.4375,  
                    x = 27550.28125,  
                    InstanceId = [[Client1_43194]],  
                    Class = [[Position]],  
                    z = 74.5
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 5]],  
              InstanceId = [[Client1_43215]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43217]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1107.5,  
                    x = 27855.17188,  
                    InstanceId = [[Client1_43218]],  
                    Class = [[Position]],  
                    z = 73.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43220]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1108.6875,  
                    x = 27852.01563,  
                    InstanceId = [[Client1_43221]],  
                    Class = [[Position]],  
                    z = 73.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43223]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1113.1875,  
                    x = 27849.57813,  
                    InstanceId = [[Client1_43224]],  
                    Class = [[Position]],  
                    z = 73
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_43226]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1115.140625,  
                    x = 27848.60938,  
                    InstanceId = [[Client1_43227]],  
                    Class = [[Position]],  
                    z = 74.296875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_43214]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_45017]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45015]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 3]],  
              Position = {
                y = -1010.21875,  
                x = 27543.04688,  
                InstanceId = [[Client1_45018]],  
                Class = [[Position]],  
                z = 73.453125
              },  
              Angle = -1.46875,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45123]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45121]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 4]],  
              Position = {
                y = -1025.171875,  
                x = 27540.26563,  
                InstanceId = [[Client1_45124]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = 0.375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45127]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45125]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 5]],  
              Position = {
                y = -1013.34375,  
                x = 27523.01563,  
                InstanceId = [[Client1_45128]],  
                Class = [[Position]],  
                z = 73.484375
              },  
              Angle = -0.234375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45131]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45129]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              Position = {
                y = -1019.734375,  
                x = 27519.25,  
                InstanceId = [[Client1_45132]],  
                Class = [[Position]],  
                z = 74.671875
              },  
              Angle = -0.046875,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45135]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45133]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chest 1]],  
              Position = {
                y = -1007.84375,  
                x = 27524.1875,  
                InstanceId = [[Client1_45136]],  
                Class = [[Position]],  
                z = 72.421875
              },  
              Angle = -0.046875,  
              Base = [[palette.entities.botobjects.chest]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45139]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45137]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -1000.9375,  
                x = 27536.5,  
                InstanceId = [[Client1_45140]],  
                Class = [[Position]],  
                z = 73.28125
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45143]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45141]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1004.03125,  
                x = 27542.40625,  
                InstanceId = [[Client1_45144]],  
                Class = [[Position]],  
                z = 73.65625
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45147]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45145]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[old chest 1]],  
              Position = {
                y = -1005,  
                x = 27545.90625,  
                InstanceId = [[Client1_45148]],  
                Class = [[Position]],  
                z = 74.0625
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.chest_old]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45151]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45149]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 1]],  
              Position = {
                y = -1021.890625,  
                x = 27537.01563,  
                InstanceId = [[Client1_45152]],  
                Class = [[Position]],  
                z = 74.921875
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.bag_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45155]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45153]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 2]],  
              Position = {
                y = -1028.03125,  
                x = 27546.23438,  
                InstanceId = [[Client1_45156]],  
                Class = [[Position]],  
                z = 74.890625
              },  
              Angle = 2.453125,  
              Base = [[palette.entities.botobjects.pack_4]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 6]],  
              InstanceId = [[Client1_45205]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45207]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1120.328125,  
                    x = 27848.64063,  
                    InstanceId = [[Client1_45208]],  
                    Class = [[Position]],  
                    z = 74.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45210]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1178.859375,  
                    x = 27857.45313,  
                    InstanceId = [[Client1_45211]],  
                    Class = [[Position]],  
                    z = 75.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45213]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1234.625,  
                    x = 27853.14063,  
                    InstanceId = [[Client1_45214]],  
                    Class = [[Position]],  
                    z = 76.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45216]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1268.828125,  
                    x = 27791.64063,  
                    InstanceId = [[Client1_45217]],  
                    Class = [[Position]],  
                    z = 73.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45219]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1262.84375,  
                    x = 27759.26563,  
                    InstanceId = [[Client1_45220]],  
                    Class = [[Position]],  
                    z = 73.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45222]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1213.71875,  
                    x = 27702.90625,  
                    InstanceId = [[Client1_45223]],  
                    Class = [[Position]],  
                    z = 77.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45225]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1208.90625,  
                    x = 27634.98438,  
                    InstanceId = [[Client1_45226]],  
                    Class = [[Position]],  
                    z = 75.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45228]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1202.171875,  
                    x = 27615.10938,  
                    InstanceId = [[Client1_45229]],  
                    Class = [[Position]],  
                    z = 73.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45231]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1203.421875,  
                    x = 27613.875,  
                    InstanceId = [[Client1_45232]],  
                    Class = [[Position]],  
                    z = 73.484375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_45204]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 4]],  
              InstanceId = [[Client1_45268]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_45267]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45270]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1201.15625,  
                    x = 27612.15625,  
                    InstanceId = [[Client1_45271]],  
                    Class = [[Position]],  
                    z = 73.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45273]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1206.59375,  
                    x = 27607.875,  
                    InstanceId = [[Client1_45274]],  
                    Class = [[Position]],  
                    z = 74.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45276]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1209.296875,  
                    x = 27608.75,  
                    InstanceId = [[Client1_45277]],  
                    Class = [[Position]],  
                    z = 74.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45279]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1208.578125,  
                    x = 27616,  
                    InstanceId = [[Client1_45280]],  
                    Class = [[Position]],  
                    z = 73.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45282]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1206.078125,  
                    x = 27622.5625,  
                    InstanceId = [[Client1_45283]],  
                    Class = [[Position]],  
                    z = 73.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45285]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1200.984375,  
                    x = 27624.03125,  
                    InstanceId = [[Client1_45286]],  
                    Class = [[Position]],  
                    z = 73.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45288]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1196.375,  
                    x = 27622.01563,  
                    InstanceId = [[Client1_45289]],  
                    Class = [[Position]],  
                    z = 73.65625
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_45365]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45363]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin mound 1]],  
              Position = {
                y = -1344.828125,  
                x = 27510.40625,  
                InstanceId = [[Client1_45366]],  
                Class = [[Position]],  
                z = 75.21875
              },  
              Angle = 0.1052268371,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45369]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45367]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 1]],  
              Position = {
                y = -1343.765625,  
                x = 27529.70313,  
                InstanceId = [[Client1_45370]],  
                Class = [[Position]],  
                z = 75.46875
              },  
              Angle = 0.890625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45373]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45371]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 2]],  
              Position = {
                y = -1330.125,  
                x = 27509.59375,  
                InstanceId = [[Client1_45374]],  
                Class = [[Position]],  
                z = 75.15625
              },  
              Angle = 0.890625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 7]],  
              InstanceId = [[Client1_45403]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_45402]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45405]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1210,  
                    x = 27617.1875,  
                    InstanceId = [[Client1_45406]],  
                    Class = [[Position]],  
                    z = 73.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45408]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1225.546875,  
                    x = 27618.04688,  
                    InstanceId = [[Client1_45409]],  
                    Class = [[Position]],  
                    z = 74.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45411]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1255.09375,  
                    x = 27603.34375,  
                    InstanceId = [[Client1_45412]],  
                    Class = [[Position]],  
                    z = 75.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45414]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1284.015625,  
                    x = 27575.70313,  
                    InstanceId = [[Client1_45415]],  
                    Class = [[Position]],  
                    z = 75.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45417]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1307.28125,  
                    x = 27553.32813,  
                    InstanceId = [[Client1_45418]],  
                    Class = [[Position]],  
                    z = 81.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45420]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1313.9375,  
                    x = 27547.40625,  
                    InstanceId = [[Client1_45421]],  
                    Class = [[Position]],  
                    z = 81.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45426]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1318.765625,  
                    x = 27544,  
                    InstanceId = [[Client1_45427]],  
                    Class = [[Position]],  
                    z = 80.90625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 8]],  
              InstanceId = [[Client1_45501]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_45500]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45503]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1318.234375,  
                    x = 27546.79688,  
                    InstanceId = [[Client1_45504]],  
                    Class = [[Position]],  
                    z = 81.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45506]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1305.796875,  
                    x = 27557.59375,  
                    InstanceId = [[Client1_45507]],  
                    Class = [[Position]],  
                    z = 81.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45509]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1276.5,  
                    x = 27589.84375,  
                    InstanceId = [[Client1_45510]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45512]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1236.65625,  
                    x = 27616.39063,  
                    InstanceId = [[Client1_45513]],  
                    Class = [[Position]],  
                    z = 75.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45515]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1227.96875,  
                    x = 27620.71875,  
                    InstanceId = [[Client1_45516]],  
                    Class = [[Position]],  
                    z = 75.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_45518]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1209.671875,  
                    x = 27619.39063,  
                    InstanceId = [[Client1_45519]],  
                    Class = [[Position]],  
                    z = 73.953125
                  }
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_45661]],  
              Name = [[Place 5]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_45663]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_45664]],  
                    x = 27628.4375,  
                    y = -1178.9375,  
                    z = 76.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_45666]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_45667]],  
                    x = 27629.04688,  
                    y = -1181.59375,  
                    z = 76.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_45669]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_45670]],  
                    x = 27631.4375,  
                    y = -1185.234375,  
                    z = 76.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_45672]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_45673]],  
                    x = 27633.09375,  
                    y = -1185.703125,  
                    z = 76.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_45675]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_45676]],  
                    x = 27632.92188,  
                    y = -1183.4375,  
                    z = 76.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_45678]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_45679]],  
                    x = 27630.54688,  
                    y = -1177.875,  
                    z = 76.953125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_45660]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_42507]]
        }
      },  
      LocationId = [[]],  
      ManualWeather = 0
    },  
    {
      InstanceId = [[Client1_42510]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_42578]],  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_42579]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]],  
            Actions = {
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_42580]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_42581]],  
                Entity = r2.RefId([[Client1_42555]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_42582]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_42583]],  
                Entity = r2.RefId([[Client1_42559]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_42584]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_42585]],  
                Entity = r2.RefId([[Client1_42563]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[starts dialog]],  
                  InstanceId = [[Client1_42586]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_42587]],  
                Entity = r2.RefId([[Client1_42567]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            }
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_43137]],  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_43138]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]],  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_43139]],  
                  Value = r2.RefId([[Client1_43132]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_43140]],  
                Entity = r2.RefId([[Client1_43111]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            }
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_43167]],  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_43168]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]],  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_43202]],  
                  Value = r2.RefId([[Client1_43198]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_43203]],  
                Entity = r2.RefId([[Client1_43127]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            }
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_43259]],  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_43260]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]],  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_43261]],  
                  Value = r2.RefId([[Client1_43243]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_43262]],  
                Entity = r2.RefId([[Client1_42977]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            }
          },  
          {
            Name = [[]],  
            InstanceId = [[Client1_45174]],  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_45175]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]],  
            Actions = {
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_45176]],  
                  Value = r2.RefId([[Client1_45169]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_45177]],  
                Entity = r2.RefId([[Client1_44975]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_45195]],  
                  Value = r2.RefId([[Client1_45183]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_45196]],  
                Entity = r2.RefId([[Client1_45029]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[begin activity sequence]],  
                  InstanceId = [[Client1_45197]],  
                  Value = r2.RefId([[Client1_45187]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_45198]],  
                Entity = r2.RefId([[Client1_45080]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            }
          }
        },  
        InstanceId = [[Client1_42508]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Events = {
      },  
      Title = [[]],  
      WeatherValue = 143,  
      InheritPos = 1,  
      Version = 5,  
      Name = [[Act 1:Act 1]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_42509]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_42555]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 3,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42553]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45440]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45441]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45442]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45443]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45403]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45527]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45528]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 0,  
              Speed = 1,  
              Angle = 3.25,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              Level = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              JacketModel = 5606446,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Thecaon]],  
              Position = {
                y = -1205,  
                x = 27618.35938,  
                InstanceId = [[Client1_42556]],  
                Class = [[Position]],  
                z = 73.609375
              },  
              ArmModel = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 2,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_42559]],  
              ActivitiesId = {
              },  
              HairType = 5623086,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 13,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 3,  
              GabaritHeight = 2,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 4,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42557]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45444]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45445]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45446]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45447]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45403]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45525]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45526]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Speed = 1,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 2,  
              Level = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 3,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Dughan]],  
              Position = {
                y = -1205.28125,  
                x = 27611.9375,  
                InstanceId = [[Client1_42560]],  
                Class = [[Position]],  
                z = 73.671875
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 5,  
              Tattoo = 25
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_42563]],  
              ActivitiesId = {
              },  
              HairType = 5623854,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 3,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 2,  
              HandsColor = 1,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42561]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45431]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45432]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45433]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45434]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45403]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45523]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45524]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 5,  
              FeetModel = 0,  
              Speed = 1,  
              Angle = 1.90625,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              SheetClient = [[basic_zorai_female.creature]],  
              ArmColor = 0,  
              Level = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 5,  
              JacketModel = 5618734,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ce-Ni]],  
              Position = {
                y = -1207.578125,  
                x = 27615.23438,  
                InstanceId = [[Client1_42564]],  
                Class = [[Position]],  
                z = 73.71875
              },  
              ArmModel = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 7,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_42591]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 0,  
              HandsModel = 5606958,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 1,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 9,  
              HandsColor = 5,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42589]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42762]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42763]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 7,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 3.015625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Ulydix]],  
              Position = {
                y = -1198.6875,  
                x = 27621.71875,  
                InstanceId = [[Client1_42592]],  
                Class = [[Position]],  
                z = 73.609375
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 5,  
              MorphTarget3 = 0,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_42642]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 12,  
              HandsModel = 5606958,  
              FeetColor = 5,  
              GabaritBreastSize = 2,  
              GabaritHeight = 0,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 5,  
              HandsColor = 2,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42640]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42764]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42765]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Aecaon]],  
              Position = {
                y = -1200.296875,  
                x = 27629.21875,  
                InstanceId = [[Client1_42643]],  
                Class = [[Position]],  
                z = 74.59375
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 7,  
              MorphTarget3 = 2,  
              Tattoo = 19
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_42646]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 3,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42644]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42719]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_42720]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42697]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42721]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42697]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Dycaon]],  
              Position = {
                y = -1209.21875,  
                x = 27632.71875,  
                InstanceId = [[Client1_42647]],  
                Class = [[Position]],  
                z = 74.890625
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 5,  
              MorphTarget3 = 2,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_42650]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 5,  
              HandsModel = 5604142,  
              FeetColor = 4,  
              GabaritBreastSize = 7,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 7,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42648]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 5,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = 2.984375,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 6,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Dioros]],  
              Position = {
                y = -1219,  
                x = 27630.95313,  
                InstanceId = [[Client1_42651]],  
                Class = [[Position]],  
                z = 75.34375
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 2,  
              MorphTarget3 = 3,  
              Tattoo = 23
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_42654]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 10,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 13,  
              GabaritHeight = 7,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 12,  
              HandsColor = 3,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42652]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42807]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_42808]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42767]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 2.328125,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 5,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Eunix]],  
              Position = {
                y = -1227.265625,  
                x = 27622.6875,  
                InstanceId = [[Client1_42655]],  
                Class = [[Position]],  
                z = 75.3125
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 19
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_42658]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 5,  
              HandsModel = 5606958,  
              FeetColor = 2,  
              GabaritBreastSize = 10,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 0,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 10,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42656]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42809]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_42810]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42767]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = 2.328125,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Kridix]],  
              Position = {
                y = -1221.9375,  
                x = 27613.48438,  
                InstanceId = [[Client1_42659]],  
                Class = [[Position]],  
                z = 74.796875
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 1,  
              MorphTarget3 = 1,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_42662]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 8,  
              HandsModel = 5604142,  
              FeetColor = 2,  
              GabaritBreastSize = 4,  
              GabaritHeight = 13,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 14,  
              HandsColor = 1,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42660]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42811]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_42812]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42767]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 1.46875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              JacketModel = 5605166,  
              WeaponRightHand = 5635886,  
              Level = 0,  
              Name = [[Deups]],  
              Position = {
                y = -1216.59375,  
                x = 27606.5625,  
                InstanceId = [[Client1_42663]],  
                Class = [[Position]],  
                z = 74.5
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 5,  
              MorphTarget3 = 0,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_42666]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 5606958,  
              FeetColor = 2,  
              GabaritBreastSize = 2,  
              GabaritHeight = 3,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 12,  
              HandsColor = 5,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42664]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45290]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45291]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45292]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[5]],  
                        InstanceId = [[Client1_45293]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45294]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = 1.46875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 4,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Gaan]],  
              Position = {
                y = -1206.015625,  
                x = 27603.26563,  
                InstanceId = [[Client1_42667]],  
                Class = [[Position]],  
                z = 74.375
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 4,  
              MorphTarget3 = 5,  
              Tattoo = 26
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_42670]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 12,  
              HandsModel = 5606958,  
              FeetColor = 2,  
              GabaritBreastSize = 5,  
              GabaritHeight = 8,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 11,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42668]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -0.015625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 1,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Ulyion]],  
              Position = {
                y = -1201.015625,  
                x = 27601.85938,  
                InstanceId = [[Client1_42671]],  
                Class = [[Position]],  
                z = 74.421875
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 6,  
              MorphTarget3 = 4,  
              Tattoo = 29
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_42674]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 2,  
              HandsModel = 5606958,  
              FeetColor = 0,  
              GabaritBreastSize = 8,  
              GabaritHeight = 8,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 6,  
              HandsColor = 1,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42672]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -0.015625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Meron]],  
              Position = {
                y = -1195.25,  
                x = 27602.54688,  
                InstanceId = [[Client1_42675]],  
                Class = [[Position]],  
                z = 74.359375
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 6,  
              MorphTarget3 = 6,  
              Tattoo = 17
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_42678]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 5604142,  
              FeetColor = 2,  
              GabaritBreastSize = 6,  
              GabaritHeight = 13,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 1,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42676]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45298]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45299]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45300]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[5]],  
                        InstanceId = [[Client1_45301]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45302]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 6,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = -1.109375,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 5,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Ulynix]],  
              Position = {
                y = -1190.40625,  
                x = 27605.65625,  
                InstanceId = [[Client1_42679]],  
                Class = [[Position]],  
                z = 74.203125
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 21
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_42682]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 3,  
              HandsModel = 5606958,  
              FeetColor = 0,  
              GabaritBreastSize = 13,  
              GabaritHeight = 10,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 0,  
              HandsColor = 5,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42680]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = -2.25,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Xathus]],  
              Position = {
                y = -1189.78125,  
                x = 27614.5,  
                InstanceId = [[Client1_42683]],  
                Class = [[Position]],  
                z = 73.578125
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 1,  
              MorphTarget3 = 5,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_42686]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 4,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 7,  
              HairColor = 3,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 4,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42684]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42760]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42761]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45295]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[5]],  
                        InstanceId = [[Client1_45296]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45268]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45297]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 2,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = -2.25,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Ibilaus]],  
              Position = {
                y = -1195.234375,  
                x = 27619.1875,  
                InstanceId = [[Client1_42687]],  
                Class = [[Position]],  
                z = 73.375
              },  
              ArmModel = 5604910,  
              MorphTarget7 = 4,  
              MorphTarget3 = 5,  
              Tattoo = 27
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_42690]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 1,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 7,  
              GabaritHeight = 12,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 2,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42688]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 2.421875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 6,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Gan]],  
              Position = {
                y = -1224.859375,  
                x = 27626.39063,  
                InstanceId = [[Client1_42691]],  
                Class = [[Position]],  
                z = 75.390625
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 1
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_42694]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 3,  
              HandsModel = 5604142,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 12,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 10,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42692]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42813]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42814]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42767]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 2.421875,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 2,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595182,  
              Level = 0,  
              Name = [[Cetis]],  
              Position = {
                y = -1228.328125,  
                x = 27618.48438,  
                InstanceId = [[Client1_42695]],  
                Class = [[Position]],  
                z = 75.203125
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 3,  
              MorphTarget3 = 4,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_42861]],  
              ActivitiesId = {
              },  
              HairType = 5422,  
              TrouserColor = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 9,  
              HandsModel = 5609774,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42859]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 5653038,  
              Speed = 0,  
              Angle = 3,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 7,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Master of Wisdom]],  
              Position = {
                y = -1202.875,  
                x = 27708.98438,  
                InstanceId = [[Client1_42862]],  
                Class = [[Position]],  
                z = 76.859375
              },  
              ArmModel = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 0,  
              Tattoo = 25
            },  
            {
              InstanceId = [[Client1_42891]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42889]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42917]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42918]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Hard Kipee]],  
              Position = {
                y = -1105.625,  
                x = 27774.92188,  
                InstanceId = [[Client1_42892]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckhdb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42895]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42893]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42909]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42910]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Vulgar  Kipee]],  
              Position = {
                y = -1115.53125,  
                x = 27772.20313,  
                InstanceId = [[Client1_42896]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckhdb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42899]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42897]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42913]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42914]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Vulgar  Kipee]],  
              Position = {
                y = -1111.546875,  
                x = 27802.6875,  
                InstanceId = [[Client1_42900]],  
                Class = [[Position]],  
                z = 74.84375
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckhdb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42903]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42901]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42911]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42912]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Vigorous  Kipee]],  
              Position = {
                y = -1124.171875,  
                x = 27775.82813,  
                InstanceId = [[Client1_42904]],  
                Class = [[Position]],  
                z = 74.5
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckhdb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42907]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42905]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42915]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42916]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42870]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kinrey]],  
              Position = {
                y = -1111.8125,  
                x = 27786.76563,  
                InstanceId = [[Client1_42908]],  
                Class = [[Position]],  
                z = 74.828125
              },  
              Angle = -2.90919137,  
              Base = [[palette.entities.creatures.ckbib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42954]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42952]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42973]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42974]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42920]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kidinak]],  
              Position = {
                y = -1195.6875,  
                x = 27834.54688,  
                InstanceId = [[Client1_42955]],  
                Class = [[Position]],  
                z = 74.578125
              },  
              Angle = -2.965913296,  
              Base = [[palette.entities.creatures.ckaib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42958]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42956]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42971]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42972]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42920]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kidinak]],  
              Position = {
                y = -1186.109375,  
                x = 27819.65625,  
                InstanceId = [[Client1_42959]],  
                Class = [[Position]],  
                z = 73.96875
              },  
              Angle = -2.965913296,  
              Base = [[palette.entities.creatures.ckaib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42950]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42948]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42969]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42970]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42920]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kipesta]],  
              Position = {
                y = -1181.828125,  
                x = 27829.76563,  
                InstanceId = [[Client1_42951]],  
                Class = [[Position]],  
                z = 74.546875
              },  
              Angle = 2.768805742,  
              Base = [[palette.entities.creatures.ckjib4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42965]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42963]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_42967]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_42968]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42920]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kidinak]],  
              Position = {
                y = -1174.765625,  
                x = 27824.0625,  
                InstanceId = [[Client1_42966]],  
                Class = [[Position]],  
                z = 74.796875
              },  
              Angle = -2.965913296,  
              Base = [[palette.entities.creatures.ckaib3]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_42977]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 8,  
              HandsModel = 5609774,  
              FeetColor = 1,  
              GabaritBreastSize = 8,  
              GabaritHeight = 6,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 7,  
              HandsColor = 2,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_42975]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43288]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_43289]],  
                      Value = r2.RefId([[Client1_43245]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_43290]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_43291]],  
                        Entity = r2.RefId([[Client1_43233]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45263]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45264]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_45265]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45266]],  
                        Entity = r2.RefId([[Client1_45246]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45304]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45305]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_45306]],  
                          Value = r2.RefId([[Client1_45292]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45307]],  
                        Entity = r2.RefId([[Client1_42666]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45309]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45310]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_45311]],  
                          Value = r2.RefId([[Client1_42762]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45312]],  
                        Entity = r2.RefId([[Client1_42591]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45314]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45315]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[stops dialog]],  
                          InstanceId = [[Client1_45316]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45317]],  
                        Entity = r2.RefId([[Client1_42817]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45319]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45320]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[stops dialog]],  
                          InstanceId = [[Client1_45321]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45322]],  
                        Entity = r2.RefId([[Client1_42567]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45475]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45476]],  
                      Value = r2.RefId([[Client1_45452]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_45477]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45478]],  
                        Entity = r2.RefId([[Client1_45458]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45552]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45553]],  
                      Value = r2.RefId([[Client1_45529]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_45554]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45555]],  
                        Entity = r2.RefId([[Client1_45544]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43243]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43244]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43245]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[6]],  
                        InstanceId = [[Client1_43246]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_43215]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45237]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45238]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45205]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45452]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45453]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45403]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45529]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45530]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 5653038,  
              Speed = 1,  
              Angle = -2.359375,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 6,  
              JacketModel = 5610542,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Princess Faneliah]],  
              Position = {
                y = -1106.921875,  
                x = 27856.51563,  
                InstanceId = [[Client1_42978]],  
                Class = [[Position]],  
                z = 73.59375
              },  
              ArmModel = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 5,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_43111]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 9,  
              HandsModel = 5606958,  
              FeetColor = 3,  
              GabaritBreastSize = 2,  
              GabaritHeight = 3,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 3,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43109]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43132]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43133]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43134]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43135]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_43077]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43205]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_43206]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_43207]],  
                          Value = r2.RefId([[Client1_43200]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_43208]],  
                        Entity = r2.RefId([[Client1_43127]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 5606702,  
              Speed = 1,  
              Angle = -2.15625,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 2,  
              Level = 0,  
              Sheet = [[ring_melee_damage_dealer_slash_b2.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              JacketModel = 5606446,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[Icarius]],  
              Position = {
                y = -1093.96875,  
                x = 27845.23438,  
                InstanceId = [[Client1_43112]],  
                Class = [[Position]],  
                z = 73.8125
              },  
              ArmModel = 5606190,  
              MorphTarget7 = 4,  
              MorphTarget3 = 1,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_43121]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 3,  
              HairColor = 3,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 7,  
              HandsColor = 5,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43119]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43123]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_43124]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_43077]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43130]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_43131]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43210]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_43211]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_43212]],  
                          Value = r2.RefId([[Client1_43200]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_43213]],  
                        Entity = r2.RefId([[Client1_43127]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -2.5,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 3,  
              Level = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              WeaponLeftHand = 0,  
              Name = [[Deuron]],  
              Position = {
                y = -1108.359375,  
                x = 27852.84375,  
                InstanceId = [[Client1_43122]],  
                Class = [[Position]],  
                z = 73.6875
              },  
              ArmModel = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 7,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_43127]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 4,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 14,  
              GabaritHeight = 5,  
              HairColor = 5,  
              Aggro = 5,  
              EyesColor = 7,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_43125]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43198]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43199]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_43200]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_43201]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_43170]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45200]],  
                    Event = {
                      Type = [[end of activity sequence]],  
                      InstanceId = [[Client1_45201]],  
                      Value = r2.RefId([[Client1_43200]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[starts dialog]],  
                          InstanceId = [[Client1_45202]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_45203]],  
                        Entity = r2.RefId([[Client1_45162]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              SheetClient = [[basic_fyros_male.creature]],  
              FeetModel = 5605422,  
              Speed = 1,  
              Angle = -0.9375,  
              Base = [[palette.entities.npcs.bandits.f_mage_atysian_curser_20]],  
              ArmColor = 0,  
              Level = 0,  
              Sex = 1,  
              BotAttackable = 0,  
              WeaponLeftHand = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 2,  
              Sheet = [[ring_magic_curser_blind_b2.creature]],  
              WeaponRightHand = 6933806,  
              JacketModel = 5606446,  
              Name = [[Dyps]],  
              Position = {
                y = -1093.4375,  
                x = 27839.21875,  
                InstanceId = [[Client1_43128]],  
                Class = [[Position]],  
                z = 73.640625
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 1,  
              Tattoo = 29
            }
          },  
          InstanceId = [[Client1_42511]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_42567]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_42565]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 1]],  
          Position = {
            y = -1206.609375,  
            x = 27610.07813,  
            InstanceId = [[Client1_42566]],  
            Class = [[Position]],  
            z = 73
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 3,  
              InstanceId = [[Client1_42568]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42569]],  
                  Who = r2.RefId([[Client1_42555]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42588]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 2,  
              InstanceId = [[Client1_42570]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42571]],  
                  Who = r2.RefId([[Client1_42559]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42575]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_42573]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42574]],  
                  Who = r2.RefId([[Client1_42563]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42576]]
                }
              },  
              Name = [[]]
            }
          },  
          Repeating = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_42817]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_42815]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 2]],  
          Position = {
            y = -1206.59375,  
            x = 27608.625,  
            InstanceId = [[Client1_42816]],  
            Class = [[Position]],  
            z = 73
          },  
          Repeating = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 10,  
              InstanceId = [[Client1_42818]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42819]],  
                  Who = r2.RefId([[Client1_42591]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42820]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 5,  
              InstanceId = [[Client1_42821]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42822]],  
                  Who = r2.RefId([[Client1_42642]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42825]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_42823]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42824]],  
                  Who = r2.RefId([[Client1_42670]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42828]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_42826]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_42827]],  
                  Who = r2.RefId([[Client1_42690]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42829]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 1
        },  
        {
          InstanceId = [[Client1_42834]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_42865]],  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_42866]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_42867]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_42868]],  
                    Entity = r2.RefId([[Client1_42853]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_42835]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone trigger 1]],  
          Position = {
            y = -1203.921875,  
            x = 27711.6875,  
            InstanceId = [[Client1_42836]],  
            Class = [[Position]],  
            z = 77.03125
          },  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Places 1]],  
              InstanceId = [[Client1_42838]],  
              Points = {
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_42840]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -0.9375,  
                    x = 4.65625,  
                    InstanceId = [[Client1_42841]],  
                    Class = [[Position]],  
                    z = 0.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_42843]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 7.75,  
                    x = 3.65625,  
                    InstanceId = [[Client1_42844]],  
                    Class = [[Position]],  
                    z = -3.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_42846]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 1.203125,  
                    x = -6.609375,  
                    InstanceId = [[Client1_42847]],  
                    Class = [[Position]],  
                    z = -0.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_42849]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2.1875,  
                    x = -4.328125,  
                    InstanceId = [[Client1_42850]],  
                    Class = [[Position]],  
                    z = -0.09375
                  }
                }
              },  
              Class = [[Region]],  
              Deletable = 0,  
              Position = {
                y = 0.390625,  
                x = -0.359375,  
                InstanceId = [[Client1_42837]],  
                Class = [[Position]],  
                z = -0.046875
              }
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_42838]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_42853]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_42851]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 3]],  
          Position = {
            y = -1205.71875,  
            x = 27709.79688,  
            InstanceId = [[Client1_42852]],  
            Class = [[Position]],  
            z = 77
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_42854]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Quiet]],  
                  InstanceId = [[Client1_42855]],  
                  Who = r2.RefId([[Client1_42861]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_42863]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_43005]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_43003]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 4]],  
          Position = {
            y = -1096.125,  
            x = 27863.45313,  
            InstanceId = [[Client1_43004]],  
            Class = [[Position]],  
            z = 75.3125
          },  
          Repeating = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_43006]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Imploring]],  
                  InstanceId = [[Client1_43007]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_43008]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_43009]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Hysterical]],  
                  InstanceId = [[Client1_43010]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_43011]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 1
        },  
        {
          InstanceId = [[Client1_43144]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_43162]],  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_43163]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_43164]],  
                      Value = r2.RefId([[Client1_43134]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_43165]],  
                    Entity = r2.RefId([[Client1_43111]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_43145]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          _Zone = [[Client1_43148]],  
          Name = [[Zone trigger 2]],  
          Position = {
            y = -1106.390625,  
            x = 27857.60938,  
            InstanceId = [[Client1_43146]],  
            Class = [[Position]],  
            z = 73
          },  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          Active = 1,  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Places 2]],  
              InstanceId = [[Client1_43148]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_43147]],  
                Class = [[Position]],  
                z = 0
              },  
              Class = [[Region]],  
              Deletable = 0,  
              Points = {
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_43150]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 3.46875,  
                    x = 3.8125,  
                    InstanceId = [[Client1_43151]],  
                    Class = [[Position]],  
                    z = 0.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_43153]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 8.4375,  
                    x = -1.734375,  
                    InstanceId = [[Client1_43154]],  
                    Class = [[Position]],  
                    z = 0.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_43156]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -6.875,  
                    x = -11.34375,  
                    InstanceId = [[Client1_43157]],  
                    Class = [[Position]],  
                    z = 2.125
                  }
                },  
                {
                  InheritPos = 1,  
                  Deletable = 0,  
                  InstanceId = [[Client1_43159]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -11,  
                    x = -1.921875,  
                    InstanceId = [[Client1_43160]],  
                    Class = [[Position]],  
                    z = -0.171875
                  }
                }
              }
            }
          },  
          Class = [[ZoneTrigger]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_43233]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_43293]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_43294]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45158]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45159]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45240]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45241]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45242]],  
                      Value = r2.RefId([[Client1_45237]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45243]],  
                    Entity = r2.RefId([[Client1_42977]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            InstanceId = [[Client1_43231]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 5]],  
          Position = {
            y = -1096.578125,  
            x = 27864.96875,  
            InstanceId = [[Client1_43232]],  
            Class = [[Position]],  
            z = 75
          },  
          Active = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 1,  
              InstanceId = [[Client1_43234]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Ecstatic]],  
                  InstanceId = [[Client1_43235]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_43236]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 2,  
              InstanceId = [[Client1_43237]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Panick]],  
                  InstanceId = [[Client1_43238]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_43239]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 2,  
              InstanceId = [[Client1_43240]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Pointfront]],  
                  InstanceId = [[Client1_43241]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_43242]]
                }
              },  
              Name = [[]]
            }
          },  
          Repeating = 0
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_43277]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_43281]],  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_43282]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_43283]],  
                      Value = r2.RefId([[Client1_43245]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_43284]],  
                    Entity = r2.RefId([[Client1_42977]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[stops dialog]],  
                      InstanceId = [[Client1_43285]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_43286]],  
                    Entity = r2.RefId([[Client1_43005]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_43278]]
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[UserTrigger 1]],  
          Position = {
            y = -1109.203125,  
            x = 27865.70313,  
            InstanceId = [[Client1_43279]],  
            Class = [[Position]],  
            z = 73
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.user_event]]
        },  
        {
          InstanceId = [[Client1_44975]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0.625,  
            x = -7.8125,  
            InstanceId = [[Client1_44974]],  
            Class = [[Position]],  
            z = -1.28125
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_44967]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 13,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 5,  
              GabaritHeight = 1,  
              HairColor = 1,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44965]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45169]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45170]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45171]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45172]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 5606702,  
              Speed = 1,  
              Angle = -1.688836694,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_melee_damage_dealer_pierce_b3.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 6,  
              JacketModel = 5606446,  
              WeaponRightHand = 5635886,  
              Level = 2,  
              Name = [[Meps]],  
              Position = {
                y = -1006.140625,  
                x = 27551.20313,  
                InstanceId = [[Client1_44968]],  
                Class = [[Position]],  
                z = 75.40625
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 0,  
              MorphTarget3 = 2,  
              Tattoo = 15
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_44971]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 1,  
              GabaritHeight = 4,  
              HairColor = 5,  
              EyesColor = 7,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 12,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44969]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -2.770940781,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_aoe_cold_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 4,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 0,  
              Name = [[Xaton]],  
              Position = {
                y = -1012.078125,  
                x = 27554.71875,  
                InstanceId = [[Client1_44972]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 1,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_44978]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 11,  
              GabaritHeight = 0,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 8,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44976]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -0.9034385681,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b3.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              JacketModel = 5606446,  
              WeaponRightHand = 6933806,  
              Level = 2,  
              Name = [[Pirus]],  
              Position = {
                y = -1005.671875,  
                x = 27544.79688,  
                InstanceId = [[Client1_44980]],  
                Class = [[Position]],  
                z = 74.015625
              },  
              ArmModel = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 6,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_44983]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 3,  
              GabaritBreastSize = 13,  
              GabaritHeight = 3,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 10,  
              HandsColor = 2,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44981]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -0.292573303,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_curser_fear_b3.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 2,  
              JacketModel = 5606446,  
              WeaponRightHand = 6933806,  
              Level = 2,  
              Name = [[Dymus]],  
              Position = {
                y = -1014.359375,  
                x = 27544.60938,  
                InstanceId = [[Client1_44985]],  
                Class = [[Position]],  
                z = 73.953125
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 7,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_44988]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 3,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 13,  
              GabaritHeight = 5,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 1,  
              HandsColor = 1,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44986]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -5.039868832,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 2,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 0,  
              Name = [[Kyrius]],  
              Position = {
                y = -1017.546875,  
                x = 27549.6875,  
                InstanceId = [[Client1_44990]],  
                Class = [[Position]],  
                z = 75.6875
              },  
              ArmModel = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_44993]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 2,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 3,  
              GabaritHeight = 7,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 10,  
              HandsColor = 3,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44991]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 6,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = 0.4966504574,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_curser_fear_b3.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 2,  
              Name = [[Meros]],  
              Position = {
                y = -1010.640625,  
                x = 27544.42188,  
                InstanceId = [[Client1_44995]],  
                Class = [[Position]],  
                z = 73.703125
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 0,  
              Tattoo = 13
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_44998]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 1,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 12,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 9,  
              HandsColor = 2,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44996]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 5,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -4.145925522,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 0,  
              Name = [[Dean]],  
              Position = {
                y = -1015.90625,  
                x = 27552.60938,  
                InstanceId = [[Client1_45000]],  
                Class = [[Position]],  
                z = 75.46875
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 5,  
              Tattoo = 20
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_45003]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 13,  
              HandsModel = 5605678,  
              FeetColor = 2,  
              GabaritBreastSize = 8,  
              GabaritHeight = 3,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45001]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -1.475571632,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 6,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 1,  
              Name = [[Zenix]],  
              Position = {
                y = -1003.515625,  
                x = 27546.40625,  
                InstanceId = [[Client1_45005]],  
                Class = [[Position]],  
                z = 74.53125
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 5,  
              Tattoo = 8
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45008]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 1,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 6,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45006]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 5,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -2.15625,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_curser_fear_b3.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 5606446,  
              WeaponRightHand = 6933806,  
              Level = 2,  
              Name = [[Lydix]],  
              Position = {
                y = -1008.8125,  
                x = 27554.98438,  
                InstanceId = [[Client1_45009]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 0,  
              Tattoo = 26
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_45012]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 5,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 14,  
              HairColor = 2,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 12,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45010]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -5.088403225,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 5594158,  
              Sheet = [[ring_light_melee_pierce_b3.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 6,  
              JacketModel = 0,  
              WeaponRightHand = 5594158,  
              Level = 2,  
              Name = [[Iorius]],  
              Position = {
                y = -1016.828125,  
                x = 27547.34375,  
                InstanceId = [[Client1_45014]],  
                Class = [[Position]],  
                z = 75.53125
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 3,  
              Tattoo = 2
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_44973]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_45029]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = -0.640625,  
            x = -0.0625,  
            InstanceId = [[Client1_45028]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_45021]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 8,  
              GabaritHeight = 0,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 8,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45019]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45183]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45184]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45185]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45186]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 6,  
              FeetModel = 5605422,  
              Speed = 1,  
              Angle = -4.858852386,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_aoe_cold_b3.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 6,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 2,  
              Name = [[Aecaon]],  
              Position = {
                y = -1023.09375,  
                x = 27543,  
                InstanceId = [[Client1_45022]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 4,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_45025]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 10,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 0,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45023]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 6,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = 0.9181873798,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_aoe_cold_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 3,  
              Name = [[Gaxius]],  
              Position = {
                y = -1025.3125,  
                x = 27535.84375,  
                InstanceId = [[Client1_45026]],  
                Class = [[Position]],  
                z = 75
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 2,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_45032]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 10,  
              HandsModel = 5605678,  
              FeetColor = 3,  
              GabaritBreastSize = 6,  
              GabaritHeight = 1,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 1,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45030]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -3.4276824,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 5,  
              JacketModel = 5606446,  
              WeaponRightHand = 6933806,  
              Level = 0,  
              Name = [[Kyron]],  
              Position = {
                y = -1027.625,  
                x = 27544.01563,  
                InstanceId = [[Client1_45034]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 29
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_45037]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 9,  
              HandsModel = 5605678,  
              FeetColor = 3,  
              GabaritBreastSize = 3,  
              GabaritHeight = 0,  
              HairColor = 5,  
              EyesColor = 2,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45035]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -2.328125,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_aoe_cold_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 6,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 3,  
              Name = [[Abylaus]],  
              Position = {
                y = -1020.375,  
                x = 27540.57813,  
                InstanceId = [[Client1_45039]],  
                Class = [[Position]],  
                z = 74.828125
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_45042]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 10,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 11,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 5,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 5,  
              HandsColor = 5,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45040]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 5,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = -3.445135832,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_melee_damage_dealer_slash_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              JacketModel = 5606446,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Boelion]],  
              Position = {
                y = -1026.265625,  
                x = 27546.84375,  
                InstanceId = [[Client1_45044]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 5,  
              Tattoo = 6
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_45047]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 2,  
              HandsModel = 5605678,  
              FeetColor = 5,  
              GabaritBreastSize = 11,  
              GabaritHeight = 9,  
              HairColor = 3,  
              EyesColor = 0,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45045]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -5.679157257,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 5594158,  
              Sheet = [[ring_light_melee_pierce_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 3,  
              JacketModel = 0,  
              WeaponRightHand = 5594158,  
              Level = 1,  
              Name = [[Xyllo]],  
              Position = {
                y = -1027.40625,  
                x = 27538.28125,  
                InstanceId = [[Client1_45049]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 2,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_45052]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 0,  
              GabaritHeight = 1,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 3,  
              HandsColor = 5,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45050]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = 0.2200557142,  
              Base = [[palette.entities.npcs.bandits.f_mage_atysian_curser_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_curser_blind_b2.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 0,  
              Name = [[Aekos]],  
              Position = {
                y = -1022.671875,  
                x = 27536.40625,  
                InstanceId = [[Client1_45054]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              ArmModel = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 3,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45057]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 5,  
              HandsModel = 5605678,  
              FeetColor = 2,  
              GabaritBreastSize = 8,  
              GabaritHeight = 6,  
              HairColor = 2,  
              EyesColor = 5,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 11,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45055]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -2.84375,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_aoe_cold_b2.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              JacketModel = 5606446,  
              WeaponRightHand = 6933806,  
              Level = 1,  
              Name = [[Iorus]],  
              Position = {
                y = -1022.125,  
                x = 27539.20313,  
                InstanceId = [[Client1_45059]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_45062]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 10,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 13,  
              GabaritHeight = 10,  
              HairColor = 3,  
              EyesColor = 0,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 7,  
              HandsColor = 5,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45060]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -2.84375,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 1,  
              Name = [[Decaon]],  
              Position = {
                y = -1026.421875,  
                x = 27541.4375,  
                InstanceId = [[Client1_45064]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 0,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_45067]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 3,  
              HandsModel = 5605678,  
              FeetColor = 4,  
              GabaritBreastSize = 8,  
              GabaritHeight = 2,  
              HairColor = 1,  
              EyesColor = 4,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 11,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45065]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 5,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -2.84375,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_curser_fear_b3.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 2,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 2,  
              Name = [[Xarius]],  
              Position = {
                y = -1024.703125,  
                x = 27537.23438,  
                InstanceId = [[Client1_45069]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 6,  
              Tattoo = 28
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45027]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_45080]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 3]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_45079]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_45072]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 9,  
              HandsModel = 5606958,  
              FeetColor = 4,  
              GabaritBreastSize = 10,  
              GabaritHeight = 13,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 0,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45070]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45187]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45188]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45191]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45192]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_42723]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 1,  
              FeetModel = 5606702,  
              Speed = 1,  
              Angle = -2.317800283,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_melee_damage_dealer_pierce_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 4,  
              JacketModel = 5607726,  
              WeaponRightHand = 5635886,  
              Level = 3,  
              Name = [[Merius]],  
              Position = {
                y = -1012.28125,  
                x = 27519.42188,  
                InstanceId = [[Client1_45073]],  
                Class = [[Position]],  
                z = 73.234375
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 5,  
              MorphTarget3 = 4,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_45076]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 7,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 1,  
              HairColor = 2,  
              EyesColor = 5,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 12,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45074]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -0.328125,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_aoe_cold_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 7,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 1,  
              Name = [[Picaon]],  
              Position = {
                y = -1008.328125,  
                x = 27523.25,  
                InstanceId = [[Client1_45077]],  
                Class = [[Position]],  
                z = 72.5
              },  
              ArmModel = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 5,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_45083]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 12,  
              HandsModel = 5605678,  
              FeetColor = 2,  
              GabaritBreastSize = 0,  
              GabaritHeight = 5,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45081]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -1.706935167,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b3.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              JacketModel = 5606446,  
              WeaponRightHand = 6933806,  
              Level = 2,  
              Name = [[Meron]],  
              Position = {
                y = -1015.8125,  
                x = 27520.34375,  
                InstanceId = [[Client1_45085]],  
                Class = [[Position]],  
                z = 73.96875
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 2,  
              Tattoo = 14
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_45088]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 0,  
              MorphTarget5 = 7,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 11,  
              HandsModel = 5605678,  
              FeetColor = 1,  
              GabaritBreastSize = 0,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 12,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45086]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 1,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -0.328125,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_curser_fear_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 7,  
              JacketModel = 5606446,  
              WeaponRightHand = 6933806,  
              Level = 3,  
              Name = [[Thecaon]],  
              Position = {
                y = -1016.59375,  
                x = 27518.01563,  
                InstanceId = [[Client1_45090]],  
                Class = [[Position]],  
                z = 74.09375
              },  
              ArmModel = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 7,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45093]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 5,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 4,  
              GabaritHeight = 6,  
              HairColor = 2,  
              EyesColor = 1,  
              TrouserModel = 5658414,  
              GabaritLegsWidth = 12,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45091]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = 0.4049132764,  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_aoe_cold_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              JacketModel = 0,  
              WeaponRightHand = 6933806,  
              Level = 3,  
              Name = [[Gan]],  
              Position = {
                y = -1013.15625,  
                x = 27518.15625,  
                InstanceId = [[Client1_45095]],  
                Class = [[Position]],  
                z = 73.375
              },  
              ArmModel = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 6,  
              Tattoo = 30
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_45098]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 2,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 12,  
              GabaritHeight = 2,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45096]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = -4.394742012,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_melee_damage_dealer_slash_b3.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 2,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595950,  
              Level = 2,  
              Name = [[Iokos]],  
              Position = {
                y = -1006.359375,  
                x = 27518.29688,  
                InstanceId = [[Client1_45100]],  
                Class = [[Position]],  
                z = 72.078125
              },  
              ArmModel = 5606190,  
              MorphTarget7 = 1,  
              MorphTarget3 = 3,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_45103]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 10,  
              HandsModel = 5604142,  
              FeetColor = 5,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 2,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 3,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45101]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 1,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = -2.387613535,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 5637166,  
              Sheet = [[ring_melee_tank_slash_b3.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 5606446,  
              WeaponRightHand = 5594670,  
              Level = 2,  
              Name = [[Aeseus]],  
              Position = {
                y = -1002.828125,  
                x = 27517.15625,  
                InstanceId = [[Client1_45105]],  
                Class = [[Position]],  
                z = 71.640625
              },  
              ArmModel = 5606190,  
              MorphTarget7 = 3,  
              MorphTarget3 = 6,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_45108]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 2,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 0,  
              HandsModel = 5605678,  
              FeetColor = 4,  
              GabaritBreastSize = 9,  
              GabaritHeight = 10,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 12,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45106]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 5,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -3.120651722,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 3,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_light_melee_pierce_b2.creature]],  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              JacketModel = 0,  
              WeaponRightHand = 5594414,  
              Level = 1,  
              Name = [[Tirius]],  
              Position = {
                y = -1016.703125,  
                x = 27522.96875,  
                InstanceId = [[Client1_45110]],  
                Class = [[Position]],  
                z = 74.171875
              },  
              ArmModel = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 2,  
              Tattoo = 11
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_45113]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 4,  
              HandsModel = 5606958,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 5,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 2,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45111]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = -0.328125,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_melee_damage_dealer_slash_b2.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 1,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Xanix]],  
              Position = {
                y = -1010.109375,  
                x = 27516.85938,  
                InstanceId = [[Client1_45115]],  
                Class = [[Position]],  
                z = 72.6875
              },  
              ArmModel = 5607470,  
              MorphTarget7 = 3,  
              MorphTarget3 = 2,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_45118]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 10,  
              HandsModel = 5605678,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 12,  
              HairColor = 5,  
              EyesColor = 7,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 11,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45116]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 5605422,  
              Speed = 0,  
              Angle = -0.328125,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 2,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_damage_dealer_cold_b4.creature]],  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              JacketModel = 5606446,  
              WeaponRightHand = 6933806,  
              Level = 3,  
              Name = [[Boeion]],  
              Position = {
                y = -1002.84375,  
                x = 27515.34375,  
                InstanceId = [[Client1_45120]],  
                Class = [[Position]],  
                z = 71.546875
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 6,  
              Tattoo = 31
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45078]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45162]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45167]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45168]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45179]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45180]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45181]],  
                      Value = r2.RefId([[Client1_45171]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45182]],  
                    Entity = r2.RefId([[Client1_44975]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45189]],  
                      Value = r2.RefId([[Client1_45185]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45190]],  
                    Entity = r2.RefId([[Client1_45029]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45193]],  
                      Value = r2.RefId([[Client1_45191]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45194]],  
                    Entity = r2.RefId([[Client1_45080]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_45160]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 6]],  
          Position = {
            y = -1022.84375,  
            x = 27525.04688,  
            InstanceId = [[Client1_45161]],  
            Class = [[Position]],  
            z = 75
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 39,  
              InstanceId = [[Client1_45163]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Cruel]],  
                  InstanceId = [[Client1_45164]],  
                  Who = r2.RefId([[Client1_43127]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45165]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45246]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_45244]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 7]],  
          Position = {
            y = -1208,  
            x = 27612.60938,  
            InstanceId = [[Client1_45245]],  
            Class = [[Position]],  
            z = 73
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45247]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Cheer]],  
                  InstanceId = [[Client1_45248]],  
                  Who = r2.RefId([[Client1_42559]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45249]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_45250]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45251]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45252]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_45253]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45254]],  
                  Who = r2.RefId([[Client1_42563]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45257]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_45255]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Panick]],  
                  InstanceId = [[Client1_45256]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45258]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 2,  
              InstanceId = [[Client1_45259]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45260]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45261]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45342]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45436]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45438]],  
                      Value = r2.RefId([[Client1_45433]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45439]],  
                    Entity = r2.RefId([[Client1_42563]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45448]],  
                      Value = r2.RefId([[Client1_45446]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45449]],  
                    Entity = r2.RefId([[Client1_42559]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45450]],  
                      Value = r2.RefId([[Client1_45442]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45451]],  
                    Entity = r2.RefId([[Client1_42555]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45454]],  
                      Value = r2.RefId([[Client1_45452]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45455]],  
                    Entity = r2.RefId([[Client1_42977]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45437]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_45340]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 8]],  
          Position = {
            y = -1204.859375,  
            x = 27602.5,  
            InstanceId = [[Client1_45341]],  
            Class = [[Position]],  
            z = 75
          },  
          Active = 0,  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45343]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45344]],  
                  Who = r2.RefId([[Client1_42666]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42977]]),  
                  Says = [[Client1_45345]]
                }
              },  
              Name = [[]]
            }
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 0
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_45349]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45359]],  
                Class = [[LogicEntityAction]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_45361]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45362]],  
                    Entity = r2.RefId([[Client1_45342]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_45360]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                }
              }
            },  
            InstanceId = [[Client1_45350]]
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[UserTrigger 2]],  
          Position = {
            y = -1203.140625,  
            x = 27601.89063,  
            InstanceId = [[Client1_45351]],  
            Class = [[Position]],  
            z = 75
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.user_event]]
        },  
        {
          InstanceId = [[Client1_45389]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 4]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_45388]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              InstanceId = [[Client1_45377]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45375]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45484]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45559]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45560]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45561]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Assault Kirosta]],  
              Position = {
                y = -1335.6875,  
                x = 27529.8125,  
                InstanceId = [[Client1_45378]],  
                Class = [[Position]],  
                z = 75.796875
              },  
              Angle = 0.8926824927,  
              Base = [[palette.entities.creatures.ckfrb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45385]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45383]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Assault Kirosta]],  
              Position = {
                y = -1338,  
                x = 27532.57813,  
                InstanceId = [[Client1_45386]],  
                Class = [[Position]],  
                z = 76.1875
              },  
              Angle = 0.8926824927,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45381]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45379]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Assault Kirosta]],  
              Position = {
                y = -1333.5,  
                x = 27520.79688,  
                InstanceId = [[Client1_45382]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = 0.8926824927,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45392]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45390]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kizarak]],  
              Position = {
                y = -1364.296875,  
                x = 27518.125,  
                InstanceId = [[Client1_45393]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = 0.8926824927,  
              Base = [[palette.entities.creatures.ckcib4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45400]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45398]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kidinak]],  
              Position = {
                y = -1329.375,  
                x = 27521.20313,  
                InstanceId = [[Client1_45401]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = 0.8926824927,  
              Base = [[palette.entities.creatures.ckaib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45396]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45394]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kidinak]],  
              Position = {
                y = -1330.59375,  
                x = 27528,  
                InstanceId = [[Client1_45397]],  
                Class = [[Position]],  
                z = 75.703125
              },  
              Angle = 0.8926824927,  
              Base = [[palette.entities.creatures.ckaib3]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45387]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45458]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45496]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45497]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_45498]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45499]],  
                    Entity = r2.RefId([[Client1_45488]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_45456]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 9]],  
          Position = {
            y = -1321.28125,  
            x = 27544.21875,  
            InstanceId = [[Client1_45457]],  
            Class = [[Position]],  
            z = 81
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45459]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45460]],  
                  Who = r2.RefId([[Client1_42555]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45463]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 2,  
              InstanceId = [[Client1_45461]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45462]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45466]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45488]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_45480]],  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_45481]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_45482]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45483]],  
                    Entity = r2.RefId([[Client1_45389]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45532]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45533]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45534]],  
                      Value = r2.RefId([[Client1_45529]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45535]],  
                    Entity = r2.RefId([[Client1_42977]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45536]],  
                      Value = r2.RefId([[Client1_45525]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45537]],  
                    Entity = r2.RefId([[Client1_42559]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45538]],  
                      Value = r2.RefId([[Client1_45527]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45539]],  
                    Entity = r2.RefId([[Client1_42555]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45540]],  
                      Value = r2.RefId([[Client1_45523]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45541]],  
                    Entity = r2.RefId([[Client1_42563]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45557]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45558]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45563]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45564]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45565]],  
                      Value = r2.RefId([[Client1_45560]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45566]],  
                    Entity = r2.RefId([[Client1_45389]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_45650]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_45651]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_45652]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45653]],  
                    Entity = r2.RefId([[Client1_45577]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[begin activity sequence]],  
                      InstanceId = [[Client1_45657]],  
                      Value = r2.RefId([[Client1_45647]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_45658]],  
                    Entity = r2.RefId([[Client1_45577]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_45486]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 10]],  
          Position = {
            y = -1321.46875,  
            x = 27545.48438,  
            InstanceId = [[Client1_45487]],  
            Class = [[Position]],  
            z = 81
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45489]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45490]],  
                  Who = r2.RefId([[Client1_42559]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45493]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_45491]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45492]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45494]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_45544]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_45681]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_45682]],  
                  Type = [[end of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            InstanceId = [[Client1_45542]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 11]],  
          Position = {
            y = -1208.109375,  
            x = 27621.10938,  
            InstanceId = [[Client1_45543]],  
            Class = [[Position]],  
            z = 73
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 2,  
              InstanceId = [[Client1_45545]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Imploring]],  
                  InstanceId = [[Client1_45546]],  
                  Who = r2.RefId([[Client1_42977]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_45547]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_45548]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_45549]],  
                  Who = r2.RefId([[Client1_42666]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[Client1_42977]]),  
                  Says = [[Client1_45550]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          InstanceId = [[Client1_45577]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 5]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_45576]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              InstanceId = [[Client1_45618]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45616]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45645]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45646]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_45647]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_45648]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_45501]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Reaper Kincher]],  
              Position = {
                y = -1372.78125,  
                x = 27497.95313,  
                InstanceId = [[Client1_45619]],  
                Class = [[Position]],  
                z = 73.5
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckdie2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45569]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45567]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kidinak]],  
              Position = {
                y = -1385.8125,  
                x = 27481.20313,  
                InstanceId = [[Client1_45570]],  
                Class = [[Position]],  
                z = 74.765625
              },  
              Angle = 0.7094846964,  
              Base = [[palette.entities.creatures.ckaib4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45580]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45578]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kipesta]],  
              Position = {
                y = -1377.9375,  
                x = 27483.28125,  
                InstanceId = [[Client1_45582]],  
                Class = [[Position]],  
                z = 73.234375
              },  
              Angle = 0.7094846964,  
              Base = [[palette.entities.creatures.ckjib4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45585]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45583]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kirosta]],  
              Position = {
                y = -1386.796875,  
                x = 27492.6875,  
                InstanceId = [[Client1_45587]],  
                Class = [[Position]],  
                z = 73.96875
              },  
              Angle = 0.7094846964,  
              Base = [[palette.entities.creatures.ckfib4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45590]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45588]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kizarak]],  
              Position = {
                y = -1370.140625,  
                x = 27490.15625,  
                InstanceId = [[Client1_45591]],  
                Class = [[Position]],  
                z = 73.234375
              },  
              Angle = 0.7094846964,  
              Base = [[palette.entities.creatures.ckcib4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45594]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45592]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Assault Kirosta]],  
              Position = {
                y = -1383.625,  
                x = 27489.78125,  
                InstanceId = [[Client1_45595]],  
                Class = [[Position]],  
                z = 73.546875
              },  
              Angle = 0.7094846964,  
              Base = [[palette.entities.creatures.ckfrb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45598]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45596]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kizoar]],  
              Position = {
                y = -1385.984375,  
                x = 27499,  
                InstanceId = [[Client1_45599]],  
                Class = [[Position]],  
                z = 74.015625
              },  
              Angle = 1.596213818,  
              Base = [[palette.entities.creatures.ckiib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45602]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45600]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kizoar]],  
              Position = {
                y = -1382.078125,  
                x = 27503.01563,  
                InstanceId = [[Client1_45603]],  
                Class = [[Position]],  
                z = 74.03125
              },  
              Angle = 1.596213818,  
              Base = [[palette.entities.creatures.ckiib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45606]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45604]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                y = -1380.65625,  
                x = 27484.70313,  
                InstanceId = [[Client1_45607]],  
                Class = [[Position]],  
                z = 73.390625
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45614]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45612]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                y = -1367.75,  
                x = 27505.29688,  
                InstanceId = [[Client1_45615]],  
                Class = [[Position]],  
                z = 74.3125
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45610]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45608]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                y = -1364.359375,  
                x = 27503.03125,  
                InstanceId = [[Client1_45611]],  
                Class = [[Position]],  
                z = 74.3125
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45622]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45620]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Reaper Kidinak]],  
              Position = {
                y = -1374.109375,  
                x = 27503.0625,  
                InstanceId = [[Client1_45624]],  
                Class = [[Position]],  
                z = 73.921875
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckaie2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45627]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45625]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Reaper Kipee]],  
              Position = {
                y = -1389.671875,  
                x = 27474.14063,  
                InstanceId = [[Client1_45628]],  
                Class = [[Position]],  
                z = 76.59375
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckhie3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45631]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45629]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Crusader Kirosta]],  
              Position = {
                y = -1396.234375,  
                x = 27482.07813,  
                InstanceId = [[Client1_45632]],  
                Class = [[Position]],  
                z = 77.5625
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckfre2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45635]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45633]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Raider Kipucker]],  
              Position = {
                y = -1402.5625,  
                x = 27486.15625,  
                InstanceId = [[Client1_45636]],  
                Class = [[Position]],  
                z = 77.953125
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckeic2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45639]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45637]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Raider Kipesta]],  
              Position = {
                y = -1418.03125,  
                x = 27453.67188,  
                InstanceId = [[Client1_45640]],  
                Class = [[Position]],  
                z = 82.234375
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckjic3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_45643]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_45641]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Raider Kincher]],  
              Position = {
                y = -1394.625,  
                x = 27458.76563,  
                InstanceId = [[Client1_45644]],  
                Class = [[Position]],  
                z = 78.578125
              },  
              Angle = 0.8578099608,  
              Base = [[palette.entities.creatures.ckdic3]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_45575]],  
            ChatSequences = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        }
      },  
      LocationId = [[Client1_42512]],  
      ManualWeather = 1
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_42500]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_42572]],  
        Class = [[TextManagerEntry]],  
        Text = [[Princess Faneliah was kidnapped by bandits two years ago, I worry so much for her!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_42575]],  
        Class = [[TextManagerEntry]],  
        Text = [[Me too, I know they bring her to the northeast of here. ]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_42576]],  
        Class = [[TextManagerEntry]],  
        Text = [[We need a strong warrior to deliver her and defeat the bandits!]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_42588]],  
        Class = [[TextManagerEntry]],  
        Text = [[Princess Faneliah was kidnapped by bandits two days ago, I worry so much for her!]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_42820]],  
        Class = [[TextManagerEntry]],  
        Text = [[I hope the Princess is safe]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_42825]],  
        Class = [[TextManagerEntry]],  
        Text = [[She was kidnapped when we were escorting Thecaon to the Gingo's valley]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_42828]],  
        Class = [[TextManagerEntry]],  
        Text = [[We need to be more carefull]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_42829]],  
        Class = [[TextManagerEntry]],  
        Text = [[Everybody worry about the Princess]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_42863]],  
        Class = [[TextManagerEntry]],  
        Text = [[To save the Princess, you have go across a lot of kittins areas. You can choose to fight them, or to find a safe path between dangerous areas.]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_43008]],  
        Class = [[TextManagerEntry]],  
        Text = [[Please! Help me!]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_43011]],  
        Class = [[TextManagerEntry]],  
        Text = [[Kill them! Save me!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_43236]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh! You did it! You're a Hero!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_43239]],  
        Class = [[TextManagerEntry]],  
        Text = [[But when you were fighting those horrible bandits, I saw the last of them running away to the west of here. He went to alert his friends!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_43242]],  
        Class = [[TextManagerEntry]],  
        Text = [[I go back to the village to tell the guards to be carefull and ready to fight. You can escort me or follow the bandit to see what will happen]]
      },  
      {
        Count = 8,  
        InstanceId = [[Client1_45165]],  
        Class = [[TextManagerEntry]],  
        Text = [[Someone mysterious delivered the Princess! Two of us have been killed! Let's take revenge and kill all this silly village!!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_45249]],  
        Class = [[TextManagerEntry]],  
        Text = [[Princess Faneliah you're safe!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_45252]],  
        Class = [[TextManagerEntry]],  
        Text = [[Yeah, you can thanks our Hero!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45257]],  
        Class = [[TextManagerEntry]],  
        Text = [[All the banidts have been killed?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_45258]],  
        Class = [[TextManagerEntry]],  
        Text = [[No! A lot of them are hiding in the moutains, and one of my kidmapper managed to escape and alert them!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45261]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hopefully the guards will protect us, cause soon or later they will attack our little town]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_45345]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bandits are attacking us as you thought Princess!! Go with the girl and hide far from here!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45357]],  
        Class = [[TextManagerEntry]],  
        Text = [[The bandits are attacking us now! Please Princess, go and hide with the]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_45463]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh! What is this brown thing?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45466]],  
        Class = [[TextManagerEntry]],  
        Text = [[A kittin mound!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45493]],  
        Class = [[TextManagerEntry]],  
        Text = [[we've woken up the kittins! ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_45494]],  
        Class = [[TextManagerEntry]],  
        Text = [[They have seen us! Let's go back to the village it will be safer than here, ESCAPE GIRLS!!!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_45547]],  
        Class = [[TextManagerEntry]],  
        Text = [[We woke up a lot of kittins! They attack the village! Please protect us!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_45550]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh! OK girls, go and hide behind the tents!]]
      }
    }
  }
}