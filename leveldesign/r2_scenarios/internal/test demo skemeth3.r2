scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_1023]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes36]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Camp des guardes]],  
      Season = [[Spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint01]]
    }
  },  
  InstanceId = [[Client1_1014]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_1012]]
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_1013]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    Road = 0,  
    TextManager = 0,  
    ActivityStep = 1,  
    TalkTo = 0,  
    NpcGrpFeature = 1,  
    EventType = 0,  
    Position = 0,  
    Location = 1,  
    Npc = 0,  
    LogicEntityBehavior = 1,  
    WayPoint = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1010]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_1017]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1015]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      LocationId = [[]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1030]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1031]],  
                x = 34996.92188,  
                y = -3816.03125,  
                z = -18.421875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1028]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1034]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1035]],  
                x = 34976.14063,  
                y = -3839.21875,  
                z = -18.53125
              },  
              Angle = 1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1032]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1038]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1039]],  
                x = 34999.92188,  
                y = -3741.59375,  
                z = -12.453125
              },  
              Angle = 2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1036]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1046]],  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1047]],  
                x = 34973.04688,  
                y = -3784.390625,  
                z = -16.265625
              },  
              Angle = 1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1044]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[bamboo I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1050]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1051]],  
                x = 34987.42188,  
                y = -3810.125,  
                z = -18.6875
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1048]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1054]],  
              Base = [[palette.entities.botobjects.chariot]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1055]],  
                x = 34992.23438,  
                y = -3817.328125,  
                z = -18.5625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1052]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1058]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1059]],  
                x = 34960.60938,  
                y = -3837.734375,  
                z = -19.015625
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1056]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1062]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1063]],  
                x = 34994.39063,  
                y = -3826.515625,  
                z = -18.453125
              },  
              Angle = 2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1060]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1066]],  
              Base = [[palette.entities.botobjects.tr_s2_lokness_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1067]],  
                x = 34944.64063,  
                y = -3756.453125,  
                z = -17.640625
              },  
              Angle = -1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1064]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[weeding I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1074]],  
              Base = [[palette.entities.botobjects.tr_s3_trumpet_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1075]],  
                x = 34999.20313,  
                y = -3765.080078,  
                z = -13.39690018
              },  
              Angle = -1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1072]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[trumperer III 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1082]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1083]],  
                x = 34955.07813,  
                y = -3809.625,  
                z = -18.328125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1080]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1086]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1088]],  
                x = 34980.01563,  
                y = -3832.671875,  
                z = -19.375
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1087]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1091]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1093]],  
                x = 34964.76563,  
                y = -3811,  
                z = -18.71875
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1092]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1096]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1097]],  
                x = 34991.21875,  
                y = -3768.28125,  
                z = -14.9375
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1094]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1100]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1101]],  
                x = 34981.95313,  
                y = -3766.59375,  
                z = -15.546875
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1098]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1104]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1105]],  
                x = 34922.9375,  
                y = -3765.53125,  
                z = -19.5
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1102]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1108]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1109]],  
                x = 34931.5625,  
                y = -3764.265625,  
                z = -19.5625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1106]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1116]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1117]],  
                x = 34968.90625,  
                y = -3817.390625,  
                z = -19.359375
              },  
              Angle = -1.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1114]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1120]],  
              Base = [[palette.entities.botobjects.barrels_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1121]],  
                x = 34992.34375,  
                y = -3813.65625,  
                z = -18.515625
              },  
              Angle = 2.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1118]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 barrels 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1223]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1222]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1225]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1226]],  
                    x = 35143.95313,  
                    y = -3770.799316,  
                    z = -20.19823074
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1228]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1229]],  
                    x = 35120.89063,  
                    y = -3775.5625,  
                    z = -16.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1231]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1232]],  
                    x = 35100.46875,  
                    y = -3788.078125,  
                    z = -17.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1234]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1235]],  
                    x = 35035.29688,  
                    y = -3788.734375,  
                    z = -19.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1237]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1238]],  
                    x = 34993.9375,  
                    y = -3807.28125,  
                    z = -18.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1240]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1241]],  
                    x = 34977.25,  
                    y = -3826.3125,  
                    z = -19.046875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              InstanceId = [[Client1_1317]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1318]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 4]],  
              Position = {
                y = -3809.625,  
                x = 34955.07813,  
                z = -18.33846855,  
                Class = [[Position]],  
                InstanceId = [[Client1_1319]]
              },  
              Angle = -0.6875,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Angle = 2.977645159,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1320]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35167.04297,  
                y = -3764.729736,  
                z = -18.66686058,  
                InstanceId = [[Client1_1323]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1322]],  
              Name = [[tryker tent 5]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Angle = 1.894459844,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1324]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35156.07422,  
                y = -3794.210205,  
                z = -18.19981956,  
                InstanceId = [[Client1_1327]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1326]],  
              Name = [[fyros tent 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Angle = 4.171336912,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1328]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35153.12891,  
                y = -3751.362305,  
                z = -19.68050957,  
                InstanceId = [[Client1_1331]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1330]],  
              Name = [[fyros tent 2]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -2.652626514,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1332]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35025.19531,  
                y = -3808.767578,  
                z = -17.08488083,  
                InstanceId = [[Client1_1335]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1334]],  
              Name = [[living wall 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.6105584502,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1336]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35022.57031,  
                y = -3804.463135,  
                z = -17.1546936,  
                InstanceId = [[Client1_1339]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1338]],  
              Name = [[living wall 2]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_gateway]],  
              Angle = -2.572207689,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1344]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35019.24609,  
                y = -3795.92041,  
                z = -17.06139946,  
                InstanceId = [[Client1_1347]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1346]],  
              Name = [[living gateway 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -2.896873236,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1348]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35018.89844,  
                y = -3786.281006,  
                z = -16.70450592,  
                InstanceId = [[Client1_1351]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1350]],  
              Name = [[living wall 3]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.2177351415,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1352]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35017.73828,  
                y = -3781.438232,  
                z = -16.18720245,  
                InstanceId = [[Client1_1355]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1354]],  
              Name = [[living wall 4]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -3.04682827,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1356]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35016.80469,  
                y = -3776.268311,  
                z = -15.53130531,  
                InstanceId = [[Client1_1359]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1358]],  
              Name = [[living wall 5]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.1363579333,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1360]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35016.18359,  
                y = -3771.389404,  
                z = -14.98519993,  
                InstanceId = [[Client1_1363]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1362]],  
              Name = [[living wall 6]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -2.982825041,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1364]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35015.45703,  
                y = -3766.412109,  
                z = -15.04884624,  
                InstanceId = [[Client1_1367]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1366]],  
              Name = [[living wall 7]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.388359189,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1368]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35014.13672,  
                y = -3761.584717,  
                z = -14.98854733,  
                InstanceId = [[Client1_1371]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1370]],  
              Name = [[living wall 8]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -3.019643068,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1372]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35012.73438,  
                y = -3756.596191,  
                z = -14.02282333,  
                InstanceId = [[Client1_1375]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1374]],  
              Name = [[living wall 9]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.1315386444,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1376]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35012.05859,  
                y = -3751.635254,  
                z = -13.80544567,  
                InstanceId = [[Client1_1379]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1378]],  
              Name = [[living wall 10]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 3.08411026,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1380]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35011.89063,  
                y = -3746.658447,  
                z = -14.02262878,  
                InstanceId = [[Client1_1383]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1382]],  
              Name = [[living wall 11]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.2241114378,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1384]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35011.44922,  
                y = -3741.747314,  
                z = -13.64819622,  
                InstanceId = [[Client1_1387]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1386]],  
              Name = [[living wall 12]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -2.787068367,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1388]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35010.0625,  
                y = -3736.930176,  
                z = -13.52868557,  
                InstanceId = [[Client1_1391]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1390]],  
              Name = [[living wall 13]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.8135742545,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1392]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35007.33203,  
                y = -3732.879395,  
                z = -13.46486282,  
                InstanceId = [[Client1_1395]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1394]],  
              Name = [[living wall 14]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -1.91361022,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1396]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35002.97266,  
                y = -3730.312988,  
                z = -13.15917397,  
                InstanceId = [[Client1_1399]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1398]],  
              Name = [[living wall 15]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 1.543002129,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1400]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34998.15234,  
                y = -3729.484863,  
                z = -13.25385571,  
                InstanceId = [[Client1_1403]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1402]],  
              Name = [[living wall 16]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_gateway]],  
              Angle = 1.551400781,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1404]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34982.62109,  
                y = -3729.087646,  
                z = -15.61695957,  
                InstanceId = [[Client1_1407]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1406]],  
              Name = [[living gateway 2]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -1.671750665,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1408]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34970.63672,  
                y = -3727.946533,  
                z = -13.63398266,  
                InstanceId = [[Client1_1411]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1410]],  
              Name = [[living wall 17]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 1.467777491,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1412]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34965.55078,  
                y = -3727.445068,  
                z = -12.83368969,  
                InstanceId = [[Client1_1415]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1414]],  
              Name = [[living wall 18]]
            },  
            {
              InstanceId = [[Client1_1418]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1419]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 5]],  
              Position = {
                y = -3729.324951,  
                x = 34989.96875,  
                z = -15.38122749,  
                Class = [[Position]],  
                InstanceId = [[Client1_1420]]
              },  
              Angle = -1.515625,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1423]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1424]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 6]],  
              Position = {
                y = -3728.686768,  
                x = 34976.20703,  
                z = -14.97656631,  
                Class = [[Position]],  
                InstanceId = [[Client1_1425]]
              },  
              Angle = -1.515625,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tr_s2_palmtree_f]],  
              Angle = 0.01745329252,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1430]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34961.44141,  
                y = -3727.133789,  
                z = -15.47463036,  
                InstanceId = [[Client1_1433]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1432]],  
              Name = [[olansi VI 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 4.677482395,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1434]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34957.18359,  
                y = -3726.963867,  
                z = -17.00458527,  
                InstanceId = [[Client1_1437]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1436]],  
              Name = [[living wall 19]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 1.572774529,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1438]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34952.14844,  
                y = -3726.890869,  
                z = -17.47514153,  
                InstanceId = [[Client1_1441]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1440]],  
              Name = [[living wall 20]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -1.578410745,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1442]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34947.0625,  
                y = -3726.729492,  
                z = -18.40556526,  
                InstanceId = [[Client1_1445]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1444]],  
              Name = [[living wall 21]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 1.564774275,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1446]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34942.0625,  
                y = -3726.77124,  
                z = -18.74781799,  
                InstanceId = [[Client1_1449]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1448]],  
              Name = [[living wall 22]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -1.543913484,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1450]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34936.89063,  
                y = -3726.799316,  
                z = -19.09365654,  
                InstanceId = [[Client1_1453]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1452]],  
              Name = [[living wall 23]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 1.319271564,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1454]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34931.92969,  
                y = -3726.226563,  
                z = -19.15477753,  
                InstanceId = [[Client1_1457]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1456]],  
              Name = [[living wall 24]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -1.97191453,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1458]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34926.94922,  
                y = -3724.535889,  
                z = -18.98351097,  
                InstanceId = [[Client1_1461]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1460]],  
              Name = [[living wall 25]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 1.090597153,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1462]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34922.38281,  
                y = -3722.430664,  
                z = -18.73413467,  
                InstanceId = [[Client1_1465]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1464]],  
              Name = [[living wall 26]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -2.116146564,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1466]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34917.79297,  
                y = -3719.946289,  
                z = -18.3273201,  
                InstanceId = [[Client1_1469]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1468]],  
              Name = [[living wall 27]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.9715842009,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1470]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34913.68359,  
                y = -3717.352051,  
                z = -18.07363892,  
                InstanceId = [[Client1_1473]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1472]],  
              Name = [[living wall 28]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -2.287595272,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1474]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34909.55078,  
                y = -3714.209473,  
                z = -17.97758865,  
                InstanceId = [[Client1_1477]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1476]],  
              Name = [[living wall 29]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = 0.8435788751,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1478]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34905.88672,  
                y = -3710.873047,  
                z = -17.4517746,  
                InstanceId = [[Client1_1481]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1480]],  
              Name = [[living wall 30]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Angle = -1.748724461,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1482]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34901.53125,  
                y = -3708.752686,  
                z = -16.80850601,  
                InstanceId = [[Client1_1485]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1484]],  
              Name = [[living wall 31]]
            },  
            {
              InstanceId = [[Client1_1488]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1489]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bamboo I 2]],  
              Position = {
                y = -3776.668945,  
                x = 34999.23438,  
                z = -14.71344852,  
                Class = [[Position]],  
                InstanceId = [[Client1_1490]]
              },  
              Angle = 1.71875,  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_1018]],  
        }
      },  
      Version = 5,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1016]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Permanent]]
    },  
    {
      InstanceId = [[Client1_1021]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1019]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      LocationId = [[Client1_1023]],  
      ManualWeather = 1,  
      WeatherValue = 78,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1042]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1043]],  
                x = 34968.17188,  
                y = -3744.34375,  
                z = -12.328125
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1040]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1145]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1146]],  
                x = 34977.01563,  
                y = -3831.953125,  
                z = -19.484375
              },  
              Angle = 1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1143]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1148]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1149]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 7,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 25,  
              EyesColor = 6,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 3,  
              MorphTarget4 = 7,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6721070,  
              HandsModel = 6721326,  
              ArmModel = 6723118,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 5,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6766638,  
              Level = 0,  
              Name = [[Rosira]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1168]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1169]],  
                x = 35147.80078,  
                y = -3772.068848,  
                z = -19.82018089
              },  
              Angle = 3.438298626,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1166]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1262]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1263]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1264]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1265]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1223]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 29,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              Level = 0,  
              Name = [[Kridix]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_1022]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1132]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1124]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1125]],  
                x = 34971.8125,  
                y = -3830.359375,  
                z = -20.046875
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1122]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 6723630,  
              TrouserModel = 6722350,  
              FeetModel = 6721070,  
              HandsModel = 6721582,  
              ArmModel = 6723118,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 1,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6765870,  
              Level = 0,  
              Name = [[Ba'Massey]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1128]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1129]],  
                x = 34974,  
                y = -3832.578125,  
                z = -19.53125
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1126]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 6721838,  
              HairColor = 3,  
              Tattoo = 15,  
              EyesColor = 3,  
              MorphTarget1 = 1,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721582,  
              ArmModel = 6722862,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6765614,  
              Level = 0,  
              Name = [[Be'Daghan]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1135]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1137]],  
                x = 34971.32813,  
                y = -3833.703125,  
                z = -19.421875
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1133]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 12,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 7,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721326,  
              ArmModel = 6722862,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 6765614,  
              Level = 0,  
              Name = [[Be'Gany]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1140]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1142]],  
                x = 34969,  
                y = -3832.09375,  
                z = -19.796875
              },  
              Angle = 1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1138]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 1,  
              HairType = 6721838,  
              HairColor = 1,  
              Tattoo = 30,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721582,  
              ArmModel = 6722862,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponLeftHand = 0,  
              Sex = 0,  
              WeaponRightHand = 6765870,  
              Level = 0,  
              Name = [[Mac'Jorn]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1131]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1130]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          MissionText = [[Find <mission_target> for me]],  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1187]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1188]],  
                  Type = [[desactivation]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            InstanceId = [[Client1_1157]]
          },  
          WaitValidationText = [[Did <mission_giver> ask you to talk to me?]],  
          Base = [[palette.entities.botobjects.bot_chat]],  
          Repeatable = 0,  
          ContextualText = [[Talk to <mission_target>]],  
          MissionGiver = r2.RefId([[Client1_1145]]),  
          MissionTarget = r2.RefId([[Client1_1168]]),  
          InheritPos = 1,  
          MissionSucceedText = [[Yes I am <mission_target>]],  
          Name = [[Mission: Talk to 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1158]],  
            x = 34976.25,  
            y = -3830.890625,  
            z = -19
          },  
          Class = [[TalkTo]],  
          InstanceId = [[Client1_1156]],  
          Components = {
          },  
          _Seed = 1146745385
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1199]],  
          Name = [[Group 2]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1191]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1192]],  
                x = 35168.125,  
                y = -3731.796875,  
                z = -14.84375
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1189]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1258]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1259]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1260]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1261]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1223]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 14,  
              HairType = 3118,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 1,  
              MorphTarget1 = 7,  
              MorphTarget2 = 3,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              Level = 0,  
              Name = [[Apothus]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1195]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1196]],  
                x = 35169.35938,  
                y = -3730.140625,  
                z = -15.03125
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1193]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 5604398,  
              HairColor = 0,  
              Tattoo = 30,  
              EyesColor = 1,  
              MorphTarget1 = 5,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 4,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Dekos]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1202]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1204]],  
                x = 35170.03125,  
                y = -3732.6875,  
                z = -14.4375
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1200]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 12,  
              HairType = 2606,  
              HairColor = 4,  
              Tattoo = 5,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              MorphTarget7 = 2,  
              MorphTarget8 = 5,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              WeaponRightHand = 5635886,  
              Level = 0,  
              Name = [[Kyron]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Angle = -2.585479021,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1266]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35171.58203,  
                y = -3730.581787,  
                z = -14.74566174,  
                InstanceId = [[Client1_1270]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1268]],  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 14,  
              HairColor = 1,  
              Tattoo = 18,  
              EyesColor = 3,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              Sex = 1,  
              HairType = 5621550,  
              JacketModel = 5607726,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              JacketColor = 4,  
              TrouserColor = 3,  
              FeetColor = 4,  
              HandsColor = 3,  
              ArmColor = 4,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Name = [[]]
            },  
            {
              Class = [[NpcCustom]],  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Angle = -2.585479021,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1271]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 35167.07031,  
                y = -3728.656982,  
                z = -15.11965466,  
                InstanceId = [[Client1_1275]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1273]],  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 6,  
              HairColor = 0,  
              Tattoo = 29,  
              EyesColor = 5,  
              MorphTarget1 = 5,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              Sex = 0,  
              HairType = 5604398,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              JacketColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              HandsColor = 0,  
              ArmColor = 0,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Name = [[Aktius]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1198]],  
            x = -17.4609375,  
            y = -35.88891602,  
            z = -4.963109016
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1197]],  
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_1278]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 3]],  
          Position = {
            y = -3773.940918,  
            x = 35155.66797,  
            z = -18.77687645,  
            Class = [[Position]],  
            InstanceId = [[Client1_1279]]
          },  
          Components = {
            {
              MorphTarget8 = 0,  
              MorphTarget3 = 4,  
              ActivitiesId = {
              },  
              Tattoo = 3,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget7 = 4,  
              GabaritArmsWidth = 12,  
              HandsModel = 5604142,  
              FeetColor = 5,  
              GabaritBreastSize = 14,  
              GabaritHeight = 8,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 2,  
              HandsColor = 5,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1283]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1284]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1285]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1286]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1287]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1223]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 5603886,  
              Speed = 1,  
              Angle = 3.001966313,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              MorphTarget6 = 6,  
              ArmModel = 5604910,  
              ArmColor = 5,  
              HairType = 3118,  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              WeaponRightHand = 5595694,  
              Name = [[]],  
              Position = {
                y = -1.024121094,  
                x = -1.10859375,  
                z = -0.008186721802,  
                Class = [[Position]],  
                InstanceId = [[Client1_1288]]
              },  
              Sex = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              InstanceId = [[Client1_1282]],  
              Level = 0
            },  
            {
              MorphTarget8 = 4,  
              MorphTarget3 = 4,  
              ActivitiesId = {
              },  
              Tattoo = 30,  
              TrouserColor = 0,  
              MorphTarget5 = 7,  
              MorphTarget7 = 3,  
              GabaritArmsWidth = 10,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 5,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 2,  
              HandsColor = 0,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1292]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 1,  
              FeetModel = 5603886,  
              Speed = 1,  
              Angle = 3.368485456,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              MorphTarget6 = 3,  
              ArmModel = 5604910,  
              ArmColor = 0,  
              HairType = 5604398,  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              WeaponRightHand = 5595950,  
              Name = [[]],  
              Position = {
                y = 0.6079589725,  
                x = 1.051562503,  
                z = 0.1691398621,  
                Class = [[Position]],  
                InstanceId = [[Client1_1293]]
              },  
              Sex = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              InstanceId = [[Client1_1291]],  
              Level = 0
            },  
            {
              MorphTarget8 = 5,  
              MorphTarget3 = 6,  
              ActivitiesId = {
              },  
              Tattoo = 5,  
              TrouserColor = 3,  
              MorphTarget5 = 3,  
              MorphTarget7 = 2,  
              GabaritArmsWidth = 11,  
              HandsModel = 5606958,  
              FeetColor = 3,  
              GabaritBreastSize = 12,  
              GabaritHeight = 4,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 3,  
              HandsColor = 0,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1297]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 5603886,  
              Speed = 1,  
              Angle = 2.809980096,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              MorphTarget6 = 0,  
              ArmModel = 5604910,  
              ArmColor = 3,  
              HairType = 2606,  
              JacketModel = 5607726,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              InheritPos = 1,  
              WeaponRightHand = 5635886,  
              Name = [[]],  
              Position = {
                y = -3.26801753,  
                x = -1.842968762,  
                z = -0.03813171387,  
                Class = [[Position]],  
                InstanceId = [[Client1_1298]]
              },  
              Sex = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              InstanceId = [[Client1_1296]],  
              Level = 0
            },  
            {
              MorphTarget8 = 1,  
              MorphTarget3 = 7,  
              ActivitiesId = {
              },  
              Tattoo = 18,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 6,  
              HandsModel = 5604142,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 6,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 4,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1302]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 5606702,  
              Speed = 0,  
              Angle = 3.281218994,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              MorphTarget7 = 0,  
              Sex = 1,  
              ArmColor = 4,  
              ArmModel = 5607470,  
              JacketModel = 5607726,  
              WeaponRightHand = 5595694,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[]],  
              Position = {
                y = -2.267041013,  
                x = 1.297656298,  
                z = 0.3629570007,  
                Class = [[Position]],  
                InstanceId = [[Client1_1303]]
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              HairType = 5621550,  
              InstanceId = [[Client1_1301]],  
              Level = 0
            },  
            {
              MorphTarget8 = 7,  
              MorphTarget3 = 4,  
              ActivitiesId = {
              },  
              Tattoo = 29,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 9,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 6,  
              GabaritHeight = 9,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 2,  
              HandsColor = 0,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1307]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 5603886,  
              Speed = 0,  
              Angle = 4.328416545,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              MorphTarget7 = 6,  
              Sex = 0,  
              ArmColor = 0,  
              ArmModel = 5604910,  
              JacketModel = 5605166,  
              WeaponRightHand = 5595694,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              WeaponLeftHand = 0,  
              Name = [[]],  
              Position = {
                y = 1.843310595,  
                x = -0.3468749523,  
                z = -0.1857242584,  
                Class = [[Position]],  
                InstanceId = [[Client1_1308]]
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              HairType = 5604398,  
              InstanceId = [[Client1_1306]],  
              Level = 0
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            InstanceId = [[Client1_1309]],  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          }
        }
      },  
      Version = 5,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1020]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Act 1:Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1155]],  
        Count = 1,  
        Text = [[hey mec, va nous cherchez des potes!]]
      }
    },  
    InstanceId = [[Client1_1011]]
  }
}