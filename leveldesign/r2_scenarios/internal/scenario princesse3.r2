scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts09]],  
      Time = 0,  
      Name = [[Two Ponds (Desert 09)]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    },  
    {
      InstanceId = [[Client1_2001]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts09]],  
      Time = 0,  
      Name = [[Two Pond (Desert 09)]],  
      Season = [[fall]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2NorthEastEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_3]]
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_4]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    TextManager = 0,  
    ChatAction = 0,  
    DefaultFeature = 0,  
    ChatStep = 0,  
    ChatSequence = 0,  
    ActionType = 0,  
    Region = 0,  
    ActivityStep = 1,  
    ActionStep = 0,  
    ZoneTrigger = 1,  
    Npc = 0,  
    RegionVertex = 0,  
    EventType = 0,  
    WayPoint = 0,  
    Position = 0,  
    Location = 0,  
    NpcCreature = 0,  
    LogicEntityBehavior = 1,  
    Road = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 3,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_8]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_6]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 0,  
      LocationId = [[]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_670]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_671]],  
                x = 27691.98438,  
                y = -1241.953125,  
                z = 77.25
              },  
              Angle = 3.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_668]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_674]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_675]],  
                x = 27696.48438,  
                y = -1249.90625,  
                z = 78.953125
              },  
              Angle = 3.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_672]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_678]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_679]],  
                x = 27692.45313,  
                y = -1258.5,  
                z = 76.984375
              },  
              Angle = 2.546875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_676]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_682]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_683]],  
                x = 27677.85938,  
                y = -1195.71875,  
                z = 75.015625
              },  
              Angle = -1.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_680]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_686]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_687]],  
                x = 27686.6875,  
                y = -1205.015625,  
                z = 75.234375
              },  
              Angle = -1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_684]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_690]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_691]],  
                x = 27680.95313,  
                y = -1221.125,  
                z = 75.03125
              },  
              Angle = -3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_688]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_694]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_695]],  
                x = 27668.79688,  
                y = -1216.40625,  
                z = 74.640625
              },  
              Angle = 0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_692]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_698]],  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_699]],  
                x = 27642.25,  
                y = -1253.765625,  
                z = 74.78125
              },  
              Angle = 1.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_696]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[zorai tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_702]],  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_703]],  
                x = 27625.70313,  
                y = -1229.0625,  
                z = 75.546875
              },  
              Angle = -0.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_700]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[zorai tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_706]],  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_707]],  
                x = 27646.46875,  
                y = -1223.859375,  
                z = 75.84375
              },  
              Angle = -1.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_704]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[zorai tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_714]],  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_715]],  
                x = 27629.10938,  
                y = -1244.234375,  
                z = 75
              },  
              Angle = 0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_712]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[zorai tent 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_718]],  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_719]],  
                x = 27652.34375,  
                y = -1220.84375,  
                z = 75
              },  
              Angle = 5.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_716]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_722]],  
              Base = [[palette.entities.botobjects.tent_matis]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_723]],  
                x = 27646.89063,  
                y = -1268.953125,  
                z = 74.78125
              },  
              Angle = -6.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_720]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[matis tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_726]],  
              Base = [[palette.entities.botobjects.tent_matis]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_727]],  
                x = 27653.59375,  
                y = -1285.875,  
                z = 74.734375
              },  
              Angle = -5.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_724]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[matis tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_730]],  
              Base = [[palette.entities.botobjects.tent_matis]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_731]],  
                x = 27667.4375,  
                y = -1286.21875,  
                z = 74.890625
              },  
              Angle = 0.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_728]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[matis tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_734]],  
              Base = [[palette.entities.botobjects.tent_matis]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_735]],  
                x = 27628.9375,  
                y = -1285.71875,  
                z = 74.671875
              },  
              Angle = 0.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_732]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[matis tent 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_738]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_739]],  
                x = 27660.78125,  
                y = -1238.15625,  
                z = 75.328125
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_736]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_742]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_743]],  
                x = 27646.09375,  
                y = -1250.453125,  
                z = 75.0625
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_740]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_746]],  
              Base = [[palette.entities.botobjects.chariot]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_747]],  
                x = 27671.96875,  
                y = -1250.3125,  
                z = 74.90625
              },  
              Angle = 2.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_744]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_750]],  
              Base = [[palette.entities.botobjects.crate1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_751]],  
                x = 27688.5625,  
                y = -1254.125,  
                z = 76.265625
              },  
              Angle = 2.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_748]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[crate 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_754]],  
              Base = [[palette.entities.botobjects.crate2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_755]],  
                x = 27687,  
                y = -1241.75,  
                z = 75.875
              },  
              Angle = 2.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_752]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 crates 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_758]],  
              Base = [[palette.entities.botobjects.barrels_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_759]],  
                x = 27662.625,  
                y = -1278.515625,  
                z = 74.984375
              },  
              Angle = 2.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_756]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 barrels 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_762]],  
              Base = [[palette.entities.botobjects.counter]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_763]],  
                x = 27655.67188,  
                y = -1265.640625,  
                z = 74.609375
              },  
              Angle = 0.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_760]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[counter 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_766]],  
              Base = [[palette.entities.botobjects.counter]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_767]],  
                x = 27683.5625,  
                y = -1246,  
                z = 75.265625
              },  
              Angle = 3.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_764]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[counter 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_770]],  
              Base = [[palette.entities.botobjects.counter]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_771]],  
                x = 27660.03125,  
                y = -1223.5,  
                z = 74.859375
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_768]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[counter 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_774]],  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_775]],  
                x = 27661.45313,  
                y = -1240.625,  
                z = 75.265625
              },  
              Angle = 2.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_772]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_778]],  
              Base = [[palette.entities.botobjects.chest_old]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_779]],  
                x = 27645.96875,  
                y = -1228.375,  
                z = 76.171875
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_776]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[old chest 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_782]],  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_783]],  
                x = 27686.65625,  
                y = -1238.953125,  
                z = 75.765625
              },  
              Angle = 3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_780]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp off 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_786]],  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_787]],  
                x = 27674.5625,  
                y = -1220.046875,  
                z = 74.9375
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_784]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp off 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_790]],  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_791]],  
                x = 27659.67188,  
                y = -1273.25,  
                z = 74.875
              },  
              Angle = 1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_788]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp off 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_794]],  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_795]],  
                x = 27638.26563,  
                y = -1248.203125,  
                z = 75.09375
              },  
              Angle = 1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_792]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp off 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_798]],  
              Base = [[palette.entities.botobjects.chariot]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_799]],  
                x = 27654.15625,  
                y = -1225.34375,  
                z = 75.234375
              },  
              Angle = 0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_796]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[chariot 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_802]],  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_803]],  
                x = 27681.14063,  
                y = -1230.203125,  
                z = 75.03125
              },  
              Angle = -2.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_800]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[cosmetics tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_806]],  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_807]],  
                x = 27683.21875,  
                y = -1235.28125,  
                z = 75.171875
              },  
              Angle = -2.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_804]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[cosmetics tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_810]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_811]],  
                x = 27673.875,  
                y = -1256.96875,  
                z = 74.921875
              },  
              Angle = -4.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_808]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_814]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_815]],  
                x = 27608.39063,  
                y = -1197.53125,  
                z = 73.671875
              },  
              Angle = -0.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_812]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_818]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_819]],  
                x = 27613.01563,  
                y = -1278.390625,  
                z = 74.953125
              },  
              Angle = 0.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_816]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_822]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_823]],  
                x = 27710.57813,  
                y = -1230.46875,  
                z = 80.28125
              },  
              Angle = 3.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_820]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_826]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_827]],  
                x = 27722.3125,  
                y = -1243.953125,  
                z = 81.78125
              },  
              Angle = 2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_824]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_841]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_843]],  
                x = 27662.8125,  
                y = -1236.21875,  
                z = 75.265625
              },  
              Angle = 3.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_839]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 4,  
              HairType = 5623854,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 2,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              MorphTarget7 = 6,  
              MorphTarget8 = 3,  
              JacketModel = 0,  
              TrouserModel = 5618222,  
              FeetModel = 5617710,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Shuai-Chon]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_834]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_835]],  
                x = 27660.25,  
                y = -1240.0625,  
                z = 75.3125
              },  
              Angle = 1.703125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_832]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 5,  
              HairType = 6702,  
              HairColor = 4,  
              Tattoo = 2,  
              EyesColor = 6,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 2,  
              MorphTarget4 = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 5653550,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ba'Mba]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_830]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_831]],  
                x = 27664.07813,  
                y = -1239.984375,  
                z = 75.203125
              },  
              Angle = 2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_828]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 11,  
              HairType = 2350,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 6,  
              MorphTarget1 = 3,  
              MorphTarget2 = 2,  
              MorphTarget3 = 4,  
              MorphTarget4 = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 2,  
              MorphTarget8 = 6,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Layla]],  
              Sex = 0,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_921]],  
              Base = [[palette.entities.botobjects.fx_ju_bugsa]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_922]],  
                x = 27638.95313,  
                y = -1244.515625,  
                z = 75.640625
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_919]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[butterflies 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_961]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_962]],  
                x = 27662.96875,  
                y = -1230.46875,  
                z = 75.140625
              },  
              Angle = -2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_959]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_995]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_996]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_964]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 1,  
              HairType = 2606,  
              HairColor = 5,  
              Tattoo = 2,  
              EyesColor = 6,  
              MorphTarget1 = 1,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 6,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 1,  
              FeetColor = 2,  
              Aggro = 15,  
              Speed = 0,  
              WeaponRightHand = 5595182,  
              WeaponLeftHand = 0,  
              Name = [[Xakos]],  
              Sex = 0,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_964]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_963]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_966]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_967]],  
                    x = 27655.75,  
                    y = -1229.578125,  
                    z = 75.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_969]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_970]],  
                    x = 27648.03125,  
                    y = -1231.765625,  
                    z = 76.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_972]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_973]],  
                    x = 27645.14063,  
                    y = -1238.703125,  
                    z = 76.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_975]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_976]],  
                    x = 27651.67188,  
                    y = -1249.015625,  
                    z = 74.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_978]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_979]],  
                    x = 27665.73438,  
                    y = -1251.21875,  
                    z = 74.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_981]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_982]],  
                    x = 27676.53125,  
                    y = -1243.421875,  
                    z = 75.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_984]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_985]],  
                    x = 27675.84375,  
                    y = -1229.859375,  
                    z = 75.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_987]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_988]],  
                    x = 27667.45313,  
                    y = -1226.015625,  
                    z = 74.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_990]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_991]],  
                    x = 27655.75,  
                    y = -1229.625,  
                    z = 75.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_993]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_994]],  
                    x = 27655.75,  
                    y = -1229.578125,  
                    z = 75.390625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_999]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1000]],  
                x = 27642.6875,  
                y = -1230.71875,  
                z = 76.390625
              },  
              Angle = -2.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_997]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1031]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1032]],  
                        Type = [[None]],  
                        TimeLimitValue = [[14]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1006]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1056]],  
                        Type = [[None]],  
                        TimeLimitValue = [[25]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1036]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 5,  
              HairType = 3118,  
              HairColor = 2,  
              Tattoo = 7,  
              EyesColor = 1,  
              MorphTarget1 = 1,  
              MorphTarget2 = 0,  
              MorphTarget3 = 3,  
              MorphTarget4 = 0,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 0,  
              MorphTarget8 = 2,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 5,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponRightHand = 5635886,  
              WeaponLeftHand = 0,  
              Name = [[Melus]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1003]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1004]],  
                x = 27640.98438,  
                y = -1236.734375,  
                z = 76.3125
              },  
              Angle = -2.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1001]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1033]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1034]],  
                        Type = [[None]],  
                        TimeLimitValue = [[14]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1006]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1055]],  
                        Type = [[None]],  
                        TimeLimitValue = [[25]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1036]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 12,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 7,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 6,  
              MorphTarget3 = 5,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 5,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponRightHand = 5635886,  
              WeaponLeftHand = 0,  
              Name = [[Meros]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1006]],  
              Name = [[Place 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1005]],  
                x = -0.0625,  
                y = -0.171875,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1008]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1009]],  
                    x = 27679.875,  
                    y = -1240.75,  
                    z = 75
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1011]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1012]],  
                    x = 27676.9375,  
                    y = -1225.8125,  
                    z = 74.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1014]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1015]],  
                    x = 27671.32813,  
                    y = -1221.125,  
                    z = 74.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1017]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1018]],  
                    x = 27643.5,  
                    y = -1228,  
                    z = 76.3125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1020]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1021]],  
                    x = 27638.46875,  
                    y = -1235.109375,  
                    z = 76.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1023]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1024]],  
                    x = 27640.67188,  
                    y = -1244.1875,  
                    z = 75.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1026]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1027]],  
                    x = 27653.32813,  
                    y = -1254.46875,  
                    z = 74.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1029]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1030]],  
                    x = 27669.10938,  
                    y = -1253.875,  
                    z = 74.8125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1036]],  
              Name = [[Route 2]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1035]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1038]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1039]],  
                    x = 27658.42188,  
                    y = -1244.578125,  
                    z = 75.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1041]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1042]],  
                    x = 27655.0625,  
                    y = -1242.328125,  
                    z = 75.421875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1044]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1045]],  
                    x = 27654.65625,  
                    y = -1238.859375,  
                    z = 75.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1047]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1048]],  
                    x = 27657.65625,  
                    y = -1233.96875,  
                    z = 75.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1050]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1051]],  
                    x = 27662.59375,  
                    y = -1233.140625,  
                    z = 75.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1053]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1054]],  
                    x = 27659.39063,  
                    y = -1233.703125,  
                    z = 75.375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1077]],  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1078]],  
                x = 27753.85938,  
                y = -1071.59375,  
                z = 74.34375
              },  
              Angle = -2.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1075]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bats 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1081]],  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1082]],  
                x = 27715.20313,  
                y = -1105.234375,  
                z = 75.125
              },  
              Angle = -2.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1079]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bats 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1085]],  
              Base = [[palette.entities.botobjects.fx_ju_bata]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1086]],  
                x = 27759.09375,  
                y = -1120.625,  
                z = 74.953125
              },  
              Angle = -2.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1083]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bats 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1097]],  
              Base = [[palette.entities.botobjects.ruin_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1098]],  
                x = 27730.85938,  
                y = -1104.5625,  
                z = 74.578125
              },  
              Angle = -2.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1095]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[wall ruin 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1101]],  
              Base = [[palette.entities.botobjects.ruin_wall_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1102]],  
                x = 27744.67188,  
                y = -1063.328125,  
                z = 74.796875
              },  
              Angle = -2.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1099]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[wall ruin 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1120]],  
              Name = [[Place 2]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1119]],  
                x = -2.34375,  
                y = -12.4375,  
                z = -0.875
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1122]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1123]],  
                    x = 27737.60938,  
                    y = -1113.84375,  
                    z = 74.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1125]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1126]],  
                    x = 27729.76563,  
                    y = -1111.6875,  
                    z = 74.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1128]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1129]],  
                    x = 27722.28125,  
                    y = -1115.234375,  
                    z = 74.84375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1131]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1132]],  
                    x = 27720.21875,  
                    y = -1121.078125,  
                    z = 75.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1134]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1135]],  
                    x = 27722.3125,  
                    y = -1136.875,  
                    z = 75.65625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1137]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1138]],  
                    x = 27736.78125,  
                    y = -1141.953125,  
                    z = 72.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1140]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1141]],  
                    x = 27747.57813,  
                    y = -1134.890625,  
                    z = 73.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1143]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1144]],  
                    x = 27752.875,  
                    y = -1122.203125,  
                    z = 74.78125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1194]],  
              Name = [[Place 3]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1193]],  
                x = 11.640625,  
                y = -14.78125,  
                z = 1.140625
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1196]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1197]],  
                    x = 27759.45313,  
                    y = -1071.046875,  
                    z = 74.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1199]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1200]],  
                    x = 27745.28125,  
                    y = -1062.953125,  
                    z = 74.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1202]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1203]],  
                    x = 27735.5,  
                    y = -1058.125,  
                    z = 74.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1205]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1206]],  
                    x = 27736.9375,  
                    y = -1081.390625,  
                    z = 71.84375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1208]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1209]],  
                    x = 27734.1875,  
                    y = -1092.796875,  
                    z = 73.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1211]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1212]],  
                    x = 27730.8125,  
                    y = -1103.9375,  
                    z = 74.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1214]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1215]],  
                    x = 27739.1875,  
                    y = -1103.65625,  
                    z = 74.359375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1217]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1218]],  
                    x = 27749.96875,  
                    y = -1100.796875,  
                    z = 74.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1220]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1221]],  
                    x = 27762.54688,  
                    y = -1089.359375,  
                    z = 74.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1223]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1224]],  
                    x = 27764.04688,  
                    y = -1082.5625,  
                    z = 74.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1226]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1227]],  
                    x = 27761.35938,  
                    y = -1071.3125,  
                    z = 74.203125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1239]],  
              Name = [[Place 4]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1241]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1242]],  
                    x = 27759.875,  
                    y = -1122.359375,  
                    z = 74.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1244]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1245]],  
                    x = 27760.51563,  
                    y = -1130.578125,  
                    z = 73.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1247]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1248]],  
                    x = 27778.60938,  
                    y = -1131.640625,  
                    z = 72.421875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1250]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1251]],  
                    x = 27791.625,  
                    y = -1141.625,  
                    z = 76.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1253]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1254]],  
                    x = 27792.40625,  
                    y = -1126.46875,  
                    z = 75.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1256]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1257]],  
                    x = 27776.75,  
                    y = -1116.515625,  
                    z = 75.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1259]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1260]],  
                    x = 27760.01563,  
                    y = -1122.25,  
                    z = 74.796875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1238]],  
                x = -0.8125,  
                y = 0.84375,  
                z = 0.046875
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1275]],  
              Name = [[Place 5]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1277]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1278]],  
                    x = 27811,  
                    y = -1170.6875,  
                    z = 76.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1280]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1281]],  
                    x = 27805.32813,  
                    y = -1159.59375,  
                    z = 79.90625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1283]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1284]],  
                    x = 27797.48438,  
                    y = -1147.671875,  
                    z = 79.3125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1286]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1287]],  
                    x = 27791.46875,  
                    y = -1146.359375,  
                    z = 76.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1289]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1290]],  
                    x = 27787.45313,  
                    y = -1151.796875,  
                    z = 74.84375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1292]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1293]],  
                    x = 27787.1875,  
                    y = -1159.96875,  
                    z = 75.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1295]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1296]],  
                    x = 27790.625,  
                    y = -1166.234375,  
                    z = 76.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1298]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1299]],  
                    x = 27796.85938,  
                    y = -1177.84375,  
                    z = 75.375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1301]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1302]],  
                    x = 27804.34375,  
                    y = -1182.015625,  
                    z = 76.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1304]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1305]],  
                    x = 27812.48438,  
                    y = -1183.90625,  
                    z = 74.78125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1274]],  
                x = -0.046875,  
                y = 2.75,  
                z = -0.5
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1345]],  
              Name = [[Place 6]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1347]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1348]],  
                    x = 27844.03125,  
                    y = -1208.578125,  
                    z = 75.28125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1350]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1351]],  
                    x = 27834.14063,  
                    y = -1192.421875,  
                    z = 74.625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1353]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1354]],  
                    x = 27835.54688,  
                    y = -1183.046875,  
                    z = 74.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1356]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1357]],  
                    x = 27849.32813,  
                    y = -1179.4375,  
                    z = 75.203125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1359]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1360]],  
                    x = 27860.03125,  
                    y = -1179.5625,  
                    z = 75.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1362]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1363]],  
                    x = 27862.8125,  
                    y = -1193.078125,  
                    z = 76.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1365]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1366]],  
                    x = 27852.70313,  
                    y = -1207.96875,  
                    z = 76.875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1344]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1378]],  
              Base = [[palette.entities.npcs.civils.m_civil_70]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1379]],  
                x = 27862.5,  
                y = -1107.484375,  
                z = 73.59375
              },  
              Angle = -1.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1376]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1448]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1449]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1451]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1452]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 3,  
              HairType = 4910,  
              HairColor = 2,  
              Tattoo = 3,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 7,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              MorphTarget7 = 7,  
              MorphTarget8 = 6,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 5609774,  
              ArmModel = 6714670,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Faneliah]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_c2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1588]],  
              Name = [[Route 3]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1587]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1590]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1591]],  
                    x = 27849.46875,  
                    y = -1097.3125,  
                    z = 74.65625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1593]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1594]],  
                    x = 27860.8125,  
                    y = -1104.328125,  
                    z = 74.34375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1808]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1809]],  
                x = 27843.20313,  
                y = -1095.078125,  
                z = 73.578125
              },  
              Angle = -1.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1806]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1857]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1855]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1927]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1928]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1194]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Assault Kirosta]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1858]],  
                x = 27760.28125,  
                y = -1097.21875,  
                z = 74.296875
              },  
              Angle = -0.3505425453,  
              Base = [[palette.entities.creatures.ckfrb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1861]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1859]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1929]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1930]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1194]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Assault Kirosta]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1862]],  
                x = 27771.15625,  
                y = -1097.203125,  
                z = 74.90625
              },  
              Angle = -0.3505425453,  
              Base = [[palette.entities.creatures.ckfrb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1865]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1863]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1925]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1926]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1194]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kipesta]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1866]],  
                x = 27762.23438,  
                y = -1107.265625,  
                z = 74.90625
              },  
              Angle = -0.3505425453,  
              Base = [[palette.entities.creatures.ckjib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1869]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1867]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1923]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1924]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1120]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Trooper Kipee]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1870]],  
                x = 27729.01563,  
                y = -1128.84375,  
                z = 74.5625
              },  
              Angle = 0.8529254794,  
              Base = [[palette.entities.creatures.ckhib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1873]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1871]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1919]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1920]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1120]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kipee]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1874]],  
                x = 27739.14063,  
                y = -1136.734375,  
                z = 73.125
              },  
              Angle = 0.8529254794,  
              Base = [[palette.entities.creatures.ckhib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1877]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1875]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1921]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1922]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1120]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kipee]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1878]],  
                x = 27729.10938,  
                y = -1141.953125,  
                z = 74
              },  
              Angle = 0.8529254794,  
              Base = [[palette.entities.creatures.ckhib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1881]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1879]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1933]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1934]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1239]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1964]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1239]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Great Trooper Kizarak]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1882]],  
                x = 27781.39063,  
                y = -1127.9375,  
                z = 73.90625
              },  
              Angle = 2.418059587,  
              Base = [[palette.entities.creatures.ckcib3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1885]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1883]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1931]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1932]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1239]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1886]],  
                x = 27764.15625,  
                y = -1123.65625,  
                z = 74.5625
              },  
              Angle = 2.418059587,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1889]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1887]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1935]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1936]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1239]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Trooper Kizoar]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1890]],  
                x = 27770.875,  
                y = -1120.171875,  
                z = 74.984375
              },  
              Angle = 2.418059587,  
              Base = [[palette.entities.creatures.ckiib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1893]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1891]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1954]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1955]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1275]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Trooper Kipucker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1894]],  
                x = 27798.17188,  
                y = -1154.6875,  
                z = 79.90625
              },  
              Angle = 2.180424452,  
              Base = [[palette.entities.creatures.ckeib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1905]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1903]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1960]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1961]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1345]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Assault Kirosta]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1906]],  
                x = 27855.375,  
                y = -1191.390625,  
                z = 75.703125
              },  
              Angle = 2.571155548,  
              Base = [[palette.entities.creatures.ckfrb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1909]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1907]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1956]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1957]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1345]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1910]],  
                x = 27841.67188,  
                y = -1198.203125,  
                z = 75.015625
              },  
              Angle = 2.571155548,  
              Base = [[palette.entities.creatures.ckdib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1913]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1911]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1958]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1959]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1345]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1914]],  
                x = 27843.48438,  
                y = -1182.9375,  
                z = 75.046875
              },  
              Angle = 2.571155548,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1917]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1915]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1962]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1963]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1345]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Power Trooper Kidinak]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1918]],  
                x = 27852.39063,  
                y = -1203.1875,  
                z = 76.484375
              },  
              Angle = 2.571155548,  
              Base = [[palette.entities.creatures.ckaib2]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_9]]
        },  
        {
          InstanceId = [[Client1_1380]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1433]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1434]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1454]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1455]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1457]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1458]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1582]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1583]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1585]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1586]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1601]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1602]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1612]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1613]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1625]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1626]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1642]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1647]],  
                    Entity = r2.RefId([[Client1_1059]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1646]],  
                      Type = [[starts dialog]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1643]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1649]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1650]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1652]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1653]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1679]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1680]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1708]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1709]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1713]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1714]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1729]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1730]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1826]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1829]],  
                    Entity = r2.RefId([[Client1_1796]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1828]],  
                      Type = [[Kill]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1827]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1838]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1839]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1841]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1844]],  
                    Entity = r2.RefId([[Client1_1796]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1843]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1842]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1846]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1849]],  
                    Entity = r2.RefId([[Client1_1796]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1848]],  
                      Type = [[Kill]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1847]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            InstanceId = [[Client1_1381]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          _Zone = [[Client1_1384]],  
          Name = [[Zone trigger 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1382]],  
            x = 27861.9375,  
            y = -1106.390625,  
            z = 73.671875
          },  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          Active = 1,  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1384]],  
              Name = [[Places 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1383]],  
                x = 1.578125,  
                y = -0.453125,  
                z = -0.25
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1386]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1387]],  
                    x = 5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1389]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1390]],  
                    x = 0,  
                    y = 5,  
                    z = 0
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1392]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1393]],  
                    x = -5,  
                    y = 0,  
                    z = 0
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1395]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1396]],  
                    x = 0,  
                    y = -5,  
                    z = 0
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              },  
              Deletable = 0
            }
          },  
          Class = [[ZoneTrigger]]
        }
      },  
      Version = 5,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_7]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Permanent]]
    },  
    {
      InstanceId = [[Client1_12]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_845]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_848]],  
                Entity = r2.RefId([[Client1_834]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_847]],  
                  Type = [[Sit Down]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_850]],  
                Entity = r2.RefId([[Client1_830]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_849]],  
                  Type = [[Sit Down]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_852]],  
                Entity = r2.RefId([[Client1_841]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_851]],  
                  Type = [[Sit Down]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_846]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1719]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1720]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1724]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1725]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        InstanceId = [[Client1_10]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 1,  
      LocationId = [[Client1_14]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_911]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_912]],  
                x = 27638.0625,  
                y = -1244.5625,  
                z = 75.59375
              },  
              Angle = 0.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_909]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_917]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_918]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 14,  
              HairType = 5624366,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 2,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              MorphTarget7 = 4,  
              MorphTarget8 = 3,  
              JacketModel = 0,  
              TrouserModel = 5618222,  
              FeetModel = 5617710,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Mu]],  
              Sex = 1,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_934]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_935]],  
                x = 27660.32813,  
                y = -1222.484375,  
                z = 74.78125
              },  
              Angle = -2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_932]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 12,  
              HairType = 5623342,  
              HairColor = 0,  
              Tattoo = 7,  
              EyesColor = 3,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 1,  
              MorphTarget8 = 3,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ba'caunin]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_938]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_939]],  
                x = 27674.39063,  
                y = -1229,  
                z = 74.984375
              },  
              Angle = 6.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_936]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 6,  
              HairType = 7470,  
              HairColor = 1,  
              Tattoo = 29,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 6,  
              MorphTarget4 = 6,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 1,  
              JacketModel = 0,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 0,  
              Speed = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Laughan]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_915]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_916]],  
                x = 27676.95313,  
                y = -1228.96875,  
                z = 75
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_913]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 8494,  
              HairColor = 3,  
              Tattoo = 28,  
              EyesColor = 3,  
              MorphTarget1 = 3,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              MorphTarget7 = 1,  
              MorphTarget8 = 7,  
              JacketModel = 5618734,  
              TrouserModel = 5618222,  
              FeetModel = 5617710,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Pu-Fu]],  
              Sex = 1,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_945]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_946]],  
                x = 27684.90625,  
                y = -1246.0625,  
                z = 75.484375
              },  
              Angle = 3.703125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_943]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 4,  
              HairType = 2350,  
              HairColor = 1,  
              Tattoo = 9,  
              EyesColor = 2,  
              MorphTarget1 = 1,  
              MorphTarget2 = 3,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 6,  
              MorphTarget8 = 6,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ioros]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_949]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_950]],  
                x = 27655.40625,  
                y = -1266.390625,  
                z = 74.640625
              },  
              Angle = -0.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_947]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 12,  
              HairType = 5934,  
              HairColor = 5,  
              Tattoo = 24,  
              EyesColor = 6,  
              MorphTarget1 = 5,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 5,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Nirni]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_953]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_954]],  
                x = 27660.14063,  
                y = -1274.328125,  
                z = 74.921875
              },  
              Angle = -0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_951]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 10,  
              HairType = 5622318,  
              HairColor = 3,  
              Tattoo = 25,  
              EyesColor = 2,  
              MorphTarget1 = 3,  
              MorphTarget2 = 4,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 0,  
              HandsModel = 5609774,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Liccio]],  
              Sex = 1,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1796]],  
              Base = [[palette.entities.npcs.bandits.f_mage_aoe_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1797]],  
                x = 27847.96875,  
                y = -1096.515625,  
                z = 73.75
              },  
              Angle = 2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1794]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1851]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1854]],  
                        Entity = r2.RefId([[Client1_1804]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1853]],  
                          Type = [[Kill]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1852]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 8,  
              HairType = 2862,  
              HairColor = 3,  
              Tattoo = 26,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              MorphTarget7 = 1,  
              MorphTarget8 = 6,  
              JacketModel = 5606446,  
              TrouserModel = 5658414,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 2,  
              Speed = 0,  
              WeaponRightHand = 6933806,  
              Level = 0,  
              Name = [[Tirius]],  
              Sex = 1,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_aoe_cold_b2.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1800]],  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1801]],  
                x = 27838.45313,  
                y = -1094.375,  
                z = 73.546875
              },  
              Angle = -0.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1798]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 10,  
              HairType = 5622062,  
              HairColor = 3,  
              Tattoo = 4,  
              EyesColor = 4,  
              MorphTarget1 = 2,  
              MorphTarget2 = 5,  
              MorphTarget3 = 4,  
              MorphTarget4 = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              MorphTarget7 = 1,  
              MorphTarget8 = 5,  
              JacketModel = 5606446,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5606958,  
              ArmModel = 5606190,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 1,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              WeaponRightHand = 5595950,  
              Level = 0,  
              Name = [[Gaan]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_damage_dealer_slash_b2.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1804]],  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1805]],  
                x = 27844.53125,  
                y = -1091.921875,  
                z = 74.078125
              },  
              Angle = -0.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1802]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1816]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1819]],  
                        Entity = r2.RefId([[Client1_1800]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1818]],  
                          Type = [[Sit Down]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1817]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 5,  
              HairType = 2862,  
              HairColor = 2,  
              Tattoo = 26,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 3,  
              MorphTarget3 = 1,  
              MorphTarget4 = 6,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              MorphTarget7 = 6,  
              MorphTarget8 = 6,  
              JacketModel = 5606446,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5606190,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 2,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponRightHand = 5594926,  
              Level = 0,  
              Name = [[Eukos]],  
              Sex = 1,  
              WeaponLeftHand = 5637166,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_b2.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InstanceId = [[Client1_1950]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1948]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1952]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1953]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1275]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Elite Trooper Kiban]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1951]],  
                x = 27792.46875,  
                y = -1150.890625,  
                z = 77.421875
              },  
              Angle = 1.167310715,  
              Base = [[palette.entities.creatures.ckgib4]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_866]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_870]],  
              Time = 6,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_871]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_830]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_896]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_887]],  
              Time = 8,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_888]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_894]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_890]],  
              Time = 8,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_891]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_841]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_895]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_897]],  
              Time = 8,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_898]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_899]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_900]],  
              Time = 9,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_901]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_830]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_904]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_902]],  
              Time = 8,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_903]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_841]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_905]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_906]],  
              Time = 9,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_907]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_908]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_868]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_869]],  
                  Type = [[start of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            InstanceId = [[Client1_864]]
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_865]],  
            x = 27663.75,  
            y = -1238.609375,  
            z = 75
          },  
          Repeating = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Active = 1
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_925]],  
          Name = [[Dialog 2]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_926]],  
              Time = 5,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_927]],  
                  Emote = [[Excited]],  
                  Who = r2.RefId([[Client1_911]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_928]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_929]],  
              Time = 7,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_930]],  
                  Emote = [[Dreamy]],  
                  Who = r2.RefId([[Client1_911]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_931]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_923]]
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_924]],  
            x = 27638.8125,  
            y = -1242.140625,  
            z = 75.890625
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 0
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_1059]],  
          Name = [[Dialog 3]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1060]],  
              Time = 44,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1061]],  
                  Emote = [[Innocent]],  
                  Who = r2.RefId([[Client1_999]]),  
                  Facing = r2.RefId([[Client1_1003]]),  
                  Says = [[Client1_1062]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1063]],  
              Time = 8,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1064]],  
                  Emote = [[Confused]],  
                  Who = r2.RefId([[Client1_1003]]),  
                  Facing = r2.RefId([[Client1_999]]),  
                  Says = [[Client1_1065]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1068]],  
              Time = 10,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1069]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_999]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_1072]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1070]],  
              Time = 5,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1071]],  
                  Emote = [[Discreet]],  
                  Who = r2.RefId([[Client1_961]]),  
                  Facing = r2.RefId([[Client1_1003]]),  
                  Says = [[Client1_1074]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_1057]]
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1058]],  
            x = 27639.51563,  
            y = -1231.859375,  
            z = 77
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 1
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_1431]],  
          Name = [[Dialog 4]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1438]],  
              Time = 5,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1439]],  
                  Emote = [[Imploring]],  
                  Who = r2.RefId([[Client1_1378]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_1440]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1441]],  
              Time = 5,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1442]],  
                  Emote = [[Annoyed]],  
                  Who = r2.RefId([[Client1_1378]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_1443]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_1444]],  
              Time = 9,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_1445]],  
                  Emote = [[Crazy]],  
                  Who = r2.RefId([[Client1_1378]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_1446]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1436]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1437]],  
                  Type = [[start of dialog]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            InstanceId = [[Client1_1429]]
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1430]],  
            x = 27867.75,  
            y = -1104.5625,  
            z = 74.234375
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 1
        }
      },  
      Version = 5,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_11]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Act 1:Act 1]]
    },  
    {
      InstanceId = [[Client1_1999]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1997]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1998]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 21,  
      LocationId = [[Client1_2001]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2000]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 2:Act 2]],  
      Version = 5
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_894]],  
        Count = 7,  
        Text = [[I worry about her...bandits are so terrible.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_895]],  
        Count = 10,  
        Text = [[I'm sure they bring her to the north east of here.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_896]],  
        Count = 13,  
        Text = [[Who will help Princess Faneliah? ]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_899]],  
        Count = 7,  
        Text = [[Maybe a GUARD could do his job??? Ahem...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_904]],  
        Count = 3,  
        Text = [[They act as if they don't hear us]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_905]],  
        Count = 6,  
        Text = [[It would be faster if we go ourselves]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_908]],  
        Count = 6,  
        Text = [[I'm afraid of the kittins...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_928]],  
        Count = 4,  
        Text = [[Woow, beautifull butterflies!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_931]],  
        Count = 2,  
        Text = [[I want to be a butterfly too...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1062]],  
        Count = 9,  
        Text = [[I can't help the princess, I have to feed my yubo...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1065]],  
        Count = 7,  
        Text = [[I can't help the princess, my wife will be jealous...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1072]],  
        Count = 1,  
        Text = [[I can't help the princess, I'm allergic to the kittins...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1073]],  
        Count = 1,  
        Text = [[I can't help the princess, she frighten me...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1074]],  
        Count = 1,  
        Text = [[I can't help the princess, she frightens me...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1440]],  
        Count = 2,  
        Text = [[Help!!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1443]],  
        Count = 3,  
        Text = [[I can't bear their stupid jokes anymore!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1446]],  
        Count = 2,  
        Text = [[Come here and help me!!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1610]],  
        Count = 1,  
        Text = [[Hey!!! Get out!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1677]],  
        Count = 1,  
        Text = [[Hey!!! Get out!]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}