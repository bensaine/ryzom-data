scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 10,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 1,  
    Npc = 0,  
    ChatAction = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    EventType = 0,  
    Position = 0,  
    ChatSequence = 0,  
    ChatStep = 0,  
    LogicEntityBehavior = 0,  
    TextManager = 0
  },  
  Acts = {
    {
      Cost = 20,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_5]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19]],  
                x = 29703.76563,  
                y = -1288.140625,  
                z = 74.5625
              },  
              Angle = -2.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_16]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_30]],  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_31]],  
                x = 29707.45313,  
                y = -1295.609375,  
                z = 73.609375
              },  
              Angle = -6.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_28]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_42]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_43]],  
                x = 29696.59375,  
                y = -1282.953125,  
                z = 75.046875
              },  
              Angle = -1.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_40]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_46]],  
              Base = [[palette.entities.botobjects.pack_1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_47]],  
                x = 29707.60938,  
                y = -1299.578125,  
                z = 72.9375
              },  
              Angle = -2.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_44]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[pack 1 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_50]],  
              Base = [[palette.entities.botobjects.pack_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_51]],  
                x = 29689.70313,  
                y = -1280.890625,  
                z = 75.0625
              },  
              Angle = -1.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_48]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[pack 3 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_62]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_63]],  
                x = 29690.78125,  
                y = -1297.625,  
                z = 74.9375
              },  
              Angle = -2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_60]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_98]],  
              Base = [[palette.entities.botobjects.totem_bird]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_99]],  
                x = 29714.40625,  
                y = -1282.21875,  
                z = 75.0625
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_96]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bird totem 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_136]],  
              Name = [[Place 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_135]],  
                x = -2.203125,  
                y = -1.078125,  
                z = -1.359375
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_138]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_139]],  
                    x = 29698.35938,  
                    y = -1289.8125,  
                    z = 75.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_141]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_142]],  
                    x = 29699.39063,  
                    y = -1288.1875,  
                    z = 75.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_144]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_145]],  
                    x = 29701.03125,  
                    y = -1287,  
                    z = 76.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_147]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_148]],  
                    x = 29704.78125,  
                    y = -1288.921875,  
                    z = 75.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_153]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_154]],  
                    x = 29706.42188,  
                    y = -1291.65625,  
                    z = 75.28125,  
                    Activity = [[Stand Still]],  
                    TimeLimit = [[No Limit]],  
                    ActivityZoneId = r2.RefId([[]]),  
                    TimeLimitValue = [[]]
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_156]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_157]],  
                    x = 29704.0625,  
                    y = -1293.796875,  
                    z = 74.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_159]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_160]],  
                    x = 29702.54688,  
                    y = -1294.03125,  
                    z = 74.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_162]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_163]],  
                    x = 29700.40625,  
                    y = -1292.09375,  
                    z = 75.4375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_414]],  
              Base = [[palette.entities.botobjects.fy_s2_palmtree_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_415]],  
                x = 29722.54688,  
                y = -1288.828125,  
                z = 75.390625
              },  
              Angle = -2.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_412]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[olansi I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_434]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_435]],  
                x = 29700.26563,  
                y = -1250.03125,  
                z = 73.296875
              },  
              Angle = -2.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_432]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga III 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_438]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_439]],  
                x = 29644.5,  
                y = -1298.9375,  
                z = 79.34375
              },  
              Angle = 0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_436]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[botoga I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_887]],  
              Name = [[Place 2]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_889]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_890]],  
                    x = 29648.95313,  
                    y = -1195.953125,  
                    z = 73.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_892]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_893]],  
                    x = 29656.5625,  
                    y = -1212.015625,  
                    z = 72.75
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_895]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_896]],  
                    x = 29676.14063,  
                    y = -1221.578125,  
                    z = 76.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_898]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_899]],  
                    x = 29680.28125,  
                    y = -1216.421875,  
                    z = 75.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_901]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_902]],  
                    x = 29669.03125,  
                    y = -1192.078125,  
                    z = 71.71875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_904]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_905]],  
                    x = 29654.10938,  
                    y = -1188.078125,  
                    z = 72.75
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_886]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1047]],  
              Base = [[palette.entities.botobjects.fy_s2_coconuts_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1048]],  
                x = 29703,  
                y = -1282.421875,  
                z = 75.015625
              },  
              Angle = -3.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1045]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[olash I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1051]],  
              Base = [[palette.entities.botobjects.fy_s2_papaleaf_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1052]],  
                x = 29685.76563,  
                y = -1245.984375,  
                z = 75.125
              },  
              Angle = -2.328125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1049]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[papalexi I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1055]],  
              Base = [[palette.entities.botobjects.fy_s2_papaleaf_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1056]],  
                x = 29692.98438,  
                y = -1236,  
                z = 76.390625
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1053]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[papalexi II 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1135]],  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1136]],  
                x = 29525.09375,  
                y = -1426.015625,  
                z = 74.96875
              },  
              Angle = 0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1133]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin mound 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1139]],  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1140]],  
                x = 29559.79688,  
                y = -1401.96875,  
                z = 78.359375
              },  
              Angle = 0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1137]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin egg 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1143]],  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1144]],  
                x = 29556.125,  
                y = -1403.375,  
                z = 78.375
              },  
              Angle = 0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1141]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin egg 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1147]],  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1148]],  
                x = 29559.73438,  
                y = -1406.109375,  
                z = 78.78125
              },  
              Angle = 0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1145]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin egg 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1151]],  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1152]],  
                x = 29557.4375,  
                y = -1406.28125,  
                z = 78.703125
              },  
              Angle = 0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1149]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin egg 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1158]],  
              Name = [[Place 3]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1160]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1161]],  
                    x = 29544.0625,  
                    y = -1413.984375,  
                    z = 76.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1163]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1164]],  
                    x = 29549.42188,  
                    y = -1427.15625,  
                    z = 75.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1166]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1167]],  
                    x = 29558.5,  
                    y = -1430.78125,  
                    z = 75.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1169]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1170]],  
                    x = 29564.89063,  
                    y = -1411.46875,  
                    z = 76.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1172]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1173]],  
                    x = 29563.26563,  
                    y = -1401.8125,  
                    z = 77.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1175]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1176]],  
                    x = 29561.10938,  
                    y = -1394.609375,  
                    z = 77.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1178]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1179]],  
                    x = 29549.82813,  
                    y = -1397.65625,  
                    z = 76.6875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1157]],  
                x = 0.296875,  
                y = -4.984375,  
                z = 0.625
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1254]],  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1255]],  
                x = 29534.0625,  
                y = -1408.359375,  
                z = 75.09375
              },  
              Angle = 0,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1252]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1262]],  
              Base = [[palette.entities.botobjects.fx_de_vapeurs]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1263]],  
                x = 29548.21875,  
                y = -1400.234375,  
                z = 76.71875
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1260]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[smoke 1]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Name = [[Permanent]],  
      Title = [[Permanent]],  
      Events = {
      }
    },  
    {
      Cost = 24,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_10]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_9]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_134]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_122]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_123]],  
                x = 29690.875,  
                y = -1305.40625,  
                z = 74.1875
              },  
              Angle = -1.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_120]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_617]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_618]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 7,  
              HairType = 6700590,  
              HairColor = 4,  
              Tattoo = 28,  
              EyesColor = 3,  
              MorphTarget1 = 7,  
              MorphTarget2 = 2,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 0,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 3,  
              Aggro = 1,  
              WeaponLeftHand = 0,  
              Name = [[guard3]],  
              Sex = 0,  
              WeaponRightHand = 6756398,  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_e1.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_172]],  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_173]],  
                x = 29690.1875,  
                y = -1306.5625,  
                z = 74.078125
              },  
              Angle = -1.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_170]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 7,  
              HairType = 2606,  
              HairColor = 4,  
              Tattoo = 24,  
              EyesColor = 1,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 4,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6704942,  
              HandsModel = 6705710,  
              ArmModel = 6704174,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 3,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6754862,  
              Name = [[guard5]],  
              Sex = 1,  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_slash_e4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_168]],  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_169]],  
                x = 29689.07813,  
                y = -1304.6875,  
                z = 74.03125
              },  
              Angle = -1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_166]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 7,  
              HairType = 6700590,  
              HairColor = 4,  
              Tattoo = 7,  
              EyesColor = 4,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 2,  
              MorphTarget4 = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 3,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 6773294,  
              WeaponRightHand = 6754350,  
              Name = [[guard2]],  
              Sex = 1,  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_melee_tank_slash_e1.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_176]],  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_177]],  
                x = 29692.39063,  
                y = -1307.265625,  
                z = 74.203125
              },  
              Angle = -1.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_174]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 7,  
              HairType = 6702,  
              HairColor = 0,  
              Tattoo = 21,  
              EyesColor = 4,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6704942,  
              HandsModel = 6705710,  
              ArmModel = 6704174,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 3,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6752302,  
              Name = [[guard4]],  
              Sex = 1,  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_blunt_e4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_130]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_131]],  
                x = 29690.42188,  
                y = -1307.96875,  
                z = 74.25
              },  
              Angle = -2.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_128]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_908]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_907]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 7,  
              HairType = 6700590,  
              HairColor = 4,  
              Tattoo = 19,  
              EyesColor = 7,  
              MorphTarget1 = 3,  
              MorphTarget2 = 0,  
              MorphTarget3 = 7,  
              MorphTarget4 = 6,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 3,  
              WeaponLeftHand = 0,  
              Name = [[guard6]],  
              Sex = 1,  
              WeaponRightHand = 6755886,  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_e1.creature]],  
              InheritPos = 1
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_133]],  
            x = 21.625,  
            y = 30.875,  
            z = 0.828125
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_132]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Cost = 0
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_468]],  
          Name = [[Group 2]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_444]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_445]],  
                x = 29722.85938,  
                y = -1280.015625,  
                z = 75.09375
              },  
              Angle = 0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_442]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1211]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1212]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 7,  
              HairType = 6700590,  
              HairColor = 4,  
              Tattoo = 2,  
              EyesColor = 3,  
              MorphTarget1 = 7,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              MorphTarget7 = 2,  
              MorphTarget8 = 5,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 3,  
              Aggro = 1,  
              WeaponLeftHand = 0,  
              Name = [[squad5]],  
              Sex = 1,  
              WeaponRightHand = 6756910,  
              ActivitiesId = {
              },  
              AutoSpawn = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_e1.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_460]],  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_461]],  
                x = 29724.46875,  
                y = -1276.796875,  
                z = 75.171875
              },  
              Angle = 0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_458]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 11,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 24,  
              EyesColor = 5,  
              MorphTarget1 = 6,  
              MorphTarget2 = 4,  
              MorphTarget3 = 5,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              MorphTarget7 = 1,  
              MorphTarget8 = 3,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6704942,  
              HandsModel = 6705710,  
              ArmModel = 6704174,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 3,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6754862,  
              Name = [[squad4]],  
              Sex = 1,  
              ActivitiesId = {
              },  
              AutoSpawn = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_slash_e4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_456]],  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_457]],  
                x = 29724.59375,  
                y = -1278.375,  
                z = 75.109375
              },  
              Angle = 0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_454]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 7,  
              HairType = 5622062,  
              HairColor = 0,  
              Tattoo = 15,  
              EyesColor = 2,  
              MorphTarget1 = 4,  
              MorphTarget2 = 5,  
              MorphTarget3 = 5,  
              MorphTarget4 = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6704942,  
              HandsModel = 6705710,  
              ArmModel = 6704174,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 3,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6754094,  
              Name = [[squad2]],  
              Sex = 1,  
              ActivitiesId = {
              },  
              AutoSpawn = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_slash_e4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_464]],  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_465]],  
                x = 29719.73438,  
                y = -1279.1875,  
                z = 75.078125
              },  
              Angle = 0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_462]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 7,  
              HairType = 6958,  
              HairColor = 5,  
              Tattoo = 8,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6704942,  
              HandsModel = 6705710,  
              ArmModel = 6704174,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 3,  
              PlayerAttackable = 0,  
              WeaponLeftHand = 6753326,  
              WeaponRightHand = 6753326,  
              Name = [[squad3]],  
              Sex = 1,  
              ActivitiesId = {
              },  
              AutoSpawn = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              Sheet = [[ring_light_melee_pierce_e4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_467]],  
            x = -12.859375,  
            y = -1.609375,  
            z = -0.03125
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_466]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Cost = 0
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_719]],  
          Name = [[Group 5]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_711]],  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_712]],  
                x = 29694.17188,  
                y = -1288.5,  
                z = 74.90625
              },  
              Angle = -1.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_709]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_930]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_931]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_933]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_934]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_935]]),  
                        ActivityZoneId = r2.RefId([[Client1_887]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1034]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1035]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1036]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_935]],  
                    Name = [[]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_936]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_937]],  
                            Emote = [[Alert]],  
                            Who = [[Client1_711]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_938]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1287]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1286]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 7,  
              HairType = 7470,  
              HairColor = 4,  
              Tattoo = 9,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 6704686,  
              TrouserModel = 6703662,  
              FeetModel = 6702638,  
              HandsModel = 6703150,  
              ArmModel = 6704174,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 3,  
              Speed = [[run]],  
              WeaponLeftHand = 0,  
              BotAttackable = 1,  
              WeaponRightHand = 0,  
              Name = [[Aedan]],  
              Sex = 0,  
              ActivitiesId = {
              },  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_715]],  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_716]],  
                x = 29707.5,  
                y = -1301.78125,  
                z = 72.5
              },  
              Angle = -3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_713]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 7,  
              HairType = 3118,  
              HairColor = 5,  
              Tattoo = 30,  
              EyesColor = 4,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 4,  
              JacketModel = 6704686,  
              TrouserModel = 6703662,  
              FeetModel = 6702638,  
              HandsModel = 6703150,  
              ArmModel = 6704174,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 3,  
              WeaponLeftHand = 0,  
              BotAttackable = 1,  
              WeaponRightHand = 0,  
              Name = [[Aenia]],  
              Sex = 1,  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_118]],  
              Base = [[palette.entities.creatures.chhpf4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_119]],  
                x = 29689.32813,  
                y = -1289.046875,  
                z = 75.15625
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_116]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              PlayerAttackable = 0,  
              InheritPos = 1,  
              Name = [[Voracious Mektoub]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_106]],  
              Base = [[palette.entities.creatures.chhdd2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_107]],  
                x = 29697.9375,  
                y = -1284.421875,  
                z = 74.96875
              },  
              Angle = -2.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_104]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              PlayerAttackable = 0,  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_102]],  
              Base = [[palette.entities.creatures.chhdd1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_103]],  
                x = 29694.78125,  
                y = -1283.46875,  
                z = 75.046875
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_100]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              PlayerAttackable = 0,  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_114]],  
              Base = [[palette.entities.creatures.chhdd3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_115]],  
                x = 29696.90625,  
                y = -1281.453125,  
                z = 75.078125
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_112]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              PlayerAttackable = 0,  
              InheritPos = 1,  
              Name = [[Rooting Mektoub]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_718]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_717]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Cost = 0
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1196]],  
          Name = [[Group 6]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1282]],  
              Base = [[palette.entities.creatures.ckjpf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1283]],  
                x = 29555.14063,  
                y = -1396.109375,  
                z = 76.859375
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1280]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1284]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1285]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kipekoo]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1188]],  
              Base = [[palette.entities.creatures.ckaif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1189]],  
                x = 29547.64063,  
                y = -1400.65625,  
                z = 76.625
              },  
              Angle = 2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1186]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kidinak]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1192]],  
              Base = [[palette.entities.creatures.ckaif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1193]],  
                x = 29541.375,  
                y = -1402.421875,  
                z = 79.25
              },  
              Angle = 1.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1190]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kidinak]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1184]],  
              Base = [[palette.entities.creatures.ckaif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1185]],  
                x = 29543.76563,  
                y = -1405.3125,  
                z = 79.9375
              },  
              Angle = 1.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1182]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kidinak]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1215]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1216]],  
                x = 29553.5,  
                y = -1412.75,  
                z = 79.75
              },  
              Angle = 2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1213]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1266]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1267]],  
                x = 29558.5,  
                y = -1405.359375,  
                z = 78.015625
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1264]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1270]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1271]],  
                x = 29554.89063,  
                y = -1407.640625,  
                z = 78.796875
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1268]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1278]],  
              Base = [[palette.entities.creatures.ckfif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1279]],  
                x = 29554.57813,  
                y = -1402.796875,  
                z = 78.125
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1276]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1274]],  
              Base = [[palette.entities.creatures.ckfif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1275]],  
                x = 29552.0625,  
                y = -1405.390625,  
                z = 79.28125
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1272]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1195]],  
            x = 13.421875,  
            y = 6.734375,  
            z = -2.296875
          },  
          Cost = 0,  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1194]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Name = [[Act 1]],  
      Title = [[Act 1]],  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_938]],  
        Count = 7,  
        Text = [[Save the mektoubs!!!!!]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}