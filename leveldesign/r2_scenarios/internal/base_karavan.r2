scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_1432]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes40]],  
      Time = 0,  
      Name = [[(Sub Tropics 40)]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    }
  },  
  InstanceId = [[Client1_1423]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_1421]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_1422]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 3,  
    Behavior = 0,  
    TextManager = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ActivityStep = 1,  
    Position = 0,  
    Location = 0,  
    Region = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1419]],  
    Class = [[MapDescription]],  
    LevelId = 4,  
    LocationId = 37,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_1424]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 83,  
      InstanceId = [[Client1_1426]],  
      ManualWeather = 0,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1425]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          InstanceId = [[Client1_1427]],  
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1434]],  
              Name = [[Place 1]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1436]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1437]],  
                    x = 38318.9375,  
                    y = -3481.984375,  
                    z = 356.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1439]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1440]],  
                    x = 38271.67188,  
                    y = -3533.9375,  
                    z = 357.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1442]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1443]],  
                    x = 38224.79688,  
                    y = -3582.234375,  
                    z = 354.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1803]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1804]],  
                    x = 38194.60938,  
                    y = -3577.46875,  
                    z = 358.265625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1445]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1446]],  
                    x = 38183.57813,  
                    y = -3563.984375,  
                    z = 358.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1923]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1924]],  
                    x = 38167.65625,  
                    y = -3532.125,  
                    z = 359.421875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1920]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1921]],  
                    x = 38150.5,  
                    y = -3502.046875,  
                    z = 357.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1448]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1449]],  
                    x = 38107.73438,  
                    y = -3496.25,  
                    z = 357.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1930]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1931]],  
                    x = 38095.35938,  
                    y = -3457.390625,  
                    z = 356.90625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1933]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1934]],  
                    x = 38106.39063,  
                    y = -3413.28125,  
                    z = 361.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1451]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1452]],  
                    x = 38124.64063,  
                    y = -3373.875,  
                    z = 354.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1454]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1455]],  
                    x = 38202.4375,  
                    y = -3375.421875,  
                    z = 357.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1457]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1458]],  
                    x = 38343.67188,  
                    y = -3355.671875,  
                    z = 354.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1652]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1653]],  
                    x = 38375.57813,  
                    y = -3415.84375,  
                    z = 357.390625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1433]],  
                x = 24.296875,  
                y = -50.046875,  
                z = -301.90625
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1461]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1462]],  
                x = 38335.96875,  
                y = -3542.65625,  
                z = 54.140625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1459]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1470]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1472]],  
                x = 38330.32813,  
                y = -3548.796875,  
                z = 54.46875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1471]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1480]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1482]],  
                x = 38324.59375,  
                y = -3554.59375,  
                z = 55.296875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1481]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1490]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1492]],  
                x = 38318.875,  
                y = -3560.171875,  
                z = 55.890625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1491]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1500]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1502]],  
                x = 38313.10938,  
                y = -3566.09375,  
                z = 55.875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1501]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1510]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1512]],  
                x = 38307.53125,  
                y = -3571.75,  
                z = 55.640625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1511]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1520]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1522]],  
                x = 38301.65625,  
                y = -3577.46875,  
                z = 56.03125
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1521]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1530]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1532]],  
                x = 38290.0625,  
                y = -3588.796875,  
                z = 57.15625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1531]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1540]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1542]],  
                x = 38295.85938,  
                y = -3583.21875,  
                z = 56.671875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1541]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 9]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1550]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1552]],  
                x = 38284.1875,  
                y = -3594.59375,  
                z = 57.03125
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1551]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 10]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1560]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1562]],  
                x = 38278.59375,  
                y = -3600.59375,  
                z = 56.34375
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1561]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 11]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1570]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1572]],  
                x = 38272.78125,  
                y = -3606.515625,  
                z = 55.46875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1571]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 12]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1575]],  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1576]],  
                x = 38339.07813,  
                y = -3539.5,  
                z = 54.078125
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1573]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan mirador 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1584]],  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1586]],  
                x = 38353.73438,  
                y = -3521.15625,  
                z = 55.3125
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1585]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan mirador 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1589]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1590]],  
                x = 38356.92188,  
                y = -3518.078125,  
                z = 55.546875
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1587]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 13]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1598]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1600]],  
                x = 38361.92188,  
                y = -3511.984375,  
                z = 55.40625
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1599]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 14]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1608]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1610]],  
                x = 38366.78125,  
                y = -3505.390625,  
                z = 54.609375
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1609]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 15]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1618]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1620]],  
                x = 38371.98438,  
                y = -3498.84375,  
                z = 54.046875
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1619]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 16]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1628]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1630]],  
                x = 38377.29688,  
                y = -3492.515625,  
                z = 54.03125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1629]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 17]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1638]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1640]],  
                x = 38382.46875,  
                y = -3486.25,  
                z = 54.40625
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1639]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 18]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1648]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1650]],  
                x = 38387.79688,  
                y = -3479.984375,  
                z = 54.828125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1649]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 19]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1661]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1663]],  
                x = 38392.8125,  
                y = -3473.6875,  
                z = 55.109375
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1662]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 20]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1671]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1673]],  
                x = 38397.90625,  
                y = -3467.34375,  
                z = 55.171875
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1672]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 21]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1681]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1683]],  
                x = 38395.76563,  
                y = -3456.53125,  
                z = 54.734375
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1682]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 22]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1686]],  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1687]],  
                x = 38399.98438,  
                y = -3463.953125,  
                z = 55.140625
              },  
              Angle = 0.703125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1684]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan mirador 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1695]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1697]],  
                x = 38391.75,  
                y = -3449.453125,  
                z = 54.59375
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1696]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 23]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1705]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1707]],  
                x = 38387.98438,  
                y = -3442.25,  
                z = 54.703125
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1706]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 24]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1715]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1717]],  
                x = 38384.23438,  
                y = -3435.125,  
                z = 54.96875
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1716]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 25]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1725]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1727]],  
                x = 38380.40625,  
                y = -3427.9375,  
                z = 55.0625
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1726]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 26]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1735]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1737]],  
                x = 38376.39063,  
                y = -3420.59375,  
                z = 54.359375
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1736]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 27]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1745]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1747]],  
                x = 38373.09375,  
                y = -3413,  
                z = 53.046875
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1746]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 28]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1750]],  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1751]],  
                x = 38369.82813,  
                y = -3407.046875,  
                z = 52.71875
              },  
              Angle = 0.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1748]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan wall 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1759]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1761]],  
                x = 38267.26563,  
                y = -3612.453125,  
                z = 54.515625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1760]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 29]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1769]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1771]],  
                x = 38261.5625,  
                y = -3618.28125,  
                z = 53.6875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1770]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 30]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1779]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1781]],  
                x = 38255.78125,  
                y = -3623.984375,  
                z = 53.609375
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1780]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 31]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1789]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1791]],  
                x = 38248.53125,  
                y = -3628.046875,  
                z = 54.234375
              },  
              Angle = -1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1790]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 32]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1799]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1801]],  
                x = 38240.54688,  
                y = -3629.46875,  
                z = 54.8125
              },  
              Angle = -1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1800]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 33]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1812]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1814]],  
                x = 38232.51563,  
                y = -3630.84375,  
                z = 55.015625
              },  
              Angle = -1.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1813]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 34]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1817]],  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1818]],  
                x = 38225.89063,  
                y = -3631.640625,  
                z = 54.9375
              },  
              Angle = -1.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1815]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan wall 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1826]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1828]],  
                x = 38220.82813,  
                y = -3628.765625,  
                z = 54.75
              },  
              Angle = -2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1827]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 35]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1836]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1838]],  
                x = 38216.0625,  
                y = -3622.390625,  
                z = 54.703125
              },  
              Angle = -2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1837]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 36]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1846]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1848]],  
                x = 38211,  
                y = -3616.1875,  
                z = 54.828125
              },  
              Angle = -2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1847]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 37]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1856]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1858]],  
                x = 38205.75,  
                y = -3610.25,  
                z = 55.015625
              },  
              Angle = -2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1857]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 38]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1866]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1868]],  
                x = 38200.32813,  
                y = -3603.453125,  
                z = 55.1875
              },  
              Angle = -2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1867]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 39]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1876]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1878]],  
                x = 38195.98438,  
                y = -3596.140625,  
                z = 55.390625
              },  
              Angle = -2.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1877]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 40]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1886]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1888]],  
                x = 38192.82813,  
                y = -3588.953125,  
                z = 55.59375
              },  
              Angle = -2.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1887]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 41]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1896]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1898]],  
                x = 38189.32813,  
                y = -3581.703125,  
                z = 55.6875
              },  
              Angle = -2.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1897]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 42]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1906]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1908]],  
                x = 38186.03125,  
                y = -3574.203125,  
                z = 55.3125
              },  
              Angle = -2.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1907]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 43]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1916]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1918]],  
                x = 38182.67188,  
                y = -3566.671875,  
                z = 54.578125
              },  
              Angle = -2.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1917]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 44]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1927]],  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1928]],  
                x = 38179.76563,  
                y = -3560.703125,  
                z = 54.03125
              },  
              Angle = -2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1925]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan wall 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1947]],  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1949]],  
                x = 38177.0625,  
                y = -3556.046875,  
                z = 53.6875
              },  
              Angle = -2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1948]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan wall 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1957]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1959]],  
                x = 38171.73438,  
                y = -3552.890625,  
                z = 53.8125
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1958]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 45]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1967]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1969]],  
                x = 38163.79688,  
                y = -3551.1875,  
                z = 54.328125
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1968]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 46]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1977]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1979]],  
                x = 38156.26563,  
                y = -3549.71875,  
                z = 54.78125
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1978]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 47]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1987]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1989]],  
                x = 38148.95313,  
                y = -3548.359375,  
                z = 54.90625
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1988]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 48]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1997]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1999]],  
                x = 38140.98438,  
                y = -3546.6875,  
                z = 53.96875
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1998]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 49]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2007]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2009]],  
                x = 38132.65625,  
                y = -3544.890625,  
                z = 52.921875
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2008]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 50]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2017]],  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2019]],  
                x = 38258.35938,  
                y = -3620.65625,  
                z = 53.65625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2018]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan mirador 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2022]],  
              Base = [[palette.entities.botobjects.karavan_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2023]],  
                x = 38398.65625,  
                y = -3462.515625,  
                z = 55.03125
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2020]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan wall 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4413]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4414]],  
                x = 38173.4375,  
                y = -3441.234375,  
                z = 54.453125
              },  
              Angle = -0.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4411]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan tent 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4422]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4424]],  
                x = 38182.70313,  
                y = -3452.828125,  
                z = 53.25
              },  
              Angle = -0.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4423]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4432]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4434]],  
                x = 38189.0625,  
                y = -3463.015625,  
                z = 52.453125
              },  
              Angle = -0.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4433]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4442]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4444]],  
                x = 38196.98438,  
                y = -3476.28125,  
                z = 52.171875
              },  
              Angle = -0.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4443]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan tent 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4447]],  
              Base = [[palette.entities.botobjects.karavan_watchtower_off]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4448]],  
                x = 38139.53125,  
                y = -3453.78125,  
                z = 58.21875
              },  
              Angle = 0.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4445]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan device 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4456]],  
              Base = [[palette.entities.botobjects.karavan_watchtower_off]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4458]],  
                x = 38126.9375,  
                y = -3505.40625,  
                z = 53.796875
              },  
              Angle = 0.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4457]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan device 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4461]],  
              Base = [[palette.entities.botobjects.karavan_device]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4462]],  
                x = 38137.23438,  
                y = -3463.390625,  
                z = 57.09375
              },  
              Angle = 0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4459]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan device 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4470]],  
              Base = [[palette.entities.botobjects.karavan_device]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4472]],  
                x = 38136.79688,  
                y = -3467.390625,  
                z = 56.109375
              },  
              Angle = 0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4471]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan device 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4480]],  
              Base = [[palette.entities.botobjects.karavan_device]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4482]],  
                x = 38136.4375,  
                y = -3472.203125,  
                z = 54.9375
              },  
              Angle = 0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4481]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan device 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4490]],  
              Base = [[palette.entities.botobjects.karavan_device]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4492]],  
                x = 38135.17188,  
                y = -3477.609375,  
                z = 53.8125
              },  
              Angle = 0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4491]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan device 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4499]],  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4500]],  
                x = 38130.1875,  
                y = -3490.140625,  
                z = 52.71875
              },  
              Angle = -0.4298487902,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4497]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan gateway 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4508]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4510]],  
                x = 38166.40625,  
                y = -3461.484375,  
                z = 53.484375
              },  
              Angle = -1.830936432,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4509]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan tent 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4518]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4520]],  
                x = 38161.8125,  
                y = -3476.640625,  
                z = 52.53125
              },  
              Angle = -1.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4519]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan tent 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4528]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4530]],  
                x = 38159.75,  
                y = -3493.328125,  
                z = 52.421875
              },  
              Angle = -1.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4529]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan tent 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4538]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4540]],  
                x = 38157.5625,  
                y = -3507.046875,  
                z = 54
              },  
              Angle = -1.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4539]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan tent 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4543]],  
              Base = [[palette.entities.botobjects.karavan_standard]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4544]],  
                x = 38140.23438,  
                y = -3510.859375,  
                z = 54.390625
              },  
              Angle = 1.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4541]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan standard 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4547]],  
              Base = [[palette.entities.botobjects.karavan_tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4548]],  
                x = 38169.53125,  
                y = -3542.25,  
                z = 53.9375
              },  
              Angle = 1.485373616,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4545]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan tent 9]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4551]],  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4552]],  
                x = 38155.53125,  
                y = -3543.125,  
                z = 54.765625
              },  
              Angle = 1.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4549]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan mirador 5]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4634]],  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4636]],  
                x = 38158.89063,  
                y = -3542.875,  
                z = 54.546875
              },  
              Angle = 1.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4635]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan mirador 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4691]],  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4692]],  
                x = 38140.40625,  
                y = -3533.46875,  
                z = 54.453125
              },  
              Angle = 0.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4689]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[bothaya III 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4695]],  
              Base = [[palette.entities.botobjects.fy_s1_baobab_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4696]],  
                x = 38165.07813,  
                y = -3524.234375,  
                z = 54.859375
              },  
              Angle = -0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4693]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[botoga III 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4699]],  
              Base = [[palette.entities.botobjects.fy_s2_lovejail_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4700]],  
                x = 38186.32813,  
                y = -3499.1875,  
                z = 53.140625
              },  
              Angle = -0.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4697]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[loojine III 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4703]],  
              Base = [[palette.entities.botobjects.fy_s2_palmtree_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4704]],  
                x = 38176.84375,  
                y = -3445.234375,  
                z = 54
              },  
              Angle = -1.546875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4701]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[olansi III 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4707]],  
              Base = [[palette.entities.botobjects.fy_s2_savantree_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4708]],  
                x = 38186.60938,  
                y = -3557.671875,  
                z = 53.6875
              },  
              Angle = 1.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4705]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[savaniel II 1]]
            }
          },  
          Ghosts = {
          }
        }
      },  
      Counters = {
      },  
      Name = [[Permanent]],  
      Title = [[]],  
      Events = {
      }
    },  
    {
      Cost = 21,  
      Behavior = {
        InstanceId = [[Client1_1428]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      StaticCost = 1,  
      InstanceId = [[Client1_1430]],  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[Client1_1432]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1429]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4495]],  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4496]],  
                x = 38143.6875,  
                y = -3486.546875,  
                z = 52.859375
              },  
              Angle = 0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4493]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan banner 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4711]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4712]],  
                x = 38179.95313,  
                y = -3509.125,  
                z = 54.234375
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4709]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_4713]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_4714]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1434]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[emissary 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4724]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4728]],  
                x = 38182.26563,  
                y = -3510.234375,  
                z = 54.359375
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4725]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_4726]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_4727]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1434]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[emissary 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4738]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4742]],  
                x = 38176.73438,  
                y = -3512.953125,  
                z = 54.625
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4739]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_4740]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_4741]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1434]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[emissary 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4752]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4756]],  
                x = 38176.42188,  
                y = -3505.703125,  
                z = 53.890625
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4753]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_4754]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_4755]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1434]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[emissary 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4766]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4770]],  
                x = 38188.15625,  
                y = -3508.71875,  
                z = 54.140625
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4767]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_4768]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_4769]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1434]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[emissary 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4780]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4784]],  
                x = 38183.04688,  
                y = -3504.640625,  
                z = 53.734375
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4781]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_4782]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_4783]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1434]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[emissary 7]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_1431]],  
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_4688]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4684]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4685]],  
                x = 38156.85938,  
                y = -3517.84375,  
                z = 55.015625
              },  
              Angle = 0.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4682]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[emissary 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4584]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4586]],  
                x = 38156.21875,  
                y = -3523.5625,  
                z = 54.953125
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4585]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4574]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4576]],  
                x = 38154.65625,  
                y = -3520.875,  
                z = 55.09375
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4575]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4564]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4566]],  
                x = 38152.45313,  
                y = -3517.921875,  
                z = 55.0625
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4565]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4555]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4556]],  
                x = 38150.57813,  
                y = -3515.59375,  
                z = 54.90625
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4553]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4594]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4596]],  
                x = 38152.625,  
                y = -3525.3125,  
                z = 54.953125
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4595]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4604]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4606]],  
                x = 38150.54688,  
                y = -3522.375,  
                z = 55.015625
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4605]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4614]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4616]],  
                x = 38148.45313,  
                y = -3519.59375,  
                z = 55.0625
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4615]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4624]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4626]],  
                x = 38146.04688,  
                y = -3516.9375,  
                z = 54.953125
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4625]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4649]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4651]],  
                x = 38148.85938,  
                y = -3527.484375,  
                z = 54.9375
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4650]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 9]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4659]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4661]],  
                x = 38147.26563,  
                y = -3524.609375,  
                z = 54.90625
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4660]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 10]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4669]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4671]],  
                x = 38145.21875,  
                y = -3522.046875,  
                z = 54.9375
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4670]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 11]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_4679]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_4681]],  
                x = 38143.25,  
                y = -3519.40625,  
                z = 54.96875
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_4680]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[guard 12]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_4687]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_4686]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Name = [[Act 1:Act 1]],  
      Title = [[]],  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1420]]
  }
}