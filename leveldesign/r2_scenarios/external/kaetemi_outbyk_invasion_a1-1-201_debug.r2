scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client4_12]],  
      IslandName = [[uiR2_Deserts33]],  
      Class = [[Location]],  
      Season = [[fall]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client4_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Description = {
    InstanceId = [[Client4_1]],  
    Class = [[MapDescription]],  
    LevelId = 3,  
    LocationId = 70,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    Title = [[Outnumbered by Kitins]],  
    RuleId = 0,  
    ShortDescription = [[A nice little town that's being invaded by evil bandits and kitins.]],  
    MaxEntities = 50,  
    MaxPlayers = 100
  },  
  Versions = {
    TextManagerEntry = 0,  
    Scenario = 1,  
    Act = 3,  
    Location = 0,  
    TextManager = 0,  
    DefaultFeature = 0,  
    MapDescription = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      ManualWeather = 0,  
      States = {
      }
    }
  },  
}