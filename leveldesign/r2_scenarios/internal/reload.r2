scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      Name = [[pop]],  
      Time = 0,  
      InstanceId = [[Client1_224]],  
      Season = [[spring]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts15]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_215]],  
  Class = [[Scenario]],  
  PlotItems = {
  },  
  VersionName = [[0.0.4]],  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    TextManager = 0,  
    Position = 0,  
    Location = 0,  
    NpcCustom = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_213]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 52,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_216]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_218]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_217]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_231]],  
              Base = [[palette.entities.botobjects.bag_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_232]],  
                x = 21550.48438,  
                y = -1844.109375,  
                z = -21.703125
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_229]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[bag 1 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_235]],  
              Base = [[palette.entities.botobjects.pack_1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_236]],  
                x = 21505.07813,  
                y = -1829.453125,  
                z = -22.15625
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_233]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[pack 1 1]]
            }
          },  
          InstanceId = [[Client1_219]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 6,  
      Behavior = {
        InstanceId = [[Client1_220]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_222]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_221]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 190,  
      LocationId = [[Client1_224]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_227]],  
              Base = [[palette.entities.creatures.ccadb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_228]],  
                x = 21525.32813,  
                y = -1823.71875,  
                z = -21.96875
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_225]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Growling Gingo]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_239]],  
              Base = [[palette.entities.npcs.guards.m_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_240]],  
                x = 21523.01563,  
                y = -1820.21875,  
                z = -22.34375
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_237]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 8,  
              HairType = 5622318,  
              HairColor = 0,  
              Tattoo = 12,  
              EyesColor = 1,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              MorphTarget7 = 2,  
              MorphTarget8 = 0,  
              Sex = 0,  
              JacketModel = 5609262,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5611054,  
              ArmModel = 5611566,  
              JacketColor = 5,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 0,  
              FeetColor = 0,  
              WeaponRightHand = 5636142,  
              WeaponLeftHand = 0,  
              Name = [[forest guard 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_male.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_243]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_244]],  
                x = 21543.32813,  
                y = -1833.34375,  
                z = -20.953125
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_241]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 8,  
              HairType = 5616686,  
              HairColor = 2,  
              Tattoo = 20,  
              EyesColor = 0,  
              MorphTarget1 = 7,  
              MorphTarget2 = 0,  
              MorphTarget3 = 7,  
              MorphTarget4 = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 5620014,  
              TrouserModel = 5619502,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 4,  
              WeaponRightHand = 5636654,  
              WeaponLeftHand = 0,  
              Name = [[jungle guard 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_male.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_247]],  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_248]],  
                x = 21507.21875,  
                y = -1838.796875,  
                z = -22.65625
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_245]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 6,  
              HairType = 2350,  
              HairColor = 5,  
              Tattoo = 28,  
              EyesColor = 1,  
              MorphTarget1 = 1,  
              MorphTarget2 = 0,  
              MorphTarget3 = 7,  
              MorphTarget4 = 6,  
              MorphTarget5 = 5,  
              MorphTarget6 = 6,  
              MorphTarget7 = 6,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5606958,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 5,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[desert armsman 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_251]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_252]],  
                x = 21498.71875,  
                y = -1823.28125,  
                z = -21.96875
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_249]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 14,  
              HairType = 5622830,  
              HairColor = 0,  
              Tattoo = 28,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 5,  
              MorphTarget8 = 0,  
              Sex = 1,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 0,  
              HandsModel = 5609774,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 2,  
              TrouserColor = 0,  
              FeetColor = 4,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[matis-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_255]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_256]],  
                x = 21523.79688,  
                y = -1829.203125,  
                z = -22.875
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_253]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 13,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 17,  
              EyesColor = 5,  
              MorphTarget1 = 0,  
              MorphTarget2 = 4,  
              MorphTarget3 = 2,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 5,  
              TrouserColor = 1,  
              FeetColor = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[fyros-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_223]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:pop]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_214]]
  }
}