scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 119,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 3,  
    Road = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    TextManager = 0,  
    ActivityStep = 0,  
    Position = 0,  
    Behavior = 0,  
    WayPoint = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Title = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Permanent]],  
      Version = 3,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_21]],  
              Class = [[Road]],  
              Position = {
                y = 0.078125,  
                x = -0.171875,  
                InstanceId = [[Client1_20]],  
                Class = [[Position]],  
                z = 0.03125
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_23]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -11152.21875,  
                    x = 31271.89063,  
                    InstanceId = [[Client1_24]],  
                    Class = [[Position]],  
                    z = -3.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_26]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -11150.09375,  
                    x = 31273.15625,  
                    InstanceId = [[Client1_27]],  
                    Class = [[Position]],  
                    z = -3.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_29]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -11145.5625,  
                    x = 31275.04688,  
                    InstanceId = [[Client1_30]],  
                    Class = [[Position]],  
                    z = -3.96875
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
        [[Client1_31]]
      },  
      Events = {
      },  
      Title = [[Act 1]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Act 1]],  
      Version = 3,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_14]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 8,  
              HandsModel = 5605678,  
              FeetColor = 4,  
              GabaritBreastSize = 6,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 9,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Angle = -1.375,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Position = {
                y = -11145.625,  
                x = 31274.90625,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = -3.859375
              },  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 5,  
              JacketModel = 0,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[fyros-dressed civilian 1]],  
              Sex = 0,  
              WeaponRightHand = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 0,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_18]],  
              ActivitiesId = {
                [[Client1_31]]
              },  
              HairType = 5622062,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 6,  
              GabaritHeight = 0,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 3,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_16]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_31]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_32]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_21]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 5605422,  
              Angle = -0.546875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Position = {
                y = -11152.82813,  
                x = 31270.95313,  
                InstanceId = [[Client1_19]],  
                Class = [[Position]],  
                z = -3.0625
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[fyros-dressed civilian 2]],  
              Sex = 0,  
              WeaponRightHand = 0,  
              MorphTarget7 = 5,  
              MorphTarget3 = 6,  
              Tattoo = 1
            }
          },  
          InstanceId = [[Client1_11]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_10]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
    }
  }
}