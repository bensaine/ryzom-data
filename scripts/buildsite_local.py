#!/usr/bin/python
# 
# \file site.py
# \brief Site configuration
# \date 2021-09-17-18-01-GMT
# \author Jan Boon (Kaetemi)
# Python port of game data build pipeline.
# Site configuration.
# 
# NeL - MMORPG Framework <http://dev.ryzom.com/projects/nel/>
# Copyright (C) 2009-2014  by authors
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 


# *** SITE INSTALLATION ***

# Use '/' in path name, not ''
# Don't put '/' at the end of a directory name


# Quality option for this site (1 for BEST, 0 for DRAFT)
BuildQuality = 1

RemapLocalFrom = "R:"
RemapLocalTo = "%%%PATH%%%"

ToolDirectories = ['%%%PATH%%%/distribution/nel_tools_win_x64', '%%%PATH%%%/distribution/ryzom_tools_win_x64']
ToolSuffix = ".exe"

# Build script directory
ScriptDirectory = "%%%PATH%%%/code/nel/tools/build_gamedata"
WorkspaceDirectory = "%%%PATH%%%/leveldesign/workspace"

# Data build directories
DatabaseDirectory = "%%%PATH%%%/graphics"
SoundDirectory = "%%%PATH%%%/sound"
SoundDfnDirectory = "%%%PATH%%%/sound/DFN"
ExportBuildDirectory = "%%%PATH%%%/pipeline/export"

# Install directories
InstallDirectory = "%%%PATH%%%/pipeline/install"
ClientDevDirectory = "%%%PATH%%%/pipeline/client_dev"
ClientDevLiveDirectory = "%%%PATH%%%/pipeline/client_dev_live"
ClientPatchDirectory = "%%%PATH%%%/pipeline/client_patch"
ClientInstallDirectory = "%%%PATH%%%/pipeline/client_install"
ShardInstallDirectory = "%%%PATH%%%/pipeline/shard"
ShardDevDirectory = "%%%PATH%%%/pipeline/shard_dev"
WorldEditInstallDirectory = "%%%PATH%%%/pipeline/worldedit"

# Utility directories
WorldEditorFilesDirectory = "%%%PATH%%%/code/ryzom/common/data_leveldesign/leveldesign/world_editor_files"

# Leveldesign directories
LeveldesignDirectory = "%%%PATH%%%/leveldesign"
LeveldesignDfnDirectory = "%%%PATH%%%/leveldesign/DFN"
LeveldesignWorldDirectory = "%%%PATH%%%/leveldesign/world"
PrimitivesDirectory = "%%%PATH%%%/leveldesign/primitives"
LeveldesignDataCommonDirectory = "%%%PATH%%%/leveldesign/common"
LeveldesignDataShardDirectory = "%%%PATH%%%/leveldesign/shard"
TranslationDirectory = "%%%PATH%%%/leveldesign/translation"

# Misc data directories
GamedevDirectory = "%%%PATH%%%/code/ryzom/client/data/gamedev"
DataCommonDirectory = "%%%PATH%%%/code/ryzom/common/data_common"
DataShardDirectory = "%%%PATH%%%/code/ryzom/server/data_shard"
WindowsExeDllCfgDirectories = ['', '%%%PATH%%%/build/fv_x64/bin/Release', '%%%PATH%%%/distribution/external_x64', '%%%PATH%%%/code/ryzom/client', '', '', '']
LinuxServiceExecutableDirectory = "%%%PATH%%%/build/server_gcc/bin"
LinuxClientExecutableDirectory = "%%%PATH%%%/build/client_gcc/bin"
PatchmanDevDirectory = "%%%PATH%%%/patchman/terminal_dev"
PatchmanCfgAdminDirectory = "%%%PATH%%%/patchman/admin_install"
PatchmanCfgDefaultDirectory = "%%%PATH%%%/patchman/default"
PatchmanBridgeServerDirectory = "%%%PATH%%%/pipeline/bridge_server"

# Sign tool
SignToolExecutable = "C:/Program Files/Microsoft SDKs/Windows/v6.0A/Bin/signtool.exe"
SignToolSha1 = ""
SignToolTimestamp = "http://timestamp.comodoca.com/authenticode"

# 3dsMax directives
MaxAvailable = 0
MaxDirectory = "C:/Program Files/Autodesk/3ds Max 2018"
MaxUserDirectory = "C:/Users/uluky/AppData/Local/Autodesk/3dsMax/2018 - 64bit/ENU"
MaxExecutable = "3dsmax.exe"


# end of file
