scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 23,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    TextManager = 0,  
    Position = 0,  
    NpcCustom = 0,  
    ActivityStep = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 14,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[ACT 0: Permanent]],  
      ActivitiesIds = {
        [[Client1_106]],  
        [[Client1_108]],  
        [[Client1_110]],  
        [[Client1_155]],  
        [[Client1_157]],  
        [[Client1_159]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_57]],  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_58]],  
                x = 26016.78125,  
                y = -2390.1875,  
                z = -9.609375
              },  
              Angle = 2.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_55]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin totem 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_61]],  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_62]],  
                x = 26012.625,  
                y = -2376.5625,  
                z = -10.171875
              },  
              Angle = -2.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_59]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin totem 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_65]],  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_66]],  
                x = 26014.73438,  
                y = -2383.625,  
                z = -9.953125
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_63]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan gateway 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_73]],  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_74]],  
                x = 26027.07813,  
                y = -2528.46875,  
                z = -10.1875
              },  
              Angle = 2.786734819,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_71]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan gateway 2]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_76]],  
              Name = [[Near Town]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_78]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_79]],  
                    x = 25971,  
                    y = -2570.328125,  
                    z = -9.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_81]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_82]],  
                    x = 26005.14063,  
                    y = -2582.25,  
                    z = -9.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_84]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_85]],  
                    x = 25978.34375,  
                    y = -2624.765625,  
                    z = -11.203125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_75]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_96]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_97]],  
                x = 25982.75,  
                y = -2584.78125,  
                z = -10.625
              },  
              Angle = -2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_94]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_108]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_109]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_108]]
              },  
              InheritPos = 1,  
              Name = [[Assault Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_100]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_101]],  
                x = 25985.28125,  
                y = -2591.0625,  
                z = -10.4375
              },  
              Angle = -2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_98]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_110]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_111]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_110]]
              },  
              InheritPos = 1,  
              Name = [[Assault Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_104]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_105]],  
                x = 25977.82813,  
                y = -2593.0625,  
                z = -10.125
              },  
              Angle = -2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_102]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_106]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_107]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_76]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_106]]
              },  
              InheritPos = 1,  
              Name = [[Assault Kirosta]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_124]],  
              Name = [[Place 1]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_126]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_127]],  
                    x = 25910.76563,  
                    y = -2624.765625,  
                    z = -9.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_129]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_130]],  
                    x = 25911.59375,  
                    y = -2651.59375,  
                    z = -10.5625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_132]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_133]],  
                    x = 25893.60938,  
                    y = -2673.125,  
                    z = -9.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_135]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_136]],  
                    x = 25870.8125,  
                    y = -2654.453125,  
                    z = -10.5625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_138]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_139]],  
                    x = 25871.70313,  
                    y = -2602.671875,  
                    z = -8.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_141]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_142]],  
                    x = 25890.39063,  
                    y = -2599.6875,  
                    z = -10.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_123]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_145]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_146]],  
                x = 25880.98438,  
                y = -2625.40625,  
                z = -9.875
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_143]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_159]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_160]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 5,  
              HairType = 8494,  
              HairColor = 3,  
              Tattoo = 31,  
              EyesColor = 7,  
              MorphTarget1 = 1,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 1,  
              MorphTarget7 = 5,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5618222,  
              FeetModel = 5617710,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[zorai-dressed civilian 1]],  
              ActivitiesId = {
                [[Client1_159]]
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_149]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_150]],  
                x = 25886.32813,  
                y = -2628.21875,  
                z = -9.78125
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_147]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_157]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_158]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 10,  
              HairType = 9006,  
              HairColor = 3,  
              Tattoo = 9,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 0,  
              MorphTarget3 = 2,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 6,  
              Sex = 1,  
              JacketModel = 5618734,  
              TrouserModel = 5618222,  
              FeetModel = 0,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 3,  
              FeetColor = 5,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[zorai-dressed civilian 2]],  
              ActivitiesId = {
                [[Client1_157]]
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_153]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_154]],  
                x = 25877.96875,  
                y = -2627.859375,  
                z = -9.890625
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_151]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_155]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_156]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_124]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 12,  
              HairType = 5624366,  
              HairColor = 5,  
              Tattoo = 24,  
              EyesColor = 4,  
              MorphTarget1 = 2,  
              MorphTarget2 = 4,  
              MorphTarget3 = 3,  
              MorphTarget4 = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 5,  
              MorphTarget8 = 0,  
              Sex = 1,  
              JacketModel = 5618734,  
              TrouserModel = 5618222,  
              FeetModel = 0,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 4,  
              TrouserColor = 0,  
              FeetColor = 2,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[zorai-dressed civilian 3]],  
              ActivitiesId = {
                [[Client1_155]]
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_163]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_164]],  
                x = 25916.75,  
                y = -2639.640625,  
                z = -12.90625
              },  
              Angle = 0.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_161]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[watch tower 1]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_167]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_168]],  
                x = 25938.67188,  
                y = -2641.390625,  
                z = -12.03125
              },  
              Angle = 0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_165]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 1,  
              HairType = 9006,  
              HairColor = 5,  
              Tattoo = 27,  
              EyesColor = 6,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              Sex = 0,  
              JacketModel = 5617454,  
              TrouserModel = 5616942,  
              FeetModel = 5616174,  
              HandsModel = 5616430,  
              ArmModel = 5619758,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 2,  
              WeaponRightHand = 5636654,  
              WeaponLeftHand = 0,  
              Name = [[jungle guard 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_male.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_171]],  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_172]],  
                x = 25937.01563,  
                y = -2630.390625,  
                z = -10.328125
              },  
              Angle = 0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_169]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 2,  
              HairType = 8750,  
              HairColor = 4,  
              Tattoo = 10,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 2,  
              MorphTarget3 = 1,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 6,  
              MorphTarget8 = 4,  
              Sex = 0,  
              JacketModel = 5620014,  
              TrouserModel = 5619502,  
              FeetModel = 5618990,  
              HandsModel = 5619246,  
              ArmModel = 5619758,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 2,  
              WeaponRightHand = 5636654,  
              WeaponLeftHand = 0,  
              Name = [[jungle guard 2]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_male.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_175]],  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_176]],  
                x = 25979.90625,  
                y = -2600.296875,  
                z = -10.078125
              },  
              Angle = -2.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_173]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6705198,  
              HandsModel = 6703150,  
              ArmModel = 6704174,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Desert Forager]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              BotAttackable = 1
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act I: Nothing]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
    }
  }
}