scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    Road = 0,  
    TextManager = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ChatSequence = 0,  
    Position = 0,  
    NpcCustom = 0,  
    ActivityStep = 0,  
    LogicEntityBehavior = 0,  
    WayPoint = 0
  },  
  Acts = {
    {
      Cost = 11,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_22]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_20]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1478.3125,  
                x = 22390.9375,  
                InstanceId = [[Client1_23]],  
                Class = [[Position]],  
                z = 81.640625
              },  
              Angle = 2.109375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_26]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_24]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -1482.703125,  
                x = 22396.6875,  
                InstanceId = [[Client1_27]],  
                Class = [[Position]],  
                z = 83.890625
              },  
              Angle = -3.078125,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_70]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_72]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1484.328125,  
                    x = 22321.5,  
                    InstanceId = [[Client1_73]],  
                    Class = [[Position]],  
                    z = 62.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_78]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1486.46875,  
                    x = 22389.98438,  
                    InstanceId = [[Client1_79]],  
                    Class = [[Position]],  
                    z = 81.921875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_69]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_115]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_113]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -1325.484375,  
                x = 22408.75,  
                InstanceId = [[Client1_116]],  
                Class = [[Position]],  
                z = 73
              },  
              Angle = 2.296875,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_119]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_117]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 2]],  
              Position = {
                y = -1309.34375,  
                x = 22414.85938,  
                InstanceId = [[Client1_120]],  
                Class = [[Position]],  
                z = 71.265625
              },  
              Angle = -3.015625,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_123]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_121]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              Position = {
                y = -1313.609375,  
                x = 22412.96875,  
                InstanceId = [[Client1_124]],  
                Class = [[Position]],  
                z = 71.234375
              },  
              Angle = -3.046875,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_127]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_125]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 3]],  
              Position = {
                y = -1304.28125,  
                x = 22413.23438,  
                InstanceId = [[Client1_128]],  
                Class = [[Position]],  
                z = 69.75
              },  
              Angle = -2.609375,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_131]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_129]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 4]],  
              Position = {
                y = -1320.6875,  
                x = 22410.28125,  
                InstanceId = [[Client1_132]],  
                Class = [[Position]],  
                z = 72.015625
              },  
              Angle = 1.828125,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_135]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_133]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 5]],  
              Position = {
                y = -1325.21875,  
                x = 22403.21875,  
                InstanceId = [[Client1_136]],  
                Class = [[Position]],  
                z = 72.609375
              },  
              Angle = 2.03125,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_139]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_137]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              Position = {
                y = -1315.171875,  
                x = 22402.67188,  
                InstanceId = [[Client1_140]],  
                Class = [[Position]],  
                z = 69.765625
              },  
              Angle = -3.560333729,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_143]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_141]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -1326.390625,  
                x = 22404.90625,  
                InstanceId = [[Client1_144]],  
                Class = [[Position]],  
                z = 73.03125
              },  
              Angle = 2.65625,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_147]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_145]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1306.015625,  
                x = 22415.67188,  
                InstanceId = [[Client1_148]],  
                Class = [[Position]],  
                z = 71
              },  
              Angle = -3.988354206,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client1_300]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_302]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1335.15625,  
                    x = 22351.46875,  
                    InstanceId = [[Client1_303]],  
                    Class = [[Position]],  
                    z = 61.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_308]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1319.328125,  
                    x = 22389.14063,  
                    InstanceId = [[Client1_309]],  
                    Class = [[Position]],  
                    z = 68.890625
                  }
                }
              },  
              Position = {
                y = -1.59375,  
                x = 6.484375,  
                InstanceId = [[Client1_299]],  
                Class = [[Position]],  
                z = 1.984375
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 3]],  
              InstanceId = [[Client1_372]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_371]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_374]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1284.203125,  
                    x = 22399.39063,  
                    InstanceId = [[Client1_375]],  
                    Class = [[Position]],  
                    z = 63.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_377]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1310.046875,  
                    x = 22401.28125,  
                    InstanceId = [[Client1_378]],  
                    Class = [[Position]],  
                    z = 68.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_380]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1310.046875,  
                    x = 22401.28125,  
                    InstanceId = [[Client1_381]],  
                    Class = [[Position]],  
                    z = 68.5
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_419]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_418]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_421]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1307.703125,  
                    x = 22394.54688,  
                    InstanceId = [[Client1_422]],  
                    Class = [[Position]],  
                    z = 67.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_424]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1319.171875,  
                    x = 22391.14063,  
                    InstanceId = [[Client1_425]],  
                    Class = [[Position]],  
                    z = 69.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_427]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1305.5625,  
                    x = 22388.48438,  
                    InstanceId = [[Client1_428]],  
                    Class = [[Position]],  
                    z = 65.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_430]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1316.15625,  
                    x = 22396.70313,  
                    InstanceId = [[Client1_431]],  
                    Class = [[Position]],  
                    z = 69.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_433]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1311.90625,  
                    x = 22386.65625,  
                    InstanceId = [[Client1_434]],  
                    Class = [[Position]],  
                    z = 66.703125
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Events = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Version = 1,  
      Name = [[Permanent]],  
      Title = [[Permanent]]
    },  
    {
      Cost = 21,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
        [[Client1_33]],  
        [[Client1_81]],  
        [[Client1_212]],  
        [[Client1_244]],  
        [[Client1_293]],  
        [[Client1_313]],  
        [[Client1_385]],  
        [[Client1_502]],  
        [[Client1_504]]
      },  
      InstanceId = [[Client1_10]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_338]],  
              ActivitiesId = {
                [[Client1_385]]
              },  
              HairType = 6721838,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 6,  
              HandsModel = 6721582,  
              FeetColor = 3,  
              GabaritBreastSize = 12,  
              GabaritHeight = 11,  
              HairColor = 1,  
              EyesColor = 4,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 1,  
              HandsColor = 1,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_336]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_385]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_386]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_372]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 5,  
              FeetModel = 6720814,  
              Angle = -1.515625,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmModel = 6722862,  
              WeaponLeftHand = 0,  
              JacketModel = 6723630,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_339]],  
                x = 22397.26563,  
                y = -1247.9375,  
                z = 69.109375
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponRightHand = 6765614,  
              ArmColor = 2,  
              Name = [[Le Mid solo qui cheat]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 3,  
              Tattoo = 15
            }
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          InstanceId = [[Client1_276]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_275]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_272]],  
              ActivitiesId = {
                [[Client1_504]]
              },  
              HairType = 2350,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 10,  
              HandsModel = 6705710,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 7,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 10,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_270]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_504]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_505]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_486]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Mid]],  
                    InstanceId = [[Client1_486]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 10,  
                        InstanceId = [[Client1_487]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_488]],  
                            Who = [[Client1_272]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_506]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 6699310,  
              Angle = 2.28125,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              Tattoo = 24,  
              MorphTarget3 = 0,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6755630,  
              Sex = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              Sheet = [[ring_melee_damage_dealer_pierce_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[Lead des albs]],  
              Position = {
                y = -1315.75,  
                x = 22406.40625,  
                InstanceId = [[Client1_273]],  
                Class = [[Position]],  
                z = 70.234375
              },  
              JacketModel = 6706990,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_260]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 0,  
              HandsModel = 6705454,  
              FeetColor = 3,  
              GabaritBreastSize = 0,  
              GabaritHeight = 9,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 8,  
              HandsColor = 1,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_258]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 6699310,  
              Angle = 2.71875,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              Tattoo = 1,  
              MorphTarget3 = 0,  
              MorphTarget7 = 7,  
              WeaponRightHand = 6755630,  
              Sex = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 2,  
              Sheet = [[ring_melee_damage_dealer_pierce_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[Alb #1254788]],  
              Position = {
                y = -1317.296875,  
                x = 22412.82813,  
                InstanceId = [[Client1_261]],  
                Class = [[Position]],  
                z = 71.734375
              },  
              JacketModel = 6707246,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_268]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 9,  
              HandsModel = 6705454,  
              FeetColor = 2,  
              GabaritBreastSize = 13,  
              GabaritHeight = 13,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 11,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_266]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 6699310,  
              Angle = -2.765625,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              Tattoo = 10,  
              MorphTarget3 = 4,  
              MorphTarget7 = 5,  
              WeaponRightHand = 6756398,  
              Sex = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 1,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[Alb #251253]],  
              Position = {
                y = -1309.328125,  
                x = 22412.14063,  
                InstanceId = [[Client1_269]],  
                Class = [[Position]],  
                z = 70.40625
              },  
              JacketModel = 6701870,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_264]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 10,  
              HandsModel = 6705710,  
              FeetColor = 5,  
              GabaritBreastSize = 5,  
              GabaritHeight = 7,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_262]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6699566,  
              Angle = 2.265625,  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              Tattoo = 27,  
              MorphTarget3 = 2,  
              MorphTarget7 = 2,  
              WeaponRightHand = 6755374,  
              Sex = 1,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[Alb #1236579]],  
              Position = {
                y = -1322.875,  
                x = 22406.0625,  
                InstanceId = [[Client1_265]],  
                Class = [[Position]],  
                z = 72.09375
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 0,  
              ArmModel = 6706734,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_274]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_298]],  
          ActivitiesId = {
            [[Client1_313]]
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_297]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_279]],  
              ActivitiesId = {
              },  
              HairType = 8238,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 9,  
              HandsModel = 6737966,  
              FeetColor = 5,  
              GabaritBreastSize = 4,  
              GabaritHeight = 6,  
              HairColor = 1,  
              EyesColor = 6,  
              TrouserModel = 6738222,  
              GabaritLegsWidth = 14,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_277]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_313]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_314]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_335]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_494]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 15,  
                        InstanceId = [[Client1_495]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_496]],  
                            Who = [[Client1_279]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_497]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 6731822,  
              Angle = 0.609375,  
              Base = [[palette.entities.npcs.bandits.z_melee_dd_220]],  
              Tattoo = 21,  
              MorphTarget3 = 5,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6772014,  
              Sex = 1,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[jungle armsman 1]],  
              Position = {
                y = -1337.890625,  
                x = 22354.59375,  
                InstanceId = [[Client1_280]],  
                Class = [[Position]],  
                z = 63.9375
              },  
              JacketModel = 6739502,  
              WeaponLeftHand = 0,  
              ArmModel = 6738990,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_283]],  
              ActivitiesId = {
                [[Client1_293]]
              },  
              HairType = 8750,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 3,  
              HandsModel = 6737710,  
              FeetColor = 2,  
              GabaritBreastSize = 3,  
              GabaritHeight = 8,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 6738222,  
              GabaritLegsWidth = 7,  
              HandsColor = 5,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_281]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_293]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 3,  
              FeetModel = 6737198,  
              Angle = 0.328125,  
              Base = [[palette.entities.npcs.bandits.z_melee_dd_220]],  
              Tattoo = 13,  
              MorphTarget3 = 1,  
              MorphTarget7 = 0,  
              WeaponRightHand = 6771758,  
              Sex = 1,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 6,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[jungle armsman 2]],  
              Position = {
                y = -1340.484375,  
                x = 22352.8125,  
                InstanceId = [[Client1_284]],  
                Class = [[Position]],  
                z = 64.765625
              },  
              JacketModel = 6734126,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_zorai_female.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_287]],  
              ActivitiesId = {
              },  
              HairType = 5624366,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 10,  
              HandsModel = 6737710,  
              FeetColor = 2,  
              GabaritBreastSize = 7,  
              GabaritHeight = 9,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 6738222,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_285]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6731566,  
              Angle = 0.328125,  
              Base = [[palette.entities.npcs.bandits.z_melee_dd_220]],  
              Tattoo = 12,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              WeaponRightHand = 6771246,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 6,  
              Sheet = [[ring_melee_damage_dealer_pierce_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[jungle armsman 3]],  
              Position = {
                y = -1339.015625,  
                x = 22350.625,  
                InstanceId = [[Client1_288]],  
                Class = [[Position]],  
                z = 64.828125
              },  
              JacketModel = 6739502,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_zorai_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_291]],  
              ActivitiesId = {
              },  
              HairType = 8494,  
              TrouserColor = 5,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 11,  
              HandsModel = 6737710,  
              FeetColor = 0,  
              GabaritBreastSize = 9,  
              GabaritHeight = 2,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6738222,  
              GabaritLegsWidth = 7,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_289]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 6737198,  
              Angle = 0.328125,  
              Base = [[palette.entities.npcs.bandits.z_melee_dd_220]],  
              Tattoo = 24,  
              MorphTarget3 = 2,  
              MorphTarget7 = 4,  
              WeaponRightHand = 6772014,  
              Sex = 0,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 4,  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[jungle armsman 4]],  
              Position = {
                y = -1335.0625,  
                x = 22350.23438,  
                InstanceId = [[Client1_292]],  
                Class = [[Position]],  
                z = 63.90625
              },  
              JacketModel = 6739502,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_zorai_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_484]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 5,  
              MorphTarget5 = 3,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 14,  
              HandsModel = 6716206,  
              FeetColor = 5,  
              GabaritBreastSize = 9,  
              GabaritHeight = 8,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 0,  
              HandsColor = 3,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_482]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6710062,  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6717486,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 3,  
              JacketModel = 6712622,  
              WeaponRightHand = 6760494,  
              ArmColor = 1,  
              Name = [[forest armsman 6]],  
              Position = {
                y = -1330.421875,  
                x = 22348.54688,  
                InstanceId = [[Client1_485]],  
                Class = [[Position]],  
                z = 62.453125
              },  
              Sex = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 5,  
              Tattoo = 22
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_480]],  
              ActivitiesId = {
              },  
              HairType = 5622830,  
              TrouserColor = 5,  
              MorphTarget5 = 3,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 3,  
              HandsModel = 6716462,  
              FeetColor = 5,  
              GabaritBreastSize = 2,  
              GabaritHeight = 10,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 11,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_478]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 1,  
              FeetModel = 6710318,  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 3,  
              JacketModel = 6717998,  
              WeaponRightHand = 6761518,  
              ArmColor = 2,  
              Name = [[forest armsman 5]],  
              Position = {
                y = -1334.96875,  
                x = 22347.1875,  
                InstanceId = [[Client1_481]],  
                Class = [[Position]],  
                z = 64.21875
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_476]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 2,  
              HandsModel = 6716206,  
              FeetColor = 1,  
              GabaritBreastSize = 10,  
              GabaritHeight = 11,  
              HairColor = 2,  
              EyesColor = 4,  
              TrouserModel = 6716718,  
              GabaritLegsWidth = 12,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_474]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 6715694,  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6717486,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              JacketModel = 6712878,  
              WeaponRightHand = 6761518,  
              ArmColor = 5,  
              Name = [[forest armsman 4]],  
              Position = {
                y = -1338.53125,  
                x = 22347.29688,  
                InstanceId = [[Client1_477]],  
                Class = [[Position]],  
                z = 65
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_472]],  
              ActivitiesId = {
              },  
              HairType = 5422,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 9,  
              HandsModel = 6716462,  
              FeetColor = 3,  
              GabaritBreastSize = 13,  
              GabaritHeight = 0,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 6716718,  
              GabaritLegsWidth = 1,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_470]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6715950,  
              Angle = 0.90625,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6717486,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 1,  
              JacketModel = 6717998,  
              WeaponRightHand = 6761262,  
              ArmColor = 3,  
              Name = [[forest armsman 3]],  
              Position = {
                y = -1341.3125,  
                x = 22348.59375,  
                InstanceId = [[Client1_473]],  
                Class = [[Position]],  
                z = 65.21875
              },  
              Sex = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 2,  
              Tattoo = 24
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_468]],  
              ActivitiesId = {
              },  
              HairType = 5622574,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 0,  
              HandsModel = 6716206,  
              FeetColor = 5,  
              GabaritBreastSize = 14,  
              GabaritHeight = 4,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 6716718,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_466]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 6715950,  
              Angle = 0.90625,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_matis_male.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 2,  
              JacketModel = 6712878,  
              WeaponRightHand = 6761262,  
              ArmColor = 3,  
              Name = [[forest armsman 2]],  
              Position = {
                y = -1343.578125,  
                x = 22351.21875,  
                InstanceId = [[Client1_469]],  
                Class = [[Position]],  
                z = 65.140625
              },  
              Sex = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 0,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_464]],  
              ActivitiesId = {
              },  
              HairType = 5622574,  
              TrouserColor = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 11,  
              HandsModel = 6716206,  
              FeetColor = 5,  
              GabaritBreastSize = 2,  
              GabaritHeight = 12,  
              HairColor = 3,  
              EyesColor = 6,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 9,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_462]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 0,  
              FeetModel = 6715950,  
              Angle = 1.140625,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 6,  
              JacketModel = 6717998,  
              WeaponRightHand = 6760494,  
              ArmColor = 0,  
              Name = [[forest armsman 1]],  
              Position = {
                y = -1344.6875,  
                x = 22353.64063,  
                InstanceId = [[Client1_465]],  
                Class = [[Position]],  
                z = 64.9375
              },  
              Sex = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 6,  
              Tattoo = 7
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_296]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_461]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 4]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_460]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_459]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_437]],  
              ActivitiesId = {
                [[Client1_502]]
              },  
              HairType = 7470,  
              TrouserColor = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 12,  
              HandsModel = 6726958,  
              FeetColor = 3,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 6727470,  
              GabaritLegsWidth = 10,  
              HandsColor = 2,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_435]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[cheat]],  
                    InstanceId = [[Client1_490]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 12,  
                        InstanceId = [[Client1_491]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_492]],  
                            Who = [[Client1_437]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_493]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_502]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_503]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_490]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 6721070,  
              Angle = 2.625,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              JacketModel = 6728494,  
              WeaponRightHand = 6766382,  
              ArmColor = 5,  
              Name = [[Lead des hibs]],  
              Position = {
                y = -1311.546875,  
                x = 22391.20313,  
                InstanceId = [[Client1_438]],  
                Class = [[Position]],  
                z = 67.796875
              },  
              Sex = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 12
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_457]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 6,  
              HandsModel = 6726958,  
              FeetColor = 1,  
              GabaritBreastSize = 12,  
              GabaritHeight = 7,  
              HairColor = 0,  
              EyesColor = 0,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 7,  
              HandsColor = 5,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_455]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = 0.09375,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 6,  
              JacketModel = 6723630,  
              WeaponRightHand = 6766894,  
              ArmColor = 2,  
              Name = [[lakeland armsman 6]],  
              Position = {
                y = -1311.734375,  
                x = 22387.03125,  
                InstanceId = [[Client1_458]],  
                Class = [[Position]],  
                z = 66.90625
              },  
              Sex = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 6,  
              Tattoo = 14
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_441]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 14,  
              HandsModel = 6727214,  
              FeetColor = 4,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 6727470,  
              GabaritLegsWidth = 10,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_439]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 6,  
              FeetModel = 6726446,  
              Angle = 3.0625,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_pierce_f2.creature]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 6,  
              JacketModel = 6723374,  
              WeaponRightHand = 6766126,  
              ArmColor = 5,  
              Name = [[lakeland armsman 2]],  
              Position = {
                y = -1305.625,  
                x = 22388.20313,  
                InstanceId = [[Client1_442]],  
                Class = [[Position]],  
                z = 65.6875
              },  
              Sex = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 7,  
              Tattoo = 18
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_445]],  
              ActivitiesId = {
              },  
              HairType = 7214,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 6,  
              HandsModel = 6727214,  
              FeetColor = 1,  
              GabaritBreastSize = 2,  
              GabaritHeight = 13,  
              HairColor = 3,  
              EyesColor = 0,  
              TrouserModel = 6727470,  
              GabaritLegsWidth = 2,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_443]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 1,  
              FeetModel = 6726702,  
              Angle = 3.0625,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_tryker_male.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 5,  
              JacketModel = 6728494,  
              WeaponRightHand = 6766638,  
              ArmColor = 4,  
              Name = [[Empathe spé empathie]],  
              Position = {
                y = -1307.671875,  
                x = 22394.23438,  
                InstanceId = [[Client1_446]],  
                Class = [[Position]],  
                z = 67.421875
              },  
              Sex = 0,  
              MorphTarget7 = 0,  
              MorphTarget3 = 5,  
              Tattoo = 19
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_449]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 14,  
              HandsModel = 6727214,  
              FeetColor = 3,  
              GabaritBreastSize = 4,  
              GabaritHeight = 6,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 11,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_447]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 6720814,  
              Angle = 2.71875,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_slash_f2.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 7,  
              JacketModel = 6728750,  
              WeaponRightHand = 6766894,  
              ArmColor = 2,  
              Name = [[Vampiir cheaté]],  
              Position = {
                y = -1316.125,  
                x = 22396.60938,  
                InstanceId = [[Client1_450]],  
                Class = [[Position]],  
                z = 69.703125
              },  
              Sex = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 7,  
              Tattoo = 17
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_453]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 1,  
              HandsModel = 6726958,  
              FeetColor = 3,  
              GabaritBreastSize = 2,  
              GabaritHeight = 13,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 5,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_451]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6721070,  
              Angle = 1.5625,  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Sheet = [[ring_melee_damage_dealer_pierce_f2.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6727982,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 4,  
              JacketModel = 6723630,  
              WeaponRightHand = 6766126,  
              ArmColor = 2,  
              Name = [[Vampiir cheat 2]],  
              Position = {
                y = -1319.125,  
                x = 22391.07813,  
                InstanceId = [[Client1_454]],  
                Class = [[Position]],  
                z = 69.46875
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 4
            }
          },  
          Cost = 0
        }
      },  
      Events = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Version = 1,  
      Name = [[Act 1]],  
      Title = [[Act 1]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_498]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_500]],  
      Version = 1,  
      Events = {
      },  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_501]]
        }
      },  
      Name = [[Act 2]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_499]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Title = [[Act 2]],  
      ManualWeather = 0,  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 7,  
        InstanceId = [[Client1_31]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_32]],  
        Class = [[TextManagerEntry]],  
        Text = [[Dis Gus ... c'est quoi une toyota ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_38]],  
        Class = [[TextManagerEntry]],  
        Text = [[Tu commences a �tre lourd avec tes questions a la con ...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_44]],  
        Class = [[TextManagerEntry]],  
        Text = [[Tu commences a etre lourd avec tes questions a la con ...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_489]],  
        Class = [[TextManagerEntry]],  
        Text = [[Bon les mecs ce coup ci on EVEITE de se faire poutrer par un mid solo ... compris ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_493]],  
        Class = [[TextManagerEntry]],  
        Text = [[(Il est marrant lui il est full stuff �pique le mid ... en plus suis sur il cheat)]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_497]],  
        Class = [[TextManagerEntry]],  
        Text = [[**** INC !!!! ****]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_506]],  
        Count = 1,  
        Text = [[Bon les mecs ce coup ci on EVITE de se faire poutrer par un mid solo ... compris ?]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}