scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_547]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_549]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    Position = 0,  
    TextManager = 0,  
    Region = 0,  
    ActivityStep = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 5,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_550]],  
      ActivitiesIds = {
        [[Client1_592]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_556]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_554]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spawn Decoration]],  
              Position = {
                y = -1994.6875,  
                x = 26951.15625,  
                InstanceId = [[Client1_557]],  
                Class = [[Position]],  
                z = -7.625
              },  
              Angle = -2.09375,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_564]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_562]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kitin Sandbox]],  
              Position = {
                y = -2032.734375,  
                x = 27076.71875,  
                InstanceId = [[Client1_565]],  
                Class = [[Position]],  
                z = 14.96875
              },  
              Angle = 2.59375,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_568]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_566]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 1]],  
              Position = {
                y = -2012.46875,  
                x = 26935.59375,  
                InstanceId = [[Client1_569]],  
                Class = [[Position]],  
                z = -3.125
              },  
              Angle = 0.703125,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_572]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_570]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[This is Spawn]],  
              Position = {
                y = -2019.390625,  
                x = 26956.45313,  
                InstanceId = [[Client1_573]],  
                Class = [[Position]],  
                z = -8.875
              },  
              Angle = -1.015625,  
              Base = [[palette.entities.botobjects.roadsign]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_576]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_574]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_592]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_593]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Spectral Genius 1]],  
              Position = {
                y = -2005.109375,  
                x = 26943.53125,  
                InstanceId = [[Client1_577]],  
                Class = [[Position]],  
                z = -6.90625
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_b]],  
              ActivitiesId = {
                [[Client1_592]]
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kami Region]],  
              InstanceId = [[Client1_579]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_578]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_581]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2015.015625,  
                    x = 26941.07813,  
                    InstanceId = [[Client1_582]],  
                    Class = [[Position]],  
                    z = -3.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_584]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1990.53125,  
                    x = 26935.17188,  
                    InstanceId = [[Client1_585]],  
                    Class = [[Position]],  
                    z = -7.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_587]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1975.796875,  
                    x = 26960.73438,  
                    InstanceId = [[Client1_588]],  
                    Class = [[Position]],  
                    z = -8.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_590]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2001.890625,  
                    x = 26966.4375,  
                    InstanceId = [[Client1_591]],  
                    Class = [[Position]],  
                    z = -7.921875
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_551]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    },  
    {
      Cost = 3,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_552]],  
      ActivitiesIds = {
        [[Client1_598]],  
        [[Client1_604]],  
        [[Client1_610]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_596]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_597]],  
                x = 26959.51563,  
                y = -1998.84375,  
                z = -8.203125
              },  
              Angle = -0.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_594]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_598]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_599]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_598]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_602]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_603]],  
                x = 26958.35938,  
                y = -1981.6875,  
                z = -7.71875
              },  
              Angle = 1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_600]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_604]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_605]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_604]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_608]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_609]],  
                x = 26938.60938,  
                y = -1992.640625,  
                z = -7.8125
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_606]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_610]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_611]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_579]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_610]]
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 4]]
            }
          },  
          InstanceId = [[Client1_553]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_548]]
  }
}