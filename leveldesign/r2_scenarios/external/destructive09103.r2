scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_2181]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_2183]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    ActivityStep = 0,  
    TextManager = 0,  
    Position = 0,  
    Behavior = 0,  
    ActionStep = 0,  
    Npc = 0,  
    ActionType = 0,  
    MapDescription = 0,  
    EventType = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2185]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_2184]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client1_2203]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
      },  
      Version = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2204]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Title = [[Act 1]],  
      States = {
      }
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
        [[Client1_2192]]
      },  
      Title = [[Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_2190]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2188]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_2194]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_2195]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_2196]],  
                            Who = [[Client1_2190]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_2206]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_2199]],  
                    Event = {
                      Type = [[end of chat step]],  
                      InstanceId = [[Client1_2198]],  
                      Value = r2.RefId([[Client1_2195]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_2200]],  
                          Value = r2.RefId([[Client1_2192]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_2201]],  
                        Entity = r2.RefId([[Client1_2190]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_2192]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_2193]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_2194]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              Position = {
                y = -2018.0625,  
                x = 26944.10938,  
                InstanceId = [[Client1_2191]],  
                Class = [[Position]],  
                z = -2.953125
              },  
              Angle = 0.28125,  
              Base = [[palette.entities.creatures.chcdb7]],  
              ActivitiesId = {
                [[Client1_2192]]
              }
            }
          },  
          InstanceId = [[Client1_2187]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_2186]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2182]],  
    Texts = {
      {
        Count = 1,  
        InstanceId = [[Client1_2197]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2205]],  
        Count = 1,  
        Text = [[LOOP]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2206]],  
        Count = 1,  
        Text = [[LOOP]]
      }
    }
  }
}