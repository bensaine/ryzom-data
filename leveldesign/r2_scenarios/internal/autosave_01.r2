scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      InstanceId = [[Client1_384]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts01]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[The Botoga's Drop (Desert 01)]],  
      Season = [[Winter]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_375]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Name = [[New scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_373]]
  },  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_374]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    NpcCustom = 0,  
    MapDescription = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 1,  
    RegionVertex = 0,  
    TextManager = 0,  
    Position = 0,  
    Location = 1,  
    Road = 0,  
    LogicEntityBehavior = 1,  
    WayPoint = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_371]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_378]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_376]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_377]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Road]],  
              Name = [[Route 1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21504.6543,  
                    y = -1299.435181,  
                    z = 79.54059601,  
                    InstanceId = [[Client1_389]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_388]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21505.1875,  
                    y = -1302.547729,  
                    z = 80.69772339,  
                    InstanceId = [[Client1_392]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_391]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21481.77734,  
                    y = -1307.092163,  
                    z = 78.80776978,  
                    InstanceId = [[Client1_395]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_394]]
                }
              },  
              Position = {
                Class = [[Position]],  
                x = 0,  
                y = 0,  
                z = 0,  
                InstanceId = [[Client1_385]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_386]]
            },  
            {
              Class = [[Road]],  
              Name = [[Route 2]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21501.54102,  
                    y = -1295.741577,  
                    z = 78.0508194,  
                    InstanceId = [[Client1_406]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_405]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21481.86914,  
                    y = -1299.120117,  
                    z = 77.11299896,  
                    InstanceId = [[Client1_409]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_408]]
                },  
                {
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21474.03516,  
                    y = -1287.078003,  
                    z = 75.19921875,  
                    InstanceId = [[Client1_412]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_411]]
                }
              },  
              Position = {
                Class = [[Position]],  
                x = 0,  
                y = 0,  
                z = 0,  
                InstanceId = [[Client1_402]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_403]]
            },  
            {
              Class = [[Region]],  
              Name = [[Place 1]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21508.94336,  
                    y = -1280.991943,  
                    z = 75.04260254,  
                    InstanceId = [[Client1_421]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_420]]
                },  
                {
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21505.72656,  
                    y = -1285.463989,  
                    z = 75.4831543,  
                    InstanceId = [[Client1_424]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_423]]
                },  
                {
                  Class = [[RegionVertex]],  
                  Position = {
                    Class = [[Position]],  
                    x = 21513.55664,  
                    y = -1289.119873,  
                    z = 75.94019318,  
                    InstanceId = [[Client1_427]]
                  },  
                  InheritPos = 1,  
                  InstanceId = [[Client1_426]]
                }
              },  
              Position = {
                Class = [[Position]],  
                x = 0,  
                y = 0,  
                z = 0,  
                InstanceId = [[Client1_417]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_418]]
            }
          },  
          InstanceId = [[Client1_379]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Permanent]],  
      Version = 5
    },  
    {
      InstanceId = [[Client1_382]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_380]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_381]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 118,  
      LocationId = [[Client1_384]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Angle = 0.2890985608,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_400]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_401]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_386]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_396]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 21500.4082,  
                y = -1298.699219,  
                z = 78.96826172,  
                InstanceId = [[Client1_399]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_398]],  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 14,  
              HairColor = 5,  
              Tattoo = 11,  
              EyesColor = 5,  
              MorphTarget1 = 2,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 0,  
              MorphTarget8 = 7,  
              Sex = 1,  
              HairType = 2862,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              JacketColor = 3,  
              TrouserColor = 3,  
              FeetColor = 4,  
              HandsColor = 2,  
              ArmColor = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Name = [[Deseus]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Angle = 2.753823996,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_428]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_429]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_418]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_413]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 21515.79297,  
                y = -1283.891602,  
                z = 75.18018341,  
                InstanceId = [[Client1_416]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_415]],  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 11,  
              HairColor = 1,  
              Tattoo = 8,  
              EyesColor = 0,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 1,  
              MorphTarget4 = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              Sex = 1,  
              HairType = 7470,  
              JacketModel = 0,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              JacketColor = 4,  
              TrouserColor = 0,  
              FeetColor = 1,  
              HandsColor = 5,  
              ArmColor = 5,  
              SheetClient = [[basic_tryker_female.creature]],  
              Name = [[Be'Riplan]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_383]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 1:Act 1]],  
      Version = 5
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_372]]
  }
}