scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts14]],  
      Time = 0,  
      Name = [[Wooded Canyon (Desert 14)]],  
      Season = [[winter]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_3]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
    {
      SheetId = 8664622,  
      Name = [[Atome]],  
      InstanceId = [[Client1_320]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    },  
    {
      SheetId = 8656942,  
      Name = [[Shell]],  
      InstanceId = [[Client1_324]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    },  
    {
      SheetId = 8665390,  
      Name = [[Plot item]],  
      InstanceId = [[Client1_331]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    }
  },  
  Name = [[Cache cache]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_4]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 2,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1]]
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    GiveItem = 0,  
    TextManager = 0,  
    TextManagerEntry = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    DefaultFeature = 0,  
    ChatSequence = 0,  
    ActionStep = 0,  
    Region = 0,  
    PlotItem = 0,  
    Road = 0,  
    ZoneTrigger = 1,  
    NpcCreature = 0,  
    EventType = 0,  
    RegionVertex = 0,  
    ActivityStep = 1,  
    NpcCustom = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    WayPoint = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_8]],  
      Behavior = {
        InstanceId = [[Client1_6]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Version = 5,  
      Name = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_7]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Road]],  
              InstanceId = [[Client1_185]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_184]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_187]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_188]],  
                    x = 30704.9375,  
                    y = -2120,  
                    z = 89.75
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_190]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_191]],  
                    x = 30677.79688,  
                    y = -2071.75,  
                    z = 92.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_193]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_194]],  
                    x = 30642.8125,  
                    y = -1969.25,  
                    z = 71.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_196]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_197]],  
                    x = 30569.03125,  
                    y = -1959.296875,  
                    z = 73.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_199]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_200]],  
                    x = 30506.40625,  
                    y = -1951.953125,  
                    z = 75.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_202]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_203]],  
                    x = 30444.25,  
                    y = -2003.578125,  
                    z = 73.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_205]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_206]],  
                    x = 30421.35938,  
                    y = -2041.890625,  
                    z = 68.65625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_208]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_209]],  
                    x = 30384.67188,  
                    y = -2140.890625,  
                    z = 69.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_211]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_212]],  
                    x = 30423.95313,  
                    y = -2227.6875,  
                    z = 78.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_214]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_215]],  
                    x = 30503.67188,  
                    y = -2270.765625,  
                    z = 80.359375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_217]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_218]],  
                    x = 30611.54688,  
                    y = -2292.65625,  
                    z = 90.625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_220]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_221]],  
                    x = 30723.07813,  
                    y = -2243.25,  
                    z = 84.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_223]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_224]],  
                    x = 30729.82813,  
                    y = -2179.703125,  
                    z = 89.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_226]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_227]],  
                    x = 30730.875,  
                    y = -2179.859375,  
                    z = 89.21875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_239]],  
              Name = [[Route 2]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_238]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_241]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_242]],  
                    x = 30650.32813,  
                    y = -2243.109375,  
                    z = 83.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_244]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_245]],  
                    x = 30556.76563,  
                    y = -2117.78125,  
                    z = 78.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_247]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_248]],  
                    x = 30427.79688,  
                    y = -2131.34375,  
                    z = 69.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_250]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_251]],  
                    x = 30467.125,  
                    y = -2212.53125,  
                    z = 80.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_253]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_254]],  
                    x = 30563.15625,  
                    y = -2200.53125,  
                    z = 82.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_256]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_257]],  
                    x = 30687.375,  
                    y = -2138.265625,  
                    z = 92.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_259]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_260]],  
                    x = 30690.8125,  
                    y = -2039.859375,  
                    z = 93.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_262]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_263]],  
                    x = 30572.4375,  
                    y = -2039.609375,  
                    z = 75.421875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_265]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_266]],  
                    x = 30543.42188,  
                    y = -2106.25,  
                    z = 77.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_268]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_269]],  
                    x = 30568.45313,  
                    y = -2177.53125,  
                    z = 83.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_271]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_272]],  
                    x = 30570.3125,  
                    y = -2181.953125,  
                    z = 84.078125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_289]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_290]],  
                x = 30656.57813,  
                y = -2293.59375,  
                z = 86.921875
              },  
              Angle = -3.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_287]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      LocationId = [[]],  
      ManualWeather = 0
    },  
    {
      InstanceId = [[Client1_12]],  
      Behavior = {
        InstanceId = [[Client1_10]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_232]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_233]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          },  
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_307]],  
            Conditions = {
            },  
            Actions = {
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_308]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Version = 5,  
      Name = [[Act 1:Act 1]],  
      WeatherValue = 19,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[]],  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_11]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_102]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_103]],  
                x = 30641.1875,  
                y = -2240.234375,  
                z = 83.75
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_100]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_285]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_286]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_185]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Be'Marrell]],  
              Sex = 0,  
              Level = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_111]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_113]],  
                x = 30636.23438,  
                y = -2242.421875,  
                z = 83.671875
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_112]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_283]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_284]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_87]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Be'Nan]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_121]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_123]],  
                x = 30635.32813,  
                y = -2230.8125,  
                z = 83.578125
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_122]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_279]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_280]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_239]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Mac'Wiley]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_131]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_133]],  
                x = 30625.51563,  
                y = -2233.125,  
                z = 83.90625
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_132]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_281]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_282]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_239]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Mac'darrroy]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_141]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_143]],  
                x = 30631.84375,  
                y = -2233.5,  
                z = 83.640625
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_142]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_277]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_278]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_70]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_335]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_336]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_19]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_337]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_338]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_36]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_339]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_340]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_53]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_341]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_342]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_70]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_343]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_344]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_87]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Ba'Massey]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_151]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_153]],  
                x = 30627.42188,  
                y = -2240.15625,  
                z = 84
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_152]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_273]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_274]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_239]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Mac'arnin]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_161]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_163]],  
                x = 30632.42188,  
                y = -2246.65625,  
                z = 83.625
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_162]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_275]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_276]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_185]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Be'laury]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_171]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_173]],  
                x = 30645.25,  
                y = -2236.703125,  
                z = 83.84375
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_172]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_236]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_237]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_185]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Ba'Laupsey]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_181]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_183]],  
                x = 30642.25,  
                y = -2246.0625,  
                z = 83.265625
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_182]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_234]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_235]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_185]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 24,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 1,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[O'Darghan]],  
              Sex = 0,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_293]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_294]],  
                x = 30646.1875,  
                y = -2295.3125,  
                z = 89.234375
              },  
              Angle = -2.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_291]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_304]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_311]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_314]],  
                        Entity = r2.RefId([[Client1_297]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_313]],  
                          Type = [[starts dialog]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_312]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 6958,  
              HairColor = 5,  
              Tattoo = 21,  
              EyesColor = 6,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 7,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 5653550,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Ba'Gan]],  
              Sex = 1,  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              InstanceId = [[Client1_382]],  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_380]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_384]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_385]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = [[Client1_19]],  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Hunt In Zone]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_386]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_387]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = [[Client1_36]],  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Hunt In Zone]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_388]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_389]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = [[Client1_53]],  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Hunt In Zone]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_390]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_391]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = [[Client1_70]],  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Hunt In Zone]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_392]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_393]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = [[Client1_87]],  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Hunt In Zone]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Rakan]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_383]],  
                x = 30555.01563,  
                y = -2132.75,  
                z = 80
              },  
              Angle = -2.729263306,  
              Base = [[palette.entities.creatures.ccelc7]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          InstanceId = [[Client1_15]],  
          Behavior = {
            InstanceId = [[Client1_16]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_333]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_334]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_346]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_349]],  
                    Entity = r2.RefId([[Client1_141]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_348]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_337]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_402]],  
                    Entity = r2.RefId([[Client1_382]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_401]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_384]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_347]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_17]],  
            x = 30628.84375,  
            y = -2303.0625,  
            z = 95
          },  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_19]],  
              Name = [[Zone 1]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_21]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_22]],  
                    x = 23.453125,  
                    y = 20.421875,  
                    z = -13.96875
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_24]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_25]],  
                    x = -6.5,  
                    y = 35.453125,  
                    z = -8.078125
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_27]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_28]],  
                    x = -29.234375,  
                    y = -5.734375,  
                    z = -2.59375
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_30]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_31]],  
                    x = 0.484375,  
                    y = -19.953125,  
                    z = -0.25
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              },  
              Deletable = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18]],  
                x = -1.4375,  
                y = 5.359375,  
                z = -0.625
              }
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_19]]
        },  
        {
          InstanceId = [[Client1_32]],  
          Behavior = {
            InstanceId = [[Client1_33]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_351]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_354]],  
                    Entity = r2.RefId([[Client1_141]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_353]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_339]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_406]],  
                    Entity = r2.RefId([[Client1_382]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_405]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_386]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_352]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone 2]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_34]],  
            x = 30716.46875,  
            y = -2134.359375,  
            z = 93.53125
          },  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_36]],  
              Name = [[Places 1]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_38]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_39]],  
                    x = 42.265625,  
                    y = -16.421875,  
                    z = -4.875
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_41]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_42]],  
                    x = -8.875,  
                    y = 40.21875,  
                    z = -2.671875
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_44]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_45]],  
                    x = -29.390625,  
                    y = -1.078125,  
                    z = -2.296875
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_47]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_48]],  
                    x = -3.59375,  
                    y = -35.953125,  
                    z = -3.140625
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              },  
              Deletable = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_35]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_36]]
        },  
        {
          InstanceId = [[Client1_49]],  
          Behavior = {
            InstanceId = [[Client1_50]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_356]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_359]],  
                    Entity = r2.RefId([[Client1_141]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_358]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_341]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_400]],  
                    Entity = r2.RefId([[Client1_382]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_399]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_388]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_357]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone 3]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_51]],  
            x = 30607.75,  
            y = -1942.125,  
            z = 73
          },  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_53]],  
              Name = [[Places 2]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_55]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_56]],  
                    x = 39.125,  
                    y = -4.15625,  
                    z = 4.953125
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_58]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_59]],  
                    x = -7.890625,  
                    y = 17.21875,  
                    z = 1.453125
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_61]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_62]],  
                    x = -22.640625,  
                    y = -30.4375,  
                    z = -1.078125
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_64]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_65]],  
                    x = 1.734375,  
                    y = -45.203125,  
                    z = -4.109375
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              },  
              Deletable = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_52]],  
                x = -0.28125,  
                y = 0,  
                z = 0
              }
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_53]]
        },  
        {
          InstanceId = [[Client1_66]],  
          Behavior = {
            InstanceId = [[Client1_67]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_361]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_364]],  
                    Entity = r2.RefId([[Client1_141]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_363]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_343]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_404]],  
                    Entity = r2.RefId([[Client1_382]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_403]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_390]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_362]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone 4]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_68]],  
            x = 30467.45313,  
            y = -1921.765625,  
            z = 69
          },  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_70]],  
              Name = [[Places 3]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_72]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_73]],  
                    x = 32.09375,  
                    y = -10.015625,  
                    z = 0.9375
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_75]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_76]],  
                    x = 1.921875,  
                    y = 37.078125,  
                    z = -4.09375
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_78]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_79]],  
                    x = -26.546875,  
                    y = -10.515625,  
                    z = -2.0625
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_81]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_82]],  
                    x = -6.640625,  
                    y = -39.75,  
                    z = 0.828125
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              },  
              Deletable = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_69]],  
                x = 0.25,  
                y = 0.4375,  
                z = 0.015625
              }
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_70]]
        },  
        {
          InstanceId = [[Client1_83]],  
          Behavior = {
            InstanceId = [[Client1_84]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_366]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_369]],  
                    Entity = r2.RefId([[Client1_141]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_368]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_335]])
                    }
                  },  
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_398]],  
                    Entity = r2.RefId([[Client1_382]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_397]],  
                      Type = [[begin activity sequence]],  
                      Value = r2.RefId([[Client1_392]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_367]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          Class = [[ZoneTrigger]],  
          Name = [[Zone 5]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_85]],  
            x = 30497.32813,  
            y = -2252.328125,  
            z = 80.296875
          },  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_87]],  
              Name = [[Places 4]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_89]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_90]],  
                    x = 47.71875,  
                    y = -23.78125,  
                    z = 9.390625
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_92]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_93]],  
                    x = 44.359375,  
                    y = 47.453125,  
                    z = 4.859375
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_95]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_96]],  
                    x = -35.671875,  
                    y = 31.015625,  
                    z = 1.40625
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_98]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_99]],  
                    x = -5.828125,  
                    y = -13.234375,  
                    z = 3.234375
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              },  
              Deletable = 0,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_86]],  
                x = -83.671875,  
                y = 10.15625,  
                z = -8.234375
              }
            }
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          _Zone = [[Client1_87]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_297]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_298]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_299]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_293]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_300]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_301]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_302]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_293]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_316]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_394]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_395]],  
                  Emote = [[]],  
                  Who = r2.RefId([[Client1_293]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_396]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Behavior = {
            InstanceId = [[Client1_295]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_296]],  
            x = 30646.4375,  
            y = -2297.609375,  
            z = 89
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 0
        },  
        {
          MissionText = [[I give you <qt1> <item1>.]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[<mission_giver> thanks you.]],  
          Behavior = {
            InstanceId = [[Client1_318]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[GiveItem]],  
          Item1Qty = 10,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.bot_request_item]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[Take items]],  
          MissionGiver = r2.RefId([[Client1_141]]),  
          Item1Id = r2.RefId([[Client1_320]]),  
          InheritPos = 1,  
          _Seed = 1144351019,  
          Name = [[Give Item Mission 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_319]],  
            x = 30645,  
            y = -2242.40625,  
            z = 83.71875
          },  
          InstanceId = [[Client1_317]],  
          Active = 1,  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        },  
        {
          MissionText = [[I give you <qt1> <item1>.]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[<mission_giver> thanks you.]],  
          Behavior = {
            InstanceId = [[Client1_322]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[GiveItem]],  
          Item1Qty = 10,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.bot_request_item]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[Take items]],  
          MissionGiver = r2.RefId([[Client1_151]]),  
          Item1Id = r2.RefId([[Client1_324]]),  
          InheritPos = 1,  
          _Seed = 1144351269,  
          Name = [[Give Item Mission 2]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_323]],  
            x = 30645.67188,  
            y = -2244.40625,  
            z = 83
          },  
          InstanceId = [[Client1_321]],  
          Active = 1,  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        },  
        {
          MissionText = [[I give you <qt1> <item1>.]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[<mission_giver> thanks you.]],  
          Behavior = {
            InstanceId = [[Client1_326]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[GiveItem]],  
          Item1Qty = 10,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.bot_request_item]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[Take items]],  
          MissionGiver = r2.RefId([[Client1_151]]),  
          Item1Id = r2.RefId([[Client1_324]]),  
          InheritPos = 1,  
          _Seed = 1144351328,  
          Name = [[Give Item Mission 3]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_327]],  
            x = 30651.39063,  
            y = -2238.28125,  
            z = 83
          },  
          InstanceId = [[Client1_325]],  
          Active = 1,  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        },  
        {
          MissionText = [[I give you <qt1> <item1>.]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[<mission_giver> thanks you.]],  
          Behavior = {
            InstanceId = [[Client1_329]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Class = [[GiveItem]],  
          Item1Qty = 1,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.bot_request_item]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[Take items]],  
          MissionGiver = r2.RefId([[]]),  
          Item1Id = r2.RefId([[Client1_331]]),  
          InheritPos = 1,  
          _Seed = 1144351386,  
          Name = [[Give Item Mission 4]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_330]],  
            x = 30647.5,  
            y = -2240.109375,  
            z = 83
          },  
          InstanceId = [[Client1_328]],  
          Active = 1,  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        }
      },  
      LocationId = [[Client1_14]],  
      ManualWeather = 1
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_300]],  
        Count = 6,  
        Text = [[Hello, can you please go and find my husband??]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_303]],  
        Count = 2,  
        Text = [[He will give some atomes, and wander somehow in this region.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_316]],  
        Count = 2,  
        Text = [[He will give some atomes, and  was wandering somehow in this region.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_396]],  
        Count = 1,  
        Text = [[Be Careful Rakan is also wandering there.]]
      }
    }
  }
}