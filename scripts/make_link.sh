#!/bin/bash

if [[ "$OSTYPE" == "msys" ]]
then
	src=$(cygpath.exe  -w -a "$1")
	cat > create_shortcut.vbs <<END
Set oWS = WScript.CreateObject("WScript.Shell")
sLinkFile = "$2.lnk"
Set oLink = oWS.CreateShortcut(sLinkFile)
    oLink.TargetPath = "$src"
    oLink.Description = "$2"   
    oLink.IconLocation = "$src, 0"
    oLink.WorkingDirectory = "$(dirname $src)"
oLink.Save
END
	cscript create_shortcut.vbs
	rm create_shortcut.vbs
else
	ln -s $1 $2
fi
