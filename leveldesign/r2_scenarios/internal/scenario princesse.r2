scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts09]],  
      Time = 0,  
      Name = [[Two Ponds (Desert 09)]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_3]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_4]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 3,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1]]
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    ChatAction = 0,  
    ActivityStep = 1,  
    ChatStep = 0,  
    ChatSequence = 0,  
    EventType = 0,  
    Npc = 0,  
    ActionStep = 0,  
    Road = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 1,  
    WayPoint = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_8]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_6]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Version = 5,  
      Name = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_670]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_668]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1241.953125,  
                x = 27691.98438,  
                InstanceId = [[Client1_671]],  
                Class = [[Position]],  
                z = 77.25
              },  
              Angle = 3.421875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_674]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_672]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1249.90625,  
                x = 27696.48438,  
                InstanceId = [[Client1_675]],  
                Class = [[Position]],  
                z = 78.953125
              },  
              Angle = 3.171875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_678]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_676]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 3]],  
              Position = {
                y = -1258.5,  
                x = 27692.45313,  
                InstanceId = [[Client1_679]],  
                Class = [[Position]],  
                z = 76.984375
              },  
              Angle = 2.546875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_682]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_680]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              Position = {
                y = -1195.71875,  
                x = 27677.85938,  
                InstanceId = [[Client1_683]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = -1.0625,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_686]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_684]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              Position = {
                y = -1205.015625,  
                x = 27686.6875,  
                InstanceId = [[Client1_687]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_690]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_688]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 3]],  
              Position = {
                y = -1221.125,  
                x = 27680.95313,  
                InstanceId = [[Client1_691]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = -3.015625,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_694]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_692]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 4]],  
              Position = {
                y = -1216.40625,  
                x = 27668.79688,  
                InstanceId = [[Client1_695]],  
                Class = [[Position]],  
                z = 74.640625
              },  
              Angle = 0.203125,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_698]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_696]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 1]],  
              Position = {
                y = -1253.765625,  
                x = 27642.25,  
                InstanceId = [[Client1_699]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              Angle = 1.0625,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_702]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_700]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 2]],  
              Position = {
                y = -1229.0625,  
                x = 27625.70313,  
                InstanceId = [[Client1_703]],  
                Class = [[Position]],  
                z = 75.546875
              },  
              Angle = -0.015625,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_706]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_704]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 3]],  
              Position = {
                y = -1223.859375,  
                x = 27646.46875,  
                InstanceId = [[Client1_707]],  
                Class = [[Position]],  
                z = 75.84375
              },  
              Angle = -1.21875,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_714]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_712]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[zorai tent 4]],  
              Position = {
                y = -1244.234375,  
                x = 27629.10938,  
                InstanceId = [[Client1_715]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = 0.046875,  
              Base = [[palette.entities.botobjects.tent_zorai]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_718]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_716]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              Position = {
                y = -1221.984375,  
                x = 27652.15625,  
                InstanceId = [[Client1_719]],  
                Class = [[Position]],  
                z = 75.125
              },  
              Angle = 5.078125,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_722]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_720]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 1]],  
              Position = {
                y = -1268.953125,  
                x = 27646.89063,  
                InstanceId = [[Client1_723]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              Angle = -6.25,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_726]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_724]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 2]],  
              Position = {
                y = -1285.875,  
                x = 27653.59375,  
                InstanceId = [[Client1_727]],  
                Class = [[Position]],  
                z = 74.734375
              },  
              Angle = -5.234375,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_730]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_728]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 3]],  
              Position = {
                y = -1286.21875,  
                x = 27667.4375,  
                InstanceId = [[Client1_731]],  
                Class = [[Position]],  
                z = 74.890625
              },  
              Angle = 0.25,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_734]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_732]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[matis tent 4]],  
              Position = {
                y = -1285.71875,  
                x = 27628.9375,  
                InstanceId = [[Client1_735]],  
                Class = [[Position]],  
                z = 74.671875
              },  
              Angle = 0.25,  
              Base = [[palette.entities.botobjects.tent_matis]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_738]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_736]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1238.140625,  
                x = 27661.14063,  
                InstanceId = [[Client1_739]],  
                Class = [[Position]],  
                z = 75.3125
              },  
              Angle = 3.109375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_742]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_740]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -1250.453125,  
                x = 27646.09375,  
                InstanceId = [[Client1_743]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = 2.4375,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_746]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_744]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              Position = {
                y = -1250.3125,  
                x = 27671.96875,  
                InstanceId = [[Client1_747]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              Angle = 2.984375,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_750]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_748]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[crate 1]],  
              Position = {
                y = -1254.125,  
                x = 27688.5625,  
                InstanceId = [[Client1_751]],  
                Class = [[Position]],  
                z = 76.265625
              },  
              Angle = 2.34375,  
              Base = [[palette.entities.botobjects.crate1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_754]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_752]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 crates 1]],  
              Position = {
                y = -1241.75,  
                x = 27687,  
                InstanceId = [[Client1_755]],  
                Class = [[Position]],  
                z = 75.875
              },  
              Angle = 2.34375,  
              Base = [[palette.entities.botobjects.crate2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_758]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_756]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 barrels 1]],  
              Position = {
                y = -1278.515625,  
                x = 27662.625,  
                InstanceId = [[Client1_759]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              Angle = 2.34375,  
              Base = [[palette.entities.botobjects.barrels_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_762]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_760]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[counter 1]],  
              Position = {
                y = -1265.640625,  
                x = 27655.67188,  
                InstanceId = [[Client1_763]],  
                Class = [[Position]],  
                z = 74.609375
              },  
              Angle = 0.796875,  
              Base = [[palette.entities.botobjects.counter]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_766]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_764]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[counter 2]],  
              Position = {
                y = -1246,  
                x = 27683.5625,  
                InstanceId = [[Client1_767]],  
                Class = [[Position]],  
                z = 75.265625
              },  
              Angle = 3.40625,  
              Base = [[palette.entities.botobjects.counter]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_770]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_768]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[counter 3]],  
              Position = {
                y = -1223.5,  
                x = 27660.03125,  
                InstanceId = [[Client1_771]],  
                Class = [[Position]],  
                z = 74.859375
              },  
              Angle = -1.78125,  
              Base = [[palette.entities.botobjects.counter]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_774]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_772]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1240.625,  
                x = 27661.45313,  
                InstanceId = [[Client1_775]],  
                Class = [[Position]],  
                z = 75.265625
              },  
              Angle = 2.609375,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_778]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_776]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[old chest 1]],  
              Position = {
                y = -1228.375,  
                x = 27645.96875,  
                InstanceId = [[Client1_779]],  
                Class = [[Position]],  
                z = 76.171875
              },  
              Angle = -1.25,  
              Base = [[palette.entities.botobjects.chest_old]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_782]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_780]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp off 1]],  
              Position = {
                y = -1238.953125,  
                x = 27686.65625,  
                InstanceId = [[Client1_783]],  
                Class = [[Position]],  
                z = 75.765625
              },  
              Angle = 3.03125,  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_786]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_784]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp off 2]],  
              Position = {
                y = -1220.046875,  
                x = 27674.5625,  
                InstanceId = [[Client1_787]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              Angle = -2.21875,  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_790]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_788]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp off 3]],  
              Position = {
                y = -1273.25,  
                x = 27659.67188,  
                InstanceId = [[Client1_791]],  
                Class = [[Position]],  
                z = 74.875
              },  
              Angle = 1.359375,  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_794]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_792]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp off 4]],  
              Position = {
                y = -1248.203125,  
                x = 27638.26563,  
                InstanceId = [[Client1_795]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = 1.359375,  
              Base = [[palette.entities.botobjects.street_lamp_off]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_798]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_796]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 2]],  
              Position = {
                y = -1225.34375,  
                x = 27654.15625,  
                InstanceId = [[Client1_799]],  
                Class = [[Position]],  
                z = 75.234375
              },  
              Angle = 0.0625,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_802]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_800]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 2]],  
              Position = {
                y = -1230.203125,  
                x = 27681.14063,  
                InstanceId = [[Client1_803]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              Angle = -2.71875,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_806]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_804]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 3]],  
              Position = {
                y = -1235.28125,  
                x = 27683.21875,  
                InstanceId = [[Client1_807]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              Angle = -2.84375,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_810]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_808]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -1256.96875,  
                x = 27673.875,  
                InstanceId = [[Client1_811]],  
                Class = [[Position]],  
                z = 74.921875
              },  
              Angle = -4.0625,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_814]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_812]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -1197.5625,  
                x = 27608.35938,  
                InstanceId = [[Client1_815]],  
                Class = [[Position]],  
                z = 73.671875
              },  
              Angle = -0.984375,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_818]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_816]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 2]],  
              Position = {
                y = -1278.390625,  
                x = 27613.01563,  
                InstanceId = [[Client1_819]],  
                Class = [[Position]],  
                z = 74.953125
              },  
              Angle = 0.734375,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_822]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_820]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 3]],  
              Position = {
                y = -1230.46875,  
                x = 27710.57813,  
                InstanceId = [[Client1_823]],  
                Class = [[Position]],  
                z = 80.28125
              },  
              Angle = 3.15625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_826]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_824]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              Position = {
                y = -1243.953125,  
                x = 27722.3125,  
                InstanceId = [[Client1_827]],  
                Class = [[Position]],  
                z = 81.78125
              },  
              Angle = 2.75,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_841]],  
              ActivitiesId = {
              },  
              HairType = 5623854,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 13,  
              HandsModel = 5617966,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 1,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 4,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_839]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 2,  
              FeetModel = 5617710,  
              Speed = 0,  
              Angle = 3.890625,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              SheetClient = [[basic_zorai_female.creature]],  
              ArmColor = 5,  
              Level = 0,  
              JacketModel = 0,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Shuai-Chon]],  
              Position = {
                y = -1236.21875,  
                x = 27662.8125,  
                InstanceId = [[Client1_843]],  
                Class = [[Position]],  
                z = 75.265625
              },  
              ArmModel = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 5,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_834]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 5613870,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 8,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_832]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 5653550,  
              Speed = 0,  
              Angle = 1.703125,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 5,  
              Level = 0,  
              JacketModel = 5614638,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ba'Mba]],  
              Position = {
                y = -1240.0625,  
                x = 27660.25,  
                InstanceId = [[Client1_835]],  
                Class = [[Position]],  
                z = 75.3125
              },  
              ArmModel = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 2,  
              Tattoo = 2
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_830]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 8,  
              HandsModel = 5605678,  
              FeetColor = 0,  
              GabaritBreastSize = 11,  
              GabaritHeight = 9,  
              HairColor = 1,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 4,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_828]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 0,  
              Speed = 0,  
              Angle = 2.46875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 0,  
              Level = 0,  
              JacketModel = 5606446,  
              Sex = 0,  
              InheritPos = 1,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 2,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Layla]],  
              Position = {
                y = -1239.984375,  
                x = 27664.07813,  
                InstanceId = [[Client1_831]],  
                Class = [[Position]],  
                z = 75.203125
              },  
              ArmModel = 0,  
              MorphTarget7 = 2,  
              MorphTarget3 = 4,  
              Tattoo = 3
            },  
            {
              InstanceId = [[Client1_921]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_919]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[butterflies 1]],  
              Position = {
                y = -1243.765625,  
                x = 27638.5625,  
                InstanceId = [[Client1_922]],  
                Class = [[Position]],  
                z = 75.71875
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.botobjects.fx_ju_bugsa]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_961]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_962]],  
                x = 27662.96875,  
                y = -1230.46875,  
                z = 75.140625
              },  
              Angle = -2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_959]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_995]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_996]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_964]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 1,  
              HairType = 2606,  
              HairColor = 5,  
              Tattoo = 2,  
              EyesColor = 6,  
              MorphTarget1 = 1,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 6,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 1,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5595182,  
              WeaponLeftHand = 0,  
              Name = [[Xakos]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_964]],  
              Name = [[Route 1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_966]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_967]],  
                    x = 27655.75,  
                    y = -1229.578125,  
                    z = 75.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_969]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_970]],  
                    x = 27648.03125,  
                    y = -1231.765625,  
                    z = 76.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_972]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_973]],  
                    x = 27645.14063,  
                    y = -1238.703125,  
                    z = 76.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_975]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_976]],  
                    x = 27651.67188,  
                    y = -1249.015625,  
                    z = 74.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_978]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_979]],  
                    x = 27665.73438,  
                    y = -1251.21875,  
                    z = 74.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_981]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_982]],  
                    x = 27676.53125,  
                    y = -1243.421875,  
                    z = 75.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_984]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_985]],  
                    x = 27675.84375,  
                    y = -1229.859375,  
                    z = 75.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_987]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_988]],  
                    x = 27667.45313,  
                    y = -1226.015625,  
                    z = 74.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_990]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_991]],  
                    x = 27655.75,  
                    y = -1229.625,  
                    z = 75.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_993]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_994]],  
                    x = 27655.75,  
                    y = -1229.578125,  
                    z = 75.390625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_963]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_999]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1000]],  
                x = 27642.6875,  
                y = -1230.71875,  
                z = 76.390625
              },  
              Angle = -2.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_997]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 5,  
              HairType = 3118,  
              HairColor = 2,  
              Tattoo = 7,  
              EyesColor = 1,  
              MorphTarget1 = 1,  
              MorphTarget2 = 0,  
              MorphTarget3 = 3,  
              MorphTarget4 = 0,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 0,  
              MorphTarget8 = 2,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 5,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5635886,  
              WeaponLeftHand = 0,  
              Name = [[Melus]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1003]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1004]],  
                x = 27640.98438,  
                y = -1236.734375,  
                z = 76.3125
              },  
              Angle = -2.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1001]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 12,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 7,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 6,  
              MorphTarget3 = 5,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 1,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 5,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5635886,  
              WeaponLeftHand = 0,  
              Name = [[Meros]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      LocationId = [[]],  
      ManualWeather = 0
    },  
    {
      InstanceId = [[Client1_12]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Name = [[]],  
            InstanceId = [[Client1_845]],  
            Actions = {
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_847]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_848]],  
                Entity = r2.RefId([[Client1_834]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_849]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_850]],  
                Entity = r2.RefId([[Client1_830]]),  
                Class = [[ActionStep]]
              },  
              {
                Action = {
                  Type = [[Sit Down]],  
                  InstanceId = [[Client1_851]],  
                  Value = r2.RefId([[]]),  
                  Class = [[ActionType]]
                },  
                InstanceId = [[Client1_852]],  
                Entity = r2.RefId([[Client1_841]]),  
                Class = [[ActionStep]]
              }
            },  
            Conditions = {
            },  
            Event = {
              Type = [[On Act Started]],  
              InstanceId = [[Client1_846]],  
              Value = r2.RefId([[]]),  
              Class = [[EventType]]
            },  
            Class = [[LogicEntityAction]]
          }
        },  
        InstanceId = [[Client1_10]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Version = 5,  
      Name = [[Act 1:Act 1]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_11]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_911]],  
              ActivitiesId = {
              },  
              HairType = 5624366,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 10,  
              HandsModel = 5617966,  
              FeetColor = 5,  
              GabaritBreastSize = 14,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 11,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_909]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_917]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_918]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 5617710,  
              Speed = 0,  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 6,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Mu]],  
              Position = {
                y = -1244.5625,  
                x = 27638.0625,  
                InstanceId = [[Client1_912]],  
                Class = [[Position]],  
                z = 75.59375
              },  
              ArmModel = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 2,  
              Tattoo = 12
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_934]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_935]],  
                x = 27660.32813,  
                y = -1222.484375,  
                z = 74.78125
              },  
              Angle = -2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_932]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 12,  
              HairType = 5623342,  
              HairColor = 0,  
              Tattoo = 7,  
              EyesColor = 3,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 1,  
              MorphTarget8 = 3,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ba'caunin]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_938]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_939]],  
                x = 27674.39063,  
                y = -1229,  
                z = 74.984375
              },  
              Angle = 6.661534786,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_936]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 6,  
              HairType = 7470,  
              HairColor = 1,  
              Tattoo = 29,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 6,  
              MorphTarget4 = 6,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 1,  
              JacketModel = 0,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Laughan]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_915]],  
              ActivitiesId = {
              },  
              HairType = 8494,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 4,  
              HandsModel = 5617966,  
              FeetColor = 5,  
              GabaritBreastSize = 10,  
              GabaritHeight = 13,  
              HairColor = 3,  
              EyesColor = 3,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_913]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 5617710,  
              Speed = 0,  
              Angle = -3.077617884,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Sex = 1,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              JacketModel = 5618734,  
              WeaponRightHand = 0,  
              Level = 0,  
              Name = [[Pu-Fu]],  
              Position = {
                y = -1228.96875,  
                x = 27676.95313,  
                InstanceId = [[Client1_916]],  
                Class = [[Position]],  
                z = 75
              },  
              ArmModel = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 7,  
              Tattoo = 28
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_945]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_946]],  
                x = 27684.90625,  
                y = -1246.0625,  
                z = 75.484375
              },  
              Angle = 3.705275774,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_943]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 4,  
              HairType = 2350,  
              HairColor = 1,  
              Tattoo = 9,  
              EyesColor = 2,  
              MorphTarget1 = 1,  
              MorphTarget2 = 3,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 6,  
              MorphTarget8 = 6,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ioros]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_949]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_950]],  
                x = 27655.40625,  
                y = -1266.390625,  
                z = 74.640625
              },  
              Angle = -0.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_947]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 12,  
              HairType = 5934,  
              HairColor = 5,  
              Tattoo = 24,  
              EyesColor = 6,  
              MorphTarget1 = 5,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 5,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Nirni]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_953]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_954]],  
                x = 27660.14063,  
                y = -1274.328125,  
                z = 74.921875
              },  
              Angle = -0.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_951]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 10,  
              HairType = 5622318,  
              HairColor = 3,  
              Tattoo = 25,  
              EyesColor = 2,  
              MorphTarget1 = 3,  
              MorphTarget2 = 4,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 0,  
              HandsModel = 5609774,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Liccio]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_866]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_868]],  
                Actions = {
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_869]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_864]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 1]],  
          Position = {
            y = -1238.609375,  
            x = 27663.75,  
            InstanceId = [[Client1_865]],  
            Class = [[Position]],  
            z = 75
          },  
          Active = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 5,  
              InstanceId = [[Client1_870]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_871]],  
                  Who = r2.RefId([[Client1_830]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_896]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 8,  
              InstanceId = [[Client1_887]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_888]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_894]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 11,  
              InstanceId = [[Client1_890]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_891]],  
                  Who = r2.RefId([[Client1_841]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_895]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 14,  
              InstanceId = [[Client1_897]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_898]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_899]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 18,  
              InstanceId = [[Client1_900]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_901]],  
                  Who = r2.RefId([[Client1_830]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_904]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 19,  
              InstanceId = [[Client1_902]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_903]],  
                  Who = r2.RefId([[Client1_841]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_905]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 21,  
              InstanceId = [[Client1_906]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_907]],  
                  Who = r2.RefId([[Client1_834]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_908]]
                }
              },  
              Name = [[]]
            }
          },  
          Repeating = 1
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_925]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_923]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[Dialog 2]],  
          Position = {
            y = -1242.140625,  
            x = 27638.8125,  
            InstanceId = [[Client1_924]],  
            Class = [[Position]],  
            z = 75.890625
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 5,  
              InstanceId = [[Client1_926]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Excited]],  
                  InstanceId = [[Client1_927]],  
                  Who = r2.RefId([[Client1_911]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_928]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 7,  
              InstanceId = [[Client1_929]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[Dreamy]],  
                  InstanceId = [[Client1_930]],  
                  Who = r2.RefId([[Client1_911]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_931]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 1
        }
      },  
      LocationId = [[Client1_14]],  
      ManualWeather = 1
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 5,  
        InstanceId = [[Client1_894]],  
        Class = [[TextManagerEntry]],  
        Text = [[I worry about her...bandits are so terrible.]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_895]],  
        Class = [[TextManagerEntry]],  
        Text = [[I'm sure they bring her to the north east of here.]]
      },  
      {
        Count = 7,  
        InstanceId = [[Client1_896]],  
        Class = [[TextManagerEntry]],  
        Text = [[Who will help Princess Faneliah? ]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_899]],  
        Class = [[TextManagerEntry]],  
        Text = [[Maybe a GUARD could do his job??? Ahem...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_904]],  
        Class = [[TextManagerEntry]],  
        Text = [[They act as if they don't hear us]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_905]],  
        Class = [[TextManagerEntry]],  
        Text = [[It would be faster if we go ourselves]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_908]],  
        Class = [[TextManagerEntry]],  
        Text = [[I'm afraid of the kittins...]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_928]],  
        Class = [[TextManagerEntry]],  
        Text = [[Woow, beautifull butterflies!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_931]],  
        Class = [[TextManagerEntry]],  
        Text = [[I want to be a butterfly too...]]
      }
    }
  }
}