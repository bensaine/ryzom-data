scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 27,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 22,  
      Class = [[Act]],  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
        [[Client1_138]],  
        [[Client1_163]],  
        [[Client1_166]],  
        [[Client1_358]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_30]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_31]],  
                x = 29704.92188,  
                y = -2218.90625,  
                z = -17.859375
              },  
              Angle = 2.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_28]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kitin Cave]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_34]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_35]],  
                x = 29676.78125,  
                y = -2123.0625,  
                z = -16.984375
              },  
              Angle = -1.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_32]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Goo Show]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_38]],  
              Base = [[palette.entities.botobjects.roadsign]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_39]],  
                x = 29649.26563,  
                y = -2176.5625,  
                z = -16.8125
              },  
              Angle = -0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_36]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Homin Camp]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_46]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_47]],  
                x = 29551.48438,  
                y = -2051.046875,  
                z = -22.125
              },  
              Angle = -2.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_44]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_50]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_51]],  
                x = 29476.76563,  
                y = -2057.046875,  
                z = -22.375
              },  
              Angle = 0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_48]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_54]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_55]],  
                x = 29518.67188,  
                y = -2093.921875,  
                z = -23.9375
              },  
              Angle = -4.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_52]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Bandit Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_58]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_59]],  
                x = 29488.76563,  
                y = -2026.875,  
                z = -19.3125
              },  
              Angle = 0.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_56]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1882]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1883]],  
                x = 29498.64063,  
                y = -2010.75,  
                z = -18.765625
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1880]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1886]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1887]],  
                x = 29517.92188,  
                y = -2017.03125,  
                z = -20.125
              },  
              Angle = -2.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1884]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_90]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_91]],  
                x = 29508.84375,  
                y = -2026.359375,  
                z = -21
              },  
              Angle = -1.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_88]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_166]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_167]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[Activity 1 : Wander Place 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 11,  
              HairType = 5621806,  
              HairColor = 4,  
              Tattoo = 22,  
              EyesColor = 5,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              MorphTarget7 = 3,  
              MorphTarget8 = 0,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 1,  
              FeetColor = 0,  
              WeaponRightHand = 0,  
              Name = [[Roodkapje]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_166]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_94]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_95]],  
                x = 29515.59375,  
                y = -2045.875,  
                z = -21.828125
              },  
              Angle = 1.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_92]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_138]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_165]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[Activity 1 : Wander Place 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 2350,  
              HairColor = 5,  
              Tattoo = 7,  
              EyesColor = 4,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              MorphTarget7 = 3,  
              MorphTarget8 = 0,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 2,  
              WeaponRightHand = 0,  
              Name = [[Ik kan al lezen]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_138]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_98]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_99]],  
                x = 29508.95313,  
                y = -2046.65625,  
                z = -21.765625
              },  
              Angle = 0.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_96]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_163]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_164]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[Activity 1 : Wander Place 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 5,  
              HairType = 2606,  
              HairColor = 4,  
              Tattoo = 7,  
              EyesColor = 7,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 2,  
              MorphTarget8 = 0,  
              Sex = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 0,  
              WeaponRightHand = 0,  
              Name = [[Grote Smurf]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_163]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_141]],  
              Name = [[Town]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_140]],  
                x = 0.21875,  
                y = 0.09375,  
                z = -0.015625
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_143]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_144]],  
                    x = 29508.5,  
                    y = -2057.015625,  
                    z = -21.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_146]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_147]],  
                    x = 29508.5,  
                    y = -2057.015625,  
                    z = -21.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_149]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_150]],  
                    x = 29469.6875,  
                    y = -2040.40625,  
                    z = -22.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_152]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_153]],  
                    x = 29504.9375,  
                    y = -2023.03125,  
                    z = -20.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_155]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_156]],  
                    x = 29532.5625,  
                    y = -2021.5,  
                    z = -20.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_158]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_159]],  
                    x = 29549.25,  
                    y = -2068.90625,  
                    z = -22.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_161]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_162]],  
                    x = 29523.15625,  
                    y = -2063.609375,  
                    z = -22.296875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_191]],  
              Name = [[Bandit Place]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_193]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_194]],  
                    x = 29534.01563,  
                    y = -2242.34375,  
                    z = -20.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_196]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_197]],  
                    x = 29533.96875,  
                    y = -2242.546875,  
                    z = -20.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_199]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_200]],  
                    x = 29527.03125,  
                    y = -2212.703125,  
                    z = -19.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_202]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_203]],  
                    x = 29503.76563,  
                    y = -2216.140625,  
                    z = -18.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_205]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_206]],  
                    x = 29506.0625,  
                    y = -2240.953125,  
                    z = -19.265625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_190]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_295]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_296]],  
                x = 29519.23438,  
                y = -2243.34375,  
                z = -22.078125
              },  
              Angle = 1.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_293]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[house ruin 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_300]],  
              Name = [[Kitin Camp]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_302]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_303]],  
                    x = 29903.57813,  
                    y = -2225.984375,  
                    z = -9.203125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_305]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_306]],  
                    x = 29777.8125,  
                    y = -2203.046875,  
                    z = -16.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_308]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_309]],  
                    x = 29777.375,  
                    y = -2127.296875,  
                    z = -17.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_311]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_312]],  
                    x = 29905.3125,  
                    y = -2142.65625,  
                    z = -14.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_299]],  
                x = -0.03125,  
                y = -0.265625,  
                z = 0.796875
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_345]],  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_346]],  
                x = 29495.01563,  
                y = -2013.671875,  
                z = -18.921875
              },  
              Angle = -1.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_343]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_358]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_359]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_348]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_403]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_404]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_404]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_405]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_406]],  
                            Emote = [[FBT]],  
                            Who = [[Client1_345]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_407]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6705198,  
              HandsModel = 6703150,  
              ArmModel = 6704174,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              Speed = [[run]],  
              WeaponRightHand = 0,  
              Name = [[Tribe Leader]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_358]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_348]],  
              Name = [[Somewhere to Leader]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_350]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_351]],  
                    x = 29495.92188,  
                    y = -2017.796875,  
                    z = -18.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_353]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_354]],  
                    x = 29497.125,  
                    y = -2019.984375,  
                    z = -19.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_356]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_357]],  
                    x = 29500.01563,  
                    y = -2023.578125,  
                    z = -19.9375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_347]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_414]],  
              Name = [[Tower to Leader]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_413]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_416]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_417]],  
                    x = 29520.78125,  
                    y = -2073.828125,  
                    z = -24.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_419]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_420]],  
                    x = 29518.3125,  
                    y = -2046.453125,  
                    z = -21.84375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_422]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_423]],  
                    x = 29500.82813,  
                    y = -2024.75,  
                    z = -20.171875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_447]],  
              Name = [[Leader to Bandits]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_446]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_449]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_450]],  
                    x = 29509.17188,  
                    y = -2073.53125,  
                    z = -23
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_452]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_453]],  
                    x = 29506.60938,  
                    y = -2090.1875,  
                    z = -22.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_455]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_456]],  
                    x = 29484.26563,  
                    y = -2112.890625,  
                    z = -22.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_458]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_459]],  
                    x = 29495.40625,  
                    y = -2194.328125,  
                    z = -17.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_461]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_462]],  
                    x = 29497.625,  
                    y = -2189.015625,  
                    z = -16.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_464]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_465]],  
                    x = 29497.1875,  
                    y = -2187.765625,  
                    z = -17.046875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_475]],  
              Name = [[Bandits To Leader]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_477]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_478]],  
                    x = 29485,  
                    y = -2111.4375,  
                    z = -22.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_480]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_481]],  
                    x = 29507.39063,  
                    y = -2088.078125,  
                    z = -22.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_483]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_484]],  
                    x = 29500.82813,  
                    y = -2024.75,  
                    z = -20.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_474]],  
                x = 0,  
                y = 0,  
                z = 0.015625
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_525]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_526]],  
                x = 29468.51563,  
                y = -2103.578125,  
                z = -24.203125
              },  
              Angle = -0.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_523]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kami Sentinel]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_529]],  
              Base = [[palette.entities.botobjects.tr_s2_bamboo_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_530]],  
                x = 29466.95313,  
                y = -2102.796875,  
                z = -23.90625
              },  
              Angle = -0.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_527]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kami Background]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_537]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_538]],  
                x = 29621.89063,  
                y = -2186.8125,  
                z = -17.65625
              },  
              Angle = 0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_535]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kitin Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_541]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_542]],  
                x = 29673.1875,  
                y = -2208.171875,  
                z = -18
              },  
              Angle = 0.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_539]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_545]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_546]],  
                x = 29679.0625,  
                y = -2222.78125,  
                z = -19.484375
              },  
              Angle = 1.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_543]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_549]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_550]],  
                x = 29690.01563,  
                y = -2226.046875,  
                z = -18.390625
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_547]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_553]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_554]],  
                x = 29680.32813,  
                y = -2111.59375,  
                z = -17.15625
              },  
              Angle = -1.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_551]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_573]],  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_574]],  
                x = 29747.34375,  
                y = -2132.53125,  
                z = -20.328125
              },  
              Angle = -1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_571]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin totem 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_18940]],  
              Name = [[Boss Zone]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_18942]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18943]],  
                    x = 29906.3125,  
                    y = -2143.09375,  
                    z = -14.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_18945]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18946]],  
                    x = 29927.65625,  
                    y = -2147.359375,  
                    z = -17.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_18948]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18949]],  
                    x = 29927.0625,  
                    y = -2170.078125,  
                    z = -18.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_18951]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18952]],  
                    x = 29906.45313,  
                    y = -2175.078125,  
                    z = -17.546875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18939]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_18968]],  
              Name = [[Kitins To Town]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18967]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18970]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18971]],  
                    x = 29748.5625,  
                    y = -2172.953125,  
                    z = -18.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18973]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18974]],  
                    x = 29744.26563,  
                    y = -2205.25,  
                    z = -17.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18976]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18977]],  
                    x = 29731.60938,  
                    y = -2217.875,  
                    z = -18.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18979]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18980]],  
                    x = 29720.89063,  
                    y = -2220.671875,  
                    z = -17.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18982]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18983]],  
                    x = 29701.6875,  
                    y = -2221.8125,  
                    z = -17.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18985]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18986]],  
                    x = 29701.67188,  
                    y = -2217.59375,  
                    z = -18.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18988]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18989]],  
                    x = 29677.60938,  
                    y = -2194.390625,  
                    z = -19.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18991]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18992]],  
                    x = 29648.35938,  
                    y = -2179.296875,  
                    z = -17.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18994]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18995]],  
                    x = 29634.65625,  
                    y = -2177.78125,  
                    z = -17.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18997]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18998]],  
                    x = 29616.59375,  
                    y = -2174.6875,  
                    z = -17.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19000]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19001]],  
                    x = 29597.875,  
                    y = -2174.796875,  
                    z = -18.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19003]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19004]],  
                    x = 29562.25,  
                    y = -2212.109375,  
                    z = -17.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19006]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19007]],  
                    x = 29545.04688,  
                    y = -2204.859375,  
                    z = -19.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19009]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19010]],  
                    x = 29514.78125,  
                    y = -2163.078125,  
                    z = -18.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19012]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19013]],  
                    x = 29484.65625,  
                    y = -2112.15625,  
                    z = -22.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19015]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19016]],  
                    x = 29509.125,  
                    y = -2086.71875,  
                    z = -23.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19018]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19019]],  
                    x = 29519.90625,  
                    y = -2051.796875,  
                    z = -21.609375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_19021]],  
              Name = [[Kitin Tower To Town]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19023]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19024]],  
                    x = 29627.9375,  
                    y = -2177.421875,  
                    z = -17.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19026]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19027]],  
                    x = 29614.51563,  
                    y = -2174.25,  
                    z = -17.515625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19029]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19030]],  
                    x = 29596.90625,  
                    y = -2174.015625,  
                    z = -18.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19032]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19033]],  
                    x = 29562,  
                    y = -2212.546875,  
                    z = -17.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19035]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19036]],  
                    x = 29544.51563,  
                    y = -2204.84375,  
                    z = -19.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19038]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19039]],  
                    x = 29513.32813,  
                    y = -2159.625,  
                    z = -19.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19041]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19042]],  
                    x = 29483.89063,  
                    y = -2111.46875,  
                    z = -23.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19044]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19045]],  
                    x = 29506.73438,  
                    y = -2092.875,  
                    z = -22.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19047]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19048]],  
                    x = 29517.8125,  
                    y = -2094.21875,  
                    z = -23.8125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19020]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      States = {
      },  
      WeatherValue = 0
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      Events = {
      },  
      ActivitiesIds = {
        [[Client1_341]],  
        [[Client1_388]]
      },  
      Title = [[Act I: Peace Mode]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_298]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]],  
      States = {
      },  
      WeatherValue = 0
    },  
    {
      Cost = 6,  
      Class = [[Act]],  
      Events = {
      },  
      ActivitiesIds = {
        [[Client1_207]],  
        [[Client1_213]],  
        [[Client1_424]],  
        [[Client1_472]],  
        [[Client1_18860]],  
        [[Client1_18862]],  
        [[Client1_18864]],  
        [[Client1_18866]]
      },  
      Title = [[Act II: Bandits are Silly]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_411]],  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_412]],  
                x = 29518.8125,  
                y = -2088.59375,  
                z = -23.890625
              },  
              Angle = -2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_409]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_18844]],  
                    LogicEntityAction = [[Client1_498]],  
                    ActionStep = [[Client1_18843]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_427]],  
                    Name = [[cbone]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_428]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_429]],  
                            Emote = [[]],  
                            Who = [[Client1_411]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_430]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_432]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_433]],  
                            Emote = [[]],  
                            Who = [[Client1_345]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_434]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_435]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_436]],  
                            Emote = [[]],  
                            Who = [[Client1_411]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_437]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_439]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_440]],  
                            Emote = [[]],  
                            Who = [[Client1_345]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_443]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_441]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_442]],  
                            Emote = [[]],  
                            Who = [[Client1_345]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_444]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_467]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_468]],  
                            Emote = [[]],  
                            Who = [[Client1_411]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_469]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_486]],  
                    Name = [[Cbkil]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_487]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_488]],  
                            Emote = [[]],  
                            Who = [[Client1_411]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_489]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_490]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_491]],  
                            Emote = [[]],  
                            Who = [[Client1_345]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_492]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_493]],  
                        Time = 9,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_494]],  
                            Emote = [[]],  
                            Who = [[Client1_411]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_495]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_506]],  
                    Name = [[cWDON]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_507]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_508]],  
                            Emote = [[]],  
                            Who = [[Client1_411]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_517]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_510]],  
                    Name = [[cwher]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_511]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_512]],  
                            Emote = [[]],  
                            Who = [[Client1_411]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_520]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_521]],  
                            Emote = [[]],  
                            Who = [[Client1_211]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_522]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_424]],  
                    Name = [[spnic]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_425]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_414]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_426]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_466]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_447]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_471]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_472]],  
                    Name = [[sBKIL]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_473]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_475]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_485]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 3118,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6707246,  
              TrouserModel = 6706222,  
              FeetModel = 6705198,  
              HandsModel = 6703150,  
              ArmModel = 6704174,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 0,  
              Name = [[Town Guard]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_424]],  
                [[Client1_472]]
              },  
              Notes = [[REPLACE WITH REAL GUARD]],  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_174]],  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_175]],  
                x = 29524.71875,  
                y = -2236.625,  
                z = -22.875
              },  
              Angle = 1.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_172]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_18866]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_18867]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 7,  
              HairType = 6958,  
              HairColor = 2,  
              Tattoo = 8,  
              EyesColor = 2,  
              MorphTarget1 = 1,  
              MorphTarget2 = 1,  
              MorphTarget3 = 3,  
              MorphTarget4 = 7,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              Sex = 0,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 5653550,  
              HandsModel = 5613870,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 5,  
              WeaponRightHand = 6935342,  
              Name = [[Follower of the Noob]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_18866]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_186]],  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_187]],  
                x = 29514.07813,  
                y = -2228.125,  
                z = -21.96875
              },  
              Angle = 1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_184]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_18860]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_18861]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 4,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 28,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 3,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              MorphTarget7 = 6,  
              MorphTarget8 = 4,  
              Sex = 0,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 5653550,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 2,  
              WeaponRightHand = 6935342,  
              Name = [[Noob Newbie]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_18860]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_178]],  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_179]],  
                x = 29522.9375,  
                y = -2229.40625,  
                z = -22.921875
              },  
              Angle = 1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_176]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_18864]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_18865]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 4,  
              HairType = 7214,  
              HairColor = 1,  
              Tattoo = 18,  
              EyesColor = 6,  
              MorphTarget1 = 4,  
              MorphTarget2 = 1,  
              MorphTarget3 = 1,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5614126,  
              FeetModel = 5653550,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 4,  
              WeaponRightHand = 6935342,  
              Name = [[Wannabe Noob]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_18864]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_182]],  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_183]],  
                x = 29512.10938,  
                y = -2236.25,  
                z = -21.53125
              },  
              Angle = 1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_180]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_18862]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_18863]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 4,  
              HairType = 5623342,  
              HairColor = 4,  
              Tattoo = 1,  
              EyesColor = 5,  
              MorphTarget1 = 4,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 5614638,  
              TrouserModel = 5614126,  
              FeetModel = 5653550,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 1,  
              WeaponRightHand = 6935342,  
              Name = [[Noob Worshipper]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_18862]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_cold_b2.creature]],  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_211]],  
              Base = [[palette.entities.npcs.bandits.t_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_212]],  
                x = 29517.0625,  
                y = -2230.171875,  
                z = -22.25
              },  
              Angle = 1.921875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_209]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_213]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_214]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_18838]]),  
                        ActivityZoneId = r2.RefId([[Client1_191]]),  
                        Name = [[Activity 1 : Wander Place 2 for 20sec]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_18838]],  
                    Name = [[cspam]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_18839]],  
                        Time = 10,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_18840]],  
                            Emote = [[]],  
                            Who = [[Client1_211]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_18841]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_498]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_18843]],  
                        Entity = r2.RefId([[Client1_411]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_18842]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_472]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_18849]],  
                        Entity = r2.RefId([[Client1_182]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_18848]],  
                          Type = [[Kill]],  
                          Value = r2.RefId([[]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_18852]],  
                        Entity = r2.RefId([[Client1_174]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_18851]],  
                          Type = [[Kill]],  
                          Value = r2.RefId([[]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_18855]],  
                        Entity = r2.RefId([[Client1_178]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_18854]],  
                          Type = [[Kill]],  
                          Value = r2.RefId([[]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_18858]],  
                        Entity = r2.RefId([[Client1_186]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_18857]],  
                          Type = [[Kill]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_497]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_18850]],  
                    LogicEntityAction = [[Client1_498]],  
                    ActionStep = [[Client1_18849]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_18853]],  
                    LogicEntityAction = [[Client1_498]],  
                    ActionStep = [[Client1_18852]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_18856]],  
                    LogicEntityAction = [[Client1_498]],  
                    ActionStep = [[Client1_18855]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_18859]],  
                    LogicEntityAction = [[Client1_498]],  
                    ActionStep = [[Client1_18858]],  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 5,  
              HairType = 5623598,  
              HairColor = 2,  
              Tattoo = 15,  
              EyesColor = 6,  
              MorphTarget1 = 1,  
              MorphTarget2 = 6,  
              MorphTarget3 = 2,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6725934,  
              TrouserModel = 6724910,  
              FeetModel = 6724142,  
              HandsModel = 6724398,  
              ArmModel = 6725422,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 2,  
              WeaponRightHand = 6935854,  
              Name = [[NOOB THE GREAT]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_213]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_magic_damage_dealer_acid_f3.creature]],  
              SheetClient = [[basic_tryker_female.creature]]
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_188]],  
      ManualWeather = 0,  
      States = {
      },  
      WeatherValue = 0
    },  
    {
      Cost = 28,  
      Class = [[Act]],  
      Events = {
      },  
      ActivitiesIds = {
        [[Client1_18868]],  
        [[Client1_18953]],  
        [[Client1_18955]],  
        [[Client1_18957]],  
        [[Client1_19049]],  
        [[Client1_19095]],  
        [[Client1_19097]],  
        [[Client1_19100]],  
        [[Client1_19103]],  
        [[Client1_19106]],  
        [[Client1_19107]],  
        [[Client1_19110]],  
        [[Client1_19154]],  
        [[Client1_19156]]
      },  
      Title = [[Act III: Kitins Invade]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_533]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_534]],  
                x = 29518.6875,  
                y = -2088.8125,  
                z = -23.875
              },  
              Angle = 1.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_531]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_19094]],  
                    LogicEntityAction = [[Client1_19091]],  
                    ActionStep = [[Client1_19093]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_19165]],  
                    LogicEntityAction = [[Client1_19160]],  
                    ActionStep = [[Client1_19164]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19095]],  
                    Name = [[sINIT]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19096]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19097]],  
                    Name = [[sLead]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19098]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_414]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19099]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19156]],  
                    Name = [[sGuar]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19157]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 6756910,  
              Name = [[Town Guard]],  
              BotAttackable = 1,  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ActivitiesId = {
                [[Client1_19095]],  
                [[Client1_19097]],  
                [[Client1_19156]]
              },  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_18961]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18962]],  
                x = 29625.42188,  
                y = -2182.765625,  
                z = -18.09375
              },  
              Angle = 1.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18959]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19049]],  
                    Name = [[sINIT]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19050]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_19021]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19051]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[15]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_19052]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19068]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_414]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19069]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_19070]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19154]],  
                    Name = [[sGuar]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19155]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_19052]],  
                    Name = [[cGrdd]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_19053]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_19054]],  
                            Emote = [[]],  
                            Who = [[Client1_18961]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19057]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_19055]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_19056]],  
                            Emote = [[]],  
                            Who = [[Client1_533]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19058]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_19060]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_19061]],  
                            Emote = [[]],  
                            Who = [[Client1_18961]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19062]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_19064]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_19065]],  
                            Emote = [[]],  
                            Who = [[Client1_533]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19066]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_19070]],  
                    Name = [[cLead]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_19071]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_19072]],  
                            Emote = [[]],  
                            Who = [[Client1_345]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19077]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_19078]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_19079]],  
                            Emote = [[]],  
                            Who = [[Client1_533]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19080]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_19082]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_19083]],  
                            Emote = [[]],  
                            Who = [[Client1_18961]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19084]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_19086]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_19087]],  
                            Emote = [[]],  
                            Who = [[Client1_345]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_19088]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_19091]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_19093]],  
                        Entity = r2.RefId([[Client1_533]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_19092]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_19097]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_19113]],  
                        Entity = r2.RefId([[Client1_18896]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_19112]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_19100]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_19090]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_19051]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_19160]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_19162]],  
                        Entity = r2.RefId([[Client1_18961]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_19161]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_19154]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_19164]],  
                        Entity = r2.RefId([[Client1_533]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_19163]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_19156]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_19159]],  
                      Type = [[end of chat step]],  
                      Value = r2.RefId([[Client1_19086]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_19158]],  
                    LogicEntityAction = [[Client1_19160]],  
                    ActionStep = [[Client1_19162]],  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 3,  
              MorphTarget3 = 3,  
              MorphTarget4 = 3,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 6756910,  
              Name = [[Tower Guard]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
                [[Client1_19049]],  
                [[Client1_19154]]
              },  
              InheritPos = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          InstanceId = [[Client1_189]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_18837]],  
          Name = [[Group III: Medium]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_593]],  
              Base = [[palette.entities.creatures.ckdib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_594]],  
                x = 29845.70313,  
                y = -2178.171875,  
                z = -20.78125
              },  
              Angle = 2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_591]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_19126]],  
                    LogicEntityAction = [[Client1_19123]],  
                    ActionStep = [[Client1_19125]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_19128]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_19132]],  
                        Entity = r2.RefId([[Client1_18896]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_19131]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_19100]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_19127]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_18868]],  
                    Name = [[sINIT]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_18869]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19106]],  
                    Name = [[sInva]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19108]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_18968]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19109]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]],  
              ActivitiesId = {
                [[Client1_18868]],  
                [[Client1_19106]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_597]],  
              Base = [[palette.entities.creatures.ckdib3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_598]],  
                x = 29858.32813,  
                y = -2173.109375,  
                z = -20.96875
              },  
              Angle = 2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_595]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18801]],  
              Base = [[palette.entities.creatures.ckdib3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18802]],  
                x = 29855.26563,  
                y = -2186.40625,  
                z = -20.9375
              },  
              Angle = 2.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_599]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18805]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18806]],  
                x = 29873.34375,  
                y = -2165.46875,  
                z = -19.46875
              },  
              Angle = 2.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18803]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18809]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18810]],  
                x = 29868.03125,  
                y = -2181.015625,  
                z = -19.578125
              },  
              Angle = 2.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18807]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18813]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18814]],  
                x = 29864.53125,  
                y = -2195.0625,  
                z = -19.625
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18811]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18817]],  
              Base = [[palette.entities.creatures.ckdib1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18818]],  
                x = 29879.6875,  
                y = -2157.671875,  
                z = -17.34375
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18815]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18821]],  
              Base = [[palette.entities.creatures.ckdib1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18822]],  
                x = 29876.5625,  
                y = -2173.375,  
                z = -18.125
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18819]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18825]],  
              Base = [[palette.entities.creatures.ckdib1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18826]],  
                x = 29871.40625,  
                y = -2191.96875,  
                z = -17.71875
              },  
              Angle = 2.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18823]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18829]],  
              Base = [[palette.entities.creatures.ckdib1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18830]],  
                x = 29866.625,  
                y = -2203.46875,  
                z = -17.765625
              },  
              Angle = 2.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18827]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Trooper Kincher]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_18836]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_18835]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_18896]],  
          Name = [[Group I: Small]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18872]],  
              Base = [[palette.entities.creatures.ckdib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18873]],  
                x = 29787.14063,  
                y = -2180.578125,  
                z = -17.078125
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18870]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_18957]],  
                    Name = [[sINIT]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_18958]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19100]],  
                    Name = [[sInva]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19101]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_18968]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19102]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_19118]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_19120]],  
                        Entity = r2.RefId([[Client1_18923]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_19119]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_19103]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_19117]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_19114]],  
                    LogicEntityAction = [[Client1_19091]],  
                    ActionStep = [[Client1_19113]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_19133]],  
                    LogicEntityAction = [[Client1_19128]],  
                    ActionStep = [[Client1_19132]],  
                    Name = [[]]
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]],  
              ActivitiesId = {
                [[Client1_18957]],  
                [[Client1_19100]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18876]],  
              Base = [[palette.entities.creatures.ckdib3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18877]],  
                x = 29791.90625,  
                y = -2174.890625,  
                z = -15.71875
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18874]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18880]],  
              Base = [[palette.entities.creatures.ckdib3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18881]],  
                x = 29792.1875,  
                y = -2184.328125,  
                z = -15.484375
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18878]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18892]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18893]],  
                x = 29799.14063,  
                y = -2187.59375,  
                z = -14.5
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18890]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18888]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18889]],  
                x = 29798.28125,  
                y = -2180.359375,  
                z = -13.703125
              },  
              Angle = -3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18886]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18884]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18885]],  
                x = 29798.21875,  
                y = -2173.765625,  
                z = -13.296875
              },  
              Angle = -3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18882]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_18895]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_18894]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_18923]],  
          Name = [[Group II: Small]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18899]],  
              Base = [[palette.entities.creatures.ckdib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18900]],  
                x = 29800,  
                y = -2156.3125,  
                z = -19.1875
              },  
              Angle = 3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18897]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_18955]],  
                    Name = [[sINIT]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_18956]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19103]],  
                    Name = [[sInva]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19104]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_18968]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19105]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_141]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_19123]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_19125]],  
                        Entity = r2.RefId([[Client1_18837]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_19124]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_19106]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_19122]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_19121]],  
                    LogicEntityAction = [[Client1_19118]],  
                    ActionStep = [[Client1_19120]],  
                    Name = [[]]
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]],  
              ActivitiesId = {
                [[Client1_18955]],  
                [[Client1_19103]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18907]],  
              Base = [[palette.entities.creatures.ckdib3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18908]],  
                x = 29804.15625,  
                y = -2159.5625,  
                z = -19.03125
              },  
              Angle = 3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18905]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18919]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18920]],  
                x = 29805.57813,  
                y = -2163.828125,  
                z = -17.546875
              },  
              Angle = 3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18917]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18915]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18916]],  
                x = 29807.92188,  
                y = -2155.890625,  
                z = -19.84375
              },  
              Angle = 3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18913]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18903]],  
              Base = [[palette.entities.creatures.ckdib3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18904]],  
                x = 29805.59375,  
                y = -2152.25,  
                z = -19.921875
              },  
              Angle = 3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18901]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Trooper Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18911]],  
              Base = [[palette.entities.creatures.ckdib2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18912]],  
                x = 29810.73438,  
                y = -2147.53125,  
                z = -20.765625
              },  
              Angle = 3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18909]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Trooper Kincher]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_18922]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_18921]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_18938]],  
          Name = [[Group IV: Boss]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_561]],  
              Base = [[palette.entities.creatures.ckdpf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_562]],  
                x = 29921.25,  
                y = -2159.015625,  
                z = -20.953125
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_559]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_18953]],  
                    Name = [[sINIT]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_18954]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_18940]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_19110]],  
                    Name = [[sInva]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_19111]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_300]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Kinchey]],  
              ActivitiesId = {
                [[Client1_18953]],  
                [[Client1_19110]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18934]],  
              Base = [[palette.entities.creatures.ckdgf4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18935]],  
                x = 29918.39063,  
                y = -2167.71875,  
                z = -19.796875
              },  
              Angle = 3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18932]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Lethal Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18930]],  
              Base = [[palette.entities.creatures.ckdgf4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18931]],  
                x = 29908.84375,  
                y = -2159.265625,  
                z = -17.015625
              },  
              Angle = 3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18928]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Lethal Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18926]],  
              Base = [[palette.entities.creatures.ckdgf4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18927]],  
                x = 29919.64063,  
                y = -2149.765625,  
                z = -19.53125
              },  
              Angle = 3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_18924]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Lethal Kincher]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_18937]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_18936]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_297]],  
      States = {
      },  
      WeatherValue = 0
    },  
    {
      Cost = 4,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act IV: Nothing]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_19136]],  
              Base = [[palette.entities.creatures.ckdif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19137]],  
                x = 29580.75,  
                y = -2186.6875,  
                z = -17.1875
              },  
              Angle = 0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_19134]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_19140]],  
              Base = [[palette.entities.creatures.ckdif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19141]],  
                x = 29584.92188,  
                y = -2171.984375,  
                z = -17.09375
              },  
              Angle = 0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_19138]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_19148]],  
              Base = [[palette.entities.creatures.ckdif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19149]],  
                x = 29587.96875,  
                y = -2162.578125,  
                z = -19.015625
              },  
              Angle = 0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_19146]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kincher]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_19152]],  
              Base = [[palette.entities.creatures.ckdif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19153]],  
                x = 29591.14063,  
                y = -2168.953125,  
                z = -19.40625
              },  
              Angle = 0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_19150]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kincher]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_18964]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_18963]],  
      States = {
      }
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act V: Unknown]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_18966]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_18965]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_221]],  
        Count = 26,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_222]],  
        Count = 1,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_223]],  
        Count = 1,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_224]],  
        Count = 4,  
        Text = [[Some bandits appeared near our cute little town! Omgwth?? KILL THEM ALL!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_225]],  
        Count = 1,  
        Text = [[Some bandits appeared near our cute little town! Omgwth?? KILL THEM ALL!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_407]],  
        Count = 6,  
        Text = [[Good morning to everyone! What a great day today!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_408]],  
        Count = 1,  
        Text = [[Good morning to everyone! What a great day today!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_430]],  
        Count = 11,  
        Text = [[Bad news!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_431]],  
        Count = 1,  
        Text = [[Bad news!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_434]],  
        Count = 2,  
        Text = [[What's wrong? Did something bad happen?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_437]],  
        Count = 2,  
        Text = [[There's a group of bandit's that just arrived near our camp, they will probly try to take over this place!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_438]],  
        Count = 1,  
        Text = [[There's a group of bandit's that just arrived near our camp, they will probly try to take over this place!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_443]],  
        Count = 1,  
        Text = [[We must do something immediately!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_444]],  
        Count = 2,  
        Text = [[Everyone! Please, go find this group of bandits, and kill them!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_445]],  
        Count = 1,  
        Text = [[Everyone! Please, go find this group of bandits, and kill them!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_469]],  
        Count = 3,  
        Text = [[Follow me!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_470]],  
        Count = 1,  
        Text = [[Follow me!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_489]],  
        Count = 3,  
        Text = [[The bandits are gone!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_492]],  
        Count = 3,  
        Text = [[Thank you all for helping us out with this! You were of great help for us!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_495]],  
        Count = 1,  
        Text = [[I will see you all later. Goodbye.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_509]],  
        Count = 2,  
        Text = [[Well Done!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_513]],  
        Count = 4,  
        Text = [[This is the bandit's camp.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_514]],  
        Count = 1,  
        Text = [[This is the bandit's camp.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_517]],  
        Count = 2,  
        Text = [[Well Done! You killed their leader!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_522]],  
        Count = 4,  
        Text = [[All you will die right here and now! GET THEM!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_18841]],  
        Count = 1,  
        Text = [[Muahahahahahaaaa!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19057]],  
        Count = 1,  
        Text = [[Anyone here? There might be a problem!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19058]],  
        Count = 4,  
        Text = [[Yes, I'm coming! What's wrong?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19059]],  
        Count = 1,  
        Text = [[Yes, I'm coming! What's wrong?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19062]],  
        Count = 5,  
        Text = [[The Kitins! They're acting strange again!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19063]],  
        Count = 1,  
        Text = [[The Kitins! They're acting strange again!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19066]],  
        Count = 5,  
        Text = [[We should go see the Town Leader.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19067]],  
        Count = 1,  
        Text = [[We should go see the Town Leader.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19073]],  
        Count = 1,  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19074]],  
        Count = 1,  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19075]],  
        Count = 1,  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19076]],  
        Count = 2,  
        Text = [[Hello there, did anything happen?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19077]],  
        Count = 1,  
        Text = [[Hello there, did anything happen?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19080]],  
        Count = 2,  
        Text = [[We're here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19081]],  
        Count = 1,  
        Text = [[We're here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19084]],  
        Count = 2,  
        Text = [[The Kitin are showing strange behavior again, they might be here soon!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19085]],  
        Count = 1,  
        Text = [[The Kitin are showing strange behavior again, they might be here soon!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19088]],  
        Count = 3,  
        Text = [[That's terrible! Please, everyone, be very careful! If they come here, they have to be killed immediately!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19089]],  
        Count = 1,  
        Text = [[That's terrible! Please, everyone, be very careful! If they come here, they have to be killed immediately!]]
      }
    }
  },  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    NpcCustom = 0,  
    ChatStep = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    Npc = 0,  
    EventType = 0,  
    Region = 0,  
    ActivityStep = 0,  
    ChatAction = 0,  
    ActionStep = 0,  
    LogicEntityReaction = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Road = 0,  
    Position = 0,  
    Behavior = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    MapDescription = 0
  }
}