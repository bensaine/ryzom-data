scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_8]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_10]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_11]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_25]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_26]],  
                x = 26949.96875,  
                y = -2014,  
                z = -6.734375
              },  
              Angle = -1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_23]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Spawn Sign]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_29]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_30]],  
                x = 26950.0625,  
                y = -2015.9375,  
                z = -6.484375
              },  
              Angle = -1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_27]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Spawn Sign]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_33]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_34]],  
                x = 26933.98438,  
                y = -2023.703125,  
                z = -1.578125
              },  
              Angle = 1.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_31]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Circle Thingy]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_37]],  
              Base = [[palette.entities.botobjects.bones]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_38]],  
                x = 26940.89063,  
                y = -2023.21875,  
                z = -1.625
              },  
              Angle = 1.697809219,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_35]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Circle Bones]]
            }
          },  
          InstanceId = [[Client1_12]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_13]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_14]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_9]]
  }
}