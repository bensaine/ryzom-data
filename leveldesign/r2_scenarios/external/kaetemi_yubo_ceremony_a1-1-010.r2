scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client2_2287]],  
      IslandName = [[uiR2_Deserts24]],  
      Class = [[Location]],  
      Season = [[winter]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client2_2278]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0,  
    NpcCustom = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    Region = 0,  
    ActivityStep = 1,  
    ChatSequence = 0,  
    LogicEntityReaction = 0,  
    EventType = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Road = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    ActionStep = 0
  },  
  Description = {
    InstanceId = [[Client2_2276]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 61,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    ShortDescription = [[The yearly anti-yubo ceremony!]],  
    MaxEntities = 50,  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 49,  
      Class = [[Act]],  
      WeatherValue = 1022,  
      InstanceId = [[Client2_2135]],  
      ActivitiesIds = {
        [[Client1_1438]],  
        [[Client1_1517]],  
        [[Client1_1582]],  
        [[Client1_1584]],  
        [[Client1_1586]],  
        [[Client1_1588]],  
        [[Client1_1762]],  
        [[Client1_1764]],  
        [[Client1_1765]],  
        [[Client1_1766]],  
        [[Client1_1767]],  
        [[Client1_1768]],  
        [[Client1_1769]],  
        [[Client1_1896]],  
        [[Client1_1898]],  
        [[Client1_1900]],  
        [[Client1_1917]],  
        [[Client1_1919]],  
        [[Client1_1921]],  
        [[Client1_1923]],  
        [[Client1_1925]],  
        [[Client1_1927]],  
        [[Client1_1954]],  
        [[Client1_2065]],  
        [[Client1_2084]],  
        [[Client1_2085]],  
        [[Client1_2086]],  
        [[Client1_2095]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1141]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1142]],  
                x = 27940.65625,  
                y = -1967.8125,  
                z = -3.140625
              },  
              Angle = 1.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1139]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[1Exit Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1145]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1146]],  
                x = 27899.29688,  
                y = -1851.296875,  
                z = -13.546875
              },  
              Angle = -2.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1143]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn House 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1149]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1150]],  
                x = 27895.9375,  
                y = -1851.203125,  
                z = -13.875
              },  
              Angle = -2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1147]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Landslide 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1153]],  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1154]],  
                x = 27931.39063,  
                y = -1850.9375,  
                z = -14.5
              },  
              Angle = -1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1151]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Tower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1157]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1158]],  
                x = 27922.95313,  
                y = -1844.8125,  
                z = -13.4375
              },  
              Angle = -0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1155]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Landslide 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1161]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1162]],  
                x = 27937.125,  
                y = -1853.234375,  
                z = -12.9375
              },  
              Angle = 2.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1159]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Landslide 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1165]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1166]],  
                x = 27892.96875,  
                y = -1838.734375,  
                z = -14.640625
              },  
              Angle = -1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1163]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Goo 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1169]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1170]],  
                x = 27937.21875,  
                y = -1840.8125,  
                z = -14.734375
              },  
              Angle = -2.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1167]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Goo 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1173]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1174]],  
                x = 27930.85938,  
                y = -1856.328125,  
                z = -13.578125
              },  
              Angle = -1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1171]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1177]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1178]],  
                x = 27898.125,  
                y = -1853.578125,  
                z = -13.796875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1175]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Fire 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1181]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1182]],  
                x = 27942.53125,  
                y = -1887.59375,  
                z = -7.96875
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1179]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn House 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1185]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1186]],  
                x = 27941.15625,  
                y = -1901.703125,  
                z = -7.15625
              },  
              Angle = 1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1183]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn House 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1189]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1190]],  
                x = 27943.17188,  
                y = -1896.890625,  
                z = -7.53125
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1187]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Landslide 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1193]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1194]],  
                x = 27939.875,  
                y = -1900.859375,  
                z = -7.34375
              },  
              Angle = 3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1191]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Fire 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1197]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1198]],  
                x = 27915.09375,  
                y = -1878.25,  
                z = -10.640625
              },  
              Angle = 1.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1195]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Home Spawn Point]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1209]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1210]],  
                x = 27895.6875,  
                y = -1877.34375,  
                z = -13.0625
              },  
              Angle = -1.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1207]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Home Circle 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1213]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1214]],  
                x = 27964.14063,  
                y = -1893.28125,  
                z = -13.71875
              },  
              Angle = 2.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1211]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Goo 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1216]],  
              Name = [[Home Region]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1215]],  
                x = -0.5625,  
                y = 1.109375,  
                z = -0.234375
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1218]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1219]],  
                    x = 27932.96875,  
                    y = -1906.75,  
                    z = -7.75
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1221]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1222]],  
                    x = 27932.32813,  
                    y = -1881.984375,  
                    z = -9.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1224]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1225]],  
                    x = 27935.84375,  
                    y = -1863.34375,  
                    z = -11.625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1227]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1228]],  
                    x = 27890.98438,  
                    y = -1861.46875,  
                    z = -14.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1230]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1231]],  
                    x = 27907.89063,  
                    y = -1877.3125,  
                    z = -11.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1233]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1234]],  
                    x = 27885.70313,  
                    y = -1905.828125,  
                    z = -9.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1236]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1237]],  
                    x = 27896.57813,  
                    y = -1904.609375,  
                    z = -6.671875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1263]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1264]],  
                x = 28058.39063,  
                y = -2034.015625,  
                z = 9.828125
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1261]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[2Begin Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1267]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1268]],  
                x = 28140.42188,  
                y = -1974.828125,  
                z = 15.875
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1265]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[3Continue Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1271]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1272]],  
                x = 28223.21875,  
                y = -1946.765625,  
                z = 32.015625
              },  
              Angle = 0.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1269]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[4Finish Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1359]],  
              Base = [[palette.entities.botobjects.kami_hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1360]],  
                x = 28334.4375,  
                y = -1838.203125,  
                z = 33.25
              },  
              Angle = -2.921875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1357]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Security Hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1363]],  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1364]],  
                x = 28311.25,  
                y = -1830.140625,  
                z = 33.375
              },  
              Angle = -1.546875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1361]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Security Watchtower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1367]],  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1368]],  
                x = 28312.84375,  
                y = -1849.75,  
                z = 33.796875
              },  
              Angle = 2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1365]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Security Watchtower 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1425]],  
              Name = [[Security Region]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1424]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1427]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1428]],  
                    x = 28310.14063,  
                    y = -1828.4375,  
                    z = 33.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1430]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1431]],  
                    x = 28335.92188,  
                    y = -1825.09375,  
                    z = 34.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1433]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1434]],  
                    x = 28338.23438,  
                    y = -1855.75,  
                    z = 34.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1436]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1437]],  
                    x = 28311.59375,  
                    y = -1851.625,  
                    z = 34.484375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1446]],  
              Base = [[palette.entities.botobjects.stele]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1447]],  
                x = 28189.9375,  
                y = -1901.8125,  
                z = 38.21875
              },  
              Angle = 0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1444]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Goo Stele 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1454]],  
              Base = [[palette.entities.botobjects.spot_goo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1455]],  
                x = 28168.67188,  
                y = -1909.078125,  
                z = 24.125
              },  
              Angle = 0.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1452]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Goo Spot 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1458]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1459]],  
                x = 28171.23438,  
                y = -1907,  
                z = 27.375
              },  
              Angle = 0.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1456]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Goo Smoke 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1462]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1463]],  
                x = 28247.25,  
                y = -1756.09375,  
                z = 28.78125
              },  
              Angle = -2.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1460]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[End Tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1466]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1467]],  
                x = 28236.39063,  
                y = -1752.5,  
                z = 27.875
              },  
              Angle = -1.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1464]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[End Hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1470]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1471]],  
                x = 28208.625,  
                y = -1816.8125,  
                z = 37.34375
              },  
              Angle = 2,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1468]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[End Wind Turbine 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1476]],  
              Name = [[End Region]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1475]],  
                x = 0.109375,  
                y = -0.203125,  
                z = 0.0625
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1478]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1479]],  
                    x = 28231.60938,  
                    y = -1770.234375,  
                    z = 31.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1481]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1482]],  
                    x = 28224.67188,  
                    y = -1743.65625,  
                    z = 28.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1484]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1485]],  
                    x = 28249.42188,  
                    y = -1743.390625,  
                    z = 29.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1487]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1488]],  
                    x = 28255.21875,  
                    y = -1772.125,  
                    z = 31.703125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1490]],  
              Name = [[Goo Region]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1489]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1492]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1493]],  
                    x = 28207.23438,  
                    y = -1903.984375,  
                    z = 34.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1495]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1496]],  
                    x = 28178.79688,  
                    y = -1887.84375,  
                    z = 40.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1498]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1499]],  
                    x = 28167.8125,  
                    y = -1910.09375,  
                    z = 22.953125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1502]],  
              Base = [[palette.entities.creatures.chdfb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1503]],  
                x = 27935.98438,  
                y = -1958.234375,  
                z = -2.390625
              },  
              Angle = 1.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1500]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1582]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1583]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1572]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1788]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1790]],  
                        Entity = r2.RefId([[Client1_1240]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1789]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1764]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1908]],  
                        Entity = r2.RefId([[Client1_1411]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1907]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1898]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1952]],  
                        Entity = r2.RefId([[Client1_1533]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1951]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1923]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1787]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[1Exit Yubo]],  
              ActivitiesId = {
                [[Client1_1582]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1506]],  
              Base = [[palette.entities.creatures.chdfb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1507]],  
                x = 28066.14063,  
                y = -2024.9375,  
                z = 10.390625
              },  
              Angle = 0.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1504]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1584]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1585]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1561]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1957]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1959]],  
                        Entity = r2.RefId([[Client1_1260]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1958]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1766]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1962]],  
                        Entity = r2.RefId([[Client1_1533]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1961]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1925]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1965]],  
                        Entity = r2.RefId([[Client1_1474]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1964]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1900]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1956]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[2Begin Yubo]],  
              ActivitiesId = {
                [[Client1_1584]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1510]],  
              Base = [[palette.entities.creatures.chdfb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1511]],  
                x = 28142.64063,  
                y = -1986.859375,  
                z = 15.640625
              },  
              Angle = -0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1508]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1586]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1587]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1550]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1968]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1970]],  
                        Entity = r2.RefId([[Client1_1260]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1969]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1767]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1973]],  
                        Entity = r2.RefId([[Client1_1533]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1972]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1927]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1976]],  
                        Entity = r2.RefId([[Client1_1474]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1975]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1954]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1967]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[3Continue Yubo]],  
              ActivitiesId = {
                [[Client1_1586]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1536]],  
              Base = [[palette.entities.creatures.chdfb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1537]],  
                x = 28232.875,  
                y = -1941.84375,  
                z = 32
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1534]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1588]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1589]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1539]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_2061]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2063]],  
                        Entity = r2.RefId([[Client1_1533]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2062]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_2065]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2068]],  
                        Entity = r2.RefId([[Client1_1260]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2067]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1768]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2165]],  
                        Entity = r2.RefId([[Client1_1474]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2164]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1954]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_2060]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[4Finish Yubo]],  
              ActivitiesId = {
                [[Client1_1588]]
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1539]],  
              Name = [[4Finish Region]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1541]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1542]],  
                    x = 28239.03125,  
                    y = -1948.859375,  
                    z = 32.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1544]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1545]],  
                    x = 28229.09375,  
                    y = -1943.828125,  
                    z = 32.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1547]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1548]],  
                    x = 28228.89063,  
                    y = -1930.3125,  
                    z = 31.359375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1538]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1550]],  
              Name = [[3Continue Region]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1552]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1553]],  
                    x = 28136.89063,  
                    y = -1997.5625,  
                    z = 17.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1555]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1556]],  
                    x = 28142.25,  
                    y = -1982.0625,  
                    z = 15.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1558]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1559]],  
                    x = 28157.4375,  
                    y = -1985.6875,  
                    z = 17.6875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1549]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1561]],  
              Name = [[2Begin Region]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1563]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1564]],  
                    x = 28080.29688,  
                    y = -2044.46875,  
                    z = 12.515625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1566]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1567]],  
                    x = 28063.35938,  
                    y = -2010.921875,  
                    z = 10.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1569]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1570]],  
                    x = 28065.32813,  
                    y = -2033.34375,  
                    z = 9.296875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1560]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1572]],  
              Name = [[1Exit Region]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1574]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1575]],  
                    x = 27949.21875,  
                    y = -1933.953125,  
                    z = -4.3125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1577]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1578]],  
                    x = 27910.0625,  
                    y = -1962.640625,  
                    z = -6.28125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1580]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1581]],  
                    x = 27937.53125,  
                    y = -1960.96875,  
                    z = -2.703125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1571]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1777]],  
              Name = [[1Exit Route]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1776]],  
                x = -2.90625,  
                y = 6.015625,  
                z = -0.859375
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1779]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1780]],  
                    x = 27924.75,  
                    y = -1941.515625,  
                    z = -7.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1782]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1783]],  
                    x = 27928,  
                    y = -1946.21875,  
                    z = -6.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1785]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1786]],  
                    x = 27931.78125,  
                    y = -1952.25,  
                    z = -4.15625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1793]],  
              Name = [[2Begin Route]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1792]],  
                x = -1.484375,  
                y = 4.15625,  
                z = -0.203125
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1795]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1796]],  
                    x = 28080.17188,  
                    y = -2010.578125,  
                    z = 11.421875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1798]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1799]],  
                    x = 28078.25,  
                    y = -2016.609375,  
                    z = 11.609375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1801]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1802]],  
                    x = 28072.59375,  
                    y = -2023.15625,  
                    z = 10.5
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1804]],  
              Name = [[3Continue Route]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1803]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1806]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1807]],  
                    x = 28131.46875,  
                    y = -2016.203125,  
                    z = 16.265625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1809]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1810]],  
                    x = 28133.875,  
                    y = -2010.671875,  
                    z = 16.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1812]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1813]],  
                    x = 28138.32813,  
                    y = -2000.625,  
                    z = 16.328125
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1815]],  
              Name = [[4Finish Route]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1814]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1817]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1818]],  
                    x = 28256.70313,  
                    y = -1967.625,  
                    z = 32.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1820]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1821]],  
                    x = 28249.21875,  
                    y = -1956.25,  
                    z = 31.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1823]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1824]],  
                    x = 28239.5625,  
                    y = -1947.09375,  
                    z = 32.21875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1839]],  
              Name = [[2Begin Guard Zone]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1838]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1841]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1842]],  
                    x = 28036.375,  
                    y = -1947.546875,  
                    z = 2.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1844]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1845]],  
                    x = 28040.25,  
                    y = -1968.34375,  
                    z = 4.359375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1847]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1848]],  
                    x = 28021.15625,  
                    y = -1963.3125,  
                    z = 3.234375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1864]],  
              Name = [[3Continue Guard Zone]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1863]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1866]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1867]],  
                    x = 28122.45313,  
                    y = -2026.140625,  
                    z = 17.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1869]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1870]],  
                    x = 28146.875,  
                    y = -2031.15625,  
                    z = 16.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1872]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1873]],  
                    x = 28122.5625,  
                    y = -2042.21875,  
                    z = 16.9375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1875]],  
              Name = [[4Finish Guard Zone]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1874]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1877]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1878]],  
                    x = 28253.78125,  
                    y = -1982.90625,  
                    z = 30.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1880]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1881]],  
                    x = 28297.65625,  
                    y = -1998.84375,  
                    z = 29.640625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1883]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1884]],  
                    x = 28286.04688,  
                    y = -1962.09375,  
                    z = 32.359375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1886]],  
              Name = [[1Exit Guard Zone]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1885]],  
                x = 0.015625,  
                y = -0.578125,  
                z = 0.140625
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1888]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1889]],  
                    x = 27907.42188,  
                    y = -1951.5625,  
                    z = -8.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1891]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1892]],  
                    x = 27937.10938,  
                    y = -1930.265625,  
                    z = -6.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1894]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1895]],  
                    x = 27921.6875,  
                    y = -1915.125,  
                    z = -10.65625
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2023]],  
              Name = [[End Route]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2022]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2025]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2026]],  
                    x = 28243.89063,  
                    y = -1782.40625,  
                    z = 29.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2028]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2029]],  
                    x = 28243.21875,  
                    y = -1777.40625,  
                    z = 29.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2031]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2032]],  
                    x = 28242.8125,  
                    y = -1772.390625,  
                    z = 29.671875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2138]],  
              Name = [[Start Kami Route]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2140]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2141]],  
                    x = 27913.15625,  
                    y = -1876.6875,  
                    z = -10.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2143]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2144]],  
                    x = 27903.09375,  
                    y = -1876.4375,  
                    z = -12.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2146]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2147]],  
                    x = 27909.39063,  
                    y = -1876.796875,  
                    z = -11.21875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2137]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_2149]],  
              Name = [[Start Civ Route]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2151]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2152]],  
                    x = 27916.26563,  
                    y = -1868.703125,  
                    z = -11.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2154]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2155]],  
                    x = 27912.78125,  
                    y = -1870.53125,  
                    z = -11.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_2157]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_2158]],  
                    x = 27910.75,  
                    y = -1874.09375,  
                    z = -11.515625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2148]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_1136]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1260]],  
          Name = [[Ceremony Group]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1240]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1241]],  
                x = 27908.625,  
                y = -1876.734375,  
                z = -11.28125
              },  
              Angle = 0,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1238]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1791]],  
                    LogicEntityAction = [[Client1_1788]],  
                    ActionStep = [[Client1_1790]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1906]],  
                    LogicEntityAction = [[Client1_1903]],  
                    ActionStep = [[Client1_1905]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1950]],  
                    LogicEntityAction = [[Client1_1903]],  
                    ActionStep = [[Client1_1949]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1960]],  
                    LogicEntityAction = [[Client1_1957]],  
                    ActionStep = [[Client1_1959]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1971]],  
                    LogicEntityAction = [[Client1_1968]],  
                    ActionStep = [[Client1_1970]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1983]],  
                    LogicEntityAction = [[Client1_1513]],  
                    ActionStep = [[Client1_1982]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2069]],  
                    LogicEntityAction = [[Client1_2061]],  
                    ActionStep = [[Client1_2068]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2179]],  
                    LogicEntityAction = [[Client1_2176]],  
                    ActionStep = [[Client1_2178]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2190]],  
                    LogicEntityAction = [[Client1_2187]],  
                    ActionStep = [[Client1_2189]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1590]],  
                    Name = [[begin]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1910]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1911]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1591]],  
                        Time = 9,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1592]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1593]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1595]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1596]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1601]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1603]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1604]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1605]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1607]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1608]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1609]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1611]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1612]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1613]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1615]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1616]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1617]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1771]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1772]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1773]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1619]],  
                    Name = [[EXIT1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1620]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1621]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1624]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1626]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1627]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1628]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1630]],  
                    Name = [[exit2]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1631]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1632]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1633]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1643]],  
                        Time = 9,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1644]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1932]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1639]],  
                    Name = [[begi1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1640]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1641]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1647]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1649]],  
                    Name = [[begi2]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1650]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1651]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1654]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1656]],  
                    Name = [[cont1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1657]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1658]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1984]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1661]],  
                    Name = [[cont2]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1662]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1663]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1664]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1939]],  
                        Time = 9,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1940]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1941]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_1943]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_1944]],  
                            Emote = [[]],  
                            Who = [[Client1_1240]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_1945]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1513]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1515]],  
                        Entity = r2.RefId([[Client1_1411]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1514]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1517]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1979]],  
                        Entity = r2.RefId([[Client1_1533]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1978]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1919]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1982]],  
                        Entity = r2.RefId([[Client1_1260]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1981]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1762]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1512]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_2056]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2058]],  
                        Entity = r2.RefId([[Client1_2016]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2057]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_2036]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_2055]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_2033]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1903]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1905]],  
                        Entity = r2.RefId([[Client1_1411]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1904]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1896]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1930]],  
                        Entity = r2.RefId([[Client1_1533]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1929]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1921]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_1949]],  
                        Entity = r2.RefId([[Client1_1240]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_1948]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_1765]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1902]],  
                      Type = [[end of chat step]],  
                      Value = r2.RefId([[Client1_1771]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1762]],  
                    Name = [[home]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1763]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1216]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1764]],  
                    Name = [[2begi]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1825]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_1630]]),  
                        ActivityZoneId = r2.RefId([[Client1_1793]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1826]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_1639]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1765]],  
                    Name = [[1exit]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1934]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1777]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1935]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_1619]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1766]],  
                    Name = [[3cont]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1936]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_1649]]),  
                        ActivityZoneId = r2.RefId([[Client1_1804]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1937]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_1656]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1767]],  
                    Name = [[4fini]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1938]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_1661]]),  
                        ActivityZoneId = r2.RefId([[Client1_1815]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1947]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1768]],  
                    Name = [[santa]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2033]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2023]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2021]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1769]],  
                    Name = [[start]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2159]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2138]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2131]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_1590]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Ceremony Master]],  
              ActivitiesId = {
                [[Client1_1762]],  
                [[Client1_1764]],  
                [[Client1_1765]],  
                [[Client1_1766]],  
                [[Client1_1767]],  
                [[Client1_1768]],  
                [[Client1_1769]]
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1256]],  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1257]],  
                x = 27907.17188,  
                y = -1874.671875,  
                z = -11.9375
              },  
              Angle = -0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1254]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6736942,  
              TrouserModel = 6735918,  
              FeetModel = 6734894,  
              HandsModel = 6735406,  
              ArmModel = 6736430,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6936622,  
              Name = [[Ceremony Guard]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1252]],  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1253]],  
                x = 27907.75,  
                y = -1878.078125,  
                z = -10.984375
              },  
              Angle = -0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1250]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6736942,  
              TrouserModel = 6735918,  
              FeetModel = 6734894,  
              HandsModel = 6735406,  
              ArmModel = 6736430,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6936622,  
              Name = [[Ceremony Guard]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              InheritPos = 1
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1259]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1258]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1474]],  
          Name = [[Security Group]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1411]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1412]],  
                x = 28323.67188,  
                y = -1827.46875,  
                z = 35.90625
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1409]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1909]],  
                    LogicEntityAction = [[Client1_1788]],  
                    ActionStep = [[Client1_1908]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1966]],  
                    LogicEntityAction = [[Client1_1957]],  
                    ActionStep = [[Client1_1965]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1977]],  
                    LogicEntityAction = [[Client1_1968]],  
                    ActionStep = [[Client1_1976]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2094]],  
                    LogicEntityAction = [[Client1_2089]],  
                    ActionStep = [[Client1_2093]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2166]],  
                    LogicEntityAction = [[Client1_2061]],  
                    ActionStep = [[Client1_2165]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2185]],  
                    LogicEntityAction = [[Client1_2176]],  
                    ActionStep = [[Client1_2184]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1438]],  
                    Name = [[1exit]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1439]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1886]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1517]],  
                    Name = [[Homes]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1518]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1216]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1896]],  
                    Name = [[2begi]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1897]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1839]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1898]],  
                    Name = [[3cont]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1899]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1864]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1900]],  
                    Name = [[4fini]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1901]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1875]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1954]],  
                    Name = [[secur]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1955]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1425]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2095]],  
                    Name = [[end]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2096]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1476]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 18,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6767150,  
              Name = [[Security Guard Leader]],  
              ActivitiesId = {
                [[Client1_1438]],  
                [[Client1_1517]],  
                [[Client1_1896]],  
                [[Client1_1898]],  
                [[Client1_1900]],  
                [[Client1_1954]],  
                [[Client1_2095]]
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1419]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1420]],  
                x = 28314.67188,  
                y = -1851.375,  
                z = 37.140625
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1417]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Security Kami]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1415]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1416]],  
                x = 28333.5625,  
                y = -1838.828125,  
                z = 39.078125
              },  
              Angle = 3.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1413]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Security Kami]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1403]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1404]],  
                x = 28321.70313,  
                y = -1836.71875,  
                z = 36.375
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1401]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6767150,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1407]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1408]],  
                x = 28318.65625,  
                y = -1837.84375,  
                z = 36.140625
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1405]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6767150,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1399]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1400]],  
                x = 28324.67188,  
                y = -1835.90625,  
                z = 36.765625
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1397]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6767150,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1395]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1396]],  
                x = 28326.8125,  
                y = -1834.015625,  
                z = 37.25
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1393]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6767150,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1391]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1392]],  
                x = 28329.82813,  
                y = -1832.828125,  
                z = 37.703125
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1389]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6767150,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]],  
              InheritPos = 1
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1473]],  
            x = -400.84375,  
            y = -91.5625,  
            z = -46.71875
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1472]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1533]],  
          Name = [[Civilian Group]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1525]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1526]],  
                x = 27910.3125,  
                y = -1873.171875,  
                z = -11.703125
              },  
              Angle = -2.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1523]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1931]],  
                    LogicEntityAction = [[Client1_1903]],  
                    ActionStep = [[Client1_1930]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1953]],  
                    LogicEntityAction = [[Client1_1788]],  
                    ActionStep = [[Client1_1952]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1963]],  
                    LogicEntityAction = [[Client1_1957]],  
                    ActionStep = [[Client1_1962]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1974]],  
                    LogicEntityAction = [[Client1_1968]],  
                    ActionStep = [[Client1_1973]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1980]],  
                    LogicEntityAction = [[Client1_1513]],  
                    ActionStep = [[Client1_1979]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2064]],  
                    LogicEntityAction = [[Client1_2061]],  
                    ActionStep = [[Client1_2063]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2182]],  
                    LogicEntityAction = [[Client1_2176]],  
                    ActionStep = [[Client1_2181]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2193]],  
                    LogicEntityAction = [[Client1_2187]],  
                    ActionStep = [[Client1_2192]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1755]],  
                    Name = [[Chat1]],  
                    Components = {
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1756]],  
                    Name = [[Chat2]],  
                    Components = {
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1757]],  
                    Name = [[Chat3]],  
                    Components = {
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1758]],  
                    Name = [[Chat4]],  
                    Components = {
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1759]],  
                    Name = [[Chat5]],  
                    Components = {
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1760]],  
                    Name = [[Chat6]],  
                    Components = {
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_1761]],  
                    Name = [[Chat7]],  
                    Components = {
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1917]],  
                    Name = [[home]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1918]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1216]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1919]],  
                    Name = [[start]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1920]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_2149]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2160]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1921]],  
                    Name = [[1exit]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1922]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1572]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1923]],  
                    Name = [[2begi]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1924]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1561]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1925]],  
                    Name = [[3cont]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1926]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1550]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1927]],  
                    Name = [[4fini]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1928]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1539]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2065]],  
                    Name = [[santa]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2066]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1476]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 14,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 6,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Krannus]],  
              ActivitiesId = {
                [[Client1_1917]],  
                [[Client1_1919]],  
                [[Client1_1921]],  
                [[Client1_1923]],  
                [[Client1_1925]],  
                [[Client1_1927]],  
                [[Client1_2065]]
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1529]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1530]],  
                x = 27912.67188,  
                y = -1873.71875,  
                z = -11.3125
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1527]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 3118,  
              HairColor = 1,  
              Tattoo = 2,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Kaori]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1521]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1522]],  
                x = 27911.53125,  
                y = -1875.1875,  
                z = -11.28125
              },  
              Angle = -2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1519]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 2,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Kyallih]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1532]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1531]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_2083]],  
          Name = [[End Boss Group]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_2016]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2017]],  
                x = 28238.26563,  
                y = -1756.703125,  
                z = 27.609375
              },  
              Angle = -1.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2014]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2059]],  
                    LogicEntityAction = [[Client1_2056]],  
                    ActionStep = [[Client1_2058]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_2036]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_2037]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_2038]],  
                            Emote = [[]],  
                            Who = [[Client1_2012]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_2039]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_2041]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_2042]],  
                            Emote = [[]],  
                            Who = [[Client1_2012]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_2043]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_2045]],  
                        Time = 6,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_2046]],  
                            Emote = [[]],  
                            Who = [[Client1_2016]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_2047]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_2051]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_2052]],  
                            Emote = [[]],  
                            Who = [[Client1_2012]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_2053]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_2089]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2093]],  
                        Entity = r2.RefId([[Client1_1474]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2092]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_2095]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_2088]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2086]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2087]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Santa's Little Helper]],  
              ActivitiesId = {
                [[Client1_2086]]
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_2012]],  
              Base = [[palette.entities.npcs.guards.z_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_2013]],  
                x = 28244.04688,  
                y = -1760.5625,  
                z = 27.9375
              },  
              Angle = -2.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_2010]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 14,  
              HairType = 6732846,  
              HairColor = 0,  
              Tattoo = 0,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6734382,  
              TrouserModel = 6733358,  
              FeetModel = 6731822,  
              HandsModel = 6732334,  
              ArmModel = 6733870,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6771246,  
              Name = [[Santa Clause]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              InheritPos = 1
            }
          },  
          ActivitiesId = {
            [[Client1_2084]],  
            [[Client1_2085]]
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_2082]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_2081]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 1,  
      Version = 1,  
      Title = [[Ceremony]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_2167]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_2176]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_2178]],  
                Entity = r2.RefId([[Client1_1260]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_2177]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1762]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_2181]],  
                Entity = r2.RefId([[Client1_1533]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_2180]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1917]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_2184]],  
                Entity = r2.RefId([[Client1_1474]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_2183]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1438]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_2175]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[Reset]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_2169]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_2168]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 729,  
      LocationId = [[Client1_1287]],  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2170]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:Reset]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_2171]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_2187]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_2189]],  
                Entity = r2.RefId([[Client1_1260]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_2188]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1769]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_2192]],  
                Entity = r2.RefId([[Client1_1533]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_2191]],  
                  Type = [[begin activity sequence]],  
                  Value = r2.RefId([[Client1_1919]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_2186]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[Start the ceremony]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_2173]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_2172]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 478,  
      LocationId = [[Client1_1287]],  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2174]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 2:Start]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1593]],  
        Count = 6,  
        Text = [[Welcome to the Anti-Yubo Ceremony.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1594]],  
        Count = 1,  
        Text = [[Welcome to the Anti-Yubo Ceremony.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1597]],  
        Count = 1,  
        Text = [[Please listen to what I am going to say.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1598]],  
        Count = 1,  
        Text = [[Please listen to what I am going to say.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1599]],  
        Count = 1,  
        Text = [[Please listen to what I am going to tell you.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1600]],  
        Count = 1,  
        Text = [[Please listen to what I am going to tell you.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1601]],  
        Count = 2,  
        Text = [[Please listen carefully.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1602]],  
        Count = 1,  
        Text = [[Please listen carefully.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1605]],  
        Count = 2,  
        Text = [[We have 4 special trained Yubo.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1606]],  
        Count = 1,  
        Text = [[We have 4 special trained Yubo.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1609]],  
        Count = 2,  
        Text = [[They each have a Tower.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1610]],  
        Count = 1,  
        Text = [[They each have a Tower.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1613]],  
        Count = 2,  
        Text = [[Your task in this Ceremony is to kill them all.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1614]],  
        Count = 1,  
        Text = [[Your task in this Ceremony is to kill them all.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1617]],  
        Count = 3,  
        Text = [[We ask you not to disturb the Ceremony.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1618]],  
        Count = 1,  
        Text = [[We ask you not to disturb the Ceremony.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1622]],  
        Count = 2,  
        Text = [[This is the first Yubo]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1623]],  
        Count = 1,  
        Text = [[This is the first Yubo]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1624]],  
        Count = 5,  
        Text = [[This is the first Yubo.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1625]],  
        Count = 1,  
        Text = [[This is the first Yubo.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1628]],  
        Count = 3,  
        Text = [[Please kill him to continue.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1629]],  
        Count = 1,  
        Text = [[Please kill him to continue.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1633]],  
        Count = 4,  
        Text = [[Well done! Please follow!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1634]],  
        Count = 1,  
        Text = [[Well done! Please follow!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1642]],  
        Count = 12,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1645]],  
        Count = 3,  
        Text = [[Please do not kill them before I arrive.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1646]],  
        Count = 1,  
        Text = [[Please do not kill them before I arrive.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1647]],  
        Count = 3,  
        Text = [[Please kill the second Yubo to continue.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1648]],  
        Count = 1,  
        Text = [[Please kill the second Yubo to continue.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1652]],  
        Count = 1,  
        Text = [[Thank you, please follow me.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1653]],  
        Count = 1,  
        Text = [[Thank you, please follow me.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1654]],  
        Count = 3,  
        Text = [[Thank you. Please follow me.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1655]],  
        Count = 1,  
        Text = [[Thank you. Please follow me.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1659]],  
        Count = 2,  
        Text = [[And here is the second Yubo.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1660]],  
        Count = 1,  
        Text = [[And here is the second Yubo.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1664]],  
        Count = 3,  
        Text = [[And now the last one.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1665]],  
        Count = 1,  
        Text = [[And now the last one.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1773]],  
        Count = 2,  
        Text = [[Please follow me.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1774]],  
        Count = 1,  
        Text = [[Please follow me.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1932]],  
        Count = 2,  
        Text = [[Do not kill them before I arrive.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1933]],  
        Count = 1,  
        Text = [[Do not kill them before I arrive.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1941]],  
        Count = 2,  
        Text = [[Please remember that I will not say anything after this, since I am limited by 7 chat sequences.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1942]],  
        Count = 1,  
        Text = [[Please remember that I will not say anything after this, since I am limited by 7 chat sequences.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1945]],  
        Count = 2,  
        Text = [[Thank you for your time.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1946]],  
        Count = 1,  
        Text = [[Thank you for your time.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1984]],  
        Count = 2,  
        Text = [[Here is the third Yubo.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1985]],  
        Count = 1,  
        Text = [[Here is the third Yubo.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2039]],  
        Count = 4,  
        Text = [[HO HO HO!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2040]],  
        Count = 1,  
        Text = [[HO HO HO!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2043]],  
        Count = 5,  
        Text = [[Merry Christmas!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2044]],  
        Count = 1,  
        Text = [[Merry Christmas!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2047]],  
        Count = 4,  
        Text = [[Seriously, an Anti-Yubo Ceremony? xD]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2048]],  
        Count = 1,  
        Text = [[Seriously, an Anti-Yubo Ceremony? xD]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2049]],  
        Count = 2,  
        Text = [[Seriously, an Anti-Yubo Ceremony?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2050]],  
        Count = 1,  
        Text = [[Seriously, an Anti-Yubo Ceremony?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2053]],  
        Count = 4,  
        Text = [[Poor Yubos... :P]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2054]],  
        Count = 1,  
        Text = [[Poor Yubos... :P]]
      }
    },  
    InstanceId = [[Client1_1133]]
  }
}