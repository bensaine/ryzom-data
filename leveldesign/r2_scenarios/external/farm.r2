scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client1_12]],  
      Season = [[winter]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts11]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 1,  
    TextManager = 0,  
    EventType = 0,  
    LogicEntityReaction = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ActionStep = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    Npc = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 4,  
    LocationId = 49,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 16,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      ManualWeather = 0,  
      LocationId = [[]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Name = [[Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_15]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_13]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -1272.21875,  
                x = 29633.64063,  
                InstanceId = [[Client1_16]],  
                Class = [[Position]],  
                z = 74.796875
              },  
              Angle = -2.03125,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_19]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_17]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -1284.515625,  
                x = 29645.3125,  
                InstanceId = [[Client1_20]],  
                Class = [[Position]],  
                z = 76.03125
              },  
              Angle = -2.75,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_23]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_21]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1302,  
                x = 29635.32813,  
                InstanceId = [[Client1_24]],  
                Class = [[Position]],  
                z = 80.734375
              },  
              Angle = -2.525073528,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_90]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_88]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1278.359375,  
                x = 29620.32813,  
                InstanceId = [[Client1_91]],  
                Class = [[Position]],  
                z = 75
              },  
              Angle = -0.7089479566,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_96]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_98]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1291.28125,  
                    x = 29689.60938,  
                    InstanceId = [[Client1_99]],  
                    Class = [[Position]],  
                    z = 71.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_101]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1290.75,  
                    x = 29697.34375,  
                    InstanceId = [[Client1_102]],  
                    Class = [[Position]],  
                    z = 74.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_104]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1296.59375,  
                    x = 29700.10938,  
                    InstanceId = [[Client1_105]],  
                    Class = [[Position]],  
                    z = 73.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_107]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1302.765625,  
                    x = 29696.42188,  
                    InstanceId = [[Client1_108]],  
                    Class = [[Position]],  
                    z = 72.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_110]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1300.953125,  
                    x = 29689.53125,  
                    InstanceId = [[Client1_111]],  
                    Class = [[Position]],  
                    z = 74.515625
                  }
                }
              },  
              Position = {
                y = -5.109375,  
                x = -61.421875,  
                InstanceId = [[Client1_95]],  
                Class = [[Position]],  
                z = 7.140625
              }
            },  
            {
              InstanceId = [[Client1_120]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_118]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -1282.453125,  
                x = 29621.01563,  
                InstanceId = [[Client1_121]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = -2.015625,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_137]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_135]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              Position = {
                y = -1275.28125,  
                x = 29628.625,  
                InstanceId = [[Client1_138]],  
                Class = [[Position]],  
                z = 74.890625
              },  
              Angle = -1.21875,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_141]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_139]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 3]],  
              Position = {
                y = -1288.53125,  
                x = 29642.95313,  
                InstanceId = [[Client1_142]],  
                Class = [[Position]],  
                z = 76.6875
              },  
              Angle = -1.203125,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_145]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_143]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -1232.109375,  
                x = 29646.65625,  
                InstanceId = [[Client1_146]],  
                Class = [[Position]],  
                z = 79.921875
              },  
              Angle = -2.064988852,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_151]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bird totem 1]],  
              Position = {
                y = -1263.859375,  
                x = 29622.85938,  
                InstanceId = [[Client1_154]],  
                Class = [[Position]],  
                z = 74
              },  
              Angle = -1.066494703,  
              Base = [[palette.entities.botobjects.totem_bird]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_181]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_179]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bothaya I 1]],  
              Position = {
                y = -1222.296875,  
                x = 29634.34375,  
                InstanceId = [[Client1_182]],  
                Class = [[Position]],  
                z = 80.90625
              },  
              Angle = -1.65625,  
              Base = [[palette.entities.botobjects.fy_s1_burnedtree_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_189]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_187]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga I 1]],  
              Position = {
                y = -1257.875,  
                x = 29651.89063,  
                InstanceId = [[Client1_190]],  
                Class = [[Position]],  
                z = 76.984375
              },  
              Angle = -1.953125,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_197]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_195]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[olansi I 1]],  
              Position = {
                y = -1220.890625,  
                x = 29611.35938,  
                InstanceId = [[Client1_198]],  
                Class = [[Position]],  
                z = 76.59375
              },  
              Angle = -1.953125,  
              Base = [[palette.entities.botobjects.fy_s2_palmtree_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_201]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_199]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[olansi V 1]],  
              Position = {
                y = -1267.03125,  
                x = 29600.95313,  
                InstanceId = [[Client1_202]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = -1.953125,  
              Base = [[palette.entities.botobjects.fy_s2_palmtree_e]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_205]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_203]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[olash I 1]],  
              Position = {
                y = -1227.90625,  
                x = 29620.35938,  
                InstanceId = [[Client1_206]],  
                Class = [[Position]],  
                z = 76.8125
              },  
              Angle = -1.551699281,  
              Base = [[palette.entities.botobjects.fy_s2_coconuts_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_209]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_207]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[olash II 1]],  
              Position = {
                y = -1258.984375,  
                x = 29609.65625,  
                InstanceId = [[Client1_210]],  
                Class = [[Position]],  
                z = 74.65625
              },  
              Angle = -6.665513992,  
              Base = [[palette.entities.botobjects.fy_s2_coconuts_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_217]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_215]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[savaniel I 1]],  
              Position = {
                y = -1275.453125,  
                x = 29594.51563,  
                InstanceId = [[Client1_218]],  
                Class = [[Position]],  
                z = 74.78125
              },  
              Angle = -1.953125,  
              Base = [[palette.entities.botobjects.fy_s2_savantree_a]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Version = 3
    },  
    {
      Cost = 21,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_10]],  
      LocationId = [[Client1_12]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_31]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_29]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1273.234375,  
                x = 29633.29688,  
                InstanceId = [[Client1_32]],  
                Class = [[Position]],  
                z = 74.828125
              },  
              Angle = -2.71875,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_35]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_33]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1280.40625,  
                x = 29632.71875,  
                InstanceId = [[Client1_36]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = -2.71875,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_39]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_37]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Voracious Mektoub]],  
              Position = {
                y = -1291.328125,  
                x = 29630.67188,  
                InstanceId = [[Client1_40]],  
                Class = [[Position]],  
                z = 77.234375
              },  
              Angle = -0.6941679716,  
              Base = [[palette.entities.creatures.chhpf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_116]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_114]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Feral Mektoub]],  
              Position = {
                y = -1280.734375,  
                x = 29634.42188,  
                InstanceId = [[Client1_117]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = -2.015625,  
              Base = [[palette.entities.creatures.chhle3]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_59]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_57]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_122]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[5]],  
                        InstanceId = [[Client1_123]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_129]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_131]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[Sit Down]],  
                          InstanceId = [[Client1_132]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_133]],  
                        Entity = r2.RefId([[Client1_59]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_130]],  
                      Value = r2.RefId([[Client1_123]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_131]],  
                    Name = [[]],  
                    InstanceId = [[Client1_134]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_133]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6699566,  
              Angle = -2.25,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Position = {
                y = -1308.65625,  
                x = 29634.65625,  
                InstanceId = [[Client1_60]],  
                Class = [[Position]],  
                z = 82.015625
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              WeaponRightHand = 6756910,  
              ArmColor = 3,  
              Name = [[desert guard 1]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 5,  
              Tattoo = 1
            }
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          InstanceId = [[Client1_71]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = -10.96875,  
            x = 2.375,  
            InstanceId = [[Client1_70]],  
            Class = [[Position]],  
            z = 0.1875
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_69]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_63]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_61]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6699566,  
              Angle = -2.71875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Position = {
                y = -1294.34375,  
                x = 29635.125,  
                InstanceId = [[Client1_64]],  
                Class = [[Position]],  
                z = 81.046875
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              WeaponRightHand = 6755886,  
              ArmColor = 3,  
              Name = [[desert guard 2]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 1,  
              Tattoo = 22
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_78]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 2,  
              EyesColor = 3,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_76]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6705198,  
              Angle = -0.25,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1299.328125,  
                x = 29630.4375,  
                InstanceId = [[Client1_79]],  
                Class = [[Position]],  
                z = 82
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753838,  
              ArmColor = 3,  
              Name = [[desert milicia 2]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 3
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_86]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 0,  
              EyesColor = 2,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_84]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6705198,  
              Angle = -2.375,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1295.71875,  
                x = 29632.73438,  
                InstanceId = [[Client1_87]],  
                Class = [[Position]],  
                z = 81.46875
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_slash_f2.creature]],  
              WeaponRightHand = 6754350,  
              ArmColor = 3,  
              Name = [[desert milicia 3]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 15
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_94]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = -6.484375,  
            x = -61.078125,  
            InstanceId = [[Client1_93]],  
            Class = [[Position]],  
            z = 6.734375
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_92]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_67]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 8,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 3,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 9,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_65]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_112]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_113]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_96]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 6699566,  
              Angle = -2.71875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              JacketModel = 6702126,  
              Position = {
                y = -1292.40625,  
                x = 29698.20313,  
                InstanceId = [[Client1_68]],  
                Class = [[Position]],  
                z = 74.09375
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]],  
              WeaponRightHand = 6755374,  
              ArmColor = 3,  
              Name = [[desert guard 3]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 1
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_82]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 9,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 1,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 8,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_80]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6699566,  
              Angle = -2.453125,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_220]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 6701614,  
              WeaponLeftHand = 6773294,  
              JacketModel = 6702126,  
              Position = {
                y = -1299.1875,  
                x = 29694.1875,  
                InstanceId = [[Client1_83]],  
                Class = [[Position]],  
                z = 73.90625
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 3,  
              Sheet = [[ring_melee_tank_slash_f2.creature]],  
              WeaponRightHand = 6754350,  
              ArmColor = 3,  
              Name = [[desert warrior 1]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 4,  
              Tattoo = 6
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_74]],  
              ActivitiesId = {
              },  
              HairType = 5623598,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 11,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_72]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6705198,  
              Angle = -2.71875,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_220]],  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 6753326,  
              JacketModel = 6707246,  
              Position = {
                y = -1291.171875,  
                x = 29690.1875,  
                InstanceId = [[Client1_75]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 2,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              WeaponRightHand = 6753326,  
              ArmColor = 3,  
              Name = [[desert milicia 1]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 7,  
              Tattoo = 8
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client1_281]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 3]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_280]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_279]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_257]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_255]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_282]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_283]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_96]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kizarak]],  
              Position = {
                y = -1285.09375,  
                x = 29717.23438,  
                InstanceId = [[Client1_258]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckcid1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_261]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_259]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kizoar]],  
              Position = {
                y = -1289.28125,  
                x = 29720.28125,  
                InstanceId = [[Client1_262]],  
                Class = [[Position]],  
                z = 75.21875
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckiid1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_253]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_251]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kirosta]],  
              Position = {
                y = -1285.765625,  
                x = 29721.10938,  
                InstanceId = [[Client1_254]],  
                Class = [[Position]],  
                z = 75.1875
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckfid1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_249]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_247]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kipucka]],  
              Position = {
                y = -1282.671875,  
                x = 29720.4375,  
                InstanceId = [[Client1_250]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckeid1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_245]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_243]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kipesta]],  
              Position = {
                y = -1279.96875,  
                x = 29718.73438,  
                InstanceId = [[Client1_246]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckjid1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_225]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_223]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kiban]],  
              Position = {
                y = -1277.40625,  
                x = 29723.60938,  
                InstanceId = [[Client1_226]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckgid1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_277]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_275]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Warrior Kidinak]],  
              Position = {
                y = -1280.109375,  
                x = 29724.21875,  
                InstanceId = [[Client1_278]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = -2.953125,  
              Base = [[palette.entities.creatures.ckaid2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_233]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_231]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kincher]],  
              Position = {
                y = -1282.96875,  
                x = 29725.48438,  
                InstanceId = [[Client1_234]],  
                Class = [[Position]],  
                z = 75.15625
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckdid1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_237]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_235]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kinrey]],  
              Position = {
                y = -1286.15625,  
                x = 29725.65625,  
                InstanceId = [[Client1_238]],  
                Class = [[Position]],  
                z = 75.328125
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckbid1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_241]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_239]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Warrior Kipee]],  
              Position = {
                y = -1289.046875,  
                x = 29726.07813,  
                InstanceId = [[Client1_242]],  
                Class = [[Position]],  
                z = 75.625
              },  
              Angle = -2.875,  
              Base = [[palette.entities.creatures.ckhid1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      Name = [[Act 1:act 1]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Version = 3
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}