scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_1023]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes36]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Camp des guardes]],  
      Season = [[Spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint01]]
    }
  },  
  InstanceId = [[Client1_1014]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_1012]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  Name = [[New scenario]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_1013]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1010]]
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    Road = 0,  
    Npc = 0,  
    ActionStep = 1,  
    ActivityStep = 1,  
    TalkTo = 0,  
    NpcGrpFeature = 1,  
    EventType = 0,  
    Position = 0,  
    Location = 1,  
    ActionType = 0,  
    LogicEntityBehavior = 1,  
    WayPoint = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_1017]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1015]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Name = [[Permanent]],  
      Events = {
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      Title = [[]],  
      Version = 5,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1030]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1031]],  
                x = 34996.92188,  
                y = -3816.03125,  
                z = -18.421875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1028]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1034]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1035]],  
                x = 34976.14063,  
                y = -3839.21875,  
                z = -18.53125
              },  
              Angle = 1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1032]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1038]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1039]],  
                x = 34999.92188,  
                y = -3741.59375,  
                z = -12.453125
              },  
              Angle = 2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1036]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1046]],  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1047]],  
                x = 34973.04688,  
                y = -3784.390625,  
                z = -16.265625
              },  
              Angle = 1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1044]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bamboo I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1050]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1051]],  
                x = 34987.42188,  
                y = -3810.125,  
                z = -18.6875
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1048]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1054]],  
              Base = [[palette.entities.botobjects.chariot]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1055]],  
                x = 34992.23438,  
                y = -3817.328125,  
                z = -18.5625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1052]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1058]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1059]],  
                x = 34960.60938,  
                y = -3837.734375,  
                z = -19.015625
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1056]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1062]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1063]],  
                x = 34994.39063,  
                y = -3826.515625,  
                z = -18.453125
              },  
              Angle = 2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1060]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1066]],  
              Base = [[palette.entities.botobjects.tr_s2_lokness_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1067]],  
                x = 34944.64063,  
                y = -3756.453125,  
                z = -17.640625
              },  
              Angle = -1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1064]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[weeding I 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1074]],  
              Base = [[palette.entities.botobjects.tr_s3_trumpet_c]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1075]],  
                x = 34999.20313,  
                y = -3765.078125,  
                z = -13.390625
              },  
              Angle = -1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1072]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[trumperer III 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1082]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1083]],  
                x = 34955.07813,  
                y = -3809.625,  
                z = -18.328125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1080]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1086]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1088]],  
                x = 34985.74219,  
                y = -3827.916748,  
                z = -19.27113724
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1087]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1091]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1093]],  
                x = 34964.76563,  
                y = -3811,  
                z = -18.71875
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1092]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1096]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1097]],  
                x = 34991.21875,  
                y = -3768.28125,  
                z = -14.9375
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1094]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1100]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1101]],  
                x = 34981.95313,  
                y = -3766.59375,  
                z = -15.546875
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1098]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1104]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1105]],  
                x = 34922.9375,  
                y = -3765.53125,  
                z = -19.5
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1102]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1108]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1109]],  
                x = 34931.5625,  
                y = -3764.265625,  
                z = -19.5625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1106]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1116]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1117]],  
                x = 34960.25,  
                y = -3809.859375,  
                z = -18.5625
              },  
              Angle = -1.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1114]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1120]],  
              Base = [[palette.entities.botobjects.barrels_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1121]],  
                x = 34992.34375,  
                y = -3813.65625,  
                z = -18.515625
              },  
              Angle = 2.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1118]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[3 barrels 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_1223]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1225]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1226]],  
                    x = 35143.95313,  
                    y = -3770.796875,  
                    z = -20.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1228]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1229]],  
                    x = 35120.89063,  
                    y = -3775.5625,  
                    z = -16.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1231]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1232]],  
                    x = 35100.46875,  
                    y = -3788.078125,  
                    z = -17.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1234]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1235]],  
                    x = 35035.29688,  
                    y = -3788.734375,  
                    z = -19.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1237]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1238]],  
                    x = 34993.9375,  
                    y = -3807.28125,  
                    z = -18.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_1240]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1241]],  
                    x = 34977.25,  
                    y = -3826.3125,  
                    z = -19.046875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1222]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1317]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1319]],  
                x = 34955.07813,  
                y = -3809.625,  
                z = -18.328125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1318]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1322]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1323]],  
                x = 35167.03125,  
                y = -3764.71875,  
                z = -18.65625
              },  
              Angle = 2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1320]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tryker tent 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1326]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1327]],  
                x = 35156.0625,  
                y = -3794.203125,  
                z = -18.1875
              },  
              Angle = 1.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1324]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1330]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1331]],  
                x = 35153.125,  
                y = -3751.359375,  
                z = -19.671875
              },  
              Angle = 4.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1328]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1334]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1335]],  
                x = 35025.1875,  
                y = -3808.765625,  
                z = -17.078125
              },  
              Angle = -2.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1332]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1338]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1339]],  
                x = 35022.5625,  
                y = -3804.453125,  
                z = -17.140625
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1336]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1346]],  
              Base = [[palette.entities.botobjects.vegetable_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1347]],  
                x = 35019.23438,  
                y = -3795.90625,  
                z = -17.046875
              },  
              Angle = -2.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1344]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living gateway 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1350]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1351]],  
                x = 35018.89063,  
                y = -3786.265625,  
                z = -16.703125
              },  
              Angle = -2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1348]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1354]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1355]],  
                x = 35017.73438,  
                y = -3781.4375,  
                z = -16.171875
              },  
              Angle = 0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1352]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1358]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1359]],  
                x = 35016.79688,  
                y = -3776.265625,  
                z = -15.53125
              },  
              Angle = -3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1356]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1362]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1363]],  
                x = 35016.17188,  
                y = -3771.375,  
                z = -14.984375
              },  
              Angle = 0.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1360]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1366]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1367]],  
                x = 35015.45313,  
                y = -3766.40625,  
                z = -15.046875
              },  
              Angle = -2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1364]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1370]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1371]],  
                x = 35014.125,  
                y = -3761.578125,  
                z = -14.984375
              },  
              Angle = 0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1368]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1374]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1375]],  
                x = 35012.73438,  
                y = -3756.59375,  
                z = -14.015625
              },  
              Angle = -3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1372]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 9]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1378]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1379]],  
                x = 35012.04688,  
                y = -3751.625,  
                z = -13.796875
              },  
              Angle = 0.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1376]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 10]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1382]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1383]],  
                x = 35011.89063,  
                y = -3746.65625,  
                z = -14.015625
              },  
              Angle = 3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1380]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 11]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1386]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1387]],  
                x = 35011.4375,  
                y = -3741.734375,  
                z = -13.640625
              },  
              Angle = 0.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1384]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 12]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1390]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1391]],  
                x = 35010.0625,  
                y = -3736.921875,  
                z = -13.515625
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1388]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 13]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1394]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1395]],  
                x = 35007.32813,  
                y = -3732.875,  
                z = -13.453125
              },  
              Angle = 0.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1392]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 14]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1398]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1399]],  
                x = 35002.96875,  
                y = -3730.3125,  
                z = -13.15625
              },  
              Angle = -1.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1396]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 15]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1402]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1403]],  
                x = 34998.14063,  
                y = -3729.484375,  
                z = -13.25
              },  
              Angle = 1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1400]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 16]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1406]],  
              Base = [[palette.entities.botobjects.vegetable_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1407]],  
                x = 34982.60938,  
                y = -3729.078125,  
                z = -15.609375
              },  
              Angle = 1.546875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1404]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living gateway 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1410]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1411]],  
                x = 34970.625,  
                y = -3727.9375,  
                z = -13.625
              },  
              Angle = -1.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1408]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 17]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1414]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1415]],  
                x = 34965.54688,  
                y = -3727.4375,  
                z = -12.828125
              },  
              Angle = 1.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1412]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 18]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1418]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1420]],  
                x = 34989.96875,  
                y = -3729.3125,  
                z = -15.375
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1419]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1423]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1425]],  
                x = 34976.20313,  
                y = -3728.671875,  
                z = -14.96875
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1424]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1432]],  
              Base = [[palette.entities.botobjects.tr_s2_palmtree_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1433]],  
                x = 34961.4375,  
                y = -3727.125,  
                z = -15.46875
              },  
              Angle = 0.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1430]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[olansi VI 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1436]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1437]],  
                x = 34957.17188,  
                y = -3726.953125,  
                z = -17
              },  
              Angle = 4.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1434]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 19]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1440]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1441]],  
                x = 34952.14063,  
                y = -3726.890625,  
                z = -17.46875
              },  
              Angle = 1.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1438]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 20]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1444]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1445]],  
                x = 34947.0625,  
                y = -3726.71875,  
                z = -18.390625
              },  
              Angle = -1.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1442]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 21]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1448]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1449]],  
                x = 34942.0625,  
                y = -3726.765625,  
                z = -18.734375
              },  
              Angle = 1.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1446]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 22]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1452]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1453]],  
                x = 34936.89063,  
                y = -3726.796875,  
                z = -19.078125
              },  
              Angle = -1.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1450]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 23]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1456]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1457]],  
                x = 34931.92188,  
                y = -3726.21875,  
                z = -19.140625
              },  
              Angle = 1.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1454]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 24]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1460]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1461]],  
                x = 34926.9375,  
                y = -3724.53125,  
                z = -18.96875
              },  
              Angle = -1.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1458]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 25]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1464]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1465]],  
                x = 34922.375,  
                y = -3722.421875,  
                z = -18.71875
              },  
              Angle = 1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1462]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 26]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1468]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1469]],  
                x = 34917.78125,  
                y = -3719.9375,  
                z = -18.3125
              },  
              Angle = -2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1466]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 27]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1472]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1473]],  
                x = 34913.67188,  
                y = -3717.34375,  
                z = -18.0625
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1470]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 28]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1476]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1477]],  
                x = 34909.54688,  
                y = -3714.203125,  
                z = -17.96875
              },  
              Angle = -2.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1474]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 29]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1480]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1481]],  
                x = 34905.875,  
                y = -3710.859375,  
                z = -17.4375
              },  
              Angle = 0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1478]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 30]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1484]],  
              Base = [[palette.entities.botobjects.vegetable_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1485]],  
                x = 34901.53125,  
                y = -3708.75,  
                z = -16.796875
              },  
              Angle = -1.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1482]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[living wall 31]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1488]],  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1490]],  
                x = 34999.23438,  
                y = -3776.65625,  
                z = -14.703125
              },  
              Angle = 1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1489]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Activities = {
                },  
                Actions = {
                }
              },  
              InheritPos = 1,  
              Name = [[bamboo I 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1585]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1586]],  
                x = 35158.8125,  
                y = -3758.78125,  
                z = -19.25
              },  
              Angle = -2.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1583]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1666]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1667]],  
                x = 34967.67188,  
                y = -3831.984375,  
                z = -19.875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1664]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 5]]
            }
          },  
          InstanceId = [[Client1_1018]]
        }
      },  
      Counters = {
      },  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1016]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 0
    },  
    {
      InstanceId = [[Client1_1021]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1019]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Name = [[Act 1:Act 1]],  
      Events = {
      },  
      WeatherValue = 78,  
      LocationId = [[Client1_1023]],  
      Title = [[]],  
      Version = 5,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1042]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1043]],  
                x = 34968.17188,  
                y = -3744.34375,  
                z = -12.328125
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1040]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1145]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1146]],  
                x = 34977.01563,  
                y = -3831.953125,  
                z = -19.484375
              },  
              Angle = 1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1143]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1148]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1149]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 7,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 25,  
              EyesColor = 6,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 3,  
              MorphTarget4 = 7,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6721070,  
              HandsModel = 6721326,  
              ArmModel = 6723118,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 5,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6766638,  
              WeaponLeftHand = 0,  
              Name = [[Rosira]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1168]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1169]],  
                x = 35147.79688,  
                y = -3772.0625,  
                z = -19.8125
              },  
              Angle = 3.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1166]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1262]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1263]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1264]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1265]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1223]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_1492]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_1493]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 29,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 0,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 1,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              WeaponLeftHand = 0,  
              Name = [[Kridix]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            }
          },  
          InstanceId = [[Client1_1022]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1132]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1124]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1125]],  
                x = 34973.32813,  
                y = -3827.296875,  
                z = -19.765625
              },  
              Angle = 5.846852994,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1122]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 0,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              MorphTarget7 = 7,  
              MorphTarget8 = 4,  
              JacketModel = 6723630,  
              TrouserModel = 6722350,  
              FeetModel = 6721070,  
              HandsModel = 6721582,  
              ArmModel = 6723118,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 1,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6765870,  
              WeaponLeftHand = 0,  
              Name = [[Ba'Massey]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1128]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1129]],  
                x = 34977.59375,  
                y = -3828.40625,  
                z = -19.5
              },  
              Angle = 3.769911184,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1126]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 6721838,  
              HairColor = 3,  
              Tattoo = 15,  
              EyesColor = 3,  
              MorphTarget1 = 1,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 5,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721582,  
              ArmModel = 6722862,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6765614,  
              WeaponLeftHand = 0,  
              Name = [[Be'Daghan]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1135]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1137]],  
                x = 34977.51563,  
                y = -3830.75,  
                z = -19.734375
              },  
              Angle = 2.617993878,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1133]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 12,  
              HairType = 6722094,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 7,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721326,  
              ArmModel = 6722862,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6765614,  
              WeaponLeftHand = 0,  
              Name = [[Be'Gany]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1140]],  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1142]],  
                x = 34975.73438,  
                y = -3831.875,  
                z = -19.890625
              },  
              Angle = 1.431169987,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1138]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 1,  
              HairType = 6721838,  
              HairColor = 1,  
              Tattoo = 30,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              JacketModel = 6723374,  
              TrouserModel = 6722606,  
              FeetModel = 6720814,  
              HandsModel = 6721582,  
              ArmModel = 6722862,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6765870,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Jorn]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1651]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1653]],  
                x = 34973.6875,  
                y = -3831.0625,  
                z = -19.890625
              },  
              Angle = 1.064650844,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1649]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 10,  
              HairType = 5612590,  
              HairColor = 1,  
              Tattoo = 30,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 0,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5615918,  
              TrouserModel = 5612846,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5636398,  
              WeaponLeftHand = 0,  
              Name = [[Rosira]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1656]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1658]],  
                x = 34976.09375,  
                y = -3826.765625,  
                z = -19.546875
              },  
              Angle = 4.276056667,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1654]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 9,  
              HairType = 5623086,  
              HairColor = 3,  
              Tattoo = 13,  
              EyesColor = 0,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              MorphTarget7 = 2,  
              MorphTarget8 = 7,  
              JacketModel = 5613358,  
              TrouserModel = 5612846,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5600814,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Rippsey]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1661]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1663]],  
                x = 34972.46875,  
                y = -3829.453125,  
                z = -19.96875
              },  
              Angle = 0.2792526803,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1659]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 0,  
              HairType = 5623086,  
              HairColor = 4,  
              Tattoo = 6,  
              EyesColor = 7,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 0,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 2,  
              ArmColor = 5,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5636398,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Cautty]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1131]],  
            x = 10.453125,  
            y = 1.34375,  
            z = 0.484375
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1130]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          MissionText = [[Find <mission_target> for me]],  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1187]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1495]],  
                    Entity = r2.RefId([[]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1494]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1188]],  
                  Type = [[desactivation]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1497]],  
                Conditions = {
                },  
                Actions = {
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1498]],  
                  Type = [[activation]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              },  
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_1502]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_1505]],  
                    Entity = r2.RefId([[]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_1504]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_1503]],  
                  Type = [[activation]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            InstanceId = [[Client1_1157]]
          },  
          WaitValidationText = [[Did <mission_giver> ask you to talk to me?]],  
          Base = [[palette.entities.botobjects.bot_chat]],  
          Repeatable = 0,  
          ContextualText = [[Talk to <mission_target>]],  
          MissionGiver = r2.RefId([[Client1_1145]]),  
          MissionTarget = r2.RefId([[Client1_1168]]),  
          InheritPos = 1,  
          _Seed = 1146745385,  
          Name = [[Mission: Talk to 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1158]],  
            x = 34976.25,  
            y = -3830.890625,  
            z = -19
          },  
          Class = [[TalkTo]],  
          InstanceId = [[Client1_1156]],  
          Components = {
          },  
          MissionSucceedText = [[Yes I am <mission_target>]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1199]],  
          Name = [[Group 2]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1191]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1192]],  
                x = 35168.125,  
                y = -3731.796875,  
                z = -14.84375
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1189]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1258]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1259]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1260]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1261]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1223]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 14,  
              HairType = 3118,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 1,  
              MorphTarget1 = 7,  
              MorphTarget2 = 3,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 1,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[Apothus]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1195]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1196]],  
                x = 35169.35938,  
                y = -3730.140625,  
                z = -15.03125
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1193]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 5604398,  
              HairColor = 0,  
              Tattoo = 30,  
              EyesColor = 1,  
              MorphTarget1 = 5,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 4,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 1,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              WeaponLeftHand = 0,  
              Name = [[Dekos]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1202]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1204]],  
                x = 35170.03125,  
                y = -3732.6875,  
                z = -14.4375
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1200]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 12,  
              HairType = 2606,  
              HairColor = 4,  
              Tattoo = 5,  
              EyesColor = 0,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              MorphTarget7 = 2,  
              MorphTarget8 = 5,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 3,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 1,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5635886,  
              WeaponLeftHand = 0,  
              Name = [[Kyron]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1268]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1270]],  
                x = 35171.57813,  
                y = -3730.578125,  
                z = -14.734375
              },  
              Angle = -2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1266]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 1,  
              Tattoo = 18,  
              EyesColor = 3,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              JacketModel = 5607726,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5595694,  
              Name = [[]],  
              Sex = 1,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1273]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1275]],  
                x = 35167.0625,  
                y = -3728.65625,  
                z = -15.109375
              },  
              Angle = -2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1271]],  
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 6,  
              HairType = 5604398,  
              HairColor = 0,  
              Tattoo = 29,  
              EyesColor = 5,  
              MorphTarget1 = 5,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5604910,  
              JacketColor = 0,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5595694,  
              Name = [[Aktius]],  
              Sex = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1539]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1541]],  
                x = 35168.9375,  
                y = -3728.03125,  
                z = -15.0625
              },  
              Angle = -2.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1537]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 3,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 18,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 7,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 2,  
              TrouserColor = 3,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6756142,  
              WeaponLeftHand = 0,  
              Name = [[Meps]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1198]],  
            x = -17.453125,  
            y = -35.875,  
            z = -4.953125
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1197]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1552]],  
          Name = [[Group 4]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1544]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1545]],  
                x = 35160.29688,  
                y = -3757.375,  
                z = -18.84375
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1542]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 1,  
              HairType = 6700590,  
              HairColor = 3,  
              Tattoo = 30,  
              EyesColor = 0,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6756910,  
              WeaponLeftHand = 0,  
              Name = [[Eukos]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1548]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1549]],  
                x = 35158.20313,  
                y = -3756.28125,  
                z = -19.203125
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1546]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 6700334,  
              HairColor = 0,  
              Tattoo = 23,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              MorphTarget7 = 6,  
              MorphTarget8 = 0,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 5,  
              FeetColor = 0,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              WeaponLeftHand = 0,  
              Name = [[Kridix]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1565]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1567]],  
                x = 35156.29688,  
                y = -3758.078125,  
                z = -19.703125
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1563]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 13,  
              HairType = 6700590,  
              HairColor = 2,  
              Tattoo = 15,  
              EyesColor = 3,  
              MorphTarget1 = 4,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 5,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 2,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6755118,  
              WeaponLeftHand = 0,  
              Name = [[Abyla]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1570]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1572]],  
                x = 35161.5625,  
                y = -3759.5625,  
                z = -18.828125
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1568]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 2,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 15,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 1,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              MorphTarget7 = 5,  
              MorphTarget8 = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6756654,  
              WeaponLeftHand = 0,  
              Name = [[Aeseus]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1575]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1577]],  
                x = 35159.60938,  
                y = -3761.484375,  
                z = -19.21875
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1573]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 12,  
              HairType = 6700334,  
              HairColor = 2,  
              Tattoo = 8,  
              EyesColor = 4,  
              MorphTarget1 = 1,  
              MorphTarget2 = 3,  
              MorphTarget3 = 7,  
              MorphTarget4 = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              MorphTarget7 = 6,  
              MorphTarget8 = 6,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701358,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 6756398,  
              WeaponLeftHand = 0,  
              Name = [[Theros]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1580]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1582]],  
                x = 35157.0625,  
                y = -3760.90625,  
                z = -19.640625
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1578]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 8,  
              HairType = 6700334,  
              HairColor = 3,  
              Tattoo = 17,  
              EyesColor = 5,  
              MorphTarget1 = 4,  
              MorphTarget2 = 6,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 1,  
              MorphTarget8 = 4,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 6756910,  
              WeaponLeftHand = 0,  
              Name = [[Diorius]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1550]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            }
          },  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1551]],  
            x = 0.078125,  
            y = 0.125,  
            z = 0.015625
          },  
          InheritPos = 1
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1597]],  
          Name = [[Group 5]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1589]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1590]],  
                x = 35153.8125,  
                y = -3777.296875,  
                z = -18.8125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1587]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 4,  
              HairType = 2606,  
              HairColor = 1,  
              Tattoo = 15,  
              EyesColor = 4,  
              MorphTarget1 = 7,  
              MorphTarget2 = 0,  
              MorphTarget3 = 6,  
              MorphTarget4 = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 1,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 3,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[Kritheus]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1593]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1594]],  
                x = 35156.3125,  
                y = -3775,  
                z = -18.578125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1591]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 3,  
              HairType = 5604398,  
              HairColor = 2,  
              Tattoo = 25,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 1,  
              MorphTarget4 = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              MorphTarget7 = 0,  
              MorphTarget8 = 6,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 2,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5595950,  
              WeaponLeftHand = 0,  
              Name = [[Iokos]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1600]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1602]],  
                x = 35155.70313,  
                y = -3780.140625,  
                z = -18.5625
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1598]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 7,  
              HairType = 5604398,  
              HairColor = 3,  
              Tattoo = 20,  
              EyesColor = 6,  
              MorphTarget1 = 2,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 6,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 6,  
              MorphTarget8 = 7,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5595182,  
              WeaponLeftHand = 0,  
              Name = [[Iosse]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1605]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1607]],  
                x = 35156.875,  
                y = -3777.734375,  
                z = -18.359375
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1603]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 5,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 6,  
              HairType = 5621806,  
              HairColor = 2,  
              Tattoo = 8,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 6,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 0,  
              ArmColor = 3,  
              HandsColor = 2,  
              TrouserColor = 1,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[Boekos]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1610]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1612]],  
                x = 35156.29688,  
                y = -3776.28125,  
                z = -18.515625
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1608]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 1,  
              HairType = 5604398,  
              HairColor = 4,  
              Tattoo = 22,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 7,  
              JacketModel = 5607726,  
              TrouserModel = 5604654,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5595182,  
              WeaponLeftHand = 0,  
              Name = [[Lyton]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1615]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1617]],  
                x = 35155.95313,  
                y = -3778.765625,  
                z = -18.515625
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1613]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 10,  
              HairType = 5621550,  
              HairColor = 2,  
              Tattoo = 22,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 2,  
              MorphTarget3 = 0,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 2,  
              MorphTarget8 = 5,  
              JacketModel = 5605166,  
              TrouserModel = 5604654,  
              FeetModel = 5603886,  
              HandsModel = 5606958,  
              ArmModel = 5607470,  
              JacketColor = 5,  
              ArmColor = 0,  
              HandsColor = 1,  
              TrouserColor = 0,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[Iodix]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1595]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            }
          },  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1596]],  
            x = -1.109375,  
            y = 2.21875,  
            z = -0.21875
          },  
          InheritPos = 1
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1628]],  
          Name = [[Group 6]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1620]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1621]],  
                x = 34962.09375,  
                y = -3809.890625,  
                z = -18.59375
              },  
              Angle = -0.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1618]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 5623598,  
              HairColor = 1,  
              Tattoo = 3,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              JacketModel = 5615918,  
              TrouserModel = 5612846,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 2,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5600302,  
              WeaponLeftHand = 0,  
              Name = [[Be'laury]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1624]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1625]],  
                x = 34967.51563,  
                y = -3812.046875,  
                z = -18.8125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1622]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 7470,  
              HairColor = 0,  
              Tattoo = 28,  
              EyesColor = 0,  
              MorphTarget1 = 1,  
              MorphTarget2 = 2,  
              MorphTarget3 = 4,  
              MorphTarget4 = 6,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              MorphTarget7 = 1,  
              MorphTarget8 = 0,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 5,  
              TrouserColor = 1,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Nary]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1631]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1633]],  
                x = 34967.54688,  
                y = -3808.890625,  
                z = -18.734375
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1629]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 3,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 7,  
              HairType = 5623086,  
              HairColor = 1,  
              Tattoo = 6,  
              EyesColor = 3,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 2,  
              MorphTarget4 = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 4,  
              ArmColor = 2,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Arlly]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1636]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1638]],  
                x = 34964.78125,  
                y = -3814.015625,  
                z = -18.890625
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1634]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 10,  
              HairType = 6702,  
              HairColor = 1,  
              Tattoo = 31,  
              EyesColor = 4,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 6,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 3,  
              ArmColor = 5,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5600302,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Jorn]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1641]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1643]],  
                x = 34964.35938,  
                y = -3807.6875,  
                z = -18.703125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1639]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 14,  
              HairType = 5612590,  
              HairColor = 3,  
              Tattoo = 22,  
              EyesColor = 0,  
              MorphTarget1 = 4,  
              MorphTarget2 = 3,  
              MorphTarget3 = 1,  
              MorphTarget4 = 7,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5613102,  
              JacketColor = 5,  
              ArmColor = 3,  
              HandsColor = 0,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              WeaponLeftHand = 0,  
              Name = [[Be'Toolly]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1646]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1648]],  
                x = 34962.28125,  
                y = -3812.859375,  
                z = -18.703125
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1644]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 13,  
              HairType = 6958,  
              HairColor = 5,  
              Tattoo = 16,  
              EyesColor = 1,  
              MorphTarget1 = 2,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 4,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5636398,  
              WeaponLeftHand = 0,  
              Name = [[O'Arty]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1626]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            }
          },  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1627]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          InheritPos = 1
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1678]],  
          Name = [[Group 7]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1670]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1671]],  
                x = 34966.15625,  
                y = -3833.890625,  
                z = -19.46875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1668]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 13,  
              HairType = 7214,  
              HairColor = 5,  
              Tattoo = 2,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 6,  
              MorphTarget4 = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 5600814,  
              WeaponLeftHand = 0,  
              Name = [[Be'Keeer]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1674]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1675]],  
                x = 34967.10938,  
                y = -3829.453125,  
                z = -20.25
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1672]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 12,  
              HairType = 7470,  
              HairColor = 3,  
              Tattoo = 6,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 0,  
              MorphTarget4 = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Laughan]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1681]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1683]],  
                x = 34970.45313,  
                y = -3831.875,  
                z = -19.78125
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1679]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 2,  
              HairType = 5623342,  
              HairColor = 3,  
              Tattoo = 28,  
              EyesColor = 0,  
              MorphTarget1 = 6,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              JacketModel = 5613358,  
              TrouserModel = 5612846,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 0,  
              TrouserColor = 2,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5601070,  
              WeaponLeftHand = 0,  
              Name = [[O'Darghan]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1686]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1688]],  
                x = 34968.53125,  
                y = -3833.828125,  
                z = -19.453125
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1684]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 7,  
              HairType = 6702,  
              HairColor = 5,  
              Tattoo = 0,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 6,  
              MorphTarget3 = 2,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 0,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 4,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 0,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              WeaponLeftHand = 0,  
              Name = [[Ba'Roley]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1691]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1693]],  
                x = 34965.34375,  
                y = -3831.59375,  
                z = -19.953125
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1689]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 12,  
              HairType = 5623598,  
              HairColor = 5,  
              Tattoo = 26,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 3,  
              MorphTarget3 = 2,  
              MorphTarget4 = 6,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              JacketModel = 5613358,  
              TrouserModel = 5615406,  
              FeetModel = 5612078,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5600302,  
              WeaponLeftHand = 0,  
              Name = [[Be'Yle]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1696]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1698]],  
                x = 34969.57813,  
                y = -3829.78125,  
                z = -20.15625
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1694]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 3,  
              HairType = 5612590,  
              HairColor = 4,  
              Tattoo = 7,  
              EyesColor = 4,  
              MorphTarget1 = 7,  
              MorphTarget2 = 6,  
              MorphTarget3 = 6,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              MorphTarget7 = 5,  
              MorphTarget8 = 7,  
              JacketModel = 5615918,  
              TrouserModel = 5615406,  
              FeetModel = 5614894,  
              HandsModel = 5615150,  
              ArmModel = 5615662,  
              JacketColor = 2,  
              ArmColor = 4,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 5600814,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Rippsey]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1676]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            }
          },  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1677]],  
            x = -0.234375,  
            y = -0.15625,  
            z = 0.03125
          },  
          InheritPos = 1
        }
      },  
      Counters = {
      },  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1020]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 1
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_1011]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1155]],  
        Count = 1,  
        Text = [[hey mec, va nous cherchez des potes!]]
      }
    }
  }
}