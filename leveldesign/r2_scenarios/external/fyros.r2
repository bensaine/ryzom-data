scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 10,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    ChatAction = 0,  
    Region = 0,  
    ActivityStep = 1,  
    ChatStep = 0,  
    ChatSequence = 0,  
    ActionStep = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    EventType = 0,  
    Position = 0,  
    Npc = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    LogicEntityReaction = 0
  },  
  Acts = {
    {
      Cost = 21,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Permanent]],  
      Version = 3,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[Permanent]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_18]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_16]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1288.140625,  
                x = 29703.76563,  
                InstanceId = [[Client1_19]],  
                Class = [[Position]],  
                z = 74.5625
              },  
              Angle = -2.15625,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_22]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_20]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 3]],  
              Position = {
                y = -1298.421875,  
                x = 29681.125,  
                InstanceId = [[Client1_23]],  
                Class = [[Position]],  
                z = 76.1875
              },  
              Angle = -0.953125,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_26]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_24]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 4]],  
              Position = {
                y = -1318.59375,  
                x = 29678.34375,  
                InstanceId = [[Client1_27]],  
                Class = [[Position]],  
                z = 75.78125
              },  
              Angle = 0.21875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_30]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_28]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              Position = {
                y = -1300.203125,  
                x = 29712.15625,  
                InstanceId = [[Client1_31]],  
                Class = [[Position]],  
                z = 73.578125
              },  
              Angle = -6.15625,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_34]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_32]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -1244.0625,  
                x = 29671.07813,  
                InstanceId = [[Client1_35]],  
                Class = [[Position]],  
                z = 77.375
              },  
              Angle = -1.765625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -1282.8125,  
                x = 29696.5625,  
                InstanceId = [[Client1_43]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = -1.796875,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 1 1]],  
              Position = {
                y = -1302.015625,  
                x = 29712.79688,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = 73.421875
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.pack_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_50]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 3 1]],  
              Position = {
                y = -1280.890625,  
                x = 29689.70313,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = -1.203125,  
              Base = [[palette.entities.botobjects.pack_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_54]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_52]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              Position = {
                y = -1288.546875,  
                x = 29687.75,  
                InstanceId = [[Client1_55]],  
                Class = [[Position]],  
                z = 75.25
              },  
              Angle = -0.6875,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_58]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_56]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -1292.3125,  
                x = 29703.78125,  
                InstanceId = [[Client1_59]],  
                Class = [[Position]],  
                z = 73.984375
              },  
              Angle = -2.265625,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_62]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_60]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1299.25,  
                x = 29692.3125,  
                InstanceId = [[Client1_63]],  
                Class = [[Position]],  
                z = 74.484375
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_70]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_68]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -1308.34375,  
                x = 29677.04688,  
                InstanceId = [[Client1_71]],  
                Class = [[Position]],  
                z = 76.96875
              },  
              Angle = -0.046875,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_74]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_72]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1293.125,  
                x = 29703.71875,  
                InstanceId = [[Client1_75]],  
                Class = [[Position]],  
                z = 73.859375
              },  
              Angle = -1.8125,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_78]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_76]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -1293.1875,  
                x = 29704.5,  
                InstanceId = [[Client1_79]],  
                Class = [[Position]],  
                z = 73.859375
              },  
              Angle = -1.765625,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_86]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_84]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 3]],  
              Position = {
                y = -1312.78125,  
                x = 29679.89063,  
                InstanceId = [[Client1_87]],  
                Class = [[Position]],  
                z = 76.46875
              },  
              Angle = 0.6875,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_98]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_96]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bird totem 1]],  
              Position = {
                y = -1282.21875,  
                x = 29714.40625,  
                InstanceId = [[Client1_99]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = -2.125,  
              Base = [[palette.entities.botobjects.totem_bird]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_136]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_138]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1310.28125,  
                    x = 29687.10938,  
                    InstanceId = [[Client1_139]],  
                    Class = [[Position]],  
                    z = 76.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_141]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1307.015625,  
                    x = 29683.89063,  
                    InstanceId = [[Client1_142]],  
                    Class = [[Position]],  
                    z = 77.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_144]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1295.46875,  
                    x = 29685.39063,  
                    InstanceId = [[Client1_145]],  
                    Class = [[Position]],  
                    z = 75.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_147]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1290.546875,  
                    x = 29695.65625,  
                    InstanceId = [[Client1_148]],  
                    Class = [[Position]],  
                    z = 76.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_153]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Activity = [[Stand Still]],  
                    InstanceId = [[Client1_154]],  
                    Class = [[Position]],  
                    y = -1299.84375,  
                    x = 29706.78125,  
                    z = 74.640625,  
                    TimeLimit = [[No Limit]],  
                    TimeLimitValue = [[]],  
                    ActivityZoneId = r2.RefId([[]])
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_156]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1299.671875,  
                    x = 29701.35938,  
                    InstanceId = [[Client1_157]],  
                    Class = [[Position]],  
                    z = 74.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_159]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1300.546875,  
                    x = 29696.34375,  
                    InstanceId = [[Client1_160]],  
                    Class = [[Position]],  
                    z = 74.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_162]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1304.515625,  
                    x = 29691.98438,  
                    InstanceId = [[Client1_163]],  
                    Class = [[Position]],  
                    z = 75.25
                  }
                }
              },  
              Position = {
                y = 0.046875,  
                x = 0.984375,  
                InstanceId = [[Client1_135]],  
                Class = [[Position]],  
                z = -1.703125
              }
            },  
            {
              InstanceId = [[Client1_406]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_404]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga II 1]],  
              Position = {
                y = -1301.65625,  
                x = 29729.60938,  
                InstanceId = [[Client1_407]],  
                Class = [[Position]],  
                z = 77.546875
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_410]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_408]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[loojine I 1]],  
              Position = {
                y = -1310.15625,  
                x = 29715.32813,  
                InstanceId = [[Client1_411]],  
                Class = [[Position]],  
                z = 72.859375
              },  
              Angle = -2.828125,  
              Base = [[palette.entities.botobjects.fy_s2_lovejail_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_414]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_412]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[olansi I 1]],  
              Position = {
                y = -1288.828125,  
                x = 29722.54688,  
                InstanceId = [[Client1_415]],  
                Class = [[Position]],  
                z = 75.390625
              },  
              Angle = -2.828125,  
              Base = [[palette.entities.botobjects.fy_s2_palmtree_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_434]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_432]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga III 1]],  
              Position = {
                y = -1250.03125,  
                x = 29700.26563,  
                InstanceId = [[Client1_435]],  
                Class = [[Position]],  
                z = 73.296875
              },  
              Angle = -2.171875,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_c]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_438]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_436]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga I 1]],  
              Position = {
                y = -1298.9375,  
                x = 29644.5,  
                InstanceId = [[Client1_439]],  
                Class = [[Position]],  
                z = 79.34375
              },  
              Angle = 0.265625,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_887]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_886]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_889]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1195.953125,  
                    x = 29648.95313,  
                    InstanceId = [[Client1_890]],  
                    Class = [[Position]],  
                    z = 73.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_892]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1212.015625,  
                    x = 29656.5625,  
                    InstanceId = [[Client1_893]],  
                    Class = [[Position]],  
                    z = 72.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_895]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1221.578125,  
                    x = 29676.14063,  
                    InstanceId = [[Client1_896]],  
                    Class = [[Position]],  
                    z = 76.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_898]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1216.421875,  
                    x = 29680.28125,  
                    InstanceId = [[Client1_899]],  
                    Class = [[Position]],  
                    z = 75.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_901]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1192.078125,  
                    x = 29669.03125,  
                    InstanceId = [[Client1_902]],  
                    Class = [[Position]],  
                    z = 71.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_904]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1188.078125,  
                    x = 29654.10938,  
                    InstanceId = [[Client1_905]],  
                    Class = [[Position]],  
                    z = 72.75
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 3]],  
              InstanceId = [[Client1_1021]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1023]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1288.28125,  
                    x = 29697.51563,  
                    InstanceId = [[Client1_1024]],  
                    Class = [[Position]],  
                    z = 74.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1026]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1295.5625,  
                    x = 29684.14063,  
                    InstanceId = [[Client1_1027]],  
                    Class = [[Position]],  
                    z = 75.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1029]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1289.75,  
                    x = 29680.53125,  
                    InstanceId = [[Client1_1030]],  
                    Class = [[Position]],  
                    z = 75.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1032]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1282.265625,  
                    x = 29687.375,  
                    InstanceId = [[Client1_1033]],  
                    Class = [[Position]],  
                    z = 75.0625
                  }
                }
              },  
              Position = {
                y = -3.15625,  
                x = 3.78125,  
                InstanceId = [[Client1_1020]],  
                Class = [[Position]],  
                z = -0.390625
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 29,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Act 1]],  
      Version = 3,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[Act 1]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_448]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 7,  
              AutoSpawn = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_446]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_483]],  
                    Name = [[]],  
                    InstanceId = [[Client1_486]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_485]]
                  },  
                  {
                    LogicEntityAction = [[Client1_483]],  
                    Name = [[]],  
                    InstanceId = [[Client1_489]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_488]]
                  },  
                  {
                    LogicEntityAction = [[Client1_483]],  
                    Name = [[]],  
                    InstanceId = [[Client1_492]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_491]]
                  },  
                  {
                    LogicEntityAction = [[Client1_483]],  
                    Name = [[]],  
                    InstanceId = [[Client1_495]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_494]]
                  },  
                  {
                    LogicEntityAction = [[Client1_483]],  
                    Name = [[]],  
                    InstanceId = [[Client1_498]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_497]]
                  },  
                  {
                    LogicEntityAction = [[Client1_483]],  
                    Name = [[]],  
                    InstanceId = [[Client1_501]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_500]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_503]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_502]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_504]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_505]],  
                        Entity = r2.RefId([[Client1_126]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_507]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_508]],  
                        Entity = r2.RefId([[Client1_168]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_510]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_511]],  
                        Entity = r2.RefId([[Client1_122]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_513]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_514]],  
                        Entity = r2.RefId([[Client1_176]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_516]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_517]],  
                        Entity = r2.RefId([[Client1_172]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_519]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_520]],  
                        Entity = r2.RefId([[Client1_130]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_469]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_470]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699566,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 21,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              Sex = 0,  
              WeaponRightHand = 6755374,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_guard_melee_tank_blunt_e3.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad1]],  
              Position = {
                y = -1277.59375,  
                x = 29719.59375,  
                InstanceId = [[Client1_449]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InstanceId = [[Client1_623]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_621]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_634]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_635]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_641]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_640]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_642]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_643]],  
                        Entity = r2.RefId([[Client1_647]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_696]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_697]],  
                        Entity = r2.RefId([[Client1_602]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_699]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_700]],  
                        Entity = r2.RefId([[Client1_610]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_702]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_703]],  
                        Entity = r2.RefId([[Client1_606]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_653]],  
                    Name = [[]],  
                    InstanceId = [[Client1_651]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_655]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Lacerating Kincher]],  
              Position = {
                y = -1345.984375,  
                x = 29625.53125,  
                InstanceId = [[Client1_624]],  
                Class = [[Position]],  
                z = 77.625
              },  
              Angle = 0.703125,  
              Base = [[palette.entities.creatures.ckddf2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_647]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_645]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_649]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_650]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_653]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_652]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_654]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_655]],  
                        Entity = r2.RefId([[Client1_623]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_687]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_688]],  
                        Entity = r2.RefId([[Client1_683]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_690]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_691]],  
                        Entity = r2.RefId([[Client1_586]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_693]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_694]],  
                        Entity = r2.RefId([[Client1_590]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_673]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_672]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_674]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_675]],  
                        Entity = r2.RefId([[Client1_586]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_678]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_677]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_679]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_680]],  
                        Entity = r2.RefId([[Client1_590]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_641]],  
                    Name = [[]],  
                    InstanceId = [[Client1_644]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_643]]
                  }
                }
              },  
              Class = [[Npc]],  
              AutoSpawn = 0,  
              InheritPos = 1,  
              Name = [[Killer Kincher]],  
              Position = {
                y = -1343.34375,  
                x = 29614.75,  
                InstanceId = [[Client1_648]],  
                Class = [[Position]],  
                z = 76.765625
              },  
              Angle = 0.8125,  
              Base = [[palette.entities.creatures.ckddf3]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_126]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              Aggro = 10,  
              EyesColor = 5,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 9,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_124]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_440]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_441]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_882]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_483]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_482]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_484]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_485]],  
                        Entity = r2.RefId([[Client1_448]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_487]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_488]],  
                        Entity = r2.RefId([[Client1_456]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_490]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_491]],  
                        Entity = r2.RefId([[Client1_464]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_493]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_494]],  
                        Entity = r2.RefId([[Client1_460]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_496]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_497]],  
                        Entity = r2.RefId([[Client1_444]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_499]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_500]],  
                        Entity = r2.RefId([[Client1_452]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_503]],  
                    Name = [[]],  
                    InstanceId = [[Client1_506]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_505]]
                  },  
                  {
                    LogicEntityAction = [[Client1_503]],  
                    Name = [[]],  
                    InstanceId = [[Client1_509]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_508]]
                  },  
                  {
                    LogicEntityAction = [[Client1_503]],  
                    Name = [[]],  
                    InstanceId = [[Client1_512]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_511]]
                  },  
                  {
                    LogicEntityAction = [[Client1_503]],  
                    Name = [[]],  
                    InstanceId = [[Client1_515]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_514]]
                  },  
                  {
                    LogicEntityAction = [[Client1_503]],  
                    Name = [[]],  
                    InstanceId = [[Client1_518]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_517]]
                  },  
                  {
                    LogicEntityAction = [[Client1_503]],  
                    Name = [[]],  
                    InstanceId = [[Client1_521]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_520]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699566,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 7,  
              MorphTarget3 = 1,  
              MorphTarget7 = 6,  
              Sex = 1,  
              WeaponRightHand = 6756910,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 4,  
              Sheet = [[ring_guard_melee_tank_slash_e3.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard1]],  
              Position = {
                y = -1280.4375,  
                x = 29712.3125,  
                InstanceId = [[Client1_127]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InstanceId = [[Client1_941]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_939]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_927]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_926]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_928]],  
                          Value = r2.RefId([[Client1_933]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_929]],  
                        Entity = r2.RefId([[Client1_711]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_954]],  
                          Value = r2.RefId([[Client1_935]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_955]],  
                        Entity = r2.RefId([[Client1_711]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_943]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_944]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Trooper Kipesta]],  
              Position = {
                y = -1326.71875,  
                x = 29640.64063,  
                InstanceId = [[Client1_942]],  
                Class = [[Position]],  
                z = 81.0625
              },  
              Angle = 0.828125,  
              Base = [[palette.entities.creatures.ckjib1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_947]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_945]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_951]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_950]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_952]],  
                          Value = r2.RefId([[Client1_1034]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_953]],  
                        Entity = r2.RefId([[Client1_711]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Roaming Yubo]],  
              Position = {
                y = -1304.03125,  
                x = 29722.03125,  
                InstanceId = [[Client1_948]],  
                Class = [[Position]],  
                z = 75.734375
              },  
              Angle = -2.71875,  
              Base = [[palette.entities.creatures.chddb2]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          InstanceId = [[Client1_134]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 30.875,  
            x = 21.625,  
            InstanceId = [[Client1_133]],  
            Class = [[Position]],  
            z = 0.828125
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_122]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 10,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 10,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_120]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_617]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_618]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699566,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 28,  
              MorphTarget3 = 4,  
              MorphTarget7 = 1,  
              Sex = 0,  
              WeaponRightHand = 6756398,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_slash_e1.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard3]],  
              Position = {
                y = -1305.40625,  
                x = 29690.875,  
                InstanceId = [[Client1_123]],  
                Class = [[Position]],  
                z = 74.1875
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_172]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 5,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_170]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6704942,  
              Angle = -1.296875,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 24,  
              MorphTarget3 = 4,  
              MorphTarget7 = 4,  
              Sex = 1,  
              JacketModel = 6707246,  
              WeaponRightHand = 6754862,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_slash_e4.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard5]],  
              Position = {
                y = -1306.5625,  
                x = 29690.1875,  
                InstanceId = [[Client1_173]],  
                Class = [[Position]],  
                z = 74.078125
              },  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_168]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_166]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699566,  
              Angle = -1.359375,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              Tattoo = 7,  
              MorphTarget3 = 2,  
              MorphTarget7 = 5,  
              Sex = 1,  
              JacketModel = 6702126,  
              WeaponRightHand = 6754350,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              Sheet = [[ring_melee_tank_slash_e1.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard2]],  
              Position = {
                y = -1304.6875,  
                x = 29689.07813,  
                InstanceId = [[Client1_169]],  
                Class = [[Position]],  
                z = 74.03125
              },  
              WeaponLeftHand = 6773294,  
              PlayerAttackable = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_176]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_174]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6704942,  
              Angle = -1.375,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 21,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              Sex = 1,  
              JacketModel = 6707246,  
              WeaponRightHand = 6752302,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              Sheet = [[ring_light_melee_blunt_e4.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard4]],  
              Position = {
                y = -1307.265625,  
                x = 29692.39063,  
                InstanceId = [[Client1_177]],  
                Class = [[Position]],  
                z = 74.203125
              },  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_130]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_128]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_908]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_907]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6699566,  
              Angle = -2.453125,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 19,  
              MorphTarget3 = 7,  
              MorphTarget7 = 3,  
              Sex = 1,  
              WeaponRightHand = 6755886,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_pierce_e1.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard6]],  
              Position = {
                y = -1307.96875,  
                x = 29690.42188,  
                InstanceId = [[Client1_131]],  
                Class = [[Position]],  
                z = 74.25
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_132]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_468]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = -1.859375,  
            x = -0.203125,  
            InstanceId = [[Client1_467]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_452]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 8,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 3,  
              AutoSpawn = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_450]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_619]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_620]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6699566,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 5,  
              MorphTarget3 = 4,  
              MorphTarget7 = 1,  
              Sex = 0,  
              WeaponRightHand = 6755886,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_e1.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad6]],  
              Position = {
                y = -1278,  
                x = 29722.39063,  
                InstanceId = [[Client1_453]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_444]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 4,  
              EyesColor = 3,  
              AutoSpawn = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_442]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 6699566,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 2,  
              MorphTarget3 = 5,  
              MorphTarget7 = 2,  
              Sex = 1,  
              WeaponRightHand = 6756910,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_e1.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad5]],  
              Position = {
                y = -1280.015625,  
                x = 29722.85938,  
                InstanceId = [[Client1_445]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_460]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 11,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              AutoSpawn = 0,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_458]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6704942,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 24,  
              MorphTarget3 = 5,  
              MorphTarget7 = 1,  
              Sex = 1,  
              JacketModel = 6707246,  
              WeaponRightHand = 6754862,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_light_melee_slash_e4.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad4]],  
              Position = {
                y = -1276.796875,  
                x = 29724.46875,  
                InstanceId = [[Client1_461]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_456]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 0,  
              EyesColor = 2,  
              AutoSpawn = 0,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_454]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6704942,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 15,  
              MorphTarget3 = 5,  
              MorphTarget7 = 0,  
              Sex = 1,  
              JacketModel = 6707246,  
              WeaponRightHand = 6754094,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_light_melee_slash_e4.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad2]],  
              Position = {
                y = -1278.375,  
                x = 29724.59375,  
                InstanceId = [[Client1_457]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_464]],  
              ActivitiesId = {
              },  
              HairType = 6958,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 5,  
              EyesColor = 1,  
              AutoSpawn = 0,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_462]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6704942,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 8,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              Sex = 1,  
              JacketModel = 6707246,  
              WeaponRightHand = 6753326,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              Sheet = [[ring_light_melee_pierce_e4.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad3]],  
              Position = {
                y = -1279.1875,  
                x = 29719.73438,  
                InstanceId = [[Client1_465]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              WeaponLeftHand = 6753326,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_tryker_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_466]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_614]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 3]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_613]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_683]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_681]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_685]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_686]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_653]],  
                    Name = [[]],  
                    InstanceId = [[Client1_689]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_688]]
                  },  
                  {
                    LogicEntityAction = [[Client1_653]],  
                    Name = [[]],  
                    InstanceId = [[Client1_692]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_691]]
                  },  
                  {
                    LogicEntityAction = [[Client1_653]],  
                    Name = [[]],  
                    InstanceId = [[Client1_695]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_694]]
                  }
                }
              },  
              Class = [[Npc]],  
              AutoSpawn = 1,  
              InheritPos = 1,  
              Name = [[Kincher 1]],  
              Position = {
                y = -1336.28125,  
                x = 29634.54688,  
                InstanceId = [[Client1_684]],  
                Class = [[Position]],  
                z = 80.921875
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.creatures.ckddf1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_590]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_588]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kincher 3]],  
              Position = {
                y = -1332.875,  
                x = 29633.375,  
                InstanceId = [[Client1_591]],  
                Class = [[Position]],  
                z = 81.609375
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.creatures.ckddf1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_586]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_584]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kincher 2]],  
              Position = {
                y = -1337.6875,  
                x = 29631.26563,  
                InstanceId = [[Client1_587]],  
                Class = [[Position]],  
                z = 80.671875
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.creatures.ckddf1]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_612]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_631]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 4]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_630]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_602]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_600]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_641]],  
                    Name = [[]],  
                    InstanceId = [[Client1_698]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_697]]
                  },  
                  {
                    LogicEntityAction = [[Client1_641]],  
                    Name = [[]],  
                    InstanceId = [[Client1_701]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_700]]
                  },  
                  {
                    LogicEntityAction = [[Client1_641]],  
                    Name = [[]],  
                    InstanceId = [[Client1_704]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_703]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_636]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_637]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              AutoSpawn = 0,  
              InheritPos = 1,  
              Name = [[Kincher 4]],  
              Position = {
                y = -1331.859375,  
                x = 29620.57813,  
                InstanceId = [[Client1_603]],  
                Class = [[Position]],  
                z = 79.921875
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.creatures.ckddf1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_610]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_608]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              AutoSpawn = 0,  
              InheritPos = 1,  
              Name = [[Kincher 5]],  
              Position = {
                y = -1332.546875,  
                x = 29625.92188,  
                InstanceId = [[Client1_611]],  
                Class = [[Position]],  
                z = 81.09375
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.creatures.ckddf1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_606]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_604]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              AutoSpawn = 0,  
              InheritPos = 1,  
              Name = [[Kincher 6]],  
              Position = {
                y = -1336.125,  
                x = 29619.8125,  
                InstanceId = [[Client1_607]],  
                Class = [[Position]],  
                z = 79.171875
              },  
              Angle = 0.5625,  
              Base = [[palette.entities.creatures.ckddf1]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_629]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_719]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 5]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_718]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_711]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_709]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_927]],  
                    Name = [[]],  
                    InstanceId = [[Client1_925]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_929]]
                  },  
                  {
                    LogicEntityAction = [[Client1_951]],  
                    Name = [[]],  
                    InstanceId = [[Client1_949]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_953]]
                  },  
                  {
                    LogicEntityAction = [[Client1_927]],  
                    Name = [[]],  
                    InstanceId = [[Client1_956]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_955]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_935]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_936]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Alert]],  
                            InstanceId = [[Client1_937]],  
                            Who = [[Client1_711]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[Client1_707]]),  
                            Says = [[Client1_938]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_930]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_931]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_933]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_934]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_935]]),  
                        ActivityZoneId = r2.RefId([[Client1_887]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1034]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[30]],  
                        InstanceId = [[Client1_1035]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1021]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1036]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6702638,  
              Speed = [[run]],  
              Angle = -1.875,  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Tattoo = 9,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              Sex = 0,  
              Position = {
                y = -1288.5,  
                x = 29694.17188,  
                InstanceId = [[Client1_712]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Aedan]],  
              BotAttackable = 1,  
              JacketModel = 6704686,  
              WeaponLeftHand = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_715]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_713]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6702638,  
              Angle = -3.046875,  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Tattoo = 30,  
              MorphTarget3 = 4,  
              MorphTarget7 = 4,  
              Sex = 1,  
              Position = {
                y = -1300.484375,  
                x = 29710.1875,  
                InstanceId = [[Client1_716]],  
                Class = [[Position]],  
                z = 73.171875
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Aenia]],  
              BotAttackable = 1,  
              JacketModel = 6704686,  
              WeaponLeftHand = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_707]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_705]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6702638,  
              Angle = 0.125,  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Tattoo = 3,  
              MorphTarget3 = 4,  
              MorphTarget7 = 4,  
              Sex = 0,  
              Position = {
                y = -1308.265625,  
                x = 29680.45313,  
                InstanceId = [[Client1_708]],  
                Class = [[Position]],  
                z = 76.546875
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Dixus]],  
              BotAttackable = 1,  
              JacketModel = 6704686,  
              WeaponLeftHand = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              InstanceId = [[Client1_118]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_116]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Voracious Mektoub]],  
              Position = {
                y = -1291.328125,  
                x = 29690.39063,  
                InstanceId = [[Client1_119]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              PlayerAttackable = 0,  
              Angle = -0.6875,  
              Base = [[palette.entities.creatures.chhpf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_106]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_104]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              Position = {
                y = -1284.421875,  
                x = 29697.9375,  
                InstanceId = [[Client1_107]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              PlayerAttackable = 0,  
              Angle = -2.640625,  
              Base = [[palette.entities.creatures.chhdd2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_102]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_100]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1283.46875,  
                x = 29694.78125,  
                InstanceId = [[Client1_103]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              PlayerAttackable = 0,  
              Angle = -1.265625,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_114]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_112]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Rooting Mektoub]],  
              Position = {
                y = -1281.453125,  
                x = 29696.90625,  
                InstanceId = [[Client1_115]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              PlayerAttackable = 0,  
              Angle = -1.84375,  
              Base = [[palette.entities.creatures.chhdd3]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_717]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_10]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 7,  
        InstanceId = [[Client1_938]],  
        Class = [[TextManagerEntry]],  
        Text = [[Save the mektoubs!!!!!]]
      }
    }
  }
}